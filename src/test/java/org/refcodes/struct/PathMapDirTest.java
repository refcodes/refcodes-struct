// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.PathMap.PathMapBuilder;

/**
 * The Class PathMapTest.
 */
public class PathMapDirTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAppendDir() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc/0/0", "00" );
		thePathMap.put( "aaa/bbb/ccc/0/1", "01" );
		thePathMap.put( "aaa/bbb/ccc/0/2", "02" );
		thePathMap.put( "aaa/bbb/ccc/1/0", "10" );
		thePathMap.put( "aaa/bbb/ccc/1/1", "11" );
		thePathMap.put( "aaa/bbb/ccc/1/2", "12" );
		thePathMap.put( "aaa/bbb/ccc/2/0", "20" );
		thePathMap.put( "aaa/bbb/ccc/2/1", "21" );
		thePathMap.put( "aaa/bbb/ccc/2/2", "22" );
		final PathMapBuilder<String> theAppendMap = new PathMapBuilderImpl<>( String.class );
		theAppendMap.put( "0", "30" );
		theAppendMap.put( "1", "31" );
		theAppendMap.put( "2", "32" );
		thePathMap.appendDirTo( "aaa/bbb/ccc", theAppendMap );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		assertEquals( "30", thePathMap.get( "aaa/bbb/ccc/3/0" ) );
		assertEquals( "31", thePathMap.get( "aaa/bbb/ccc/3/1" ) );
		assertEquals( "32", thePathMap.get( "aaa/bbb/ccc/3/2" ) );
	}

	@Test
	public void testDir1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "_root/whatever/0/aaaaa", "0aaaa" );
		thePathMap.put( "_root/whatever/0/bbbbb", "0bbbb" );
		thePathMap.put( "_root/whatever/0/ccccc", "0cccc" );
		thePathMap.put( "_root/whatever/1/aaaaa", "1aaaa" );
		thePathMap.put( "_root/whatever/1/bbbbb", "1bbbb" );
		thePathMap.put( "_root/whatever/1/ccccc", "1cccc" );
		final boolean isArray1 = thePathMap.isIndexDir( "_root" );
		final boolean isArray2 = thePathMap.isIndexDir( "_root/whatever" );
		assertFalse( isArray1 );
		assertTrue( isArray2 );
		int[] theIndexes = thePathMap.getDirIndexes( "_root/whatever" );
		if ( theIndexes.length != 2 ) {
			System.out.println( Arrays.toString( theIndexes ) );
		}
		assertEquals( 2, theIndexes.length );
		try {
			theIndexes = thePathMap.getDirIndexes();
			fail( "No indexes at root path expected!" );
		}
		catch ( IllegalArgumentException expected ) {
			/* expected */
		}
		for ( int theIndexe : theIndexes ) {
			assertTrue( thePathMap.hasDirAt( "_root/whatever", theIndexe ) );
		}
		assertFalse( thePathMap.hasDirAt( "_root/whatever/X" ) );
		assertTrue( thePathMap.hasDirAt( "_root/whatever/0" ) );
		assertTrue( thePathMap.hasDirAt( "_root/whatever/1" ) );
		assertFalse( thePathMap.hasDirAt( "_root/whatever/2" ) );
		String ePath;
		final PathMap<String> theDir = thePathMap.getDirAt( "_root/whatever", 0 );
		for ( String eKey : theDir.keySet() ) {
			ePath = thePathMap.toPath( "_root/whatever", "0", eKey );
			assertTrue( thePathMap.containsKey( ePath ) );
		}
		final int lastIndex = thePathMap.lastDirIndex( "_root/whatever" );
		assertEquals( 1, lastIndex );
		final int nextIndex = thePathMap.nextDirIndex( "_root/whatever" );
		assertEquals( 2, nextIndex );
	}

	@Test
	public void testDir2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "0/aaaaa", "0aaaa" );
		thePathMap.put( "0/bbbbb", "0bbbb" );
		thePathMap.put( "0/ccccc", "0cccc" );
		thePathMap.put( "1/aaaaa", "1aaaa" );
		thePathMap.put( "1/bbbbb", "1bbbb" );
		thePathMap.put( "1/ccccc", "1cccc" );
		final boolean isArray1 = thePathMap.isIndexDir( "_root" );
		final boolean isArray2 = thePathMap.isIndexDir();
		assertFalse( isArray1 );
		assertTrue( isArray2 );
		int[] theIndexes = thePathMap.getDirIndexes();
		assertEquals( 2, theIndexes.length );
		try {
			theIndexes = thePathMap.getDirIndexes( "/0" );
			fail( "No indexes at sub-path expected!" );
		}
		catch ( IllegalArgumentException expected ) {
			/* expected */
		}
		for ( int theIndexe : theIndexes ) {
			assertTrue( thePathMap.hasDirAt( theIndexe ) );
		}
		assertFalse( thePathMap.hasDirAt( "/X" ) );
		assertTrue( thePathMap.hasDirAt( "/0" ) );
		assertTrue( thePathMap.hasDirAt( "/1" ) );
		assertFalse( thePathMap.hasDirAt( "/2" ) );
		String ePath;
		final PathMap<String> theDir = thePathMap.getDirAt( 0 );
		for ( String eKey : theDir.keySet() ) {
			ePath = thePathMap.toPath( "0", eKey );
			assertTrue( thePathMap.containsKey( ePath ) );
		}
		final int lastIndex = thePathMap.lastDirIndex();
		assertEquals( 1, lastIndex );
		final int nextIndex = thePathMap.nextDirIndex();
		assertEquals( 2, nextIndex );
	}

	@Test
	public void testDir3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "_root/whatever/2/aaaaa", "0aaaa" );
		thePathMap.put( "_root/whatever/2/bbbbb", "0bbbb" );
		thePathMap.put( "_root/whatever/2/ccccc", "0cccc" );
		thePathMap.put( "_root/whatever/5/aaaaa", "1aaaa" );
		thePathMap.put( "_root/whatever/5/bbbbb", "1bbbb" );
		thePathMap.put( "_root/whatever/5/ccccc", "1cccc" );
		final boolean isArray1 = thePathMap.isIndexDir( "_root" );
		final boolean isArray2 = thePathMap.isIndexDir( "_root/whatever" );
		assertFalse( isArray1 );
		assertTrue( isArray2 );
		int[] theIndexes = thePathMap.getDirIndexes( "_root/whatever" );
		assertEquals( 2, theIndexes.length );
		try {
			theIndexes = thePathMap.getDirIndexes();
			fail( "No indexes at root path expected!" );
		}
		catch ( IllegalArgumentException expected ) {
			/* expected */
		}
		for ( int theIndexe : theIndexes ) {
			assertTrue( thePathMap.hasDirAt( "_root/whatever", theIndexe ) );
		}
		assertFalse( thePathMap.hasDirAt( "_root/whatever/X" ) );
		assertFalse( thePathMap.hasDirAt( "_root/whatever/1" ) );
		assertTrue( thePathMap.hasDirAt( "_root/whatever/2" ) );
		assertFalse( thePathMap.hasDirAt( "_root/whatever/3" ) );
		assertFalse( thePathMap.hasDirAt( "_root/whatever/4" ) );
		assertTrue( thePathMap.hasDirAt( "_root/whatever/5" ) );
		assertFalse( thePathMap.hasDirAt( "_root/whatever/6" ) );
		String ePath;
		final PathMap<String> theDir = thePathMap.getDirAt( "_root/whatever", 2 );
		for ( String eKey : theDir.keySet() ) {
			ePath = thePathMap.toPath( "_root/whatever", "2", eKey );
			assertTrue( thePathMap.containsKey( ePath ) );
		}
		final int lastIndex = thePathMap.lastDirIndex( "_root/whatever" );
		assertEquals( 5, lastIndex );
		final int nextIndex = thePathMap.nextDirIndex( "_root/whatever" );
		assertEquals( 6, nextIndex );
	}
}
