// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.ClassStructMap.ClassStructMapBuilder;

public class ClassStructMapTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSimpleType1() {
		final ClassStructMapBuilder theBuilder = new ClassStructMapBuilderImpl( Sensor.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				System.out.println( eKey + ": " + theBuilder.get( eKey ) );
			}
		}
		assertEquals( theBuilder.get( "name" ), String.class );
		assertEquals( theBuilder.get( "value" ), Integer.class );
	}

	@Test
	public void testSimpleType2() {
		final ClassStructMapBuilder theBuilder = new ClassStructMapBuilderImpl( SensorPack.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				System.out.println( eKey + ": " + theBuilder.get( eKey ) );
			}
		}
		assertEquals( 8, theBuilder.size() );
		for ( String eKey : theBuilder.sortedKeys() ) {
			if ( eKey.endsWith( "name" ) ) {
				assertEquals( theBuilder.get( eKey ), String.class );
			}
			if ( eKey.endsWith( "value" ) ) {
				assertEquals( theBuilder.get( eKey ), Integer.class );
			}
		}
	}

	@Test
	public void testArrayType1() {
		final ClassStructMapBuilder theBuilder = new ClassStructMapBuilderImpl( Sensor[].class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				System.out.println( eKey + ": " + theBuilder.get( eKey ) );
			}
		}
		assertTrue( theBuilder.isArrayType() );
	}

	@Test
	public void testArrayType2() {
		final ClassStructMapBuilder theBuilder = new ClassStructMapBuilderImpl( Sensors.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				System.out.println( eKey + ": " + theBuilder.get( eKey ) );
			}
		}
	}

	@Test
	public void testArrayType3() {
		final ClassStructMapBuilder theBuilder = new ClassStructMapBuilderImpl( Sensors[].class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				System.out.println( eKey + ": " + theBuilder.get( eKey ) );
			}
		}
	}

	@Test
	public void testArrayType4() {
		final String eToPath;
		final Class<?> eClass;
		final ClassStructMapBuilder theBuilder = new ClassStructMapBuilderImpl( Measurements[].class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				System.out.println( eKey + ": " + theBuilder.get( eKey ) );
			}
		}
		eToPath = theBuilder.toPath( "*", "results", "*" );
		eClass = theBuilder.get( eToPath );
		assertFalse( eClass.isArray() );
		assertEquals( Short.class, eClass );
	}

	@Test
	public void testArrayType5() {
		final ClassStructMapBuilder theBuilder = new ClassStructMapBuilderImpl( MulitMeasurements[].class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.sortedKeys() ) {
				System.out.println( eKey + ": " + theBuilder.get( eKey ) );
			}
		}
		assertTrue( theBuilder.isCompositeArrayDir() );
		assertFalse( theBuilder.isPrimitiveArrayType() );
		final ClassStructMap theArrayDir = theBuilder.getCompositeArrayDir();
		assertFalse( theArrayDir.isCompositeArrayDir( "ints" ) );
		assertFalse( theArrayDir.isPrimitiveArrayType( "ints" ) );
		assertTrue( theArrayDir.isWrapperArrayType( "ints" ) );
		assertFalse( theArrayDir.isCompositeArrayDir( "ints" ) );
		assertEquals( Integer.class, theArrayDir.getWrapperArrayType( "ints" ) );
		assertEquals( String.class, theArrayDir.getSimpleArrayType( "strings" ) );
		assertEquals( Short.class, theArrayDir.getWrapperArrayType( "shorts" ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class Sensor {
		private int _value;
		private String _name;

		public Sensor() {}

		public Sensor( String aName, int aValue ) {
			_name = aName;
			_value = aValue;
		}

		public String getName() {
			return _name;
		}

		public int getValue() {
			return _value;
		}
	}

	public static class SensorPack {
		public Sensor _sensorA;
		public Sensor _sensorB;
		public Sensor _sensorC;
		public Sensor _sensorD;

		public SensorPack( Sensor aSensorA, Sensor aSensorB, Sensor aSensorC, Sensor aSensorD ) {
			_sensorA = aSensorA;
			_sensorB = aSensorB;
			_sensorC = aSensorC;
			_sensorD = aSensorD;
		}
	}

	public static class Sensors {
		public Sensor[] _sensors;

		public Sensors( Sensor... aSensors ) {
			_sensors = aSensors;
		}
	}

	public static class Measurements {
		public Short[] _results;

		public Measurements( Short... aResults ) {
			_results = aResults;
		}
	}

	public static class MulitMeasurements {
		public Short[] _shorts;
		public int[] _ints;
		public String[] _strings;
	}
}