// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.PathMap.PathMapBuilder;

public class PathComparatorTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPathComparator1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc/0", "0" );
		thePathMap.put( "aaa/bbb/ccc/1", "1" );
		thePathMap.put( "aaa/bbb/ccc/2", "2" );
		thePathMap.put( "aaa/bbb/ccc/3", "3" );
		thePathMap.put( "aaa/bbb/ccc/4", "4" );
		thePathMap.put( "aaa/bbb/ccc/5", "5" );
		thePathMap.put( "aaa/bbb/ccc/6", "6" );
		int i = 0;
		for ( String eKey : thePathMap.sortedKeys() ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eKey + " = " + thePathMap.get( eKey ) );
			}
			assertEquals( Integer.toString( i ), thePathMap.get( eKey ) );
			i++;
		}
	}

	@Test
	public void testPathComparator2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/10/bbb/ccc/6", "9" );
		thePathMap.put( "aaa/1/bbb/ccc/0", "0" );
		thePathMap.put( "aaa/2/bbb/ccc/0", "1" );
		thePathMap.put( "aaa/3/bbb/ccc/0", "2" );
		thePathMap.put( "aaa/5/bbb/ccc/1", "4" );
		thePathMap.put( "aaa/6/bbb/ccc/2", "5" );
		thePathMap.put( "aaa/7/bbb/ccc/3", "6" );
		thePathMap.put( "aaa/8/bbb/ccc/4", "7" );
		thePathMap.put( "aaa/9/bbb/ccc/5", "8" );
		thePathMap.put( "aaa/4/bbb/ccc/0", "3" );
		int i = 0;
		for ( String eKey : thePathMap.sortedKeys() ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eKey + " = " + thePathMap.get( eKey ) );
			}
			assertEquals( Integer.toString( i ), thePathMap.get( eKey ) );
			i++;
		}
	}
}
