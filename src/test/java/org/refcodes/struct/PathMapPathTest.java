// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.PathMap.PathMapBuilder;

/**
 * The Class PathMapTest.
 */
public class PathMapPathTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testToExternalPath() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String theFromPath = "/a/b/c/d/e";
		final String theToPath = thePathMap.toExternalPath( theFromPath, ':' );
		assertEquals( "a:b:c:d:e", theToPath );
	}

	@Test
	public void testFromExternalPath() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String theFromPath = "::a:b:c:::d:e";
		final String theToPath = thePathMap.fromExternalPath( theFromPath, ':' );
		assertEquals( "/a/b/c/d/e", theToPath );
	}

	@Test
	public void testParentPath1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		assertTrue( thePathMap.hasParentPath( "/animals/dog" ) );
		assertEquals( "/animals", thePathMap.toParentPath( "/animals/dog" ) );
		assertFalse( thePathMap.hasParentPath( "/animals" ) );
		try {
			thePathMap.toParentPath( "/" );
			fail( "There is no parent path!" );
		}
		catch ( IllegalArgumentException ignore ) {}
		final String theParent = thePathMap.toParentPath( "/animals/" );
		assertEquals( thePathMap.getRootPath(), theParent );
	}

	@Test
	public void testRootPath() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		String theRootPath = "/";
		assertTrue( thePathMap.isRootPath( theRootPath ) );
		theRootPath = "/bla";
		assertFalse( thePathMap.isRootPath( theRootPath ) );
	}

	@Test
	public void testToLeaf1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "/bla/blub/foo/bar/";
		final String thePathChild = thePathMap.toLeaf( thePath );
		assertEquals( thePathChild, "bar" );
	}

	@Test
	public void testToLeaf2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "bla/blub/foo/bar";
		final String thePathChild = thePathMap.toLeaf( thePath );
		assertEquals( thePathChild, "bar" );
	}

	@Test
	public void testToLeaf3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "/bla/";
		final String thePathChild = thePathMap.toLeaf( thePath );
		assertEquals( thePathChild, "bla" );
	}

	@Test
	public void testToLeaf4() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "bla";
		final String thePathChild = thePathMap.toLeaf( thePath );
		assertEquals( thePathChild, "bla" );
	}

	@Test
	public void testToLeaf5() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "/";
		final String thePathChild = thePathMap.toLeaf( thePath );
		assertEquals( null, thePathChild );
	}

	@Test
	public void testToPath1() {
		final PathMapBuilder<String> theBuilder = new PathMapBuilderImpl<>( String.class );
		String thePath = theBuilder.toPath( "aaa", "bbb", "ccc" );
		assertEquals( "/aaa/bbb/ccc", thePath );
		thePath = theBuilder.toPath( "aaa/", "/bbb/", "/ccc/" );
		assertEquals( "/aaa/bbb/ccc", thePath );
		thePath = theBuilder.toPath( "////aaa/", "/bbb//", "/ccc/" );
		assertEquals( "/aaa/bbb/ccc", thePath );
	}

	@Test
	public void testToPath2() {
		final PathMapBuilder<String> theBuilder = new PathMapBuilderImpl<>( String.class );
		final String[] someElements = { "1", "2", "3" };
		final List<String> someList = Arrays.asList( "aaa", "bbb", "ccc" );
		final String thePath = theBuilder.toPath( "head", someElements, "body", someList, "tail" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( thePath );
		}
		assertEquals( "/head/1/2/3/body/aaa/bbb/ccc/tail", thePath );
	}

	@Test
	public void testToPath3() {
		final PathMapBuilder<String> theBuilder = new PathMapBuilderImpl<>( String.class );
		final String[] anyElements = { "x", "y", "z" };
		final List<Object> someList = Arrays.asList( anyElements, "aaa", "bbb", "ccc" );
		final Object[] someElements = { "1", "2", "3", someList };
		final String thePath = theBuilder.toPath( "head", someElements, "tail" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( thePath );
		}
		assertEquals( "/head/1/2/3/x/y/z/aaa/bbb/ccc/tail", thePath );
	}

	@Test
	public void testToPathElements1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "/bla/blub/foo/bar/";
		final String[] thePathElements = thePathMap.toPathElements( thePath );
		assertEquals( 4, thePathElements.length );
		assertEquals( "bla", thePathElements[0] );
		assertEquals( "blub", thePathElements[1] );
		assertEquals( "foo", thePathElements[2] );
		assertEquals( "bar", thePathElements[3] );
	}

	@Test
	public void testToPathElements2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "bla/blub/foo/bar";
		final String[] thePathElements = thePathMap.toPathElements( thePath );
		assertEquals( 4, thePathElements.length );
		assertEquals( "bla", thePathElements[0] );
		assertEquals( "blub", thePathElements[1] );
		assertEquals( "foo", thePathElements[2] );
		assertEquals( "bar", thePathElements[3] );
	}

	@Test
	public void testToPathElements3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "/";
		final String[] thePathElements = thePathMap.toPathElements( thePath );
		assertEquals( null, thePathElements );
	}

	@Test
	public void testToNormalizedPath1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( thePathMap.getRootPath(), theNormalizedPath );
	}

	@Test
	public void testToNormalizedPath2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "/";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( thePathMap.getRootPath(), theNormalizedPath );
	}

	@Test
	public void testToNormalizedPath3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "//";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( thePathMap.getRootPath(), theNormalizedPath );
	}

	@Test
	public void testToNormalizedPath4() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "///";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( thePathMap.getRootPath(), theNormalizedPath );
	}

	@Test
	public void testToNormalizedPath5() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "/a/b/c/";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( "/a/b/c", theNormalizedPath );
	}

	@Test
	public void testToNormalizedPath6() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "a///b/////c//";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( "/a/b/c", theNormalizedPath );
	}

	@Test
	public void testToNormalizedPath7() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "///a/b/c/";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( "/a/b/c", theNormalizedPath );
	}

	@Test
	public void testToNormalizedPath8() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final String thePath = "a/b/c/";
		final String theNormalizedPath = thePathMap.toNormalizedPath( thePath );
		assertEquals( "/a/b/c", theNormalizedPath );
	}
}
