// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class PathMapTest.
 */
public class PathMapTestFixures {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private PathMapTestFixures() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * The Class Dog.
	 */
	public static class Dog {

		private String firstName;

		private String lastName;

		private Map<String, Object> properties = new HashMap<>();

		/**
		 * Instantiates a new dog.
		 *
		 * @param firstName the first name
		 * @param lastName the last name
		 */
		public Dog( String firstName, String lastName ) {
			this.firstName = firstName;
			this.lastName = lastName;
			properties.put( "length", 2 );
			properties.put( "height", 1 );
			final Integer[] values = new Integer[] { 0, 1, 2, 3, 4 };
			properties.put( "counter", values );
			properties.put( "clone", values );
			properties.put( "recursion", new Map<?, ?>[] { properties } );
		}

		/**
		 * Gets the first name.
		 *
		 * @return the first name
		 */
		public String getFirstName() {
			return firstName;
		}

		/**
		 * Gets the last name.
		 *
		 * @return the last name
		 */
		public String getLastName() {
			return lastName;
		}

		/**
		 * Gets the properties.
		 *
		 * @return the properties
		 */
		public Map<String, Object> getProperties() {
			return properties;
		}

		/**
		 * Sets the first name.
		 *
		 * @param firstName the new first name
		 */
		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		/**
		 * Sets the last name.
		 *
		 * @param lastName the new last name
		 */
		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}

		/**
		 * Sets the properties.
		 *
		 * @param properties the properties
		 */
		public void setProperties( Map<String, Object> properties ) {
			this.properties = properties;
		}
	}

	/**
	 * The Class Person.
	 */
	public static class Person {

		private String firstName;

		private String lastName;

		private Dog dog;

		/**
		 * Instantiates a new person.
		 */
		public Person() {};

		/**
		 * Instantiates a new person.
		 *
		 * @param firstName the first name
		 * @param lastName the last name
		 * @param dogFirst the dog first
		 * @param dogLast the dog last
		 */
		public Person( String firstName, String lastName, String dogFirst, String dogLast ) {
			this.firstName = firstName;
			this.lastName = lastName;
			dog = new Dog( dogFirst, dogLast );
		}

		/**
		 * Gets the dog.
		 *
		 * @return the dog
		 */
		public Dog getDog() {
			return dog;
		}

		/**
		 * Gets the first name.
		 *
		 * @return the first name
		 */
		public String getFirstName() {
			return firstName;
		}

		/**
		 * Gets the last name.
		 *
		 * @return the last name
		 */
		public String getLastName() {
			return lastName;
		}

		/**
		 * Sets the dog.
		 *
		 * @param dog the new dog
		 */
		public void setDog( Dog dog ) {
			this.dog = dog;
		}

		/**
		 * Sets the first name.
		 *
		 * @param firstName the new first name
		 */
		public void setFirstName( String firstName ) {
			this.firstName = firstName;
		}

		/**
		 * Sets the last name.
		 *
		 * @param lastName the new last name
		 */
		public void setLastName( String lastName ) {
			this.lastName = lastName;
		}
	}

	// @formatter:off
	static String[][] FLAT_MAP_1 = new String[][]{
		new String[]{ "/dog/firstName", "Snowy" },
		new String[]{ "/dog/lastName", "Milou" },
		new String[]{ "/dog/properties/clone/0", "0" },
		new String[]{ "/dog/properties/clone/1", "1" },
		new String[]{ "/dog/properties/clone/2", "2" },
		new String[]{ "/dog/properties/clone/3", "3" },
		new String[]{ "/dog/properties/clone/4", "4" },
		new String[]{ "/dog/properties/counter/0", "0" },
		new String[]{ "/dog/properties/counter/1", "1" },
		new String[]{ "/dog/properties/counter/2", "2" },
		new String[]{ "/dog/properties/counter/3", "3" },
		new String[]{ "/dog/properties/counter/4", "4" },
		new String[]{ "/dog/properties/height", "1" },
		new String[]{ "/dog/properties/length", "2" },
		new String[]{ "/firstName", "Nolan" },
		new String[]{ "/lastName", "Bushnell" }
	};
	// @formatter:on

	// @formatter:off
	static String[][] FLAT_MAP_2 = new String[][]{
		new String[]{ "/firstName", "Snowy" },
		new String[]{ "/lastName", "Milou" },
		new String[]{ "/properties/clone/0", "0" },
		new String[]{ "/properties/clone/1", "1" },
		new String[]{ "/properties/clone/2", "2" },
		new String[]{ "/properties/clone/3", "3" },
		new String[]{ "/properties/clone/4", "4" },
		new String[]{ "/properties/counter/0", "0" },
		new String[]{ "/properties/counter/1", "1" },
		new String[]{ "/properties/counter/2", "2" },
		new String[]{ "/properties/counter/3", "3" },
		new String[]{ "/properties/counter/4", "4" },
		new String[]{ "/properties/height", "1" },
		new String[]{ "/properties/length", "2" }
	};
	// @formatter:on

	// @formatter:off
	static String[][] FLAT_MAP_3 = new String[][]{
		new String[]{ "/clone/0", "0" },
		new String[]{ "/clone/1", "1" },
		new String[]{ "/clone/2", "2" },
		new String[]{ "/clone/3", "3" },
		new String[]{ "/clone/4", "4" },
		new String[]{ "/counter/0", "0" },
		new String[]{ "/counter/1", "1" },
		new String[]{ "/counter/2", "2" },
		new String[]{ "/counter/3", "3" },
		new String[]{ "/counter/4", "4" },
		new String[]{ "/height", "1" },
		new String[]{ "/length", "2" }
	};
	// @formatter:on

	// @formatter:off
	static String[][] INSERT_FROM_1 = new String[][]{
		new String[]{ "/clone/0", "0" },
		new String[]{ "/clone/1", "1" },
		new String[]{ "/clone/2", "2" },
		new String[]{ "/clone/3", "3" },
		new String[]{ "/clone/4", "4" },
		new String[]{ "/counter/0", "0" },
		new String[]{ "/counter/1", "1" },
		new String[]{ "/counter/2", "2" },
		new String[]{ "/counter/3", "3" },
		new String[]{ "/counter/4", "4" },
		new String[]{ "/dog/firstName", "Snowy" },
		new String[]{ "/dog/lastName", "Milou" },
		new String[]{ "/dog/properties/clone/0", "0" },
		new String[]{ "/dog/properties/clone/1", "1" },
		new String[]{ "/dog/properties/clone/2", "2" },
		new String[]{ "/dog/properties/clone/3", "3" },
		new String[]{ "/dog/properties/clone/4", "4" },
		new String[]{ "/dog/properties/counter/0", "0" },
		new String[]{ "/dog/properties/counter/1", "1" },
		new String[]{ "/dog/properties/counter/2", "2" },
		new String[]{ "/dog/properties/counter/3", "3" },
		new String[]{ "/dog/properties/counter/4", "4" },
		new String[]{ "/dog/properties/height", "1" },
		new String[]{ "/dog/properties/length", "2" },
		new String[]{ "/firstName", "Nolan" },
		new String[]{ "/height", "1" },
		new String[]{ "/lastName", "Bushnell" },
		new String[]{ "/length", "2" }

	};
	// @formatter:on

	// @formatter:off
	static String[][] INSERT_TO_1 = new String[][]{
		new String[]{ "/cat/dog/firstName", "Snowy" },
		new String[]{ "/cat/dog/lastName", "Milou" },
		new String[]{ "/cat/dog/properties/clone/0", "0" },
		new String[]{ "/cat/dog/properties/clone/1", "1" },
		new String[]{ "/cat/dog/properties/clone/2", "2" },
		new String[]{ "/cat/dog/properties/clone/3", "3" },
		new String[]{ "/cat/dog/properties/clone/4", "4" },
		new String[]{ "/cat/dog/properties/counter/0", "0" },
		new String[]{ "/cat/dog/properties/counter/1", "1" },
		new String[]{ "/cat/dog/properties/counter/2", "2" },
		new String[]{ "/cat/dog/properties/counter/3", "3" },
		new String[]{ "/cat/dog/properties/counter/4", "4" },
		new String[]{ "/cat/dog/properties/height", "1" },
		new String[]{ "/cat/dog/properties/length", "2" },
		new String[]{ "/cat/firstName", "Nolan" },
		new String[]{ "/cat/lastName", "Bushnell" },
		new String[]{ "/dog/firstName", "Snowy" },
		new String[]{ "/dog/lastName", "Milou" },
		new String[]{ "/dog/properties/clone/0", "0" },
		new String[]{ "/dog/properties/clone/1", "1" },
		new String[]{ "/dog/properties/clone/2", "2" },
		new String[]{ "/dog/properties/clone/3", "3" },
		new String[]{ "/dog/properties/clone/4", "4" },
		new String[]{ "/dog/properties/counter/0", "0" },
		new String[]{ "/dog/properties/counter/1", "1" },
		new String[]{ "/dog/properties/counter/2", "2" },
		new String[]{ "/dog/properties/counter/3", "3" },
		new String[]{ "/dog/properties/counter/4", "4" },
		new String[]{ "/dog/properties/height", "1" },
		new String[]{ "/dog/properties/length", "2" },
		new String[]{ "/firstName", "Nolan" },
		new String[]{ "/lastName", "Bushnell" }
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	static String[][] INSERT_1 = new String[][]{
		new String[]{ "/cat/firstName", "Snowy" },
		new String[]{ "/cat/lastName", "Milou" },
		new String[]{ "/cat/properties/clone/0", "0" },
		new String[]{ "/cat/properties/clone/1", "1" },
		new String[]{ "/cat/properties/clone/2", "2" },
		new String[]{ "/cat/properties/clone/3", "3" },
		new String[]{ "/cat/properties/clone/4", "4" },
		new String[]{ "/cat/properties/counter/0", "0" },
		new String[]{ "/cat/properties/counter/1", "1" },
		new String[]{ "/cat/properties/counter/2", "2" },
		new String[]{ "/cat/properties/counter/3", "3" },
		new String[]{ "/cat/properties/counter/4", "4" },
		new String[]{ "/cat/properties/height", "1" },
		new String[]{ "/cat/properties/length", "2" },
		new String[]{ "/dog/firstName", "Snowy" },
		new String[]{ "/dog/lastName", "Milou" },
		new String[]{ "/dog/properties/clone/0", "0" },
		new String[]{ "/dog/properties/clone/1", "1" },
		new String[]{ "/dog/properties/clone/2", "2" },
		new String[]{ "/dog/properties/clone/3", "3" },
		new String[]{ "/dog/properties/clone/4", "4" },
		new String[]{ "/dog/properties/counter/0", "0" },
		new String[]{ "/dog/properties/counter/1", "1" },
		new String[]{ "/dog/properties/counter/2", "2" },
		new String[]{ "/dog/properties/counter/3", "3" },
		new String[]{ "/dog/properties/counter/4", "4" },
		new String[]{ "/dog/properties/height", "1" },
		new String[]{ "/dog/properties/length", "2" },
		new String[]{ "/firstName", "Nolan" },
		new String[]{ "/lastName", "Bushnell" }
	};
	// @formatter:on

	// @formatter:off
	static String[][] QUERY = new String[][]{
		new String[]{ "/cat/dog/firstName", "Snowy" },
		new String[]{ "/cat/dog/lastName", "Milou" },
		new String[]{ "/cat/dog/properties/clone/0", "0" },
		new String[]{ "/cat/dog/properties/clone/1", "1" },
		new String[]{ "/cat/dog/properties/clone/2", "2" },
		new String[]{ "/cat/dog/properties/clone/3", "3" },
		new String[]{ "/cat/dog/properties/clone/4", "4" },
		new String[]{ "/cat/dog/properties/counter/0", "0" },
		new String[]{ "/cat/dog/properties/counter/1", "1" },
		new String[]{ "/cat/dog/properties/counter/2", "2" },
		new String[]{ "/cat/dog/properties/counter/3", "3" },
		new String[]{ "/cat/dog/properties/counter/4", "4" },
		new String[]{ "/cat/dog/properties/height", "1" },
		new String[]{ "/cat/dog/properties/length", "2" },
		new String[]{ "/cat/firstName", "Nolan" },
		new String[]{ "/cat/lastName", "Bushnell" },
		new String[]{ "/dog/firstName", "Snowy" },
		new String[]{ "/dog/lastName", "Milou" },
		new String[]{ "/dog/properties/clone/0", "0" },
		new String[]{ "/dog/properties/clone/1", "1" },
		new String[]{ "/dog/properties/clone/2", "2" },
		new String[]{ "/dog/properties/clone/3", "3" },
		new String[]{ "/dog/properties/clone/4", "4" },
		new String[]{ "/dog/properties/counter/0", "0" },
		new String[]{ "/dog/properties/counter/1", "1" },
		new String[]{ "/dog/properties/counter/2", "2" },
		new String[]{ "/dog/properties/counter/3", "3" },
		new String[]{ "/dog/properties/counter/4", "4" },
		new String[]{ "/dog/properties/height", "1" },
		new String[]{ "/dog/properties/length", "2" },
		new String[]{ "/cat/properties/clone/0", "0" },
		new String[]{ "/cat/properties/clone/1", "1" },
		new String[]{ "/cat/properties/clone/2", "2" },
		new String[]{ "/cat/properties/clone/3", "3" },
		new String[]{ "/cat/properties/clone/4", "4" },
		new String[]{ "/cat/properties/counter/0", "0" },
		new String[]{ "/cat/properties/counter/1", "1" },
		new String[]{ "/cat/properties/counter/2", "2" },
		new String[]{ "/cat/properties/counter/3", "3" },
		new String[]{ "/cat/properties/counter/4", "4" },
		new String[]{ "/cat/properties/height", "1" },
		new String[]{ "/cat/properties/length", "2" },
		new String[]{ "/firstName", "Nolan" },
		new String[]{ "/lastName", "Bushnell" }
	};
	// @formatter:on
}
