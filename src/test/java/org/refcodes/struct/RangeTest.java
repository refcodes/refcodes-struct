package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.Range.RangeBuilder;
import org.refcodes.struct.RangeImpl.RangeBuilderImpl;

/**
 * The Class RangeTest.
 */
public class RangeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRange1() {
		final RangeBuilder<Integer> theRange = new RangeBuilderImpl<>();
		theRange.withMinValue( 10 ).withMaxValue( 20 );
		assertFalse( theRange.isMember( 5 ) );
		assertFalse( theRange.isMember( 9 ) );
		assertTrue( theRange.isMember( 10 ) );
		assertTrue( theRange.isMember( 15 ) );
		assertTrue( theRange.isMember( 20 ) );
		assertFalse( theRange.isMember( 21 ) );
		assertFalse( theRange.isMember( 25 ) );
	}

	@Test
	public void testRange2() {
		final Range<Integer> theRange = Range.toRange( "1-10", Integer.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theRange );
		}
		assertEquals( 1, theRange.getMinValue() );
		assertEquals( 10, theRange.getMaxValue() );
	}
}
