// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class TypeUtilityTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testToElementType1() {
		final List<String> theCollection = new ArrayList<>();
		theCollection.add( "Hallo" );
		theCollection.add( "Welt" );
		final Class<String> theType = TypeUtility.toElelemtType( theCollection );
		assertNotNull( theType );
	}

	@Test
	public void testToElementType2() {
		final List<Vehicle> theCollection = new ArrayList<>();
		theCollection.add( new Car() );
		theCollection.add( new KeiCar() );
		final Class<Vehicle> theType = TypeUtility.toElelemtType( theCollection );
		assertNotNull( theType );
	}

	@Test
	public void testToElementType3() {
		final List<Vehicle> theCollection = new ArrayList<>();
		theCollection.add( null );
		theCollection.add( new Bicycle() );
		theCollection.add( new KeiCar() );
		final Class<Vehicle> theType = TypeUtility.toElelemtType( theCollection );
		assertNotNull( theType );
	}

	@Test
	public void testToArray1() {
		final List<String> theCollection = new ArrayList<>();
		theCollection.add( "Hallo" );
		theCollection.add( "Welt" );
		final String[] theArray = TypeUtility.toArray( theCollection );
		assertEquals( theArray[0], theCollection.get( 0 ) );
		assertEquals( theArray[1], theCollection.get( 1 ) );
	}

	@Test
	public void testToArray2() {
		final List<Vehicle> theCollection = new ArrayList<>();
		theCollection.add( new Car() );
		theCollection.add( new KeiCar() );
		final Vehicle[] theArray = TypeUtility.toArray( theCollection );
		assertEquals( theArray[0], theCollection.get( 0 ) );
		assertEquals( theArray[1], theCollection.get( 1 ) );
	}

	@Test
	public void testToArray3() {
		final List<Vehicle> theCollection = new ArrayList<>();
		theCollection.add( null );
		theCollection.add( new Bicycle() );
		theCollection.add( new KeiCar() );
		final Vehicle[] theArray = TypeUtility.toArray( theCollection );
		assertEquals( theArray[0], theCollection.get( 0 ) );
		assertEquals( theArray[1], theCollection.get( 1 ) );
		assertEquals( theArray[2], theCollection.get( 2 ) );
	}

	@Test
	public void testToArray4() {
		final List<Vehicle> theCollection = new ArrayList<>();
		final Vehicle[] theArray = TypeUtility.toArray( theCollection );
		assertNull( theArray );
	}

	@Test
	public void testToArray5() {
		final List<Vehicle> theCollection = new ArrayList<>();
		theCollection.add( null );
		theCollection.add( null );
		final Vehicle[] theArray = TypeUtility.toArray( theCollection );
		assertNull( theArray );
	}

	@Test
	public void testToArray6() {
		final List<Machine> theCollection = new ArrayList<>();
		theCollection.add( new Car() );
		theCollection.add( new KeiCar() );
		final Machine[] theArray = TypeUtility.toArray( theCollection );
		assertEquals( theArray[0], theCollection.get( 0 ) );
		assertEquals( theArray[1], theCollection.get( 1 ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static interface Machine {
		void doSomething();
	};

	public static class Vehicle {};

	public static class Bicycle extends Vehicle implements Machine {
		@Override
		public void doSomething() {}
	};

	public static class Car extends Vehicle implements Machine {
		@Override
		public void doSomething() {}
	};

	public static class KeiCar extends Car {};
}
