// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.Test;

public class BoundedIteratorTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testBoundedIterator1() {
		final List<String> theAsList = Arrays.asList( new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" } );
		final Iterator<String> theIterator = theAsList.iterator();
		int count = 0;
		final int bounds = 0;
		final BoundedIterator<String> e = new BoundedIterator<>( theIterator, bounds );
		String eStr;
		while ( e.hasNext() ) {
			eStr = e.next();
			count++;
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + count + "/" + bounds + "|" + theAsList.size() + "] --> " + eStr );
			}
		}
		assertEquals( bounds, count );
	}

	@Test
	public void testBoundedIterator2() {
		final List<String> theAsList = Arrays.asList( new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" } );
		final Iterator<String> theIterator = theAsList.iterator();
		int count = 0;
		final int bounds = 1;
		final BoundedIterator<String> e = new BoundedIterator<>( theIterator, bounds );
		String eStr;
		while ( e.hasNext() ) {
			eStr = e.next();
			count++;
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + count + "/" + bounds + "|" + theAsList.size() + "] --> " + eStr );
			}
		}
		assertEquals( bounds, count );
	}

	@Test
	public void testBoundedIterator3() {
		final List<String> theAsList = Arrays.asList( new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" } );
		final Iterator<String> theIterator = theAsList.iterator();
		int count = 0;
		final int bounds = 5;
		final BoundedIterator<String> e = new BoundedIterator<>( theIterator, bounds );
		String eStr;
		while ( e.hasNext() ) {
			eStr = e.next();
			count++;
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + count + "/" + bounds + "|" + theAsList.size() + "] --> " + eStr );
			}
		}
		assertEquals( bounds, count );
	}

	@Test
	public void testBoundedIterator4() {
		final List<String> theAsList = Arrays.asList( new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" } );
		final Iterator<String> theIterator = theAsList.iterator();
		int count = 0;
		final int bounds = 6;
		final BoundedIterator<String> e = new BoundedIterator<>( theIterator, bounds );
		String eStr;
		while ( e.hasNext() ) {
			eStr = e.next();
			count++;
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + count + "/" + bounds + "|" + theAsList.size() + "] --> " + eStr );
			}
		}
		assertEquals( bounds, count );
	}

	@Test
	public void testBoundedIterator5() {
		final List<String> theAsList = Arrays.asList( new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" } );
		final Iterator<String> theIterator = theAsList.iterator();
		int count = 0;
		final int bounds = 10;
		final BoundedIterator<String> e = new BoundedIterator<>( theIterator, bounds );
		String eStr;
		while ( e.hasNext() ) {
			eStr = e.next();
			count++;
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + count + "/" + bounds + "|" + theAsList.size() + "] --> " + eStr );
			}
		}
		assertEquals( bounds, count );
	}

	@Test
	public void testBoundedIterator6() {
		final List<String> theAsList = Arrays.asList( new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" } );
		final Iterator<String> theIterator = theAsList.iterator();
		int count = 0;
		final int bounds = 11;
		final BoundedIterator<String> e = new BoundedIterator<>( theIterator, bounds );
		String eStr;
		while ( e.hasNext() ) {
			eStr = e.next();
			count++;
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + count + "/" + bounds + "|" + theAsList.size() + "] --> " + eStr );
			}
		}
		assertEquals( theAsList.size(), count );
	}

	@Test
	public void testBoundedIterator7() {
		final List<String> theAsList = Arrays.asList( new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" } );
		final Iterator<String> theIterator = theAsList.iterator();
		int count = 0;
		final int bounds = 99;
		final BoundedIterator<String> e = new BoundedIterator<>( theIterator, bounds );
		String eStr;
		while ( e.hasNext() ) {
			eStr = e.next();
			count++;
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + count + "/" + bounds + "|" + theAsList.size() + "] --> " + eStr );
			}
		}
		assertEquals( theAsList.size(), count );
	}
}
