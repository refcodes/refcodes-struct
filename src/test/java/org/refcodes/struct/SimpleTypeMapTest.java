// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.UUID;
import org.junit.jupiter.api.Test;

/**
 * @author steiner
 */
public class SimpleTypeMapTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPrimitiveTypeMap1() {
		final DataStructure theStruct2 = new DataStructure( true, (byte) 0xF, 'A', (short) 277, 32536, 987671, 17.3F, 18.4D, "Hallo Welt!" );
		final DataStructure theStructA = new DataStructure( true, (byte) 0x77, 'X', (short) 129, 5161, 51615161, 12.2F, 121238.9999, "Hello World!", theStruct2 );
		final SimpleTypeMap theMapA = new SimpleTypeMapImpl( theStructA );
		Object eValue;
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theMapA.sortedKeys() ) {
				eValue = theMapA.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final DataStructure theStructB = theMapA.toType( DataStructure.class );
		final SimpleTypeMap theMapB = new SimpleTypeMapImpl( theStructB );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "--------------------------------------------------------------------------------" );
			for ( String eKey : theMapB.sortedKeys() ) {
				eValue = theMapB.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		assertEquals( theStructA, theStructB );
	}

	@Test
	public void testPrimitiveTypeMap2() {
		final DataStructure theStruct2 = new DataStructure( true, (byte) 0xF, 'A', (short) 277, 32536, 987671, 17.3F, 18.4D, "Hallo Welt!" );
		final DataStructure theStructA = new DataStructure( true, (byte) 0x77, 'X', (short) 129, 5161, 51615161, 12.2F, 121238.9999, "Hello World!", theStruct2 );
		final DataStructure theStructB = new DataStructure( true, (byte) 0x44, 'C', (short) 31, 6151, 1615161, 99887.223F, 28238.10, "Hi World!" );
		final ArrayStructure theArrayA = new ArrayStructure( new int[] { 1, 2, 3, 4, 5 }, new DataStructure[] { theStructA, theStructB, theStruct2 } );
		final SimpleTypeMap theMapA = new SimpleTypeMapImpl( theArrayA );
		Object eValue;
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theMapA.sortedKeys() ) {
				eValue = theMapA.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final ArrayStructure theArrayB = theMapA.toType( ArrayStructure.class );
		final SimpleTypeMap theMapB = new SimpleTypeMapImpl( theArrayB );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "--------------------------------------------------------------------------------" );
			for ( String eKey : theMapB.sortedKeys() ) {
				eValue = theMapB.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		assertEquals( theArrayA, theArrayB );
	}

	@Test
	public void testClass() {
		final Class<?> theClass = DataStructure.class;
		final SimpleTypeMap theMap = new SimpleTypeMapImpl( theClass );
		Object eValue;
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theMap.sortedKeys() ) {
				eValue = theMap.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final Class<?> theResult = theMap.toType( Class.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( theClass, theResult );
	}

	@Test
	public void testUUID() {
		final UUID theUUID = UUID.randomUUID();
		final SimpleTypeMap theMap = new SimpleTypeMapImpl( theUUID );
		Object eValue;
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theMap.sortedKeys() ) {
				eValue = theMap.get( eKey );
				System.out.println( eKey + ": " + eValue + ( eValue != null ? " (" + eValue.getClass().getSimpleName() + ")" : "" ) );
			}
		}
		final UUID theResult = theMap.toType( UUID.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( theUUID, theResult );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class ArrayStructure {
		private int[] _ints;
		private DataStructure[] _dataStructures;

		public ArrayStructure() {}

		public ArrayStructure( int[] ints, DataStructure[] dataStructures ) {
			_ints = ints;
			_dataStructures = dataStructures;
		}

		public int[] getInts() {
			return _ints;
		}

		public void setInts( int[] ints ) {
			this._ints = ints;
		}

		public DataStructure[] getDataStructures() {
			return _dataStructures;
		}

		public void setDataStructures( DataStructure[] dataStructures ) {
			this._dataStructures = dataStructures;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode( _dataStructures );
			result = prime * result + Arrays.hashCode( _ints );
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final ArrayStructure other = (ArrayStructure) obj;
			if ( !Arrays.equals( _dataStructures, other._dataStructures ) ) {
				return false;
			}
			if ( !Arrays.equals( _ints, other._ints ) ) {
				return false;
			}
			return true;
		}
	}

	public static class DataStructure {
		private DataStructure _dataStructure = null;
		private boolean _boolean;
		private byte _byte;
		private char _char;
		private short _short;
		private int _int;
		private long _long;
		private float _float;
		private double _double;
		private String _string;

		public DataStructure() {}

		public DataStructure( DataStructure aDataStructure ) {
			_dataStructure = aDataStructure;
		}

		public DataStructure( boolean aBoolean, byte aByte, char aChar, short aShort, int aInt, long aLong, float aFloat, double aDouble, String aString ) {
			_boolean = aBoolean;
			_byte = aByte;
			_char = aChar;
			_short = aShort;
			_int = aInt;
			_long = aLong;
			_float = aFloat;
			_double = aDouble;
			_string = aString;
		}

		public DataStructure( boolean aBoolean, byte aByte, char aChar, short aShort, int aInt, long aLong, float aFloat, double aDouble, String aString, DataStructure aDataStructure ) {
			_boolean = aBoolean;
			_byte = aByte;
			_char = aChar;
			_short = aShort;
			_int = aInt;
			_long = aLong;
			_float = aFloat;
			_double = aDouble;
			_string = aString;
			_dataStructure = aDataStructure;
		}

		public DataStructure getDataStructure() {
			return _dataStructure;
		}

		public void setDataStructure( DataStructure _dataStructure ) {
			this._dataStructure = _dataStructure;
		}

		public boolean isBoolean() {
			return _boolean;
		}

		public void setBoolean( boolean _boolean ) {
			this._boolean = _boolean;
		}

		public byte getByte() {
			return _byte;
		}

		public void setByte( byte _byte ) {
			this._byte = _byte;
		}

		public char getChar() {
			return _char;
		}

		public void setChar( char _char ) {
			this._char = _char;
		}

		public short getShort() {
			return _short;
		}

		public void setShort( short _short ) {
			this._short = _short;
		}

		public int getInt() {
			return _int;
		}

		public void setInt( int _int ) {
			this._int = _int;
		}

		public long getLong() {
			return _long;
		}

		public void setLong( long _long ) {
			this._long = _long;
		}

		public float getFloat() {
			return _float;
		}

		public void setFloat( float _float ) {
			this._float = _float;
		}

		public double getDouble() {
			return _double;
		}

		public void setDouble( double _double ) {
			this._double = _double;
		}

		public String getString() {
			return _string;
		}

		public void setString( String _string ) {
			this._string = _string;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( _boolean ? 1231 : 1237 );
			result = prime * result + _byte;
			result = prime * result + _char;
			result = prime * result + ( ( _dataStructure == null ) ? 0 : _dataStructure.hashCode() );
			final long temp;
			temp = Double.doubleToLongBits( _double );
			result = prime * result + (int) ( temp ^ ( temp >>> 32 ) );
			result = prime * result + Float.floatToIntBits( _float );
			result = prime * result + _int;
			result = prime * result + (int) ( _long ^ ( _long >>> 32 ) );
			result = prime * result + _short;
			result = prime * result + ( ( _string == null ) ? 0 : _string.hashCode() );
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final DataStructure other = (DataStructure) obj;
			if ( _boolean != other._boolean ) {
				return false;
			}
			if ( _byte != other._byte ) {
				return false;
			}
			if ( _char != other._char ) {
				return false;
			}
			if ( _dataStructure == null ) {
				if ( other._dataStructure != null ) {
					return false;
				}
			}
			else if ( !_dataStructure.equals( other._dataStructure ) ) {
				return false;
			}
			if ( Double.doubleToLongBits( _double ) != Double.doubleToLongBits( other._double ) ) {
				return false;
			}
			if ( Float.floatToIntBits( _float ) != Float.floatToIntBits( other._float ) ) {
				return false;
			}
			if ( _int != other._int ) {
				return false;
			}
			if ( _long != other._long ) {
				return false;
			}
			if ( _short != other._short ) {
				return false;
			}
			if ( _string == null ) {
				if ( other._string != null ) {
					return false;
				}
			}
			else if ( !_string.equals( other._string ) ) {
				return false;
			}
			return true;
		}
	}
}
