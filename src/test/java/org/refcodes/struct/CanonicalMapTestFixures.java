// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CanonicalMapTestFixures {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private CanonicalMapTestFixures() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * The Class Dog.
	 */
	public static class Dog {

		private String _firstName;
		private String _lastName;

		private final List<String> _likes = new ArrayList<>();

		/**
		 * Instantiates a new dog.
		 */
		public Dog() {}

		/**
		 * Instantiates a new dog.
		 *
		 * @param aFirstName the first name
		 * @param aLastName the last name
		 */
		public Dog( String aFirstName, String aLastName ) {
			_firstName = aFirstName;
			_lastName = aLastName;
		}

		/**
		 * Adds the like.
		 *
		 * @param aLike the like
		 */
		public void addLike( String aLike ) {
			_likes.add( aLike );
		}

		/**
		 * Gets the first name.
		 *
		 * @return the first name
		 */
		public String getFirstName() {
			return _firstName;
		}

		/**
		 * Gets the last name.
		 *
		 * @return the last name
		 */
		public String getLastName() {
			return _lastName;
		}

		/**
		 * Gets the likes.
		 *
		 * @return the likes
		 */
		public List<String> getLikes() {
			return _likes;
		}

		/**
		 * Sets the first name.
		 *
		 * @param _firstName the new first name
		 */
		public void setFirstName( String _firstName ) {
			this._firstName = _firstName;
		}

		/**
		 * Sets the last name.
		 *
		 * @param _lastName the new last name
		 */
		public void setLastName( String _lastName ) {
			this._lastName = _lastName;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class Person.
	 */
	public static class Person {

		private String _firstName;
		private String _lastName;
		private boolean _computerPioneer;
		private int _age;
		private Dog _dog;
		private int[] _numbers;
		private Map<String, String> _hobbies = new HashMap<>();

		/**
		 * Instantiates a new person.
		 */
		public Person() {}

		/**
		 * Instantiates a new person.
		 *
		 * @param aFirstName the first name
		 * @param aLastName the last name
		 * @param aComputerPioneer the computer pioneer
		 * @param aAge the age
		 */
		public Person( String aFirstName, String aLastName, boolean aComputerPioneer, int aAge ) {
			_firstName = aFirstName;
			_lastName = aLastName;
			_computerPioneer = aComputerPioneer;
			_age = aAge;
		}

		/**
		 * Adds the hobby.
		 *
		 * @param aKey the key
		 * @param aValue the value
		 */
		public void addHobby( String aKey, String aValue ) {
			_hobbies.put( aKey, aValue );
		}

		/**
		 * Gets the age.
		 *
		 * @return the age
		 */
		public int getAge() {
			return _age;
		}

		/**
		 * Gets the dog.
		 *
		 * @return the dog
		 */
		public Dog getDog() {
			return _dog;
		}

		/**
		 * Gets the first name.
		 *
		 * @return the first name
		 */
		public String getFirstName() {
			return _firstName;
		}

		/**
		 * Gets the hobbies.
		 *
		 * @return the hobbies
		 */
		public Map<String, String> getHobbies() {
			return _hobbies;
		}

		/**
		 * Gets the last name.
		 *
		 * @return the last name
		 */
		public String getLastName() {
			return _lastName;
		}

		/**
		 * Gets the numbers.
		 *
		 * @return the numbers
		 */
		public int[] getNumbers() {
			return _numbers;
		}

		/**
		 * Checks if is computer pioneer.
		 *
		 * @return true, if is computer pioneer
		 */
		public boolean isComputerPioneer() {
			return _computerPioneer;
		}

		/**
		 * Sets the age.
		 *
		 * @param _age the new age
		 */
		public void setAge( int _age ) {
			this._age = _age;
		}

		/**
		 * Sets the computer pioneer.
		 *
		 * @param _computerPioneer the new computer pioneer
		 */
		public void setComputerPioneer( boolean _computerPioneer ) {
			this._computerPioneer = _computerPioneer;
		}

		/**
		 * Sets the dog.
		 *
		 * @param dog the new dog
		 */
		public void setDog( Dog dog ) {
			this._dog = dog;
		}

		/**
		 * Gets the hobbies.
		 *
		 * @return the hobbies
		 */
		public void setHobbies( Map<String, String> aHobbies ) {
			_hobbies = aHobbies;
		}

		/**
		 * Sets the last name.
		 *
		 * @param _lastName the new last name
		 */
		public void setLastName( String _lastName ) {
			this._lastName = _lastName;
		}

		/**
		 * Sets the numbers.
		 *
		 * @param aNumbers the new numbers
		 */
		public void setNumbers( int[] aNumbers ) {
			_numbers = aNumbers;
		}
	}

	// @formatter:off
	static String[][] PROPERTIES = {
		{ "/age", "78" }, 
		{ "/computerPioneer", "true" }, 
		{ "/dog/firstName", "Snoopy" }, 
		{ "/dog/lastName", "Struppi" }, 
		{ "/dog/likes/0", "Dog food" }, 
		{ "/dog/likes/1", "Chocolate" }, 
		{ "/dog/likes/2", "Water" }, 
		{ "/dog/likes/3", null }, 
		{ "/dog/likes/4", "Beer" }, 
		{ "/firstName", "Nolan" }, 
		{ "/hobbies/Chop-Lifter", "HIGH" }, 
		{ "/hobbies/Computing", "HIGH" }, 
		{ "/hobbies/Winter Games", "MEDIUM" }, 
		{ "/lastName", "Bushnell" }, 
		{ "/numbers/0", "5" }, 
		{ "/numbers/1", "1" }, 
		{ "/numbers/2", "6" }, 
		{ "/numbers/3", "1" } 
	};
	// @formatter:on

}