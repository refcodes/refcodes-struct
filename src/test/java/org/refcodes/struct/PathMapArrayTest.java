// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.PathMap.PathMapBuilder;
import org.refcodes.struct.PathMapTestFixures.Person;

/**
 * The Class PathMapTest.
 */
public class PathMapArrayTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAppendValueTo() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc/0", "0" );
		thePathMap.put( "aaa/bbb/ccc/1", "1" );
		thePathMap.put( "aaa/bbb/ccc/2", "2" );
		thePathMap.put( "aaa/bbb/ccc/3", "3" );
		thePathMap.put( "aaa/bbb/ccc/4", "4" );
		thePathMap.put( "aaa/bbb/ccc/5", "5" );
		thePathMap.put( "aaa/bbb/ccc/6", "6" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		thePathMap.appendValueTo( new String[] { "aaa", "bbb", "ccc" }, "7" );
		assertEquals( "7", thePathMap.get( "aaa/bbb/ccc/7" ) );
	}

	@Test
	public void testArrayIndexes1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc/0", "0" );
		thePathMap.put( "aaa/bbb/ccc/1", "1" );
		thePathMap.put( "aaa/bbb/ccc/2", "2" );
		thePathMap.put( "aaa/bbb/ccc/3", "3" );
		thePathMap.put( "aaa/bbb/ccc/4", "4" );
		thePathMap.put( "aaa/bbb/ccc/5", "5" );
		thePathMap.put( "aaa/bbb/ccc/6", "6" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		final int[] indexes = thePathMap.getArrayIndexes( "aaa/bbb/ccc" );
		assertEquals( 7, indexes.length );
		assertEquals( 0, indexes[0] );
		assertEquals( 1, indexes[1] );
		assertEquals( 2, indexes[2] );
		assertEquals( 3, indexes[3] );
		assertEquals( 4, indexes[4] );
		assertEquals( 5, indexes[5] );
		assertEquals( 6, indexes[6] );
		assertTrue( thePathMap.hasValueAt( "aaa/bbb/ccc", 0 ) );
		assertTrue( thePathMap.hasValueAt( "aaa/bbb/ccc/0" ) );
		assertFalse( thePathMap.hasValueAt( "aaa/bbb/ccc", 7 ) );
		assertFalse( thePathMap.hasValueAt( "aaa/bbb/ccc/7" ) );
	}

	@Test
	public void testArrayIndexes2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "/0", "0" );
		thePathMap.put( "/1", "1" );
		thePathMap.put( "/2", "2" );
		thePathMap.put( "3", "3" );
		thePathMap.put( "4", "4" );
		thePathMap.put( "5", "5" );
		thePathMap.put( "6", "6" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		final int[] indexes = thePathMap.getArrayIndexes();
		assertEquals( 7, indexes.length );
		assertEquals( 0, indexes[0] );
		assertEquals( 1, indexes[1] );
		assertEquals( 2, indexes[2] );
		assertEquals( 3, indexes[3] );
		assertEquals( 4, indexes[4] );
		assertEquals( 5, indexes[5] );
		assertEquals( 6, indexes[6] );
		assertTrue( thePathMap.hasValueAt( 0 ) );
		assertTrue( thePathMap.hasValueAt( "0" ) );
		assertFalse( thePathMap.hasValueAt( 7 ) );
		assertFalse( thePathMap.hasValueAt( "7" ) );
		final boolean isArray = thePathMap.isArray();
		assertTrue( isArray );
	}

	@Test
	public void testArrayIndexes3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "/0", "0" );
		thePathMap.put( "/1", "1" );
		thePathMap.put( "/2", "2" );
		thePathMap.put( "/3", "3" );
		thePathMap.put( "/4", "4" );
		thePathMap.put( "/5", "5" );
		thePathMap.put( "/6", "6" );
		thePathMap.put( "/X", "X" );
		try {
			thePathMap.getArrayIndexes();
			fail( "Expected failure as we have an invalid index <X> twice!" );
		}
		catch ( IllegalArgumentException expected ) {}
		final boolean isArray = thePathMap.isArray();
		assertFalse( isArray );
	}

	@Test
	public void testArrayIndexes4() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc/0", "0" );
		thePathMap.put( "aaa/bbb/ccc/1", "1" );
		thePathMap.put( "aaa/bbb/ccc/2", "2" );
		thePathMap.put( "aaa/bbb/ccc/3", "3" );
		thePathMap.put( "aaa/bbb/ccc/4", "4" );
		thePathMap.put( "aaa/bbb/ccc/5", "5" );
		thePathMap.put( "aaa/bbb/ccc/6", "6" );
		thePathMap.put( "aaa/bbb/ccc/X", "X" );
		try {
			thePathMap.getArrayIndexes( "aaa/bbb/ccc" );
			fail( "Expected failure as we have an invalid index <X> twice!" );
		}
		catch ( IllegalArgumentException expected ) {}
		final boolean isArray = thePathMap.isArray( "aaa/bbb/ccc" );
		assertFalse( isArray );
	}

	@Test
	public void testArrayIndexes5() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		int[] indexes = thePathMap.getArrayIndexes();
		assertEquals( 0, indexes.length );
		int lastIndex = thePathMap.lastArrayIndex();
		assertEquals( -1, lastIndex );
		int nextIndex = thePathMap.nextArrayIndex();
		assertEquals( 0, nextIndex );
		indexes = thePathMap.getArrayIndexes( thePathMap.getRootPath() );
		assertEquals( 0, indexes.length );
		lastIndex = thePathMap.lastArrayIndex( thePathMap.getRootPath() );
		assertEquals( -1, lastIndex );
		nextIndex = thePathMap.nextArrayIndex( thePathMap.getRootPath() );
		assertEquals( 0, nextIndex );
		String[] theArray = thePathMap.getArray();
		assertEquals( 0, theArray.length );
		theArray = thePathMap.getArray( thePathMap.getRootPath() );
		assertEquals( 0, theArray.length );
	}

	@Test
	public void testArrayIndexes6() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc", "dummy" );
		final int[] indexes = thePathMap.getArrayIndexes( "aaa/bbb/ccc/ddd" );
		assertEquals( 0, indexes.length );
		final int lastIndex = thePathMap.lastArrayIndex( "aaa/bbb/ccc/ddd" );
		assertEquals( -1, lastIndex );
		final int nextIndex = thePathMap.nextArrayIndex( "aaa/bbb/ccc/ddd" );
		assertEquals( 0, nextIndex );
	}

	@Test
	public void testArrayIndexes7() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "/", "dummy" );
		assertEquals( "dummy", thePathMap.get( thePathMap.getRootPath() ) );
		try {
			thePathMap.getArrayIndexes();
			fail( "Expected an <IllegalArgumentException>!" );
		}
		catch ( IllegalArgumentException expected ) {}
		assertFalse( thePathMap.isArray() );
	}

	@Test
	public void testRemoveValueFrom1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc/0", "0" );
		thePathMap.put( "aaa/bbb/ccc/1", "1" );
		thePathMap.put( "aaa/bbb/ccc/2", "2" );
		thePathMap.put( "aaa/bbb/ccc/3", "3" );
		thePathMap.put( "aaa/bbb/ccc/4", "4" );
		thePathMap.put( "aaa/bbb/ccc/5", "5" );
		thePathMap.put( "aaa/bbb/ccc/6", "6" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		assertEquals( "0", thePathMap.getValueAt( "aaa/bbb/ccc", 0 ) );
		assertEquals( "1", thePathMap.getValueAt( "aaa/bbb/ccc", 1 ) );
		assertEquals( "2", thePathMap.getValueAt( "aaa/bbb/ccc", 2 ) );
		assertEquals( "3", thePathMap.getValueAt( "aaa/bbb/ccc", 3 ) );
		assertEquals( "4", thePathMap.getValueAt( "aaa/bbb/ccc", 4 ) );
		assertEquals( "5", thePathMap.getValueAt( "aaa/bbb/ccc", 5 ) );
		assertEquals( "6", thePathMap.getValueAt( "aaa/bbb/ccc", 6 ) );
		final String theValue = thePathMap.removeValueAt( "aaa/bbb/ccc", 6 );
		assertEquals( "6", theValue );
		assertEquals( "0", thePathMap.getValueAt( "aaa/bbb/ccc", 0 ) );
		assertEquals( "1", thePathMap.getValueAt( "aaa/bbb/ccc", 1 ) );
		assertEquals( "2", thePathMap.getValueAt( "aaa/bbb/ccc", 2 ) );
		assertEquals( "3", thePathMap.getValueAt( "aaa/bbb/ccc", 3 ) );
		assertEquals( "4", thePathMap.getValueAt( "aaa/bbb/ccc", 4 ) );
		assertEquals( "5", thePathMap.getValueAt( "aaa/bbb/ccc", 5 ) );
		assertEquals( null, thePathMap.getValueAt( "aaa/bbb/ccc", 6 ) );
	}

	@Test
	public void testRemoveValueFrom2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "aaa/bbb/ccc/0", "0" );
		thePathMap.put( "aaa/bbb/ccc/1", "1" );
		thePathMap.put( "aaa/bbb/ccc/2", "2" );
		thePathMap.put( "aaa/bbb/ccc/3", "3" );
		thePathMap.put( "aaa/bbb/ccc/4", "4" );
		thePathMap.put( "aaa/bbb/ccc/5", "5" );
		thePathMap.put( "aaa/bbb/ccc/6", "6" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		assertEquals( "0", thePathMap.getValueAt( "aaa/bbb/ccc", 0 ) );
		assertEquals( "1", thePathMap.getValueAt( "aaa/bbb/ccc", 1 ) );
		assertEquals( "2", thePathMap.getValueAt( "aaa/bbb/ccc", 2 ) );
		assertEquals( "3", thePathMap.getValueAt( "aaa/bbb/ccc", 3 ) );
		assertEquals( "4", thePathMap.getValueAt( "aaa/bbb/ccc", 4 ) );
		assertEquals( "5", thePathMap.getValueAt( "aaa/bbb/ccc", 5 ) );
		assertEquals( "6", thePathMap.getValueAt( "aaa/bbb/ccc", 6 ) );
		final String theValue = thePathMap.removeValueAt( new String[] { "aaa", "bbb", "ccc" }, 6 );
		assertEquals( "6", theValue );
		assertEquals( "0", thePathMap.getValueAt( "aaa/bbb/ccc", 0 ) );
		assertEquals( "1", thePathMap.getValueAt( "aaa/bbb/ccc", 1 ) );
		assertEquals( "2", thePathMap.getValueAt( "aaa/bbb/ccc", 2 ) );
		assertEquals( "3", thePathMap.getValueAt( "aaa/bbb/ccc", 3 ) );
		assertEquals( "4", thePathMap.getValueAt( "aaa/bbb/ccc", 4 ) );
		assertEquals( "5", thePathMap.getValueAt( "aaa/bbb/ccc", 5 ) );
		assertEquals( null, thePathMap.getValueAt( "aaa/bbb/ccc", 6 ) );
	}

	@Test
	public void testArray1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final String thePath = "/dog/properties/counter";
		assertTrue( thePathMap.isArray( thePath ) );
		final String[] theRecords = thePathMap.getArray( thePath );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( int i = 0; i < theRecords.length; i++ ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "[" + i + "] := " + theRecords[i] );
				}
			}
		}
		assertEquals( 5, theRecords.length );
	}

	@Test
	public void testArray2() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final String thePath = "/dog/properties/counter";
		thePathMap.put( new String[] { thePath, "7" }, "7" );
		final String[] theRecords = thePathMap.getArray( thePath );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( int i = 0; i < theRecords.length; i++ ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "[" + i + "] := " + theRecords[i] );
				}
			}
		}
		assertNull( theRecords[5] );
		assertNull( theRecords[6] );
		assertEquals( 8, theRecords.length );
		assertTrue( thePathMap.isArray( "/dog/properties/counter" ) );
	}

	@Test
	public void testArray3() {
		final PathMapBuilder<String> theBuilder = new PathMapBuilderImpl<>( String.class );
		theBuilder.put( "/0", "0" );
		theBuilder.put( "/1", "1" );
		theBuilder.put( "/2", "2" );
		assertTrue( theBuilder.isArray() );
		assertTrue( theBuilder.hasValueAt( 0 ) );
		assertTrue( theBuilder.hasValueAt( 1 ) );
		assertTrue( theBuilder.hasValueAt( 2 ) );
		assertFalse( theBuilder.hasValueAt( 3 ) );
		assertEquals( theBuilder.getValueAt( 0 ), "0" );
		assertEquals( theBuilder.getValueAt( 1 ), "1" );
		assertEquals( theBuilder.getValueAt( 2 ), "2" );
	}

	@Test
	public void testArray4() {
		final PathMapBuilder<String> theBuilder = new PathMapBuilderImpl<>( String.class );
		theBuilder.put( "test/0", "0" );
		theBuilder.put( "test/1", "1" );
		theBuilder.put( "test/2", "2" );
		assertTrue( theBuilder.isArray( "test" ) );
		assertTrue( theBuilder.hasValueAt( "test", 0 ) );
		assertTrue( theBuilder.hasValueAt( "test", 1 ) );
		assertTrue( theBuilder.hasValueAt( "test", 2 ) );
		assertFalse( theBuilder.hasValueAt( "test", 3 ) );
		assertEquals( theBuilder.getValueAt( "test", 0 ), "0" );
		assertEquals( theBuilder.getValueAt( "test", 1 ), "1" );
		assertEquals( theBuilder.getValueAt( "test", 2 ), "2" );
	}

	@Test
	public void testArray5() {
		final PathMapBuilder<String> theBuilder = new PathMapBuilderImpl<>( String.class );
		theBuilder.put( "/root/test/0", "0" );
		theBuilder.put( "/root/test/1", "1" );
		theBuilder.put( "/root/test/2", "2" );
		assertTrue( theBuilder.isArray( "root", "test" ) );
		assertTrue( theBuilder.hasValueAt( new String[] { "root", "test" }, 0 ) );
		assertTrue( theBuilder.hasValueAt( new String[] { "root", "test" }, 1 ) );
		assertTrue( theBuilder.hasValueAt( new String[] { "root", "test" }, 2 ) );
		assertFalse( theBuilder.hasValueAt( new String[] { "root", "test" }, 3 ) );
		assertEquals( theBuilder.getValueAt( new String[] { "root", "test" }, 0 ), "0" );
		assertEquals( theBuilder.getValueAt( new String[] { "root", "test" }, 1 ), "1" );
		assertEquals( theBuilder.getValueAt( new String[] { "root", "test" }, 2 ), "2" );
	}
}
