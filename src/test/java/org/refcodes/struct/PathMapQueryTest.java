// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.PathMap.PathMapBuilder;

/**
 * The Class PathMapTest.
 */
public class PathMapQueryTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testQuery1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.query( "/dog/*" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( "Snowy", theQueryMap.get( "/dog/firstName" ) );
		assertEquals( "Milou", theQueryMap.get( "/dog/lastName" ) );
		assertEquals( 2, theQueryMap.size() );
	}

	@Test
	public void testQuery2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.query( "**firstName**" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( "Snowy", theQueryMap.get( "/cat/dog/firstName" ) );
		assertEquals( "Nolan", theQueryMap.get( "/cat/firstName" ) );
		assertEquals( "Snowy", theQueryMap.get( "/dog/firstName" ) );
		assertEquals( "Nolan", theQueryMap.get( "/firstName" ) );
		assertEquals( 4, theQueryMap.size() );
	}

	@Test
	public void testQuery3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.query( "/???/*/clone/1" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( "1", theQueryMap.get( "/cat/properties/clone/1" ) );
		assertEquals( "1", theQueryMap.get( "/dog/properties/clone/1" ) );
		assertEquals( 2, theQueryMap.size() );
	}

	@Test
	public void testQuery4() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.query( "/???/*/cl??e/1" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( "1", theQueryMap.get( "/cat/properties/clone/1" ) );
		assertEquals( "1", theQueryMap.get( "/dog/properties/clone/1" ) );
		assertEquals( 2, theQueryMap.size() );
	}

	@Test
	public void testQuery5() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.query( "/???/*/cl??e?/1" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( 0, theQueryMap.size() );
	}

	@Test
	public void testQuery6() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.query( "/??/*/clone/1" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( 0, theQueryMap.size() );
	}

	@Test
	public void testQueryBetween() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.queryBetween( "/dog", "/*", "/to" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( "Snowy", theQueryMap.get( "/to/firstName" ) );
		assertEquals( "Milou", theQueryMap.get( "/to/lastName" ) );
		assertEquals( 2, theQueryMap.size() );
	}

	@Test
	public void testQueryFrom() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.queryFrom( "/*", "/dog" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( "Snowy", theQueryMap.get( "/firstName" ) );
		assertEquals( "Milou", theQueryMap.get( "/lastName" ) );
		assertEquals( 2, theQueryMap.size() );
	}

	@Test
	public void testQueryTo() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		for ( int i = 0; i < PathMapTestFixures.QUERY.length; i++ ) {
			thePathMap.put( PathMapTestFixures.QUERY[i][0], PathMapTestFixures.QUERY[i][1] );
		}
		final PathMap<String> theQueryMap = thePathMap.queryTo( "/dog/*", "/to" );
		final List<String> theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theQueryMap.get( eKey ) );
			}
		}
		assertEquals( "Snowy", theQueryMap.get( "/to/dog/firstName" ) );
		assertEquals( "Milou", theQueryMap.get( "/to/dog/lastName" ) );
		assertEquals( 2, theQueryMap.size() );
	}
}
