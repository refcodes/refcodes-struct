package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * The Class PropertyTest.
 */
public class PropertyTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testKey() {
		final String theKey = "key";
		final String theTupel = theKey + "=";
		final Property theProperty = new PropertyImpl( theTupel );
		assertEquals( theKey, theProperty.getKey() );
		assertEquals( "", theProperty.getValue() );
	}

	@Test
	public void testTupel1() {
		final String theKey = "key";
		final String theValue = "value";
		final String theTupel = theKey + "=" + theValue;
		final Property theProperty = new PropertyImpl( theTupel );
		assertEquals( theKey, theProperty.getKey() );
		assertEquals( theValue, theProperty.getValue() );
	}

	@Test
	public void testTupel2() {
		final String theKey = "key";
		final String theValue = "val=ue";
		final String theTupel = theKey + "=" + theValue;
		final Property theProperty = new PropertyImpl( theTupel );
		assertEquals( theKey, theProperty.getKey() );
		assertEquals( theValue, theProperty.getValue() );
	}

	@Test
	public void testValue() {
		final String theValue = "value";
		final String theTupel = "=" + theValue;
		final Property theProperty = new PropertyImpl( theTupel );
		assertEquals( "", theProperty.getKey() );
		assertEquals( theValue, theProperty.getValue() );
	}
}
