// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * @author steiner
 */
public class TypeModeTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////
	// @formatter:off
	private static final Class<?>[] ALL_TYPES = {
		Boolean.TYPE,								                //  1
		Byte.TYPE,									                //  2
		Character.TYPE,								                //  3
		Short.TYPE,									                //  4
		Integer.TYPE,								                //  5
		Long.TYPE,									                //  6
		Float.TYPE,									                //  7
		Double.TYPE,								                //  8
		Boolean.class,                                              //  9
		Byte.class,                                                 // 10
		Character.class,                                            // 11
		Short.class,                                                // 12
		Integer.class,                                              // 13
		Long.class,                                                 // 14
		Float.class,                                                // 15
		Double.class,                                               // 16
		String.class,                                               // 17
		Enum.class,                                                 // 18
		Class.class,                                                // 19
		PrimitiveArrayType.BOOLEAN.getType(),                       // 20
		PrimitiveArrayType.BYTE.getType(),                          // 21
		PrimitiveArrayType.CHAR.getType(),                          // 22
		PrimitiveArrayType.SHORT.getType(),                         // 23
		PrimitiveArrayType.INT.getType(),                           // 24
		PrimitiveArrayType.LONG.getType(),                          // 25
		PrimitiveArrayType.FLOAT.getType(),                         // 26
		PrimitiveArrayType.DOUBLE.getType(),                        // 27
		Boolean[].class,                                            // 28
		Byte[].class,                                               // 29
		Character[].class,                                          // 30
		Short[].class,                                              // 31
		Integer[].class,                                            // 32
		Long[].class,                                               // 33
		Float[].class,                                              // 34
		Double[].class,                                             // 35
		String[].class,                                             // 36
		Enum[].class,                                               // 37
		Class[].class,                                              // 38
	};
	
	private static final boolean [] PRIMITIVE_TYPES = {
		true,  // Boolean.TYPE,								        //  1
		true,  // Byte.TYPE,									    //  2
		true,  // Character.TYPE,								    //  3
		true,  // Short.TYPE,									    //  4
		true,  // Integer.TYPE,								        //  5
		true,  // Long.TYPE,									    //  6
		true,  // Float.TYPE,									    //  7
		true,  // Double.TYPE,								  	    //  8
		false, // Boolean.class,                                    //  9
		false, // Byte.class,                                       // 10
		false, // Character.class,                                  // 11
		false, // Short.class,                                      // 12
		false, // Integer.class,                                    // 13
		false, // Long.class,                                       // 14
		false, // Float.class,                                      // 15
		false, // Double.class,                                     // 16
		false, // String.class,                                     // 17
		false, // Enum.class,                                       // 18
		false, // Class.class,                                      // 19
		false, // PrimitiveArrayType.BOOLEAN_ARRAY_TYPE,            // 20
		false, // PrimitiveArrayType.BYTE_ARRAY_TYPE,               // 21
		false, // PrimitiveArrayType.CHAR_ARRAY_TYPE,               // 22
		false, // PrimitiveArrayType.SHORT_ARRAY_TYPE,              // 23
		false, // PrimitiveArrayType.INT_ARRAY_TYPE,                // 24
		false, // PrimitiveArrayType.LONG_ARRAY_TYPE,               // 25
		false, // PrimitiveArrayType.FLOAT_ARRAY_TYPE,              // 26
		false, // PrimitiveArrayType.DOUBLE_ARRAY_TYPE,             // 27
		false, // Boolean[].class,                                  // 28
		false, // Byte[].class,                                     // 29
		false, // Character[].class,                                // 30
		false, // Short[].class,                                    // 31
		false, // Integer[].class,                                  // 32
		false, // Long[].class,                                     // 33
		false, // Float[].class,                                    // 34
		false, // Double[].class,                                   // 35
		false, // String[].class,                                   // 36
		false, // Enum[].class,                                     // 37
		false  // Class[].class,                                    // 38
	};
	
	private static final boolean [] PRIMITIVE_ARRAY_TYPES = {
		false, // Boolean.TYPE,								        //  1
		false, // Byte.TYPE,									    //  2
		false, // Character.TYPE,								    //  3
		false, // Short.TYPE,									    //  4
		false, // Integer.TYPE,								        //  5
		false, // Long.TYPE,									    //  6
		false, // Float.TYPE,									    //  7
		false, // Double.TYPE,								  	    //  8
		false, // Boolean.class,                                    //  9
		false, // Byte.class,                                       // 10
		false, // Character.class,                                  // 11
		false, // Short.class,                                      // 12
		false, // Integer.class,                                    // 13
		false, // Long.class,                                       // 14
		false, // Float.class,                                      // 15
		false, // Double.class,                                     // 16
		false, // String.class,                                     // 17
		false, // Enum.class,                                       // 18
		false, // Class.class,                                      // 19
		true,  // PrimitiveArrayType.BOOLEAN_ARRAY_TYPE,            // 20
		true,  // PrimitiveArrayType.BYTE_ARRAY_TYPE,               // 21
		true,  // PrimitiveArrayType.CHAR_ARRAY_TYPE,               // 22
		true,  // PrimitiveArrayType.SHORT_ARRAY_TYPE,              // 23
		true,  // PrimitiveArrayType.INT_ARRAY_TYPE,                // 24
		true,  // PrimitiveArrayType.LONG_ARRAY_TYPE,               // 25
		true,  // PrimitiveArrayType.FLOAT_ARRAY_TYPE,              // 26
		true,  // PrimitiveArrayType.DOUBLE_ARRAY_TYPE,             // 27
		false, // Boolean[].class,                                  // 28
		false, // Byte[].class,                                     // 29
		false, // Character[].class,                                // 30
		false, // Short[].class,                                    // 31
		false, // Integer[].class,                                  // 32
		false, // Long[].class,                                     // 33
		false, // Float[].class,                                    // 34
		false, // Double[].class,                                   // 35
		false, // String[].class,                                   // 36
		false, // Enum[].class,                                     // 37
		false  // Class[].class,                                    // 38
	};
	
	private static final boolean [] WRAPPER_TYPES = {
		false, // Boolean.TYPE,								        //  1
		false, // Byte.TYPE,									    //  2
		false, // Character.TYPE,								    //  3
		false, // Short.TYPE,									    //  4
		false, // Integer.TYPE,								        //  5
		false, // Long.TYPE,									    //  6
		false, // Float.TYPE,									    //  7
		false, // Double.TYPE,								  	    //  8
		true,  // Boolean.class,                                    //  9
		true,  // Byte.class,                                       // 10
		true,  // Character.class,                                  // 11
		true,  // Short.class,                                      // 12
		true,  // Integer.class,                                    // 13
		true,  // Long.class,                                       // 14
		true,  // Float.class,                                      // 15
		true,  // Double.class,                                     // 16
		false, // String.class,                                     // 17
		false, // Enum.class,                                       // 18
		false, // Class.class,                                      // 19
		false, // PrimitiveArrayType.BOOLEAN_ARRAY_TYPE,            // 20
		false, // PrimitiveArrayType.BYTE_ARRAY_TYPE,               // 21
		false, // PrimitiveArrayType.CHAR_ARRAY_TYPE,               // 22
		false, // PrimitiveArrayType.SHORT_ARRAY_TYPE,              // 23
		false, // PrimitiveArrayType.INT_ARRAY_TYPE,                // 24
		false, // PrimitiveArrayType.LONG_ARRAY_TYPE,               // 25
		false, // PrimitiveArrayType.FLOAT_ARRAY_TYPE,              // 26
		false, // PrimitiveArrayType.DOUBLE_ARRAY_TYPE,             // 27
		false, // Boolean[].class,                                  // 28
		false, // Byte[].class,                                     // 29
		false, // Character[].class,                                // 30
		false, // Short[].class,                                    // 31
		false, // Integer[].class,                                  // 32
		false, // Long[].class,                                     // 33
		false, // Float[].class,                                    // 34
		false, // Double[].class,                                   // 35
		false, // String[].class,                                   // 36
		false, // Enum[].class,                                     // 37
		false  // Class[].class,                                    // 38
	};
	
	private static final boolean [] WRAPPER_ARRAY_TYPES = {
		false, // Boolean.TYPE,								        //  1
		false, // Byte.TYPE,									    //  2
		false, // Character.TYPE,								    //  3
		false, // Short.TYPE,									    //  4
		false, // Integer.TYPE,								        //  5
		false, // Long.TYPE,									    //  6
		false, // Float.TYPE,									    //  7
		false, // Double.TYPE,								  	    //  8
		false, // Boolean.class,                                    //  9
		false, // Byte.class,                                       // 10
		false, // Character.class,                                  // 11
		false, // Short.class,                                      // 12
		false, // Integer.class,                                    // 13
		false, // Long.class,                                       // 14
		false, // Float.class,                                      // 15
		false, // Double.class,                                     // 16
		false, // String.class,                                     // 17
		false, // Enum.class,                                       // 18
		false, // Class.class,                                      // 19
		false, // PrimitiveArrayType.BOOLEAN_ARRAY_TYPE,            // 20
		false, // PrimitiveArrayType.BYTE_ARRAY_TYPE,               // 21
		false, // PrimitiveArrayType.CHAR_ARRAY_TYPE,               // 22
		false, // PrimitiveArrayType.SHORT_ARRAY_TYPE,              // 23
		false, // PrimitiveArrayType.INT_ARRAY_TYPE,                // 24
		false, // PrimitiveArrayType.LONG_ARRAY_TYPE,               // 25
		false, // PrimitiveArrayType.FLOAT_ARRAY_TYPE,              // 26
		false, // PrimitiveArrayType.DOUBLE_ARRAY_TYPE,             // 27
		true,  // Boolean[].class,                                  // 28
		true,  // Byte[].class,                                     // 29
		true,  // Character[].class,                                // 30
		true,  // Short[].class,                                    // 31
		true,  // Integer[].class,                                  // 32
		true,  // Long[].class,                                     // 33
		true,  // Float[].class,                                    // 34
		true,  // Double[].class,                                   // 35
		false, // String[].class,                                   // 36
		false, // Enum[].class,                                     // 37
		false  // Class[].class,                                    // 38
	};
	
	private static final boolean [] SIMPLE_TYPES = {
		true,  // Boolean.TYPE,								        //  1
		true,  // Byte.TYPE,									    //  2
		true,  // Character.TYPE,								    //  3
		true,  // Short.TYPE,									    //  4
		true,  // Integer.TYPE,								        //  5
		true,  // Long.TYPE,									    //  6
		true,  // Float.TYPE,									    //  7
		true,  // Double.TYPE,								  	    //  8
		true,  // Boolean.class,                                    //  9
		true,  // Byte.class,                                       // 10
		true,  // Character.class,                                  // 11
		true,  // Short.class,                                      // 12
		true,  // Integer.class,                                    // 13
		true,  // Long.class,                                       // 14
		true,  // Float.class,                                      // 15
		true,  // Double.class,                                     // 16
		true,  // String.class,                                     // 17
		true,  // Enum.class,                                       // 18
		true,  // Class.class,                                      // 19
		false, // PrimitiveArrayType.BOOLEAN_ARRAY_TYPE,            // 20
		false, // PrimitiveArrayType.BYTE_ARRAY_TYPE,               // 21
		false, // PrimitiveArrayType.CHAR_ARRAY_TYPE,               // 22
		false, // PrimitiveArrayType.SHORT_ARRAY_TYPE,              // 23
		false, // PrimitiveArrayType.INT_ARRAY_TYPE,                // 24
		false, // PrimitiveArrayType.LONG_ARRAY_TYPE,               // 25
		false, // PrimitiveArrayType.FLOAT_ARRAY_TYPE,              // 26
		false, // PrimitiveArrayType.DOUBLE_ARRAY_TYPE,             // 27
		false, // Boolean[].class,                                  // 28
		false, // Byte[].class,                                     // 29
		false, // Character[].class,                                // 30
		false, // Short[].class,                                    // 31
		false, // Integer[].class,                                  // 32
		false, // Long[].class,                                     // 33
		false, // Float[].class,                                    // 34
		false, // Double[].class,                                   // 35
		false, // String[].class,                                   // 36
		false, // Enum[].class,                                     // 37
		false  // Class[].class,                                    // 38
	};
	
	private static final boolean [] SIMPLE_ARRAY_TYPES = {
		false, // Boolean.TYPE,								        //  1
		false, // Byte.TYPE,									    //  2
		false, // Character.TYPE,								    //  3
		false, // Short.TYPE,									    //  4
		false, // Integer.TYPE,								        //  5
		false, // Long.TYPE,									    //  6
		false, // Float.TYPE,									    //  7
		false, // Double.TYPE,								  	    //  8
		false, // Boolean.class,                                    //  9
		false, // Byte.class,                                       // 10
		false, // Character.class,                                  // 11
		false, // Short.class,                                      // 12
		false, // Integer.class,                                    // 13
		false, // Long.class,                                       // 14
		false, // Float.class,                                      // 15
		false, // Double.class,                                     // 16
		false, // String.class,                                     // 17
		false, // Enum.class,                                       // 18
		false, // Class.class,                                      // 19
		true,  // PrimitiveArrayType.BOOLEAN_ARRAY_TYPE,            // 20
		true,  // PrimitiveArrayType.BYTE_ARRAY_TYPE,               // 21
		true,  // PrimitiveArrayType.CHAR_ARRAY_TYPE,               // 22
		true,  // PrimitiveArrayType.SHORT_ARRAY_TYPE,              // 23
		true,  // PrimitiveArrayType.INT_ARRAY_TYPE,                // 24
		true,  // PrimitiveArrayType.LONG_ARRAY_TYPE,               // 25
		true,  // PrimitiveArrayType.FLOAT_ARRAY_TYPE,              // 26
		true,  // PrimitiveArrayType.DOUBLE_ARRAY_TYPE,             // 27
		true,  // Boolean[].class,                                  // 28
		true,  // Byte[].class,                                     // 29
		true,  // Character[].class,                                // 30
		true,  // Short[].class,                                    // 31
		true,  // Integer[].class,                                  // 32
		true,  // Long[].class,                                     // 33
		true,  // Float[].class,                                    // 34
		true,  // Double[].class,                                   // 35
		true,  // String[].class,                                   // 36
		true,  // Enum[].class,                                     // 37
		true   // Class[].class,                                    // 38
	};
	
	private static final Class<?>[] TO_PRIMITIVE_TYPES = {
		Boolean.TYPE,								                //  1
		Byte.TYPE,									                //  2
		Character.TYPE,								                //  3
		Short.TYPE,									                //  4
		Integer.TYPE,								                //  5
		Long.TYPE,									                //  6
		Float.TYPE,									                //  7
		Double.TYPE,								                //  8
		Boolean.TYPE,                                               //  9
		Byte.TYPE,                                                  // 10
		Character.TYPE,                                             // 11
		Short.TYPE,                                                 // 12
		Integer.TYPE,                                               // 13
		Long.TYPE,                                                  // 14
		Float.TYPE,                                                 // 15
		Double.TYPE,                                                // 16
		null,                                                       // 17
		null,                                                       // 18
		null,                                                       // 19
		PrimitiveArrayType.BOOLEAN.getType(),                       // 20
		PrimitiveArrayType.BYTE.getType(),                          // 21
		PrimitiveArrayType.CHAR.getType(),                          // 22
		PrimitiveArrayType.SHORT.getType(),                         // 23
		PrimitiveArrayType.INT.getType(),                           // 24
		PrimitiveArrayType.LONG.getType(),                          // 25
		PrimitiveArrayType.FLOAT.getType(),                         // 26
		PrimitiveArrayType.DOUBLE.getType(),                        // 27
		PrimitiveArrayType.BOOLEAN.getType(),                       // 28
		PrimitiveArrayType.BYTE.getType(),                          // 29
		PrimitiveArrayType.CHAR.getType(),                          // 30
		PrimitiveArrayType.SHORT.getType(),                         // 31
		PrimitiveArrayType.INT.getType(),                           // 32
		PrimitiveArrayType.LONG.getType(),                          // 33
		PrimitiveArrayType.FLOAT.getType(),                         // 34
		PrimitiveArrayType.DOUBLE.getType(),                        // 35
		null,                                                       // 36
		null,                                                       // 37
		null,                                                       // 38
	};
	
	private static final Class<?>[] TO_WRAPPER_TYPES = {
		Boolean.class,  							                //  1
		Byte.class,     							                //  2
		Character.class,							                //  3
		Short.class,    							                //  4
		Integer.class,  							                //  5
		Long.class,     							                //  6
		Float.class,    							                //  7
		Double.class,   							                //  8
		Boolean.class,                                              //  9
		Byte.class,                                                 // 10
		Character.class,                                            // 11
		Short.class,                                                // 12
		Integer.class,                                              // 13
		Long.class,                                                 // 14
		Float.class,                                                // 15
		Double.class,                                               // 16
		null,                                                       // 17
		null,                                                       // 18
		null,                                                       // 19
		Boolean[].class,                                            // 20
		Byte[].class,                                               // 21
		Character[].class,                                          // 22
		Short[].class,                                              // 23
		Integer[].class,                                            // 24
		Long[].class,                                               // 25
		Float[].class,                                              // 26
		Double[].class,                                             // 27
		Boolean[].class,                                            // 28
		Byte[].class,                                               // 29
		Character[].class,                                          // 30
		Short[].class,                                              // 31
		Integer[].class,                                            // 32
		Long[].class,                                               // 33
		Float[].class,                                              // 34
		Double[].class,                                             // 35
		null,                                                       // 36
		null,                                                       // 37
		null,                                                       // 38
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testTypeMode() {
		for ( int i = 0; i < ALL_TYPES.length; i++ ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "]> Testing <" + ALL_TYPES[i] + "> ..." );
			}
			assertEquals( PRIMITIVE_TYPES[i], SimpleType.isPrimitiveType( ALL_TYPES[i] ) );
			assertEquals( WRAPPER_TYPES[i], SimpleType.isWrapperType( ALL_TYPES[i] ) );
			assertEquals( SIMPLE_TYPES[i], SimpleType.isSimpleType( ALL_TYPES[i] ) );
			assertEquals( PRIMITIVE_ARRAY_TYPES[i], SimpleType.isPrimitiveArrayType( ALL_TYPES[i] ) );
			assertEquals( WRAPPER_ARRAY_TYPES[i], SimpleType.isWrapperArrayType( ALL_TYPES[i] ) );
			assertEquals( SIMPLE_ARRAY_TYPES[i], SimpleType.isSimpleArrayType( ALL_TYPES[i] ) );
			assertEquals( TO_PRIMITIVE_TYPES[i], SimpleType.toPrimitiveType( ALL_TYPES[i], true ) );
			assertEquals( TO_WRAPPER_TYPES[i], SimpleType.toWrapperType( ALL_TYPES[i], true ) );
		}
	}

	@Disabled("Edge case for debugging")
	@Test
	public void testEdgeCase() {
		assertEquals( TO_PRIMITIVE_TYPES[19], SimpleType.toPrimitiveType( ALL_TYPES[19], true ) );
	}
}
