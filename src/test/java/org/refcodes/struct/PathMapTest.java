// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.PathMap.PathMapBuilder;
import org.refcodes.struct.PathMapTestFixures.Person;

/**
 * The Class PathMapTest.
 */
public class PathMapTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	public static enum Names {
		ANTON, NOLAN, STEVE
	};

	@Test
	public void testEnumeration() {
		final PathMapBuilder<String> theProperties = new CanonicalMapBuilderImpl();
		theProperties.put( "/enum", Names.ANTON.name() );
		final Enum<?> theEnum = theProperties.toType( "/enum", Names.class );
		assertEquals( Names.ANTON, theEnum );
	}

	@Test
	public void testUUID() {
		final PathMapBuilder<String> theProperties = new CanonicalMapBuilderImpl();
		final UUID theUUID = UUID.randomUUID();
		theProperties.insertTo( "/uuid", theUUID );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theProperties.toPrintable() );
		}
		final UUID theNewUUID = theProperties.toType( "/uuid", UUID.class );
		assertEquals( theUUID, theNewUUID );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theUUID + " --> " + theNewUUID );
		}
	}

	@Test
	public void testEdgeCase() {
		final PathMapBuilder<String> theProperties = new CanonicalMapBuilderImpl();
		EdgeCase theEdgeCase = new EdgeCase( true );
		theProperties.insertTo( "/edge/case", theEdgeCase );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theProperties.sortedKeys() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		assertEquals( theProperties.size(), 1 );
		assertEquals( "true", theProperties.get( "/edge/case/group" ) );
		theEdgeCase = theProperties.toType( "/edge/case", EdgeCase.class );
		assertTrue( theEdgeCase.isGroup );
		theProperties.put( "/edge/case/group", "false" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theProperties.sortedKeys() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		theEdgeCase = theProperties.toType( "/edge/case", EdgeCase.class );
		assertFalse( theEdgeCase.isGroup );
	}

	@Test
	public void testDate() throws ParseException {
		final PathMapBuilder<String> theProperties = new CanonicalMapBuilderImpl();
		theProperties.put( "date1", "2021-02-03T12:18:33.2912547+01:00" );
		theProperties.put( "date2", "2020-02-03T11:18:33.2912547Z" );
		final Date theDate1 = theProperties.toType( "/date1", Date.class );
		final Date theDate2 = theProperties.toType( "/date2", Date.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "date1 = " + theDate1.toString() );
			System.out.println( "date2 = " + theDate2.toString() );
		}
		theProperties.insertTo( "after/date1", theDate1 );
		theProperties.insertTo( "after/date2", theDate2 );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theProperties.sortedKeys() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		final Date theAfterDate1 = theProperties.toType( "/after/date1", Date.class );
		final Date theAfterDate2 = theProperties.toType( "/after/date2", Date.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "after date1 = " + theAfterDate1.toString() );
			System.out.println( "after date2 = " + theAfterDate2.toString() );
		}
		assertEquals( theDate1.getTime(), theAfterDate1.getTime() );
		assertEquals( theDate2.getTime(), theAfterDate2.getTime() );
	}

	@Test
	public void testDirs1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theDirs = thePathMap.dirs( "/dog/properties/counter" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eDir : theDirs ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eDir );
				}
			}
		}
		assertEquals( 0, theDirs.size() );
	}

	@Test
	public void testDirs2() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theDirs = thePathMap.dirs( "/dog" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eDir : theDirs ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eDir );
				}
			}
		}
		assertEquals( 1, theDirs.size() );
	}

	@Test
	public void testDirs3() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theDirs = thePathMap.dirs( "/" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eDir : theDirs ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eDir );
				}
			}
		}
		assertEquals( 1, theDirs.size() );
	}

	@Test
	public void testDirs4() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theDirs = thePathMap.dirs( "" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eDir : theDirs ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eDir );
				}
			}
		}
		assertEquals( 1, theDirs.size() );
	}

	@Test
	public void testEntries() {
		final Person thePerson = new Person( "Nolan", "Bushnell", "Snowy", "Milou" );
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( thePerson );
		boolean hasEntry = thePathMap.isChild( "dogz" );
		assertFalse( hasEntry );
		hasEntry = thePathMap.isChild( "dog" );
		assertTrue( hasEntry );
		hasEntry = thePathMap.isChild( "firstName" );
		assertTrue( hasEntry );
		hasEntry = thePathMap.isChild( "surname" );
		assertFalse( hasEntry );
	}

	@Test
	public void testEntries1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theEntries = thePathMap.children( "/" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eRecord : theEntries ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eRecord );
				}
			}
		}
		assertEquals( 3, theEntries.size() );
	}

	@Test
	public void testEntries2() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theEntries = thePathMap.children( "/dog/properties" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eRecord : theEntries ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eRecord );
				}
			}
		}
		assertEquals( 4, theEntries.size() );
	}

	@Test
	public void testInsert1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		thePathMap.insertBetween( "/cat", thePathMap, "/dog" );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.INSERT_1[i][0] );
			assertEquals( eValue, PathMapTestFixures.INSERT_1[i][1] );
			i++;
		}
		assertEquals( PathMapTestFixures.INSERT_1.length, theKeys.size() );
	}

	@Test
	public void testInsertFrom1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		thePathMap.insertFrom( thePathMap, "/dog/properties" );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.INSERT_FROM_1[i][0] );
			assertEquals( eValue, PathMapTestFixures.INSERT_FROM_1[i][1] );
			i++;
		}
		assertEquals( PathMapTestFixures.INSERT_FROM_1.length, theKeys.size() );
	}

	@Test
	public void testInsertTo1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		thePathMap.insertTo( "/cat", thePathMap );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.INSERT_TO_1[i][0] );
			assertEquals( eValue, PathMapTestFixures.INSERT_TO_1[i][1] );
			i++;
		}
		assertEquals( PathMapTestFixures.INSERT_TO_1.length, theKeys.size() );
	}

	@Test
	public void testPathMap1() {
		final Person thePerson = new Person( "Nolan", "Bushnell", "Snowy", "Milou" );
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( thePerson );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.FLAT_MAP_1[i][0] );
			assertEquals( eValue, PathMapTestFixures.FLAT_MAP_1[i][1] );
			i++;
		}
	}

	@Test
	public void testPathMap2() {
		final Person thePerson = new Person( "Nolan", "Bushnell", "Snowy", "Milou" );
		PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( thePerson );
		final Map<String, ?> theMap = thePathMap.toMap();
		thePathMap = new CanonicalMapBuilderImpl( theMap );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.FLAT_MAP_1[i][0] );
			assertEquals( eValue, PathMapTestFixures.FLAT_MAP_1[i][1] );
			i++;
		}
	}

	@Test
	public void testPathMap3() {
		final Person thePerson = new Person( "Nolan", "Bushnell", "Snowy", "Milou" );
		PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( thePerson );
		final Map<String, ?> theMap = thePathMap.toMap( "dog/" );
		thePathMap = new CanonicalMapBuilderImpl( theMap );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.FLAT_MAP_2[i][0] );
			assertEquals( eValue, PathMapTestFixures.FLAT_MAP_2[i][1] );
			i++;
		}
	}

	@Test
	public void testPathMap4() {
		final Person thePerson = new Person( "Nolan", "Bushnell", "Snowy", "Milou" );
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl();
		thePathMap.insertFrom( thePerson, "dog" );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.FLAT_MAP_2[i][0] );
			assertEquals( eValue, PathMapTestFixures.FLAT_MAP_2[i][1] );
			i++;
		}
	}

	@Test
	public void testPathMap5() {
		final PathMapBuilder<String> theBuilder = new CanonicalMapBuilderImpl();
		theBuilder.put( "/file/separator", "\\" );
		theBuilder.put( "/file/encoding", "UTF-8" );
		theBuilder.put( "/file/encoding/pkg", "sun.io" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theBuilder.keySet() ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eKey + " := " + theBuilder.get( eKey ) );
				}
			}
		}
		// ----------------------------------------------------
		// The map causes the implicit array ("/file/encoding")
		// to be declared explicitly as array:
		// ----------------------------------------------------
		final Map<String, ?> theMap = theBuilder.toMap();
		// ----------------------------------------------------
		final PathMapBuilder<String> theReverse = new CanonicalMapBuilderImpl( theMap );
		if ( IS_LOG_TESTS_ENABLED ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
			for ( String eKey : theReverse.keySet() ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eKey + " := " + theReverse.get( eKey ) );
				}
			}
		}
		assertEquals( theBuilder.get( "/file/separator" ), theReverse.get( "/file/separator" ) );
		assertEquals( theBuilder.get( "/file/encoding" ), theReverse.get( "/file/encoding" ) );
		assertEquals( theBuilder.get( "/file/encoding/pkg" ), theReverse.get( "/file/encoding/pkg" ) );
	}

	@Test
	public void testRecords1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theRecords = thePathMap.leaves( "/dog/properties/counter" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eRecord : theRecords ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eRecord );
				}
			}
		}
		assertEquals( 5, theRecords.size() );
	}

	@Test
	public void testRecords2() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theRecords = thePathMap.leaves( "/" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eRecord : theRecords ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eRecord );
				}
			}
		}
		assertEquals( 2, theRecords.size() );
	}

	@Test
	public void testRecords3() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final Set<String> theRecords = thePathMap.leaves( "" );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eRecord : theRecords ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eRecord );
				}
			}
		}
		assertEquals( 2, theRecords.size() );
	}

	@Test
	public void testRetrieve1() {
		PathMap<String> thePathMap = new CanonicalMapImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final String theToPath = "/new";
		thePathMap = thePathMap.retrieveBetween( "/dog/properties", theToPath );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, theToPath + PathMapTestFixures.FLAT_MAP_3[i][0] );
			assertEquals( eValue, PathMapTestFixures.FLAT_MAP_3[i][1] );
			i++;
		}
		assertEquals( PathMapTestFixures.FLAT_MAP_3.length, theKeys.size() );
	}

	@Test
	public void testRetrieveFrom1() {
		PathMap<String> thePathMap = new CanonicalMapImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		thePathMap = thePathMap.retrieveFrom( "/dog/properties" );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, PathMapTestFixures.FLAT_MAP_3[i][0] );
			assertEquals( eValue, PathMapTestFixures.FLAT_MAP_3[i][1] );
			i++;
		}
		assertEquals( PathMapTestFixures.FLAT_MAP_3.length, theKeys.size() );
	}

	@Test
	public void testRetrieveTo1() {
		PathMap<String> thePathMap = new CanonicalMapImpl( new Person( "Nolan", "Bushnell", "Snowy", "Milou" ) );
		final String theToPath = "/new";
		thePathMap = thePathMap.retrieveTo( theToPath );
		final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
		Collections.sort( theKeys );
		String eValue;
		int i = 0;
		for ( String eKey : theKeys ) {
			eValue = thePathMap.get( eKey );
			if ( IS_LOG_TESTS_ENABLED ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "\"" + eKey + "\", \"" + eValue + "\"" );
				}
			}
			assertEquals( eKey, theToPath + PathMapTestFixures.FLAT_MAP_1[i][0] );
			assertEquals( eValue, PathMapTestFixures.FLAT_MAP_1[i][1] );
			i++;
		}
		assertEquals( PathMapTestFixures.FLAT_MAP_1.length, theKeys.size() );
	}

	@Test
	public void testToDataStructure() {
		final Person thePerson = new Person( "Nolan", "Bushnell", "Snowy", "Milou" );
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl( thePerson );
		final Object theDataStructure = thePathMap.toDataStructure();
		final PathMapBuilder<String> theStructreMap = new CanonicalMapBuilderImpl( theDataStructure );
		for ( String eKey : thePathMap.keySet() ) {
			assertEquals( theStructreMap.get( eKey ), thePathMap.get( eKey ) );
		}
		for ( String eKey : theStructreMap.keySet() ) {
			assertEquals( theStructreMap.get( eKey ), thePathMap.get( eKey ) );
		}
	}

	@Test
	public void testUseCase1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl();
		thePathMap.put( "/level1/level1.1/level1.1.1", "1.1.1" );
		thePathMap.put( new String[] { "/level1", "level1.1", "level1.1.2" }, "1.1.2" );
		thePathMap.put( new String[] { "/level1/level1.1", "/level1.1.3" }, "1.1.3" );
		thePathMap.put( new String[] { "level1/level1.2/level1.2.1" }, "1.2.1" );
		thePathMap.put( new String[] { "/level1/level1.2/level1.2.2/" }, "1.2.2" );
		thePathMap.put( new String[] { "/level1/", "/level1.2/", "/level1.2.3/" }, "1.2.3" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		final PathMapBuilder<String> theFromMap = new CanonicalMapBuilderImpl();
		theFromMap.put( "/level1.3.1", "1.3.1" );
		theFromMap.put( "/level1.3.2/", "1.3.2" );
		theFromMap.put( new String[] { "level1.3.3" }, "1.3.3" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( theFromMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theFromMap.get( eKey ) );
			}
		}
		thePathMap.insertTo( "/level1/level1.3", theFromMap );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		// @formatter:off
		final String[][] theExpected = { 
			{"/level1/level1.1/level1.1.1", "1.1.1"}, 
			{"/level1/level1.1/level1.1.2", "1.1.2"}, 
			{"/level1/level1.1/level1.1.3", "1.1.3"}, 
			{"/level1/level1.2/level1.2.1", "1.2.1"}, 
			{"/level1/level1.2/level1.2.2", "1.2.2"}, 
			{"/level1/level1.2/level1.2.3", "1.2.3"}, 
			{"/level1/level1.3/level1.3.1", "1.3.1"}, 
			{"/level1/level1.3/level1.3.2", "1.3.2"}, 
			{"/level1/level1.3/level1.3.3", "1.3.3"}
		};
		// @formatter:on
		assertEquals( theExpected.length, thePathMap.size() );
		for ( String[] aTheExpected : theExpected ) {
			assertEquals( aTheExpected[1], thePathMap.get( aTheExpected[0] ) );
		}
	}

	@Test
	public void testUseCase2() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl();
		thePathMap.put( "/level1/level1.1/level1.1.1", "1.1.1" );
		thePathMap.put( new String[] { "/level1", "level1.1", "level1.1.2" }, "1.1.2" );
		thePathMap.put( new String[] { "/level1/level1.1", "/level1.1.3" }, "1.1.3" );
		thePathMap.put( new String[] { "level1/level1.2/level1.2.1" }, "1.2.1" );
		thePathMap.put( new String[] { "/level1/level1.2/level1.2.2/" }, "1.2.2" );
		thePathMap.put( new String[] { "/level1/", "/level1.2/", "/level1.2.3/" }, "1.2.3" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		final PathMapBuilder<String> theFromMap = new CanonicalMapBuilderImpl();
		theFromMap.put( "xxx/level1.3.1", "1.3.1" );
		theFromMap.put( "xxx/level1.3.2/", "1.3.2" );
		theFromMap.put( new String[] { "/xxx/", "/level1.3.3/" }, "1.3.3" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( theFromMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + theFromMap.get( eKey ) );
			}
		}
		thePathMap.insertBetween( "/level1/level1.3", theFromMap, "xxx" );
		if ( IS_LOG_TESTS_ENABLED ) {
			final List<String> theKeys = new ArrayList<>( thePathMap.keySet() );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				System.out.println( eKey + " = " + thePathMap.get( eKey ) );
			}
		}
		// @formatter:off
		final String[][] theExpected = { 
			{"/level1/level1.1/level1.1.1", "1.1.1"}, 
			{"/level1/level1.1/level1.1.2", "1.1.2"}, 
			{"/level1/level1.1/level1.1.3", "1.1.3"}, 
			{"/level1/level1.2/level1.2.1", "1.2.1"}, 
			{"/level1/level1.2/level1.2.2", "1.2.2"}, 
			{"/level1/level1.2/level1.2.3", "1.2.3"}, 
			{"/level1/level1.3/level1.3.1", "1.3.1"}, 
			{"/level1/level1.3/level1.3.2", "1.3.2"}, 
			{"/level1/level1.3/level1.3.3", "1.3.3"}
		};
		// @formatter:on
		assertEquals( theExpected.length, thePathMap.size() );
		for ( String[] aTheExpected : theExpected ) {
			assertEquals( aTheExpected[1], thePathMap.get( aTheExpected[0] ) );
		}
	}

	@Test
	public void testOverride1() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl();
		thePathMap.put( "valid", "!!!" );
		thePathMap.put( "enabled", "!!!" );
		thePathMap.put( "verified", "!!!" );
		final TestOverride theValue = thePathMap.toType( TestOverride.class );
		assertTrue( theValue.isVerified() );
		assertTrue( theValue.isEnabled() );
		assertTrue( theValue.isValid() );
	}

	@Test
	public void testOverride2() {
		final PathMapBuilder<String> thePathMap = new CanonicalMapBuilderImpl();
		thePathMap.put( "valid", "!!!" );
		thePathMap.put( "enabled", "???" );
		thePathMap.put( "verified", "!!!" );
		final TestOverride theValue = thePathMap.toType( TestOverride.class );
		assertTrue( theValue.isVerified() );
		assertFalse( theValue.isEnabled() );
		assertTrue( theValue.isValid() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public static class EdgeCase {
		boolean isGroup;

		public EdgeCase() {}

		public EdgeCase( boolean aIsGroup ) {
			isGroup = aIsGroup;
		}

		public boolean isGroup() {
			return isGroup;
		}

		public void setGroup( boolean aIsGroup ) {
			isGroup = aIsGroup;
		}
	}

	public static class TestOverride {
		private boolean _isEnabled = false;
		private boolean _isValid = false;
		private boolean _isVerified = false;

		public boolean isEnabled() {
			return _isEnabled;
		}

		public boolean isValid() {
			return _isValid;
		}

		public boolean isVerified() {
			return _isVerified;
		}

		public void setValid( boolean isValid ) {
			_isValid = isValid;
		}

		public void setEnabled( boolean isEnabled ) {
			_isEnabled = isEnabled;
		}

		public void setVerified( boolean isVerified ) {
			_isVerified = isVerified;
		}

		public void setValid( String isValid ) {
			_isValid = "!!!".equals( isValid );
		}

		public void setEnabled( String isEnabled ) {
			_isEnabled = "!!!".equals( isEnabled );
		}

		public void setVerified( String isVerified ) {
			_isVerified = "!!!".equals( isVerified );
		}
	}
}
