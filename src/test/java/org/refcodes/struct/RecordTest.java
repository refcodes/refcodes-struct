// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.PathMap.PathMapBuilder;

public class RecordTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRecord1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final Person thePerson = new Person( "John", "Romero", 99 );
		thePathMap.insert( thePerson );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		final Person theOther = thePathMap.toType( Person.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		assertEquals( thePerson, theOther );
		assertFalse( thePerson == theOther );
		assertEquals( theOther.firstName, "John" );
		assertEquals( theOther.lastName, "Romero" );
		assertEquals( theOther.level, 99 );
	}

	@Test
	public void testRecord2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final Person thePerson = new Person( "John", "Romero", 99 );
		thePathMap.insert( thePerson );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		thePathMap.put( "/lastName", "Carmack" );
		final Person theOther = thePathMap.toType( Person.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		assertNotEquals( thePerson, theOther );
		assertFalse( thePerson == theOther );
		assertEquals( theOther.firstName, "John" );
		assertEquals( theOther.lastName, "Carmack" );
		assertEquals( theOther.level, 99 );
	}

	@Test
	public void testRecord3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final Family theFamily = new Family( new Person( "she", "mother", 98 ), new Person( "he", "father", 99 ), new Person( "it", "child", 97 ) );
		thePathMap.insert( theFamily );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		final Family theOthers = thePathMap.toType( Family.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theOthers );
		}
		assertEquals( theFamily, theOthers );
		assertFalse( theFamily == theOthers );
	}

	@Test
	public void testRecord4() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final Family theFamily = new Family( new Person( "she", "mother", 98 ), new Person( "he", "father", 99 ), new Person( "it", "child", 97 ) );
		thePathMap.insert( theFamily );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		thePathMap.put( "/mother/firstName", "Roberta" );
		thePathMap.put( "/mother/lastName", "Williams" );
		thePathMap.put( "/children/1/firstName", "it2" );
		thePathMap.put( "/children/1/lastName", "child2" );
		thePathMap.put( "/children/1/level", "2" );
		final Family theOthers = thePathMap.toType( Family.class );
		final Family theExpected = new Family( new Person( "Roberta", "Williams", 98 ), new Person( "he", "father", 99 ), new Person( "it", "child", 97 ), new Person( "it2", "child2", 2 ) );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theExpected );
			System.out.println( theOthers );
		}
		assertEquals( theExpected, theOthers );
		assertNotEquals( theFamily, theOthers );
		assertFalse( theFamily == theOthers );
	}

	@Test
	public void testRecord5() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final Anybody thePerson = new Anybody( "John", "Romero", 99, new Somebody( "Roberta", "Williams", 23 ) );
		thePathMap.insert( thePerson );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		final Anybody theOther = thePathMap.toType( Anybody.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		assertEquals( thePerson, theOther );
		assertFalse( thePerson == theOther );
		assertEquals( theOther.firstName, "John" );
		assertEquals( theOther.lastName, "Romero" );
		assertEquals( theOther.level, 99 );
		assertEquals( theOther.someBody().getFirstName(), "Roberta" );
		assertEquals( theOther.someBody().getLastName(), "Williams" );
		assertEquals( theOther.someBody().getLevel(), 23 );
	}

	@Test
	public void testEdgeCase1() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		final Somebody thePerson = new Somebody( "John", "Romero", 99 );
		thePathMap.insert( thePerson );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		final Somebody theOther = thePathMap.toType( Somebody.class );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : thePathMap.sortedKeys() ) {
				System.out.println( eKey + "=" + thePathMap.get( eKey ) );
			}
		}
		assertEquals( thePerson, theOther );
		assertFalse( thePerson == theOther );
		assertEquals( theOther.getFirstName(), "John" );
		assertEquals( theOther.getLastName(), "Romero" );
		assertEquals( theOther.getLevel(), 99 );
	}

	/**
	 * See {@link TypeUtility#fromRecord(Object, Class)} "special case array".
	 */
	@Test
	public void testEdgeCase2() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "port", "8080" );
		thePathMap.put( "path", "C:\\commands" );
		thePathMap.put( "path/separator", ":" );
		final Config theConfig = thePathMap.toType( Config.class );
		assertEquals( 8080, theConfig.port );
		assertEquals( "C:\\commands", theConfig.path );
	}

	/**
	 * See {@link TypeUtility#fromRecord(Object, Class)} "defaults for missing
	 * values".
	 */
	@Test
	public void testEdgeCase3() {
		final PathMapBuilder<String> thePathMap = new PathMapBuilderImpl<>( String.class );
		thePathMap.put( "path", "C:\\commands" );
		final Config theConfig = thePathMap.toType( Config.class );
		assertEquals( "C:\\commands", theConfig.path );
		assertEquals( 0, theConfig.port );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	static record Config( char sign, int port, String path, Integer anotherPort ) {}

	static record Person( String firstName, String lastName, int level ) {}

	static record Anybody( String firstName, String lastName, int level, Somebody someBody ) {}

	static class Somebody {
		String _firstName;
		String _lastName;
		int _level;

		public Somebody() {}

		public Somebody( String aFirstName, String aLastName, int aLevel ) {
			_firstName = aFirstName;
			_lastName = aLastName;
			_level = aLevel;
		}

		public String getFirstName() {
			return _firstName;
		}

		public void setFirstName( String aFirstName ) {
			_firstName = aFirstName;
		}

		public String getLastName() {
			return _lastName;
		}

		public void setLastName( String aLastName ) {
			_lastName = aLastName;
		}

		public int getLevel() {
			return _level;
		}

		public void setLevel( int aLevel ) {
			_level = aLevel;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ( ( _firstName == null ) ? 0 : _firstName.hashCode() );
			result = prime * result + ( ( _lastName == null ) ? 0 : _lastName.hashCode() );
			result = prime * result + _level;
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Somebody other = (Somebody) obj;
			if ( _firstName == null ) {
				if ( other._firstName != null ) {
					return false;
				}
			}
			else if ( !_firstName.equals( other._firstName ) ) {
				return false;
			}
			if ( _lastName == null ) {
				if ( other._lastName != null ) {
					return false;
				}
			}
			else if ( !_lastName.equals( other._lastName ) ) {
				return false;
			}
			return _level != other._level ? false : true;
		}
	}

	static class Family {
		Person _mother;
		Person _father;
		Person[] _children;

		public Family() {}

		public Family( Person mother, Person father, Person... children ) {
			_mother = mother;
			_father = father;
			_children = children;
		}

		public Person getMother() {
			return _mother;
		}

		public void setMother( Person aMother ) {
			_mother = aMother;
		}

		public Person getFather() {
			return _father;
		}

		public void setFather( Person aFather ) {
			_father = aFather;
		}

		public Person[] getChildren() {
			return _children;
		}

		public void setChildren( Person[] aChildren ) {
			_children = aChildren;
		}

		@Override
		public String toString() {
			return "Family [_mother=" + _mother + ", _father=" + _father + ", _children=" + Arrays.toString( _children ) + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode( _children );
			result = prime * result + ( ( _father == null ) ? 0 : _father.hashCode() );
			result = prime * result + ( ( _mother == null ) ? 0 : _mother.hashCode() );
			return result;
		}

		@Override
		public boolean equals( Object obj ) {
			if ( this == obj ) {
				return true;
			}
			if ( obj == null ) {
				return false;
			}
			if ( getClass() != obj.getClass() ) {
				return false;
			}
			final Family other = (Family) obj;
			if ( !Arrays.equals( _children, other._children ) ) {
				return false;
			}
			if ( _father == null ) {
				if ( other._father != null ) {
					return false;
				}
			}
			else if ( !_father.equals( other._father ) ) {
				return false;
			}
			if ( _mother == null ) {
				if ( other._mother != null ) {
					return false;
				}
			}
			else if ( !_mother.equals( other._mother ) ) {
				return false;
			}
			return true;
		}
	}
}
