// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapTestFixures.Dog;
import org.refcodes.struct.CanonicalMapTestFixures.Person;

public class CanonicalMapTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testDates() {
		final CanonicalMapBuilder theCanonicalMap = new CanonicalMapBuilderImpl();
		final Date theDate = new Date();
		final LocalDate theLocalDate = LocalDate.now();
		final LocalDateTime theLocalDateTime = LocalDateTime.now();
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Date          = " + theDate.toString() );
			System.out.println( "LocalDate     = " + theLocalDate.toString() );
			System.out.println( "LocalDateTime = " + theLocalDateTime.toString() );
		}
		theCanonicalMap.insertTo( "date", theDate );
		theCanonicalMap.insertTo( "localDate", theLocalDate );
		theCanonicalMap.insertTo( "localDateTime", theLocalDateTime );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theCanonicalMap.sortedKeys() ) {
				System.out.println( eKey + "=" + theCanonicalMap.get( eKey ) );
			}
		}
		final Date theDateResult = theCanonicalMap.toType( Date.class, "date" );
		final LocalDate theLocalDateResult = theCanonicalMap.toType( LocalDate.class, "localDate" );
		final LocalDateTime theLocalDateTimeResult = theCanonicalMap.toType( LocalDateTime.class, "localDateTime" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Date result          = " + theDateResult.toString() );
			System.out.println( "LocalDate result     = " + theLocalDateResult.toString() );
			System.out.println( "LocalDateTime result = " + theLocalDateTimeResult.toString() );
		}
		assertEquals( theDate, theDateResult );
		assertEquals( theLocalDate, theLocalDateResult );
		assertEquals( theLocalDateTime, theLocalDateTimeResult );
	}

	@Test
	public void testAsArray() {
		final CanonicalMapBuilder theCanonicalMap = new CanonicalMapBuilderImpl();
		String[] theArray = theCanonicalMap.asArray( "array" );
		assertNull( theArray );
		theCanonicalMap.put( "array", "" );
		theArray = theCanonicalMap.asArray( "array" );
		assertNull( theArray );
		theCanonicalMap.put( "array", "1" );
		theArray = theCanonicalMap.asArray( "array" );
		assertEquals( 1, theArray.length );
		assertEquals( "1", theArray[0] );
	}

	@Test
	public void testCanonicalPathMap() {
		final Person thePerson = new Person( "Nolan", "Bushnell", true, 78 );
		thePerson.setDog( new Dog( "Snoopy", "Struppi" ) );
		thePerson.setNumbers( new int[] { 5, 1, 6, 1 } );
		thePerson.getDog().addLike( "Dog food" );
		thePerson.getDog().addLike( "Chocolate" );
		thePerson.getDog().addLike( "Water" );
		thePerson.getDog().addLike( null );
		thePerson.getDog().addLike( "Beer" );
		thePerson.addHobby( "Computing", "HIGH" );
		thePerson.addHobby( "Winter Games", "MEDIUM" );
		thePerson.addHobby( "Chop-Lifter", "HIGH" );
		final CanonicalMapBuilder theCanonicalMap = new CanonicalMapBuilderImpl( thePerson );
		final List<String> theKeys = new ArrayList<>( theCanonicalMap.keySet() );
		Collections.sort( theKeys );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eKey : theKeys ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "{\"" + eKey + "\", \"" + theCanonicalMap.get( eKey ) + "\"}, " );
				}
			}
		}
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println();
		}
		final Person theZombie = theCanonicalMap.toType( Person.class );
		final CanonicalMapBuilder theResult = new CanonicalMapBuilderImpl( theZombie );
		assertEquals( CanonicalMapTestFixures.PROPERTIES.length, theResult.size() );
		for ( String[] eProperty : CanonicalMapTestFixures.PROPERTIES ) {
			assertEquals( eProperty[1], theResult.get( eProperty[0] ) );
		}
	}

	@Test
	public void testToSourceCode() {
		final CanonicalMapBuilder theCanonicalMap = new CanonicalMapBuilderImpl();
		theCanonicalMap.put( "/company/name", "Atari" );
		theCanonicalMap.put( "/company/url", "http://atari.com" );
		theCanonicalMap.put( "/firstName", "Nolan" );
		theCanonicalMap.put( "/lastName", "Bushnell" );
		theCanonicalMap.putArray( "/items", new String[] { "Atari", "Commodore", "Tandy" } );
		if ( IS_LOG_TESTS_ENABLED ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( theCanonicalMap.toSourceCode( "PersonMap", "org.acme.domainclasses" ) );
			}
		}
		assertEquals( "Atari", theCanonicalMap.getValueAt( "items", 0 ) );
		assertEquals( "Commodore", theCanonicalMap.getValueAt( "items", 1 ) );
		assertEquals( "Tandy", theCanonicalMap.getValueAt( "items", 2 ) );
	}
}