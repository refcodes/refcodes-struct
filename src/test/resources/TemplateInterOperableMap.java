// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################
// #########################################################################

// /////////////////////////////////////////////////////////////////////////
// TEMPLATE:
// /////////////////////////////////////////////////////////////////////////
public interface TemplateInterOperableMapBuilder<T> extends InterOperableMapBuilder<T> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPut( Collection<?> aPathElements, T aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPut( Object[] aPathElements, T aValue ) throws NumberFormatException {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPut( Relation<String, T> aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPut( String aKey, T aValue ) {
		put( aKey, aValue );
		return this;
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	default TemplateInterOperableMapBuilder<T> withPut( Object aPath, T aValue ) {
	//		put( toPath( aPath ), aValue );
	//		return this;
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPut( String[] aPathElements, T aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutBoolean( Object aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutBoolean( String aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutBoolean( String[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutByte( Collection<?> aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutByte( Object aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutByte( Object[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutByte( String aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutByte( String[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutChar( Collection<?> aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutChar( Object aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutChar( Object[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutChar( String aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutChar( String[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <C> TemplateInterOperableMapBuilder<T> withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <C> TemplateInterOperableMapBuilder<T> withPutClass( Object aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <C> TemplateInterOperableMapBuilder<T> withPutClass( Object[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <C> TemplateInterOperableMapBuilder<T> withPutClass( String aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <C> TemplateInterOperableMapBuilder<T> withPutClass( String[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutDouble( Collection<?> aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutDouble( Object aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutDouble( Object[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutDouble( String aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutDouble( String[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <E extends Enum<E>> TemplateInterOperableMapBuilder<T> withPutEnum( Collection<?> aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <E extends Enum<E>> TemplateInterOperableMapBuilder<T> withPutEnum( Object aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <E extends Enum<E>> TemplateInterOperableMapBuilder<T> withPutEnum( Object[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <E extends Enum<E>> TemplateInterOperableMapBuilder<T> withPutEnum( String aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default <E extends Enum<E>> TemplateInterOperableMapBuilder<T> withPutEnum( String[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutFloat( Collection<?> aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutFloat( Object aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutFloat( Object[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutFloat( String aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutFloat( String[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutInt( Collection<?> aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutInt( Object aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutInt( Object[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutInt( String aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutInt( String[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutLong( Collection<?> aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutLong( Object aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutLong( Object[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutLong( String aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutLong( String[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutShort( Collection<?> aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutShort( Object aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutShort( Object[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutShort( String aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutShort( String[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutString( Collection<?> aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutString( Object aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutString( Object[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutString( String aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default TemplateInterOperableMapBuilder<T> withPutString( String[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}
}
