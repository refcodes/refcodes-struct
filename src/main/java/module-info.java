module org.refcodes.struct {
	requires org.refcodes.data;
	requires org.refcodes.matcher;
	requires org.refcodes.time;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;

	exports org.refcodes.struct;
}