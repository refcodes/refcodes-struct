// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.SimpleTypeMap.SimpleTypeMapBuilder;

/**
 * An implementation of the {@link SimpleTypeMapBuilder}.
 */
public class SimpleTypeMapBuilderImpl extends PathMapBuilderImpl<Object> implements SimpleTypeMapBuilder {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link SimpleTypeMapBuilder} instance using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public SimpleTypeMapBuilderImpl() {
		super( Object.class );
	}

	/**
	 * Create a {@link SimpleTypeMapBuilder} instance using the provided path
	 * delimiter for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapBuilderImpl( char aDelimiter ) {
		super( aDelimiter, Object.class );
	}

	/**
	 * Create a {@link SimpleTypeMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public SimpleTypeMapBuilderImpl( Object aObj ) {
		super( aObj, Object.class );
	}

	/**
	 * Create a {@link SimpleTypeMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public SimpleTypeMapBuilderImpl( String aToPath, Object aObj ) {
		super( aToPath, aObj, Object.class );
	}

	/**
	 * Creates a {@link SimpleTypeMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public SimpleTypeMapBuilderImpl( Object aObj, String aFromPath ) {
		super( aObj, aFromPath, Object.class );
	}

	/**
	 * Creates a {@link SimpleTypeMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public SimpleTypeMapBuilderImpl( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath, Object.class );
	}

	/**
	 * Creates a {@link SimpleTypeMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapBuilderImpl( Object aObj, char aDelimiter ) {
		super( aObj, aDelimiter, Object.class );
	}

	/**
	 * Creates a {@link SimpleTypeMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapBuilderImpl( String aToPath, Object aObj, char aDelimiter ) {
		super( aToPath, aObj, aDelimiter, Object.class );
	}

	/**
	 * Creates a {@link SimpleTypeMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapBuilderImpl( Object aObj, String aFromPath, char aDelimiter ) {
		super( aObj, aFromPath, aDelimiter, Object.class );
	}

	/**
	 * Create a {@link SimpleTypeMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapBuilderImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter ) {
		super( aToPath, aObj, aFromPath, aDelimiter, Object.class );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMapBuilder retrieveFrom( String aFromPath ) {
		return new SimpleTypeMapBuilderImpl( super.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMapBuilder retrieveTo( String aToPath ) {
		return new SimpleTypeMapBuilderImpl( super.retrieveTo( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMapBuilder withPut( String aKey, Object aValue ) {
		put( aKey, aValue );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PathMap<Object> fromObject( Object aFrom ) {
		return new SimpleTypeMapBuilderImpl( aFrom, getDelimiter() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Object fromInstance( Object aValue ) {
		if ( SimpleType.isSimpleType( aValue.getClass() ) ) {
			return aValue;
		}
		//	if ( Number.class.isAssignableFrom( aValue.getClass() ) || Boolean.class.isAssignableFrom( aValue.getClass() ) ) {
		//		return aValue;
		//	}
		//	else if ( Character.class.isAssignableFrom( aValue.getClass() ) ) {
		//		return aValue;
		//	}
		//	else if ( Enum.class.isAssignableFrom( aValue.getClass() ) ) {
		//		return ((Enum<?>) aValue).name();
		//	}
		//	else if ( Class.class.isAssignableFrom( aValue.getClass() ) ) {
		//		return ((Class<?>) aValue).getName();
		//	}
		//	else if ( String.class.equals( aValue.getClass() ) ) {
		//		return aValue;
		//	}
		return null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

}
