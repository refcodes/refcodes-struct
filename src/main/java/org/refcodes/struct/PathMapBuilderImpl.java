// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.RecordComponent;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.PathMap.PathMapBuilder;

/**
 * The {@link PathMapBuilderImpl} class implements the {@link PathMapBuilder}
 * interface and provides a hook {@link #createBackingMap()} method which may be
 * overridden for providing another {@link Map} type as backing map than the
 * default one.
 *
 * @param <T> The type of the value elements.
 */
public class PathMapBuilderImpl<T> implements PathMapBuilder<T>, Serializable {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_DEBUG_OUTPUT = false;
	private static final String ESCAPE = "\\";
	private static final String SPECIAL_REGEX_CHARS = ".^$*+?()[{\\|";
	private static final String[] BLACKLISTED_PROPERTIES = { "class" };

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private final Map<String, T> _backingMap;
	private final char _delimiter;
	private final Class<T> _type;
	private final char _annotator = ANNOTATOR;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link PathMapBuilder} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( Class<T> aType ) {
		this( Delimiter.PATH.getChar(), aType );
	}

	/**
	 * Create a {@link PathMapBuilder} instance using the provided path
	 * delimiter for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( char aDelimiter, Class<T> aType ) {
		_backingMap = createBackingMap();
		_delimiter = aDelimiter;
		_type = aType;
	}

	/**
	 * Create a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( Object aObj, Class<T> aType ) {
		this( aObj, Delimiter.PATH.getChar(), aType );
	}

	/**
	 * Create a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( String aToPath, Object aObj, Class<T> aType ) {
		this( aToPath, aObj, Delimiter.PATH.getChar(), aType );
	}

	/**
	 * Creates a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( Object aObj, String aFromPath, Class<T> aType ) {
		this( aObj, aFromPath, Delimiter.PATH.getChar(), aType );
	}

	/**
	 * Creates a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( String aToPath, Object aObj, String aFromPath, Class<T> aType ) {
		this( aToPath, aObj, aFromPath, Delimiter.PATH.getChar(), aType );
	}

	/**
	 * Creates a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( Object aObj, char aDelimiter, Class<T> aType ) {
		this( aDelimiter, aType );
		fromValue( getRootPath(), aObj );
	}

	/**
	 * Creates a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( String aToPath, Object aObj, char aDelimiter, Class<T> aType ) {
		this( aDelimiter, aType );
		insertTo( aToPath, aObj );
	}

	/**
	 * Creates a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( Object aObj, String aFromPath, char aDelimiter, Class<T> aType ) {
		this( aDelimiter, aType );
		insertFrom( aObj, aFromPath );
	}

	/**
	 * Create a {@link PathMapBuilder} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapBuilderImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter, Class<T> aType ) {
		this( aDelimiter, aType );
		insertBetween( aToPath, aObj, aFromPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aPath ) {
		return _backingMap.containsKey( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T get( Object aPath ) {
		return _backingMap.get( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T put( String aPath, T aValue ) {
		T theValue = null;
		if ( aValue != null ) {
			theValue = fromInstance( aValue );
			if ( theValue == null ) {
				throw new IllegalArgumentException( "The key <" + aPath + "> cannot be set as the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> is not of a supported type!" );
			}
		}
		return _backingMap.put( toNormalizedPath( aPath ), theValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T remove( Object aPath ) {
		return _backingMap.remove( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getOrDefault( Object aPath, T publicValue ) {
		return _backingMap.getOrDefault( toNormalizedPath( InterOperableMap.asKey( aPath ) ), publicValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T putIfAbsent( String aPath, T value ) {
		return _backingMap.putIfAbsent( toNormalizedPath( aPath ), value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove( Object aPath, Object value ) {
		return _backingMap.remove( toNormalizedPath( InterOperableMap.asKey( aPath ) ), value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean replace( String aPath, T oldValue, T newValue ) {
		return _backingMap.replace( toNormalizedPath( aPath ), oldValue, newValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T replace( String aPath, T value ) {
		return _backingMap.replace( toNormalizedPath( aPath ), value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T computeIfAbsent( String aPath, Function<? super String, ? extends T> mappingFunction ) {
		return _backingMap.computeIfAbsent( toNormalizedPath( aPath ), mappingFunction );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T computeIfPresent( String aPath, BiFunction<? super String, ? super T, ? extends T> remappingFunction ) {
		return _backingMap.computeIfPresent( toNormalizedPath( aPath ), remappingFunction );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T compute( String aPath, BiFunction<? super String, ? super T, ? extends T> remappingFunction ) {
		return _backingMap.compute( toNormalizedPath( aPath ), remappingFunction );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T merge( String aPath, T value, BiFunction<? super T, ? super T, ? extends T> remappingFunction ) {
		return _backingMap.merge( toNormalizedPath( aPath ), value, remappingFunction );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PathMap<T> retrieveTo( String aToPath ) {
		final PathMapBuilder<T> theToPathMap = new PathMapBuilderImpl<>( getDelimiter(), _type );
		StructureUtility.retrieveTo( this, aToPath, theToPathMap );
		return theToPathMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PathMap<T> retrieveFrom( String aFromPath ) {
		final PathMapBuilderImpl<T> theToPathMap = new PathMapBuilderImpl<>( getDelimiter(), _type );
		StructureUtility.retrieveFrom( this, aFromPath, theToPathMap );
		return theToPathMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _delimiter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getAnnotator() {
		return _annotator;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T> getType() {
		return _type;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return StructureUtility.toDataStructure( this, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insert( Object aFrom ) {
		final PathMap<T> thePathMap = fromObject( aFrom );
		for ( String ePath : thePathMap.paths() ) {
			put( ePath, thePathMap.get( ePath ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertBetween( String aToPath, Object aFrom, String aFromPath ) {
		final PathMap<T> thePathMap = fromObject( aFrom );
		insertTo( aToPath, thePathMap.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertFrom( Object aFrom, String aFromPath ) {
		PathMap<T> thePathMap = fromObject( aFrom );
		thePathMap = thePathMap.retrieveFrom( aFromPath );
		for ( String ePath : thePathMap.paths() ) {
			put( ePath, thePathMap.get( ePath ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertTo( String aToPath, Object aFrom ) {
		aToPath = toNormalizedPath( aToPath );
		if ( aFrom == null ) {
			put( aToPath, null );
		}
		else {
			final PathMap<T> thePathMap = fromObject( aFrom );
			for ( String ePath : thePathMap.paths() ) {
				put( toPath( aToPath, ePath ), thePathMap.get( ePath ) );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void merge( Object aFrom ) {
		final PathMap<T> thePathMap = fromObject( aFrom );
		for ( String ePath : thePathMap.paths() ) {
			if ( get( ePath ) == null ) {
				put( ePath, thePathMap.get( ePath ) );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		final PathMap<T> thePathMap = fromObject( aFrom );
		mergeTo( aToPath, thePathMap.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeFrom( Object aFrom, String aFromPath ) {
		PathMap<T> thePathMap = fromObject( aFrom );
		thePathMap = thePathMap.retrieveFrom( aFromPath );
		for ( String ePath : thePathMap.paths() ) {
			if ( get( ePath ) == null ) {
				put( ePath, thePathMap.get( ePath ) );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeTo( String aToPath, Object aFrom ) {
		aToPath = toNormalizedPath( aToPath );
		if ( aFrom == null && !containsKey( aToPath ) ) {
			put( aToPath, null );
		}
		else {
			final PathMap<T> thePathMap = fromObject( aFrom );
			String eToPath;
			for ( String ePath : thePathMap.paths() ) {
				eToPath = toPath( aToPath, ePath );
				if ( get( eToPath ) == null ) {
					put( eToPath, thePathMap.get( ePath ) );
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PathMapBuilder<T> withPut( String aKey, T aValue ) {
		put( aKey, aValue );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// BACKING MAP:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _backingMap.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _backingMap.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsValue( Object value ) {
		return _backingMap.containsValue( value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putAll( Map<? extends String, ? extends T> m ) {
		_backingMap.putAll( m );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		_backingMap.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _backingMap.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<T> values() {
		return _backingMap.values();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Entry<String, T>> entrySet() {
		return _backingMap.entrySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object o ) {
		return _backingMap.equals( o );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return _backingMap.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void forEach( BiConsumer<? super String, ? super T> action ) {
		_backingMap.forEach( action );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void replaceAll( BiFunction<? super String, ? super T, ? extends T> function ) {
		_backingMap.replaceAll( function );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <TYPE> TYPE toType( String aFromPath, Class<TYPE> aType ) {
		final T theValue = get( aFromPath );
		if ( theValue != null && !hasChildren( aFromPath ) ) {
			final TYPE theInstance = toInstance( theValue, aType );
			if ( theInstance != null ) {
				return theInstance;
			}
		}
		return PathMapBuilder.super.toType( aFromPath, aType );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void fromPathMap( String aToPath, PathMap<?> aPathMap, Set<Object> aVisited ) {
		if ( !aVisited.contains( aPathMap ) ) {
			aVisited.add( aPathMap );
			Object eValue;
			String eBasePath;
			String theRegex = "" + aPathMap.getDelimiter();
			if ( SPECIAL_REGEX_CHARS.contains( theRegex ) ) {
				theRegex = ESCAPE + theRegex;
			}
			for ( String ePath : aPathMap.paths() ) {
				eBasePath = toPath( aToPath, ePath.replaceAll( theRegex, getRootPath() ) );
				eValue = aPathMap.get( ePath );
				fromValue( eBasePath, eValue, aVisited );
			}
			aVisited.remove( aPathMap );
		}
	}

	private void fromMap( String aToPath, Map<?, ?> aMap, Set<Object> aVisited ) {
		if ( !aVisited.contains( aMap ) ) {
			aVisited.add( aMap );
			String eKey;
			Object eValue;
			String eBasePath;
			for ( Object eObj : aMap.keySet() ) {
				eKey = ( eObj instanceof String ) ? (String) eObj : ( eObj != null ? eObj.toString() : null );
				eBasePath = toPath( aToPath, eKey );
				eValue = aMap.get( eObj );
				if ( IS_DEBUG_OUTPUT ) {
					System.out.println( aToPath + ": fromMap(): " + aMap.getClass().getName() + " := " + aMap.toString() );
				}
				fromValue( eBasePath, eValue, aVisited );
			}
			aVisited.remove( aMap );
		}
	}

	private void fromKeys( String aToPath, Keys<?, ?> aKeys, Set<Object> aVisited ) {
		if ( !aVisited.contains( aKeys ) ) {
			aVisited.add( aKeys );
			String eKey;
			Object eValue;
			String eBasePath;
			for ( Object eObj : aKeys.keySet() ) {
				eKey = ( eObj instanceof String ) ? (String) eObj : ( eObj != null ? eObj.toString() : null );
				eBasePath = toPath( aToPath, eKey );
				eValue = aKeys.get( eObj );
				fromValue( eBasePath, eValue, aVisited );
			}
			aVisited.remove( aKeys );
		}
	}

	private void fromCollection( String aToPath, Collection<?> aCollection, Set<Object> aVisited ) {
		if ( !aVisited.contains( aCollection ) ) {
			aVisited.add( aCollection );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aCollection ) {
				eBasePath = toPath( aToPath, index );
				fromValue( eBasePath, eValue, aVisited );
				index++;
			}
			aVisited.remove( aCollection );
		}
	}

	private void fromArray( String aToPath, Object[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					if ( IS_DEBUG_OUTPUT ) {
						System.out.println( aToPath + ": fromArray(): " + aArray.getClass().getName() + " := " + aArray.toString() );
					}
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromArray( String aToPath, boolean[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			if ( !aVisited.contains( aArray ) ) {
				aVisited.add( aArray );
				int index = 0;
				String eBasePath;
				for ( Object eValue : aArray ) {
					if ( eValue != null ) {
						eBasePath = toPath( aToPath, index );
						fromValue( eBasePath, eValue, aVisited );
					}
					index++;
				}
				aVisited.remove( aArray );
			}
		}
	}

	private void fromArray( String aToPath, byte[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromArray( String aToPath, char[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromArray( String aToPath, short[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromArray( String aToPath, int[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromArray( String aToPath, long[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromArray( String aToPath, float[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromArray( String aToPath, double[] aArray, Set<Object> aVisited ) {
		if ( !aVisited.contains( aArray ) ) {
			aVisited.add( aArray );
			int index = 0;
			String eBasePath;
			for ( Object eValue : aArray ) {
				if ( eValue != null ) {
					eBasePath = toPath( aToPath, index );
					fromValue( eBasePath, eValue, aVisited );
				}
				index++;
			}
			aVisited.remove( aArray );
		}
	}

	private void fromObject( String aToPath, Object aObj, Set<Object> aVisited ) {
		if ( !aVisited.contains( aObj ) ) {
			aVisited.add( aObj );
			final T theObj = fromInstance( aObj );
			if ( theObj != null ) {
				put( aToPath, theObj );
			}
			else {
				final Class<? extends Object> theClass = aObj.getClass();
				final Map<String, Object> theMap = new HashMap<>();
				String ePropertyName;
				Object eInvoked;

				if ( theClass.isRecord() ) {
					// Components |-->
					final RecordComponent[] theComponents = theClass.getRecordComponents();
					if ( theComponents != null && theComponents.length > 0 ) {
						for ( RecordComponent eComponent : theComponents ) {
							try {
								ePropertyName = eComponent.getName();
								if ( !isBlackListed( ePropertyName ) && !theMap.containsKey( ePropertyName ) ) {
									eInvoked = eComponent.getAccessor().invoke( aObj );
									// @formatter:off
									if ( /* eInvoked != null && */ !aVisited.contains( eInvoked ) /* && !isBlacklisted( eInvoked.getClass() ) */ ) {
										theMap.put( ePropertyName, eInvoked );
									}
									// @formatter:on
								}
							}
							catch ( IllegalArgumentException | IllegalAccessException | InvocationTargetException ignore ) {}
						}
					}
					// Components <--|
				}
				else {
					// Methods |-->
					final Method[] theMethods = theClass.getMethods();
					if ( theMethods != null && theMethods.length > 0 ) {
						for ( Method eMethod : theMethods ) {
							if ( TypeUtility.isGetter( eMethod ) ) {
								try {
									ePropertyName = TypeUtility.toGetterPropertyName( eMethod );
									if ( !isBlackListed( ePropertyName ) && !theMap.containsKey( ePropertyName ) ) {
										// Don't in Java 9 |-->
										try {
											eMethod.setAccessible( true );
										}
										catch ( Exception ignore ) {}
										// Don't in Java 9 <--|
										eInvoked = eMethod.invoke( aObj );
									// @formatter:off
									if ( /* eInvoked != null && */ !aVisited.contains( eInvoked ) /* && !isBlacklisted( eInvoked.getClass() ) */ ) {
										theMap.put( ePropertyName, eInvoked );
									}
									// @formatter:on
									}
								}
								catch ( IllegalArgumentException | IllegalAccessException | InvocationTargetException ignore ) {}
							}
						}
					}
					// Methods <--|
					// Fields |-->
					final Field[] theFields = theClass.getDeclaredFields();
					if ( theFields != null && theFields.length > 0 ) {
						for ( Field eField : theFields ) {
							try {
								ePropertyName = TypeUtility.toPropertyName( eField );
								if ( !isBlackListed( ePropertyName ) && !theMap.containsKey( ePropertyName ) ) {
									// Don't in Java 9 |-->
									try {
										eField.setAccessible( true );
									}
									catch ( Exception ignore ) {}
									// Don't in Java 9 <--|
									// Ignore "transient" fields |-->
									if ( !Modifier.isTransient( eField.getModifiers() ) ) {
										eInvoked = eField.get( aObj );
										if ( eInvoked != null && !aVisited.contains( eInvoked ) ) { // && !isBlacklisted( eInvoked.getClass() )
											theMap.put( ePropertyName, eInvoked );
										}
									}
									// Ignore "transient" fields <--|
								}
							}
							catch ( IllegalArgumentException | IllegalAccessException ignore ) {}
						}
					}
					// Fields <--|
				}

				if ( !theMap.isEmpty() ) {
					if ( IS_DEBUG_OUTPUT ) {
						System.out.println( aToPath + ": fromObject(): " + theClass.getName() + " := " + aObj.toString() );
					}
					fromValue( aToPath, theMap, aVisited );
				}

			}
			aVisited.remove( aObj );
		}
	}

	protected void fromValue( String aToPath, Object aValue, Set<Object> aVisited ) {
		aToPath = toNormalizedPath( aToPath );
		if ( aValue == null ) {
			put( aToPath, null );
		}
		else if ( aValue instanceof PathMap<?> ) {
			if ( IS_DEBUG_OUTPUT ) {
				System.out.println( aToPath + ": fromValue(): " + aValue.getClass().getName() + " := " + aValue.toString() );
			}
			fromPathMap( aToPath, (PathMap<?>) aValue, aVisited );
		}
		else if ( aValue instanceof Map<?, ?> ) {
			if ( IS_DEBUG_OUTPUT ) {
				System.out.println( aToPath + ": fromValue(): " + aValue.getClass().getName() + " := " + aValue.toString() );
			}
			fromMap( aToPath, (Map<?, ?>) aValue, aVisited );
		}
		else if ( aValue instanceof Keys<?, ?> ) {
			fromKeys( aToPath, (Keys<?, ?>) aValue, aVisited );
		}
		else if ( aValue instanceof Collection<?> ) {
			fromCollection( aToPath, (Collection<?>) aValue, aVisited );
		}
		else if ( aValue.getClass().isArray() ) {
			final Class<?> theType = aValue.getClass().getComponentType();

			// boolean:
			if ( theType.equals( boolean.class ) ) {
				fromArray( aToPath, (boolean[]) aValue, aVisited );
			}
			// byte:
			else if ( theType.equals( byte.class ) ) {
				fromArray( aToPath, (byte[]) aValue, aVisited );
			}
			// char:
			else if ( theType.equals( char.class ) ) {
				fromArray( aToPath, (char[]) aValue, aVisited );
			}
			// short:
			else if ( theType.equals( short.class ) ) {
				fromArray( aToPath, (short[]) aValue, aVisited );
			}
			// int:
			else if ( theType.equals( int.class ) ) {
				fromArray( aToPath, (int[]) aValue, aVisited );
			}
			// long:
			else if ( theType.equals( long.class ) ) {
				fromArray( aToPath, (long[]) aValue, aVisited );
			}
			// float:
			else if ( theType.equals( float.class ) ) {
				fromArray( aToPath, (float[]) aValue, aVisited );
			}
			// double:
			else if ( theType.equals( double.class ) ) {
				if ( IS_DEBUG_OUTPUT ) {
					System.out.println( aToPath + ": fromValue(): " + aValue.getClass().getName() + " := " + aValue.toString() );
				}
				fromArray( aToPath, (double[]) aValue, aVisited );
			}
			else {
				if ( IS_DEBUG_OUTPUT ) {
					System.out.println( aToPath + ": fromValue(): " + aValue.getClass().getName() + " := " + aValue.toString() );
				}
				fromArray( aToPath, (Object[]) aValue, aVisited );
			}
		}
		else {
			fromObject( aToPath, aValue, aVisited );
		}
	}

	private boolean isBlackListed( String aPropertyName ) {
		for ( String eBlackListed : BLACKLISTED_PROPERTIES ) {
			if ( eBlackListed.equals( aPropertyName ) ) {
				return true;
			}
		}
		return false;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook method for intercepting or modifying the process of object
	 * introspection.
	 * 
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	protected void fromValue( String aToPath, Object aObj ) {
		fromValue( aToPath, aObj, new HashSet<>() );
	}

	/**
	 * Hook method when internally creating {@link PathMap} sub-types in the
	 * according {@link PathMap} sub-type implementations.
	 * 
	 * @param aFrom The {@link Object} from which to create a {@link PathMap}
	 *        (sub-type) instance.
	 * 
	 * @return The according {@link PathMap} (sub-type) instance.
	 */
	protected PathMap<T> fromObject( Object aFrom ) {
		return new PathMapImpl<>( aFrom, getDelimiter(), getType() );
	}

	/**
	 * Hook method when introspecting an object to help conversion of that
	 * object to the target types. By default this methods just tests for
	 * assignment conformity as of {@link Class#isAssignableFrom(Class)} with
	 * the type retrieved by {@link #getType()}.
	 * 
	 * @param aValue The value which is to be transformed to the expected type
	 *        as of {@link #getType()}.
	 * 
	 * @return The converted type or null if conversion is not possible.
	 */
	@SuppressWarnings("unchecked")
	protected T fromInstance( Object aValue ) {
		if ( _type.isAssignableFrom( aValue.getClass() ) ) {
			return (T) aValue;
		}
		if ( _type.isAssignableFrom( String.class ) ) {
			return (T) SimpleType.fromSimpleType( aValue );
		}
		return null;
	}

	/**
	 * Hook method when reconstructing an object, opposite of
	 * {@link #fromInstance(Object)}, tries to convert the given value to the
	 * desired type.
	 * 
	 * @param <TYPE> The type to which to convert to.
	 * 
	 * @param aValue The value to by converted from this instance's type as of
	 *        {@link #getType()} to the desired type.
	 * 
	 * @param aType The type to which to convert to.
	 * 
	 * @return The converted type or null if conversion is not possible.
	 */
	@SuppressWarnings("unchecked")
	protected <TYPE> TYPE toInstance( T aValue, Class<TYPE> aType ) {
		if ( aType.isAssignableFrom( aValue.getClass() ) ) {
			return (TYPE) aValue;
		}
		return null;
	}

	/**
	 * Hook for creating an individual backing {@link Map} to be used by the
	 * {@link PathMapImpl} type (defaults to {@link LinkedHashMap}).
	 * 
	 * @return The backing {@link Map} to be used.
	 */
	protected Map<String, T> createBackingMap() {
		return PathMap.toDefaultBackingMap();
	}
}
