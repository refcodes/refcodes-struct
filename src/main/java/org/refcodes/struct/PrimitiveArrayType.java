// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.mixin.TypeAccessor;

/**
 * The {@link PrimitiveArrayType} enumeration contains the {@link Class}
 * representation of the primitive array types.
 */
@SuppressWarnings("rawtypes")
public enum PrimitiveArrayType implements TypeAccessor {

	BOOLEAN(boolean[].class),

	BYTE(byte[].class),

	CHAR(char[].class),

	DOUBLE(double[].class),

	INT(int[].class),

	FLOAT(float[].class),

	LONG(long[].class),

	SHORT(short[].class);

	private Class<?> _type;

	private PrimitiveArrayType( Class<?> aType ) {
		_type = aType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> getType() {
		return _type;
	}

	/**
	 * Tests whether the given class represents a primitive array type.
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a primitive array type, else false.
	 */
	public static boolean isPrimitiveArrayType( Class<?> aClass ) {
		for ( PrimitiveArrayType eType : PrimitiveArrayType.values() ) {
			if ( eType.getType().isAssignableFrom( aClass ) ) {
				return true;
			}
		}
		return false;
	}
}
