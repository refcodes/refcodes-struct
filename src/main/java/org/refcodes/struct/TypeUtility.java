// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.RecordComponent;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.refcodes.data.Literal;

/**
 * The {@link TypeUtility} uses reflection to fiddle around with primitives,
 * types, classes, instances and the like.
 */
public class TypeUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[] SETTER_PREFIXES = { "set" };
	private static final String[] GETTER_PREFIXES = { "is", "has", "get" };
	private static final String[] FIELD_PREFIXES = { "_" };

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private TypeUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a generic {@link Collection}'s elements into an array which's
	 * elements are assignment-compliant to the collection's generic type. In
	 * case the collection is empty or merely contains <code>null</code> values,
	 * then null is returned as no type can be inferred from any elements inside
	 * the collection.
	 * 
	 * @param <T> The type of the collection.
	 * @param aCollection The {@link Collection} to convert.
	 * 
	 * @return The array which's elements are assignment-compliant to the
	 *         collection's generic type or null if the collection was empty or
	 *         merely contains <code>null</code> values.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] toArray( Collection<T> aCollection ) {
		if ( aCollection == null || aCollection.isEmpty() ) {
			return null;
		}
		final Class<T> theElementType = toElelemtType( aCollection );
		if ( theElementType == null ) {
			return null;
		}
		final int theSize = aCollection.size();
		final T[] theArray = (T[]) Array.newInstance( theElementType, theSize );
		int i = 0;
		for ( T eElement : aCollection ) {
			theArray[i] = eElement;
			i++;
		}
		return theArray;
	}

	/**
	 * Converts a given data structure to an array if it is a map which's keys
	 * can be converted to array indices. Else the provided data structure is
	 * returned as is.
	 * 
	 * @param aValue The data structure to be converted to an array if possible.
	 * 
	 * @return The array representation of the data structure or if not possible
	 *         the provided data structure as is.
	 */
	public static Object toUnwrappedMap( Object aValue ) {
		if ( aValue instanceof Map theMap ) {
			return toArrayFromMap( theMap );
		}
		return aValue;
	}

	/**
	 * Determines the collection's elements' least common type. Attention: Use
	 * only when you have a direct type hierarchy as ambiguous type hierarchies
	 * may cause {@link ClassCastException} exceptions to be thrown in your
	 * code!
	 * 
	 * @param <T> The generic type of the collection.
	 * @param aCollection The collection for which to determine the element's
	 *        generic type.
	 * 
	 * @return The generic type as of best knowledge or <code>null</code> if it
	 *         cannot be determined.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> toElelemtType( final Collection<T> aCollection ) {
		final List<Class<?>> theClasses = new ArrayList<>();
		for ( T eElement : aCollection ) {
			if ( eElement != null ) {
				theClasses.add( eElement.getClass() );
			}
		}
		final List<Class<?>> theElementTypes = toCommonAncestors( theClasses );
		return theElementTypes.size() != 0 ? (Class<T>) theElementTypes.get( 0 ) : null;
	}

	/**
	 * Determines the common ancestors for an array of types.
	 * 
	 * @param aTypes The classes for which to determine the common ancestors.
	 * 
	 * @return A {@link List} with the according common ancestors.
	 */
	public static List<Class<?>> toCommonAncestors( Collection<Class<?>> aTypes ) {
		final Set<Class<?>> theCommonAncestors = new LinkedHashSet<>();
		if ( aTypes != null && !aTypes.isEmpty() ) {
			final Iterator<Class<?>> e = aTypes.iterator();
			theCommonAncestors.addAll( toSuperTypes( e.next() ) );
			while ( e.hasNext() ) {
				theCommonAncestors.retainAll( toSuperTypes( e.next() ) );
			}
		}
		return new LinkedList<>( theCommonAncestors );
	}

	/**
	 * Determines the super types (class and interfaces) of a given type
	 * (including itself).
	 * 
	 * @param aType The type for which to determine all super types.
	 * 
	 * @return The according super types (including itself).
	 */
	public static Set<Class<?>> toSuperTypes( Class<?> aType ) {
		final Set<Class<?>> theClasses = new LinkedHashSet<>();
		theClasses.add( aType );
		final Class<?> theSuper = aType.getSuperclass();
		if ( theSuper != null && theSuper != Object.class ) {
			theClasses.addAll( toSuperTypes( theSuper ) );
		}
		for ( Class<?> eInterface : aType.getInterfaces() ) {
			theClasses.addAll( toSuperTypes( eInterface ) );
		}
		return theClasses;
	}

	/**
	 * Creates an instance of the given type filled with the data provided by
	 * the given Data-Structure (a mixture of array and {@link Map} objects).
	 *
	 * @param <T> the generic type
	 * @param aValue The Data-Structure, a mixture of arrays and {@link Map}
	 *        instance, from which to construct the instance of the given type.
	 * @param aType The type for which an instance is to be created.
	 * 
	 * @return The instance filled by the data from the given Data-Structure
	 * 
	 * @throws InstantiationException thrown when an application tries to create
	 *         an instance of a class using the newInstance method in class
	 *         {@link Class}, but the specified class object cannot be
	 *         instantiated.
	 * @throws IllegalAccessException thrown when an application tries to
	 *         reflectively create an instance (other than an array), set or get
	 *         a field, or invoke a method, but the currently executing method
	 *         does not have access to the definition of the specified class,
	 *         field, method or constructor.
	 * @throws NoSuchMethodException thrown when a particular method cannot be
	 *         found.
	 * @throws InvocationTargetException wraps an exception thrown by an invoked
	 *         method or constructor.
	 * @throws ClassNotFoundException thrown when an application tries to load
	 *         in a class through its string name but no definition for the
	 *         class with the specified name could be found.
	 * @throws SecurityException thrown by the security manager to indicate a
	 *         security violation.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T toType( Object aValue, Class<T> aType ) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
		// Array:
		if ( aType.isArray() ) {
			return toArray( aValue, aType );
		}
		// Value ~ Type:
		else if ( aType.isAssignableFrom( aValue.getClass() ) ) {
			return (T) aValue;
		}
		// Array -> Map
		else if ( aValue != null && aValue.getClass().isArray() && aType.isAssignableFrom( Map.class ) ) {
			final Map<Object, Object> theMap = new HashMap<>();
			final Object[] theArray = (Object[]) aValue;
			for ( int i = 0; i < theArray.length; i++ ) {
				if ( theArray[i] != null ) {
					theMap.put( Integer.toString( i ), theArray[i] );
				}
			}
			return toInstance( theMap, aType );
		}
		// Other:
		else {
			return toInstance( aValue, aType );
		}
	}

	/**
	 * Updates the provided instance with the data provided by the given value
	 * (a mixture of array and {@link Map} objects). In case the instance to be
	 * updated represents an array, then at most it is filled with the provided
	 * value's elements, even if the value provides more elements than would
	 * fit. On the other hand, if the instance's array to be updated provides
	 * room for more elements than the value can provide, then the remaining
	 * instance's elements are left as are.
	 *
	 * @param <T> the generic type
	 * @param aValue The Data-Structure, a mixture of arrays and {@link Map}
	 *        instance, from which to update the instance of the given type.
	 * @param aInstance The instance to be updated.
	 * 
	 * @throws InstantiationException thrown when an application tries to create
	 *         an instance of a class using the newInstance method in class
	 *         {@link Class}, but the specified class object cannot be
	 *         instantiated.
	 * @throws IllegalAccessException thrown when an application tries to
	 *         reflectively create an instance (other than an array), set or get
	 *         a field, or invoke a method, but the currently executing method
	 *         does not have access to the definition of the specified class,
	 *         field, method or constructor.
	 * @throws NoSuchMethodException thrown when a particular method cannot be
	 *         found.
	 * @throws InvocationTargetException wraps an exception thrown by an invoked
	 *         method or constructor.
	 * @throws ClassNotFoundException thrown when an application tries to load
	 *         in a class through its string name but no definition for the
	 *         class with the specified name could be found.
	 * @throws SecurityException thrown by the security manager to indicate a
	 *         security violation.
	 */
	@SuppressWarnings("unchecked")
	public static <T> void toInstance( Object aValue, T aInstance ) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
		// Array:
		final Class<T> theClass = (Class<T>) aInstance.getClass();
		if ( theClass.isArray() ) {
			final T theInstance = toArray( aValue, theClass );
			final int fromLength = Array.getLength( theInstance );
			final int toLength = Array.getLength( aInstance );
			for ( int i = 0; i < fromLength && i < toLength; i++ ) {
				Array.set( aInstance, i, Array.get( theInstance, i ) );
			}
		}
		// Other:
		else {
			updateInstance( aValue, aInstance );
		}
	}

	/**
	 * Tests whether we have a setter method. A setter method must not return a
	 * value and expect exactly one argument. It also must be public.
	 * 
	 * @param aMethod The {@link Method} to be tested.
	 * 
	 * @return True in case we have a setter method, else false.
	 */
	public static boolean isSetter( Method aMethod ) {
		final String thePropertyName = toSetterPropertyName( aMethod );
		if ( thePropertyName == null || thePropertyName.isEmpty() ) {
			return false;
		}
		return Modifier.isPublic( aMethod.getModifiers() ) && aMethod.getParameterCount() == 1 && Void.TYPE.equals( aMethod.getReturnType() );
	}

	/**
	 * Tests whether we have a getter method. A getter method must return a
	 * value (other than {@link Void}) and expect none arguments. It also must
	 * be public.
	 * 
	 * @param aMethod The {@link Method} to be tested.
	 * 
	 * @return True in case we have a getter method, else false.
	 */
	public static boolean isGetter( Method aMethod ) {
		final String thePropertyName = toGetterPropertyName( aMethod );
		if ( thePropertyName == null || thePropertyName.isEmpty() ) {
			return false;
		}
		return Modifier.isPublic( aMethod.getModifiers() ) && aMethod.getParameterCount() == 0 && !Void.TYPE.equals( aMethod.getReturnType() );
	}

	/**
	 * Converts a method's name to a property name.
	 * 
	 * @param aMethod The {@link Method} which's name is to be converted to a
	 *        property name.
	 * 
	 * @return The according property name or null if we do not have a property
	 *         method.
	 */
	public static String toPropertyName( Method aMethod ) {

		// Setter:
		if ( isSetter( aMethod ) ) {
			return toSetterPropertyName( aMethod );
		}
		// Getter:
		else if ( isGetter( aMethod ) ) {
			return toGetterPropertyName( aMethod );
		}
		return null;
	}

	/**
	 * Converts a field's name to a property name.
	 * 
	 * @param aField The {@link Field} which's name is to be converted to a
	 *        property name.
	 * 
	 * @return The according property name or null if we do not have a property
	 *         method.
	 */
	public static String toPropertyName( Field aField ) {
		final String theName = aField.getName();
		return fromFieldName( theName );
	}

	/**
	 * Retrieves all properties detected in the provided type.
	 * 
	 * @param aType The type from which to determine the properties.
	 * 
	 * @return The according detected properties.
	 */
	public static String[] toProperties( Class<?> aType ) {
		final List<String> theProperties = new ArrayList<>();
		String eProperty;

		if ( aType.isRecord() ) {
			final RecordComponent[] theComponents = aType.getRecordComponents();
			for ( RecordComponent theComponent : theComponents ) {
				eProperty = theComponent.getName();
				if ( eProperty != null && !theProperties.contains( eProperty ) ) {
					theProperties.add( eProperty );
				}
			}
		}
		else {
			final Method[] theMethods = aType.getMethods();
			for ( Method theMethod : theMethods ) {
				eProperty = toPropertyName( theMethod );
				if ( eProperty != null && !theProperties.contains( eProperty ) ) {
					theProperties.add( eProperty );
				}
			}
			final Field[] theFields = aType.getFields();
			for ( Field theField : theFields ) {
				eProperty = toPropertyName( theField );
				if ( eProperty != null && !theProperties.contains( eProperty ) ) {
					theProperties.add( eProperty );
				}
			}
		}
		return theProperties.toArray( new String[theProperties.size()] );
	}

	/**
	 * Converts the provided object reference to the required array type. In
	 * case a {@link Map} is provided, it's keys are interpreted as array
	 * indices for the array to be created.
	 * 
	 * @param <T> The type of the array to which to convert to.
	 * @param aValue The object reference which to convert.
	 * @param aType The type to which to convert to.
	 * 
	 * @return The according array type.
	 * 
	 * @throws IllegalArgumentException thrown in case there were reflection or
	 *         introspection problems when converting the object to the required
	 *         array type.
	 */
	public static <T> T toArrayType( Object aValue, Class<T> aType ) {
		//	if ( !aValue.getClass().isArray() ) {
		//		throw new IllegalArgumentException( "The provided value <" + aValue + "> of type <" + aValue.getClass() + "> must be an array type!" );
		//	}
		if ( !aType.isArray() ) {
			throw new IllegalArgumentException( "The provided type <" + aType + "> must be an array type!" );
		}
		try {
			return toArray( aValue, aType );
		}
		catch ( NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | ClassNotFoundException e ) {
			throw new IllegalArgumentException( "Cannot create an array type <" + aType + "> for the given value <" + aValue + ")!", e );
		}
	}

	/**
	 * This method creates a {@link Map} with the attributes found in the given
	 * instance.The values of the attributes are not introspected any more!
	 * 
	 * @param <T> The type of the instance to be introspected.
	 * @param aInstance The instance from which to get the attributes.
	 * 
	 * @return The {@link Map} containing the attributes of the provided
	 *         instance, the keys being the attribute names, the values being
	 *         the attribute values
	 */
	@SuppressWarnings("unchecked")
	public static <T> Map<String, Object> toMap( T aInstance ) {
		final Class<T> theType = (Class<T>) aInstance.getClass();
		final Map<String, Object> theResult = new HashMap<>();

		// Methods |-->
		final Method[] theMethods = theType.getDeclaredMethods();
		if ( theMethods != null && theMethods.length > 0 ) {
			String ePropertyName;
			Object eValue;
			for ( Method eMethod : theMethods ) {
				if ( isGetter( eMethod ) ) {
					ePropertyName = toGetterPropertyName( eMethod );
					// Don't in Java 9 |-->
					try {
						eMethod.setAccessible( true );
					}
					catch ( Exception ignore ) {}
					// Don't in Java 9 <--|
					try {
						eValue = eMethod.invoke( aInstance );
						theResult.put( ePropertyName, eValue );
					}
					catch ( IllegalAccessException | IllegalArgumentException | InvocationTargetException ignore ) {}
				}
			}
		}
		// Methods <--|

		// Fields |-->
		final Field[] theFields = theType.getDeclaredFields();
		if ( theFields != null && theFields.length > 0 ) {
			String ePropertyName;
			Object eValue;
			for ( Field eField : theFields ) {
				ePropertyName = toPropertyName( eField );
				// Don't in Java 9 |-->
				try {
					eField.setAccessible( true );
				}
				catch ( Exception ignore ) {}
				// Don't in Java 9 <--|
				try {
					eValue = eField.get( aInstance );
					theResult.put( ePropertyName, eValue );
				}
				catch ( IllegalArgumentException | IllegalAccessException ignore ) {}
			}
		}
		// Fields <--|

		return theResult;
	}

	/**
	 * Determines the "default" constructor of the provided type. The arguments
	 * order of the constructor match the {@link RecordComponent} order as of
	 * {@link Class#getRecordComponents()}.
	 * 
	 * @param aType The type for which to retrieve the constructor.
	 * 
	 * @return The according constructor or null if none matching was found.
	 */
	public static Constructor<?> toConstructor( Class<Record> aType ) {
		final RecordComponent[] theComponents = aType.getRecordComponents();
		return toConstructor( aType, theComponents );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the Java Bean property name from the provided {@link Method}
	 * instance.
	 *
	 * @param aMethod The method from which to retrieve the Java Bean property
	 *        name.
	 * 
	 * @return the Java property name or null if there is no such getter.
	 */
	protected static String toGetterPropertyName( Method aMethod ) {
		final String theName = aMethod.getName();
		return toGetterPropertyName( theName );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static Object toArrayFromMap( Map<?, ?> aMap ) {
		int index;
		// Determine the length of the array |-->
		int theMax = -1;
		for ( Object eKey : aMap.keySet() ) {
			try {
				index = eKey instanceof Integer eInt ? eInt.intValue() : ( Integer.valueOf( eKey instanceof String ? (String) eKey : ( eKey != null ? eKey.toString() : null ) ) );
				if ( index < 0 ) {
					return toMapWithStringKeys( aMap );
				}
				if ( index > theMax ) {
					theMax = index;
				}
			}
			catch ( NumberFormatException e ) {
				return toMapWithStringKeys( aMap );
			}
		}
		final Object[] theObjects = new Object[theMax + 1];
		// Determine the length of the array <--|

		// Fill the array |-->
		for ( Object eKey : aMap.keySet() ) {
			try {
				index = eKey instanceof Integer eInt ? eInt.intValue() : ( Integer.valueOf( eKey instanceof String ? (String) eKey : ( eKey != null ? eKey.toString() : null ) ) );
				if ( theObjects[index] != null ) {
					return toMapWithStringKeys( aMap );
				}
				theObjects[index] = toUnwrappedMap( aMap.get( eKey ) );
			}
			catch ( NumberFormatException e ) {
				return toMapWithStringKeys( aMap );
			}
		}
		// Fill the array <--|

		return theObjects;
	}

	private static Map<String, ?> toMapWithStringKeys( Map<?, ?> aMap ) {
		final Map<String, Object> theMap = new HashMap<>();
		for ( Object eKey : aMap.keySet() ) {
			theMap.put( eKey instanceof String ? (String) eKey : ( eKey != null ? eKey.toString() : null ), toUnwrappedMap( aMap.get( eKey ) ) );
		}
		return theMap;
	}

	// -------------------------------------------------------------------------

	private static Constructor<?> toConstructor( Class<Record> aType, RecordComponent[] aComponents ) {
		if ( aComponents.length != 0 ) {
			final Constructor<?>[] theCtors = aType.getDeclaredConstructors();
			Parameter[] eParams;
			for ( Constructor<?> eCtor : theCtors ) {
				eParams = eCtor.getParameters();
				if ( eParams != null && eParams.length == aComponents.length ) {
					out: {
						for ( int i = 0; i < eParams.length; i++ ) {
							if ( !eParams[i].getType().equals( aComponents[i].getType() ) ) {
								break out;
							}
						}
						return eCtor;
					}
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static <T> T toInstance( Object aValue, Class<T> aType ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		if ( aValue instanceof String ) {
			try {
				final Class<?> theClass = Class.forName( (String) aValue );
				if ( aType.isAssignableFrom( theClass ) ) {
					final Constructor<?> theCtor = theClass.getConstructor();
					return (T) theCtor.newInstance();
				}
			}
			catch ( NoClassDefFoundError | Exception ignore ) {}
		}
		if ( aType.isEnum() && aValue instanceof String ) {
			final Enum<?>[] theEnums = (Enum<?>[]) aType.getEnumConstants();
			for ( Enum<?> eEnum : theEnums ) {
				if ( eEnum.name().equals( aValue ) ) {
					return (T) eEnum;
				}
			}
		}
		if ( aType.isRecord() ) {
			return fromRecord( aValue, (Class<Record>) aType );
		}
		else if ( aValue instanceof String && SimpleType.isSimpleType( aType ) ) {
			return SimpleType.toSimpleType( (String) aValue, aType );
		}
		else {
			return fromClass( aValue, aType );
		}
	}

	//	@SuppressWarnings("unchecked")
	//	private static <T> T toInstance( Object aValue, Class<T> aType ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
	//		if ( aValue instanceof String ) {
	//			try {
	//				Class<?> theClass = Class.forName( (String) aValue );
	//				if ( aType.isAssignableFrom( theClass ) ) {
	//					Constructor<?> theCtor = theClass.getConstructor();
	//					return (T) theCtor.newInstance();
	//				}
	//			}
	//			catch ( NoClassDefFoundError | Exception ignore ) {}
	//		}
	//
	//		if ( aType.isEnum() && aValue instanceof String ) {
	//			Enum<?>[] theEnums = (Enum<?>[]) aType.getEnumConstants();
	//			for ( Enum<?> eEnum : theEnums ) {
	//				if ( eEnum.name().equals( aValue ) ) {
	//					return (T) eEnum;
	//				}
	//			}
	//		}
	//		Constructor<T> theCtor = aType.getConstructor();
	//		// Don't in Java 9 |-->
	//		try {
	//			theCtor.setAccessible( true );
	//		}
	//		catch ( Exception ignore ) {}
	//		// Don't in Java 9 <--|
	//		T theToInstance = theCtor.newInstance();
	//		toInstance( aValue, theToInstance );
	//		return theToInstance;
	//	}

	// TODO 2024-06-01 Think about supporting prefixing attributes with "has" or "is" to align with constructor parameters when creating record instances.
	@SuppressWarnings("unchecked")
	private static <T> T fromRecord( Object aValue, Class<Record> aType ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		final RecordComponent[] theComponents = aType.getRecordComponents();
		final Constructor<?> theCtor = toConstructor( aType, theComponents );
		if ( aValue instanceof Map ) {
			final Map<String, ?> theMap = (Map<String, ?>) aValue;
			final Set<String> theInjected = new HashSet<>();
			// Components |-->
			final List<Object> theArgs = new ArrayList<>();
			final Parameter[] theParams = theCtor.getParameters();
			if ( theParams != null && theParams.length > 0 ) {
				String ePropertyName;
				Parameter eParam;
				for ( int i = 0; i < theParams.length; i++ ) {
					eParam = theParams[i];
					ePropertyName = theComponents[i].getName();
					if ( !theInjected.contains( ePropertyName ) ) {
						Object eValue = theMap.get( ePropertyName );
						try {
							if ( eValue != null ) {
								// Special case: Array |-->
								if ( eValue.getClass().isArray() && !eParam.getType().isArray() && !Collection.class.isAssignableFrom( eParam.getType() ) ) {
									final Object[] eArray = (Object[]) eValue;
									for ( Object eElement : eArray ) {
										if ( eElement != null ) {
											// We may have a nested structure containing additional maps with the nested structure's elements |--> 
											if ( eElement.getClass().isAssignableFrom( eParam.getType() ) ) {
												addToArgs( theArgs, eElement, eParam );
												theInjected.add( ePropertyName );
											}
											// We may have a nested structure containing additional maps with the nested structure's elements <--|
										}
									}
								}
								// Special case: Array <--|
								else {
									addToArgs( theArgs, eValue, eParam );
									theInjected.add( ePropertyName );
								}
							}
							// Use defaults for missing values |--> 
							else {
								eValue = SimpleType.isPrimitiveType( eParam.getType() ) ? ( eParam.getType() == boolean.class ? "false" : "0" ) : null;
								addToArgs( theArgs, eValue, eParam );
								theInjected.add( ePropertyName );
							}
							// Use defaults for missing values |-->
						}
						catch ( IllegalArgumentException e ) {
							throw new IllegalArgumentException( "Unable to determine property <" + ePropertyName + "> of type <" + aType.getName() + "> from value <" + eValue + ">!", e );
						}
					}
				}
			}
			// Components <--|
			try {
				return (T) theCtor.newInstance( theArgs.toArray() );
			}
			catch ( IllegalArgumentException e ) {
				throw new IllegalArgumentException( "Cannot create instance for type <" + aType.getName() + "> with arguments " + Arrays.toString( theArgs.toArray() ), e );
			}
		}
		throw new IllegalArgumentException( "Unable to match the value <" + aValue + "> " + ( aValue != null ? "of type <" + aValue.getClass().getName() + "> " : " " ) + "with type <" + aType + ">!" );
	}

	private static <T> T fromClass( Object aValue, Class<T> aType ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		T theToInstance = toInstance( aType );
		if ( theToInstance == null ) {
			final Constructor<T> theCtor = aType.getConstructor();
			// Don't in Java 9 |-->
			try {
				theCtor.setAccessible( true );
			}
			catch ( Exception ignore ) {}
			// Don't in Java 9 <--|
			theToInstance = theCtor.newInstance();
		}
		toInstance( aValue, theToInstance );
		return theToInstance;
	}

	@SuppressWarnings("unchecked")
	private static <T> T toInstance( Class<T> aType ) {
		if ( Map.class.equals( aType ) ) {
			return (T) new HashMap<Object, Object>();
		}
		else if ( List.class.equals( aType ) ) {
			return (T) new ArrayList<Object>();
		}
		else if ( Set.class.equals( aType ) ) {
			return (T) new HashSet<Object>();
		}
		else if ( Collection.class.equals( aType ) ) {
			return (T) new ArrayList<Object>();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static <T> void updateInstance( Object aValue, T aInstance ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		final Class<T> aType = (Class<T>) aInstance.getClass();
		if ( aValue instanceof Map ) {
			final Map<?, ?> theMap = (Map<?, ?>) aValue;
			if ( aInstance instanceof Map ) {
				final Map<Object, Object> theToMap = (Map<Object, Object>) aInstance;
				for ( Object eKey : theMap.keySet() ) {
					theToMap.put( eKey, theMap.get( eKey ) );
				}
			}
			else {
				final Set<String> theInjected = new HashSet<>();
				// Methods |-->
				final Method[] theMethods = aType.getDeclaredMethods();
				if ( theMethods != null && theMethods.length > 0 ) {
					// Take care of identically named methods with differing argument types |-->
					final Set<String> theFailedProperties = new HashSet<>();
					Exception theFailedException = null;
					// Take care of identically named methods with differing argument types <--|
					String ePropertyName;
					for ( Method eMethod : theMethods ) {
						if ( isSetter( eMethod ) ) {
							try {
								ePropertyName = toSetterPropertyName( eMethod );
								if ( !theInjected.contains( ePropertyName ) ) {
									final Object eValue = theMap.get( ePropertyName );
									if ( eValue != null ) {
										// Don't in Java 9 |-->
										try {
											eMethod.setAccessible( true );
										}
										catch ( Exception ignore ) {}
										// Don't in Java 9 <--|
										// Special case: Array |-->
										if ( eValue.getClass().isArray() && !eMethod.getParameters()[0].getType().isArray() && !Collection.class.isAssignableFrom( eMethod.getParameters()[0].getType() ) ) {
											final Object[] eArray = (Object[]) eValue;
											Object eObj = null;
											for ( Object eElement : eArray ) {
												if ( eElement != null ) {
													if ( eObj != null && eElement instanceof Map ) {
														updateInstance( eElement, eObj );
													}
													else {
														try {
															eObj = addToMethod( aInstance, eElement, eMethod );
															theInjected.add( ePropertyName );
														}
														catch ( Exception e ) {
															theFailedProperties.add( ePropertyName ); // Are there yet more overloaded candidates?
															theFailedException = theFailedException == null ? e : theFailedException;
														}
													}
												}
											}
										}
										// Special case: Array <--|
										else {
											try {
												addToMethod( aInstance, eValue, eMethod );
												theInjected.add( ePropertyName );
											}
											catch ( Exception e ) {
												theFailedProperties.add( ePropertyName ); // Are there yet more overloaded candidates?
												theFailedException = theFailedException == null ? e : theFailedException;
											}
										}
									}
								}
							}
							catch ( IllegalArgumentException | IllegalAccessException ignore ) {}
						}
					}
					// Are there yet unsatisfied candidates? |-->
					if ( !theFailedProperties.isEmpty() ) {
						for ( String eInjectedName : theInjected ) {
							theFailedProperties.remove( eInjectedName );

						}
						if ( !theFailedProperties.isEmpty() ) {
							if ( theFailedException != null ) {
								throw new NoSuchMethodNotInvokedException( "Unable to satisfy the following properties " + Arrays.toString( theFailedProperties.toArray() ) + " (without the square braces) as there are no according methods with the required argument types!", theFailedException );
							}
							else {
								throw new NoSuchMethodException( "Unable to satisfy the following properties " + Arrays.toString( theFailedProperties.toArray() ) + " (without the square braces) as there are no according methods with the required argument types!" );
							}
						}
					}
					// Are there yet unsatisfied candidates? <--|
				}
				// Methods <--|
				// Fields |-->
				final Field[] theFields = aType.getDeclaredFields();
				if ( theFields != null && theFields.length > 0 ) {
					String ePropertyName;
					for ( Field eField : theFields ) {
						try {
							ePropertyName = toPropertyName( eField );
							if ( !theInjected.contains( ePropertyName ) ) {
								final Object eValue = theMap.get( ePropertyName );
								if ( eValue != null ) {
									// Don't in Java 9 |-->
									try {
										eField.setAccessible( true );
									}
									catch ( Exception ignore ) {}
									// Don't in Java 9 <--|
									// Special case: Array |-->
									if ( eValue.getClass().isArray() && !eField.getType().isArray() && !Collection.class.isAssignableFrom( eField.getType() ) ) {
										final Object[] eArray = (Object[]) eValue;
										for ( Object eElement : eArray ) {
											if ( eElement != null ) {
												addToField( aInstance, eElement, eField );
												theInjected.add( ePropertyName );
											}
										}
									}
									// Special case: Array <--|
									else {
										addToField( aInstance, eValue, eField );
										theInjected.add( ePropertyName );
									}
								}
							}
						}
						catch ( IllegalArgumentException | IllegalAccessException ignore ) {}
					}
				}
				// Fields <--|
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> Object addToMethod( T aToInstance, Object aToValue, Method aToMethod ) throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
		final String theToText = aToValue.toString();
		final Class<?> theToType = aToMethod.getParameters()[0].getType();
		// String:
		if ( theToType.equals( String.class ) ) {
			aToMethod.invoke( aToInstance, theToText );
		}
		else if ( theToText.length() != 0 && !Literal.NULL.getValue().equals( theToText ) ) {
			// Simple types:
			if ( !SimpleType.invokeSimpleType( aToInstance, aToMethod, theToText, theToType ) ) {
				// List:
				if ( List.class.isAssignableFrom( theToType ) ) {
					final List<?> theToList = toList( aToValue, (Class<? extends List<?>>) theToType, aToMethod.getParameters()[0].getParameterizedType() );
					aToMethod.invoke( aToInstance, theToList );
				}
				// Map:
				else if ( Map.class.isAssignableFrom( theToType ) ) {
					final Map<?, ?> theToMap = toMap( aToValue, (Class<? extends Map<?, ?>>) theToType, aToMethod.getParameters()[0].getParameterizedType() );
					aToMethod.invoke( aToInstance, theToMap );
				}
				// Other:
				else {
					final Object theInstance = toType( aToValue, theToType );
					aToMethod.invoke( aToInstance, theInstance );
					return theInstance;
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static <T> void addToField( T aToInstance, Object aToValue, Field aToField ) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
		final String theToText = aToValue.toString();
		final Class<?> theToType = aToField.getType();
		// String:
		if ( theToType.equals( String.class ) ) {
			aToField.set( aToInstance, theToText );
		}
		else if ( theToText.length() != 0 && !Literal.NULL.getValue().equals( theToText ) ) {
			// Simple types:
			if ( !SimpleType.invokeSimpleType( aToInstance, aToField, theToText, theToType ) ) {
				// List:
				if ( List.class.isAssignableFrom( theToType ) ) {
					final List<?> theToList = toList( aToValue, (Class<? extends List<?>>) theToType, aToField.getGenericType() );
					aToField.set( aToInstance, theToList );
				}
				// Map:
				else if ( Map.class.isAssignableFrom( theToType ) ) {
					final Map<?, ?> theToMap = toMap( aToValue, (Class<? extends Map<?, ?>>) theToType, aToField.getGenericType() );
					aToField.set( aToInstance, theToMap );
				}
				// Other:
				else {
					aToField.set( aToInstance, toType( aToValue, theToType ) );
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> void addToArgs( List<Object> aArgs, Object aToValue, Parameter aToParam ) throws InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
		final String theToText = aToValue != null ? aToValue.toString() : null;
		final Class<?> theToType = aToParam.getType();
		// String:
		if ( theToType.equals( String.class ) ) {
			aArgs.add( theToText );
		}
		else if ( theToText != null && theToText.length() != 0 ) { // && (!Literal.NULL.getValue().equals( theToText )) ) {
			// Simple types:
			final Object theValue = SimpleType.toSimpleType( theToText, theToType );
			if ( theValue != null ) {
				aArgs.add( theValue );
			}
			else {
				// List:
				if ( List.class.isAssignableFrom( theToType ) ) {
					final List<?> theToList = toList( aToValue, (Class<? extends List<?>>) theToType, aToParam.getParameterizedType() );
					aArgs.add( theToList );
				}
				// Map:
				else if ( Map.class.isAssignableFrom( theToType ) ) {
					final Map<?, ?> theToMap = toMap( aToValue, (Class<? extends Map<?, ?>>) theToType, aToParam.getParameterizedType() );
					aArgs.add( theToMap );
				}
				// Other:
				else {
					aArgs.add( toType( aToValue, theToType ) );
				}
			}
		}
		else {
			aArgs.add( null );
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> T toArray( Object aValue, Class<T> aType ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		final T theToInstance;
		final Class<?> theToType = aType.getComponentType();
		aValue = toUnwrappedMap( aValue );
		final Object[] theArray = (Object[]) aValue;
		theToInstance = (T) Array.newInstance( aType.getComponentType(), theArray.length );
		String eToText;
		for ( int i = 0; i < theArray.length; i++ ) {
			if ( theArray[i] != null ) {
				eToText = theArray[i].toString();
				// String:
				if ( theToType.equals( String.class ) ) {
					Array.set( theToInstance, i, eToText );
				}
				else if ( eToText.length() != 0 && !Literal.NULL.getValue().equals( eToText ) ) {
					// Simple types:
					if ( !SimpleType.invokeSimpleType( theToInstance, i, eToText, theToType ) ) {
						// Other:
						Array.set( theToInstance, i, toType( theArray[i], theToType ) );
					}
				}
			}
		}
		return theToInstance;
	}

	@SuppressWarnings("unchecked")
	private static Map<?, ?> toMap( Object aToValue, Class<? extends Map<?, ?>> aToType, Type aToGenericType ) throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
		final Map<Object, Object> theToMap;
		final Map<Object, Object> theMap = (Map<Object, Object>) aToValue;
		if ( aToType.equals( Map.class ) ) {
			theToMap = new HashMap<>();
		}
		else {
			theToMap = (Map<Object, Object>) aToType.getConstructor().newInstance();
		}
		Class<?> theKeyType = Object.class;
		Class<?> theValueType = Object.class;
		if ( aToGenericType instanceof ParameterizedType theGenericType ) {
			theKeyType = toClass( theGenericType.getActualTypeArguments()[0] );
			theValueType = toClass( theGenericType.getActualTypeArguments()[1] );
		}
		for ( Object eKey : theMap.keySet() ) {
			final Object theValue = theMap.get( eKey );
			if ( theValue != null ) {
				theToMap.put( toValue( eKey, theKeyType ), toValue( theValue, theValueType ) );
			}
		}
		return theMap;
	}

	@SuppressWarnings("unchecked")
	private static List<?> toList( Object aToValue, Class<? extends List<?>> aToType, Type aToGenericType ) throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
		List<Object> theToList = null;
		aToValue = toUnwrappedMap( aToValue );
		final Object[] theArray = (Object[]) aToValue;
		if ( aToType.equals( List.class ) ) {
			theToList = new ArrayList<>();
		}
		else {
			theToList = (List<Object>) aToType.getConstructor().newInstance();
		}
		Class<?> theType = Object.class;
		if ( aToGenericType instanceof ParameterizedType ) {
			theType = toClass( ( (ParameterizedType) aToGenericType ).getActualTypeArguments()[0] );
		}
		for ( Object aTheArray : theArray ) {
			if ( aTheArray == null ) {
				theToList.add( null );
			}
			else {
				theToList.add( toValue( aTheArray, theType ) );
			}
		}
		return theToList;
	}

	private static Object toValue( Object aValue, Class<?> aType ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		// Key |-->
		final String theValue = aValue.toString();
		Object theResult = null;
		// String:
		if ( aType.equals( String.class ) ) {
			theResult = theValue;
		}
		else if ( theValue.length() != 0 && !Literal.NULL.getValue().equals( theValue ) ) {
			// Simple type:
			theResult = SimpleType.toSimpleType( theValue, aType );
			// Other:
			if ( theResult == null ) {
				theResult = toType( aValue, aType );
			}
		}
		return theResult;
	}

	private static Class<?> toClass( Type aType ) throws ClassNotFoundException {
		if ( aType instanceof Class ) {
			return (Class<?>) aType;
		}
		return Class.forName( aType.getTypeName() );
	}

	private static String toPropertyName( String aName ) {
		final char[] theChars = aName.toCharArray();
		int i = 0;
		while ( i < theChars.length && Character.isUpperCase( theChars[i] ) ) {
			theChars[i] = Character.toLowerCase( theChars[i] );
			i++;
		}
		return new String( theChars );
	}

	private static String toSetterPropertyName( Method aMethod ) {
		final String theName = aMethod.getName();
		return toSetterPropertyName( theName );
	}

	private static String toSetterPropertyName( String aName ) {
		SETTER: {
			for ( String ePrefix : SETTER_PREFIXES ) {
				if ( aName.startsWith( ePrefix ) ) {
					aName = aName.substring( ePrefix.length() );
					break SETTER;
				}
			}
			return null;
		}
		if ( aName.length() > 0 ) {
			if ( Character.isLowerCase( aName.charAt( 0 ) ) ) {
				return null;
			}
			return toPropertyName( aName );
		}
		return null;
	}

	private static String toGetterPropertyName( String aName ) {
		GETTER: {
			for ( String ePrefix : GETTER_PREFIXES ) {
				if ( aName.startsWith( ePrefix ) ) {
					aName = aName.substring( ePrefix.length() );
					break GETTER;
				}
			}
			return null;
		}
		if ( aName.length() > 0 ) {
			if ( Character.isLowerCase( aName.charAt( 0 ) ) ) {
				return null;
			}
			return toPropertyName( aName );
		}
		return null;
	}

	private static String fromFieldName( String aFieldName ) {
		String eName = aFieldName;
		for ( String ePrefix : FIELD_PREFIXES ) {
			if ( eName.startsWith( ePrefix ) ) {
				eName = aFieldName.substring( ePrefix.length() );
				break;
			}
		}
		if ( eName.length() > 0 ) {
			aFieldName = eName;
		}
		eName = toGetterPropertyName( aFieldName );
		if ( eName != null && eName.length() > 0 ) {
			aFieldName = eName;
		}
		return toPropertyName( aFieldName );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Extension of the {@link NoSuchMethodException} with an additional aCause
	 * {@link #getCause()}
	 */
	public static class NoSuchMethodNotInvokedException extends NoSuchMethodException {

		private static final long serialVersionUID = 1L;

		private final Throwable _cause;

		/**
		 * {@inheritDoc}
		 */
		public NoSuchMethodNotInvokedException( String aMessage, Throwable aCause ) {
			super( aMessage );
			_cause = aCause;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Throwable getCause() {
			return _cause;
		}
	}
}
