// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.data.Delimiter;

/**
 * A {@link Property} is defined as being {@link String} representing a
 * property's key and a {@link String} representing the proprty's value.
 */
public interface Property extends Relation<String, String> {

	/**
	 * Returns the key/value-pair as a property {@link String} "key=value".
	 * 
	 * @return This {@link Property} as a {@link String}
	 */
	default String toProperty() {
		return getKey() + Delimiter.PROPERTY.getChar() + getValue();
	}

	/**
	 * Adds builder functionality to a {@link Property}.
	 */
	public interface PropertyBuilder extends Property, RelationBuilder<String, String> {

		/**
		 * With key.
		 *
		 * @param aKey the key
		 * 
		 * @return the property builder
		 */
		@Override
		default PropertyBuilder withKey( String aKey ) {
			setKey( aKey );
			return this;
		}

		/**
		 * With value.
		 *
		 * @param aValue the value
		 * 
		 * @return the property builder
		 */
		@Override
		default PropertyBuilder withValue( String aValue ) {
			setValue( aValue );
			return this;
		}
	}
}
