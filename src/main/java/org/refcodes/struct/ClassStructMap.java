// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.refcodes.data.Wildcard;

/**
 * The {@link ClassStructMap} disects a {@link Class} into its simple types
 * (considering {@link String} and {@link Enum} types as well as primitive
 * types).
 */
public interface ClassStructMap extends PathMap<Class<?>>, TypeModeAccessor {

	String ARRAY_SELECTOR = Wildcard.FILE.getValue();

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Interface ClassStructMapBuilder.
	 */
	public interface ClassStructMapBuilder extends MutableClassStructMap, PathMapBuilder<Class<?>> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPut( Collection<?> aPathElements, Class<?> aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPut( Object[] aPathElements, Class<?> aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPut( Relation<String, Class<?>> aProperty ) {
			put( aProperty );
			return this;
		}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default ClassStructMapBuilder withPut( String aKey, String aValue ) {
		//		put( aKey, aValue );
		//		return this;
		//	}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default ClassStructMapBuilder withPut( Object aPath, String aValue ) {
		//		put( toPath( aPath ), aValue );
		//		return this;
		//	}

		/**
		 * Builder method for {@link #put(Property)}.
		 *
		 * @param aProperty the property to be put.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		default ClassStructMapBuilder withPut( Property aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPut( String[] aKey, Class<?> aValue ) {
			put( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsert( PathMap<Class<?>> aFrom ) {
			insert( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<Class<?>> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( Object aToPath, PathMap<Class<?>> aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( Object[] aToPathElements, PathMap<Class<?>> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( String aToPath, PathMap<Class<?>> aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertBetween( String[] aToPathElements, PathMap<Class<?>> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( PathMap<Class<?>> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( PathMap<Class<?>> aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( PathMap<Class<?>> aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( PathMap<Class<?>> aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertFrom( PathMap<Class<?>> aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( Collection<?> aToPathElements, PathMap<Class<?>> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( Object aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( Object aToPath, PathMap<Class<?>> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( Object[] aToPathElements, PathMap<Class<?>> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( String aToPath, PathMap<Class<?>> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withInsertTo( String[] aToPathElements, PathMap<Class<?>> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMerge( PathMap<Class<?>> aFrom ) {
			merge( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<Class<?>> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( Object aToPath, PathMap<Class<?>> aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( Object[] aToPathElements, PathMap<Class<?>> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( String aToPath, PathMap<Class<?>> aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeBetween( String[] aToPathElements, PathMap<Class<?>> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( PathMap<Class<?>> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( PathMap<Class<?>> aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( PathMap<Class<?>> aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( PathMap<Class<?>> aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeFrom( PathMap<Class<?>> aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( Collection<?> aToPathElements, PathMap<Class<?>> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( Object aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( Object aToPath, PathMap<Class<?>> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( Object[] aToPathElements, PathMap<Class<?>> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( String aToPath, PathMap<Class<?>> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withMergeTo( String[] aToPathElements, PathMap<Class<?>> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<Class<?>> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( int aIndex, PathMap<Class<?>> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( Object aPath, int aIndex, PathMap<Class<?>> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<Class<?>> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( String aPath, int aIndex, PathMap<Class<?>> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<Class<?>> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMapBuilder withRemovePaths( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// MUTATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Interface MutableClassStructMap.
	 */
	public interface MutableClassStructMap extends ClassStructMap, MutablePathMap<Class<?>>, Map<String, Class<?>> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default boolean containsValue( Object value ) {
			return values().contains( value );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insert(Object)}.
		 * 
		 * @param aFrom The {@link ClassStructMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 */
		default void insert( ClassStructMap aFrom ) {
			insert( (Object) aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link ClassStructMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertBetween( String aToPath, ClassStructMap aFrom, String aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link ClassStructMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertFrom( ClassStructMap aFrom, String aFromPath ) {
			insertFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link ClassStructMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( String aToPath, ClassStructMap aFrom ) {
			insertTo( aToPath, (Object) aFrom );
		}

		/**
		 * Adds the given element related to the given key. As the key passed is
		 * an enumeration, the {@link Enum#toString()} method is used on the
		 * enumeration to resolve the enumeration to a {link String} key.
		 * 
		 * @param aKey The key for which to add the element.
		 * @param aValue The value to be related with the given key.
		 * 
		 * @return The value being replaced by the provided value or null if
		 *         none value has been replaced.
		 */
		default String put( Object aKey, String aValue ) {
			return put( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ), aValue );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Class<?> put( Object[] aPathElements, Class<?> aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * Puts the key/value-pair from the provided {@link Property}.
		 * 
		 * @param aProperty The property's key/value to be inserted.
		 * 
		 * @return The property value overwritten by the provided property.
		 */
		default String put( Property aProperty ) {
			return put( aProperty.getKey(), aProperty.getValue() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Class<?> put( Relation<String, Class<?>> aRelation ) {
			return put( aRelation.getKey(), aRelation.getValue() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Class<?> put( String[] aPathElements, Class<?> aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * This method inserts all elements (key/value-pairs) found in the
		 * provided {@link ClassStructMap} instances of interoperability
		 * reasons.
		 * 
		 * @param aProperties A {@link ClassStructMap} containing the key/value
		 *        pairs to be inserted.
		 */
		default void putAll( ClassStructMap aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( (String) eKey ) );
			}
		}

		/**
		 * This method inserts all elements (key/value-pairs) found in the
		 * provided {@link java.util.Properties} instances of interoperability
		 * reasons.
		 * 
		 * @param aProperties A {@link java.util.Properties} containing the
		 *        key/value-pairs to be inserted.
		 */
		default void putAll( java.util.Properties aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( (String) eKey ) );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default void putAll( Map<? extends String, ? extends Class<?>> aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( (String) eKey ) );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( Collection<?> aPathElements, int aIndex, PathMap<Class<?>> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( int aIndex, Object aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( int aIndex, PathMap<Class<?>> aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( Object aPath, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( Object aPath, int aIndex, PathMap<Class<?>> aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( Object[] aPathElements, int aIndex, PathMap<Class<?>> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( String aPath, int aIndex, Object aDir ) {
			final ClassStructMap theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( String aPath, int aIndex, PathMap<Class<?>> aDir ) {
			final ClassStructMap theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap putDirAt( String[] aPathElements, int aIndex, PathMap<Class<?>> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeAll( Collection<?> aPathQueryElements ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeAll( Object... aPathQueryElements ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeAll( Object aPathQuery ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeAll( aPathQuery ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeAll( Pattern aRegExp ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeAll( aRegExp ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeAll( String... aPathQueryElements ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeAll( String aPathQuery ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeAll( aPathQuery ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeDirAt( int aIndex ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeDirAt( aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeDirAt( Object aPath, int aIndex ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeDirAt( aPath, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeDirAt( Object[] aPathElements, int aIndex ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeDirAt( aPathElements, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeDirAt( String aPath, int aIndex ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeDirAt( aPath, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeDirAt( String[] aPathElements, int aIndex ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeDirAt( aPathElements, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeFrom( Collection<?> aPathElements ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeFrom( Object... aPathElements ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeFrom( Object aPath ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeFrom( aPath ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeFrom( String aPath ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeFrom( aPath ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removeFrom( String... aPathElements ) {
			return new ClassStructMapImpl( MutablePathMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removePaths( Collection<?> aPaths ) {
			return new ClassStructMapImpl( MutablePathMap.super.removePaths( aPaths ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ClassStructMap removePaths( String... aPaths ) {
			return new ClassStructMapImpl( MutablePathMap.super.removePaths( aPaths ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Map<String, String> toDump( Map<String, String> aDump ) {
			Class<?> eValue;
			for ( String eKey : keySet() ) {
				eValue = get( eKey );
				aDump.put( eKey, eValue != null ? eValue.getName() : null );
			}
			return aDump;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default boolean containsValue( Object value ) {
		return values().contains( value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDirAt( Collection<?> aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDirAt( Object aPath, int aIndex ) {
		return getDirAt( toPath( aPath ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDirAt( Object[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDirAt( String[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap query( Pattern aRegExp ) {
		return new ClassStructMapImpl( PathMap.super.query( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap query( String aPathQuery ) {
		return new ClassStructMapImpl( PathMap.super.query( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		return new ClassStructMapImpl( PathMap.super.queryBetween( aFromPath, aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		return new ClassStructMapImpl( PathMap.super.queryBetween( aFromPath, aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap queryFrom( Pattern aRegExp, String aFromPath ) {
		return new ClassStructMapImpl( PathMap.super.queryFrom( aRegExp, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap queryFrom( String aPathQuery, String aFromPath ) {
		return new ClassStructMapImpl( PathMap.super.queryFrom( aPathQuery, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap queryTo( Pattern aRegExp, String aToPath ) {
		return new ClassStructMapImpl( PathMap.super.queryTo( aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap queryTo( String aPathQuery, String aToPath ) {
		return new ClassStructMapImpl( PathMap.super.queryTo( aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap retrieveBetween( Object aFromPath, Object aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap retrieveBetween( String aFromPath, String aToPath ) {
		final ClassStructMap thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap retrieveBetween( String[] aFromPath, String[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap retrieveFrom( Object aParentPath ) {
		return retrieveFrom( toPath( aParentPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	ClassStructMap retrieveFrom( String aFromPath );

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap getDir( Collection<?> aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDir( Object aPath ) {
		return getDir( toPath( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDir( Object... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDir( String aPath ) {
		return retrieveFrom( aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap getDir( String... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap[] getDirs( Collection<?> aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap[] getDirs( Object aPath ) {
		return getDirs( toPath( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap[] getDirs( Object... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap[] getDirs( String aPath ) {
		final ClassStructMap[] theDirs;
		if ( isIndexDir( aPath ) ) {
			final int[] theIndexes = getDirIndexes( aPath );
			theDirs = new ClassStructMap[theIndexes[theIndexes.length - 1] + 1];
			for ( int i : theIndexes ) {
				theDirs[theIndexes[i]] = getDirAt( aPath, theIndexes[i] );
			}
		}
		else {
			theDirs = new ClassStructMap[] { getDir( aPath ) };
		}
		return theDirs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap[] getDirs( String... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */

	@Override
	default ClassStructMap retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap retrieveTo( Object aToPath ) {
		return retrieveTo( toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	ClassStructMap retrieveTo( String aToPath );

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ClassStructMap retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * This method creates a {@link Map} instance from this
	 * {@link ClassStructMap} instance's elements (key/value-pairs) as of
	 * interoperability reasons.
	 * 
	 * @return A {@link Map} object from the herein contained key/value-pairs .
	 */
	@Override
	default Map<String, Class<?>> toMap() {
		final Map<String, Class<?>> theProperties = new HashMap<>();
		for ( String eKey : keySet() ) {
			theProperties.put( eKey, get( eKey ) );
		}
		return theProperties;
	}

	/**
	 * This method creates a {@link java.util.Properties} instance from this
	 * {@link Properties} instance's elements (key/value-pairs) as of
	 * interoperability reasons.
	 * 
	 * @return A {@link java.util.Properties} object from the herein contained
	 *         key/value-pairs .
	 */
	default java.util.Properties toProperties() {
		final java.util.Properties theProperties = new java.util.Properties();
		for ( String eKey : keySet() ) {
			theProperties.put( toPropertyPath( eKey ), get( eKey ) );
		}
		return theProperties;
	}

	// -------------------------------------------------------------------------

	/**
	 * Returns the array selector used in paths to identify an array type.
	 * 
	 * @return The array selector denoting array types.
	 */
	default String getArraySelector() {
		return ARRAY_SELECTOR;
	}

	/**
	 * Determines if the elements below the root path can be treated as an
	 * array.
	 *
	 * @return True in case we have an array structure below the root path or
	 *         not.
	 */
	default boolean isArrayType() {
		return isArrayType( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as an array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArrayType( String... aPathElements ) {
		return isArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as an array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArrayType( Collection<?> aPathElements ) {
		return isArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as an
	 * array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArrayType( Object aPath ) {
		return isArrayType( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as an array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArrayType( Object... aPathElements ) {
		return isArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as an
	 * array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArrayType( String aPath ) {
		return isSimpleArrayType( aPath ) || isCompositeArrayDir( aPath );
	}

	// -------------------------------------------------------------------------

	/**
	 * Determines if the elements below the root path can be treated as a simple
	 * type array.
	 *
	 * @return True in case we have a simple type types) array structure below
	 *         the root path or not.
	 */
	default boolean isSimpleArrayType() {
		return isSimpleArrayType( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * simple type array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a simple type types) array structure below
	 *         the path or not.
	 */
	default boolean isSimpleArrayType( Object aPath ) {
		return isSimpleArrayType( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a simple type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a simple type types) array structure below
	 *         the path or not.
	 */
	default boolean isSimpleArrayType( Object... aPathElements ) {
		return isSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a simple type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a simple type types) array structure below
	 *         the path or not.
	 */
	default boolean isSimpleArrayType( String... aPathElements ) {
		return isSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a simple type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a simple type types) array structure below
	 *         the path or not.
	 */
	default boolean isSimpleArrayType( Collection<?> aPathElements ) {
		return isSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * simple type array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a simple type types) array structure below
	 *         the path or not.
	 */
	default boolean isSimpleArrayType( String aPath ) {
		aPath = toNormalizedPath( aPath );

		// Array type? |-->
		Class<?> theClass = get( aPath );
		if ( theClass != null && theClass.isArray() ) {
			theClass = theClass.getComponentType();
			if ( SimpleType.isSimpleArrayType( theClass ) ) {
				return true;
			}
		}
		// Array type? <--|

		// Array notation? |-->
		theClass = get( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
		if ( theClass != null ) {
			if ( SimpleType.isSimpleType( theClass ) ) {
				return true;
			}
		}
		// Array notation? <--|

		return false;
	}

	/**
	 * Retrieves the simple type of the array represented by the root path.
	 *
	 * @return The array's simple type in case we have a simple type array
	 *         structure below the root path, else null
	 */
	default Class<?> getSimpleArrayType() {
		return getSimpleArrayType( getRootPath() );
	}

	/**
	 * Retrieves the simple type of the array represented by the given path.
	 *
	 * @param aPath the path representing an array of simple type.
	 * 
	 * @return The array's simple type in case we have a simple type array
	 *         structure below the path, else null.
	 */
	default Class<?> getSimpleArrayType( Object aPath ) {
		return getSimpleArrayType( toPath( aPath ) );
	}

	/**
	 * Retrieves the simple type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of simple type.
	 * 
	 * @return The array's simple type in case we have a simple type array
	 *         structure below the path, else null.
	 */
	default Class<?> getSimpleArrayType( Object... aPathElements ) {
		return getSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the simple type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of simple type.
	 * 
	 * @return The array's simple type in case we have a simple type array
	 *         structure below the path, else null.
	 */
	default Class<?> getSimpleArrayType( String... aPathElements ) {
		return getSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the simple type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of simple type.
	 * 
	 * @return The array's simple type in case we have a simple type array
	 *         structure below the path, else null.
	 */
	default Class<?> getSimpleArrayType( Collection<?> aPathElements ) {
		return getSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the simple type of the array represented by the given path.
	 *
	 * @param aPath the path representing an array of simple type.
	 * 
	 * @return The array's simple type in case we have a simple type array
	 *         structure below the path, else null.
	 */
	default Class<?> getSimpleArrayType( String aPath ) {
		aPath = toNormalizedPath( aPath );

		// Array type? |-->
		Class<?> theClass = get( aPath );
		if ( theClass != null && theClass.isArray() ) {
			theClass = theClass.getComponentType();
			if ( SimpleType.isSimpleArrayType( theClass ) ) {
				return theClass;
			}
		}
		// Array type? <--|

		// Array notation? |-->
		theClass = get( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
		if ( theClass != null ) {
			if ( SimpleType.isSimpleType( theClass ) ) {
				return theClass;
			}
		}
		// Array notation? <--|

		throw new IllegalArgumentException( "The provided path <" + aPath + "> does not direct to a simple array type!" );
	}

	// -------------------------------------------------------------------------

	/**
	 * Determines if the elements below the root path can be treated as a simple
	 * type.
	 *
	 * @return True in case we have a simple type types) below the root path or
	 *         not.
	 */
	default boolean isSimpleType() {
		return isSimpleType( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * simple type.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a simple type types) below the path or not.
	 */
	default boolean isSimpleType( Object aPath ) {
		return isSimpleType( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a simple type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a simple type types) below the path or not.
	 */
	default boolean isSimpleType( Object... aPathElements ) {
		return isSimpleType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a simple type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a simple type types) below the path or not.
	 */
	default boolean isSimpleType( String... aPathElements ) {
		return isSimpleType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a simple type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a simple type types) below the path or not.
	 */
	default boolean isSimpleType( Collection<?> aPathElements ) {
		return isSimpleType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * simple type.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a simple type types) below the path or not.
	 */
	default boolean isSimpleType( String aPath ) {
		if ( !isSimpleArrayType() ) {
			aPath = toNormalizedPath( aPath );
			final Class<?> theClass = get( aPath );
			if ( theClass != null ) {
				return SimpleType.isSimpleType( theClass );
			}
		}
		return false;
	}

	/**
	 * Retrieves the simple type represented by the root path.
	 * 
	 * @return The simple type in case we have a simple type types) structure
	 *         below the root path, else null
	 */
	default Class<?> getSimpleType() {
		return getSimpleArrayType( getRootPath() );
	}

	/**
	 * Retrieves the simple type represented by the given path.
	 *
	 * @param aPath the path representing an array of simple type.
	 * 
	 * @return The simple type in case we have a simple type types) below the
	 *         path, else null.
	 */
	default Class<?> getSimpleType( Object aPath ) {
		return getSimpleArrayType( toPath( aPath ) );
	}

	/**
	 * Retrieves the simple type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of simple type.
	 * 
	 * @return The simple type in case we have a simple type types) below the
	 *         path, else null.
	 */
	default Class<?> getSimpleType( Object... aPathElements ) {
		return getSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the simple type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of simple type.
	 * 
	 * @return The simple type in case we have a simple type types) below the
	 *         path, else null.
	 */
	default Class<?> getSimpleType( String... aPathElements ) {
		return getSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the simple type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of simple type.
	 * 
	 * @return The simple type in case we have a simple type types) below the
	 *         path, else null.
	 */
	default Class<?> getSimpleType( Collection<?> aPathElements ) {
		return getSimpleArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the simple type represented by the given path.
	 *
	 * @param aPath the path representing an array of simple type.
	 * 
	 * @return The simple type in case we have a simple type types) below the
	 *         path, else null.
	 */
	default Class<?> getSimpleType( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final Class<?> theClass = get( aPath );
		return SimpleType.isSimpleType( theClass ) ? theClass : null;
	}

	// -------------------------------------------------------------------------

	/**
	 * Determines if the elements below the root path can be treated as a
	 * primitive type array.
	 *
	 * @return True in case we have a primitive type types) array structure
	 *         below the root path or not.
	 */
	default boolean isPrimitiveArrayType() {
		return isPrimitiveArrayType( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * primitive type array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a primitive type types) array structure
	 *         below the path or not.
	 */
	default boolean isPrimitiveArrayType( Object aPath ) {
		return isPrimitiveArrayType( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a primitive type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a primitive type types) array structure
	 *         below the path or not.
	 */
	default boolean isPrimitiveArrayType( Object... aPathElements ) {
		return isPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a primitive type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a primitive type types) array structure
	 *         below the path or not.
	 */
	default boolean isPrimitiveArrayType( String... aPathElements ) {
		return isPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a primitive type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a primitive type types) array structure
	 *         below the path or not.
	 */
	default boolean isPrimitiveArrayType( Collection<?> aPathElements ) {
		return isPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * primitive type array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a primitive type types) array structure
	 *         below the path or not.
	 */
	default boolean isPrimitiveArrayType( String aPath ) {
		aPath = toNormalizedPath( aPath );

		// Array type? |-->
		Class<?> theClass = get( aPath );
		if ( theClass != null && theClass.isArray() ) {
			theClass = theClass.getComponentType();
			if ( SimpleType.isPrimitiveArrayType( theClass ) ) {
				return true;
			}
		}
		// Array type? <--|

		// Array notation? |-->
		theClass = get( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
		if ( theClass != null ) {
			if ( SimpleType.isPrimitiveType( theClass ) ) {
				return true;
			}
		}
		// Array notation? <--|

		return false;
	}

	/**
	 * Retrieves the primitive type of the array represented by the root path.
	 *
	 * @return The array's primitive type in case we have a primitive type array
	 *         structure below the root path, else null
	 */
	default Class<?> getPrimitiveArrayType() {
		return getPrimitiveArrayType( getRootPath() );
	}

	/**
	 * Retrieves the primitive type of the array represented by the given path.
	 *
	 * @param aPath the path representing an array of primitive type.
	 * 
	 * @return The array's primitive type in case we have a primitive type array
	 *         structure below the path, else null.
	 */
	default Class<?> getPrimitiveArrayType( Object aPath ) {
		return getPrimitiveArrayType( toPath( aPath ) );
	}

	/**
	 * Retrieves the primitive type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of primitive type.
	 * 
	 * @return The array's primitive type in case we have a primitive type array
	 *         structure below the path, else null.
	 */
	default Class<?> getPrimitiveArrayType( Object... aPathElements ) {
		return getPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the primitive type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of primitive type.
	 * 
	 * @return The array's primitive type in case we have a primitive type array
	 *         structure below the path, else null.
	 */
	default Class<?> getPrimitiveArrayType( String... aPathElements ) {
		return getPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the primitive type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of primitive type.
	 * 
	 * @return The array's primitive type in case we have a primitive type array
	 *         structure below the path, else null.
	 */
	default Class<?> getPrimitiveArrayType( Collection<?> aPathElements ) {
		return getPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the primitive type of the array represented by the given path.
	 *
	 * @param aPath the path representing an array of primitive type.
	 * 
	 * @return The array's primitive type in case we have a primitive type array
	 *         structure below the path, else null.
	 */
	default Class<?> getPrimitiveArrayType( String aPath ) {
		aPath = toNormalizedPath( aPath );

		// Array type? |-->
		Class<?> theClass = get( aPath );
		if ( theClass != null && theClass.isArray() ) {
			theClass = theClass.getComponentType();
			if ( SimpleType.isPrimitiveArrayType( theClass ) ) {
				return theClass;
			}
		}
		// Array type? <--|

		// Array notation? |-->
		theClass = get( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
		if ( theClass != null ) {
			if ( SimpleType.isPrimitiveType( theClass ) ) {
				return theClass;
			}
		}
		// Array notation? <--|

		throw new IllegalArgumentException( "The provided path <" + aPath + "> does not direct to a primitive array type!" );
	}

	// -------------------------------------------------------------------------

	/**
	 * Determines if the elements below the root path can be treated as a
	 * primitive type.
	 *
	 * @return True in case we have a primitive type types) below the root path
	 *         or not.
	 */
	default boolean isPrimitiveType() {
		return isPrimitiveType( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * primitive type.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a primitive type types) below the path or
	 *         not.
	 */
	default boolean isPrimitiveType( Object aPath ) {
		return isPrimitiveType( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a primitive type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a primitive type types) below the path or
	 *         not.
	 */
	default boolean isPrimitiveType( Object... aPathElements ) {
		return isPrimitiveType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a primitive type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a primitive type types) below the path or
	 *         not.
	 */
	default boolean isPrimitiveType( String... aPathElements ) {
		return isPrimitiveType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a primitive type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a primitive type types) below the path or
	 *         not.
	 */
	default boolean isPrimitiveType( Collection<?> aPathElements ) {
		return isPrimitiveType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * primitive type.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a primitive type types) below the path or
	 *         not.
	 */
	default boolean isPrimitiveType( String aPath ) {
		if ( !isPrimitiveArrayType() ) {
			aPath = toNormalizedPath( aPath );
			final Class<?> theClass = get( aPath );
			if ( theClass != null ) {
				return SimpleType.isPrimitiveType( theClass );
			}
		}
		return false;
	}

	/**
	 * Retrieves the primitive type represented by the root path.
	 * 
	 * @return The primitive type in case we have a primitive type types)
	 *         structure below the root path, else null
	 */
	default Class<?> getPrimitiveType() {
		return getPrimitiveArrayType( getRootPath() );
	}

	/**
	 * Retrieves the primitive type represented by the given path.
	 *
	 * @param aPath the path representing an array of primitive type.
	 * 
	 * @return The primitive type in case we have a primitive type types) below
	 *         the path, else null.
	 */
	default Class<?> getPrimitiveType( Object aPath ) {
		return getPrimitiveArrayType( toPath( aPath ) );
	}

	/**
	 * Retrieves the primitive type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of primitive type.
	 * 
	 * @return The primitive type in case we have a primitive type types) below
	 *         the path, else null.
	 */
	default Class<?> getPrimitiveType( Object... aPathElements ) {
		return getPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the primitive type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of primitive type.
	 * 
	 * @return The primitive type in case we have a primitive type types) below
	 *         the path, else null.
	 */
	default Class<?> getPrimitiveType( String... aPathElements ) {
		return getPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the primitive type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of primitive type.
	 * 
	 * @return The primitive type in case we have a primitive type types) below
	 *         the path, else null.
	 */
	default Class<?> getPrimitiveType( Collection<?> aPathElements ) {
		return getPrimitiveArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the primitive type represented by the given path.
	 *
	 * @param aPath the path representing an array of primitive type.
	 * 
	 * @return The primitive type in case we have a primitive type types) below
	 *         the path, else null.
	 */
	default Class<?> getPrimitiveType( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final Class<?> theClass = get( aPath );
		return SimpleType.isPrimitiveType( theClass ) ? theClass : null;
	}

	// -------------------------------------------------------------------------

	/**
	 * Determines if the elements below the root path can be treated as a
	 * wrapper type array.
	 *
	 * @return True in case we have a wrapper type array structure below the
	 *         root path or not.
	 */
	default boolean isWrapperArrayType() {
		return isWrapperArrayType( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * wrapper type array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a wrapper type array structure below the
	 *         path or not.
	 */
	default boolean isWrapperArrayType( Object aPath ) {
		return isWrapperArrayType( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a wrapper type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a wrapper type array structure below the
	 *         path or not.
	 */
	default boolean isWrapperArrayType( Object... aPathElements ) {
		return isWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a wrapper type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a wrapper type array structure below the
	 *         path or not.
	 */
	default boolean isWrapperArrayType( String... aPathElements ) {
		return isWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a wrapper type array.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a wrapper type array structure below the
	 *         path or not.
	 */
	default boolean isWrapperArrayType( Collection<?> aPathElements ) {
		return isWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * wrapper type array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a wrapper type array structure below the
	 *         path or not.
	 */
	default boolean isWrapperArrayType( String aPath ) {
		aPath = toNormalizedPath( aPath );

		// Array type? |-->
		Class<?> theClass = get( aPath );
		if ( theClass != null && theClass.isArray() ) {
			theClass = theClass.getComponentType();
			if ( SimpleType.isWrapperArrayType( theClass ) ) {
				return true;
			}
		}
		// Array type? <--|

		// Array notation? |-->
		theClass = get( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
		if ( theClass != null ) {
			if ( SimpleType.isWrapperType( theClass ) ) {
				return true;
			}
		}
		// Array notation? <--|

		return false;
	}

	/**
	 * Retrieves the wrapper type of the array represented by the root path.
	 *
	 * @return The array's wrapper type in case we have a wrapper type array
	 *         structure below the root path, else null
	 */
	default Class<?> getWrapperArrayType() {
		return getWrapperArrayType( getRootPath() );
	}

	/**
	 * Retrieves the wrapper type of the array represented by the given path.
	 *
	 * @param aPath the path representing an array of wrapper type.
	 * 
	 * @return The array's wrapper type in case we have a wrapper type array
	 *         structure below the path, else null.
	 */
	default Class<?> getWrapperArrayType( Object aPath ) {
		return getWrapperArrayType( toPath( aPath ) );
	}

	/**
	 * Retrieves the wrapper type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of wrapper type.
	 * 
	 * @return The array's wrapper type in case we have a wrapper type array
	 *         structure below the path, else null.
	 */
	default Class<?> getWrapperArrayType( Object... aPathElements ) {
		return getWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the wrapper type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of wrapper type.
	 * 
	 * @return The array's wrapper type in case we have a wrapper type array
	 *         structure below the path, else null.
	 */
	default Class<?> getWrapperArrayType( String... aPathElements ) {
		return getWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the wrapper type of the array represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of wrapper type.
	 * 
	 * @return The array's wrapper type in case we have a wrapper type array
	 *         structure below the path, else null.
	 */
	default Class<?> getWrapperArrayType( Collection<?> aPathElements ) {
		return getWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the wrapper type of the array represented by the given path.
	 *
	 * @param aPath the path representing an array of wrapper type.
	 * 
	 * @return The array's wrapper type in case we have a wrapper type array
	 *         structure below the path, else null.
	 */
	default Class<?> getWrapperArrayType( String aPath ) {
		aPath = toNormalizedPath( aPath );

		// Array type? |-->
		Class<?> theClass = get( aPath );
		if ( theClass != null && theClass.isArray() ) {
			theClass = theClass.getComponentType();
			if ( SimpleType.isWrapperArrayType( theClass ) ) {
				return theClass;
			}
		}
		// Array type? <--|

		// Array notation? |-->
		theClass = get( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
		if ( theClass != null ) {
			if ( SimpleType.isWrapperType( theClass ) ) {
				return theClass;
			}
		}
		// Array notation? <--|

		throw new IllegalArgumentException( "The provided path <" + aPath + "> does not direct to a wrapper array type!" );
	}

	// -------------------------------------------------------------------------

	/**
	 * Determines if the elements below the root path can be treated as a
	 * wrapper type.
	 *
	 * @return True in case we have a wrapper type below the root path or not.
	 */
	default boolean isWrapperType() {
		return isWrapperType( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * wrapper type.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a wrapper type below the path or not.
	 */
	default boolean isWrapperType( Object aPath ) {
		return isWrapperType( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a wrapper type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a wrapper type below the path or not.
	 */
	default boolean isWrapperType( Object... aPathElements ) {
		return isWrapperType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a wrapper type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a wrapper type below the path or not.
	 */
	default boolean isWrapperType( String... aPathElements ) {
		return isWrapperType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a wrapper type.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a wrapper type below the path or not.
	 */
	default boolean isWrapperType( Collection<?> aPathElements ) {
		return isWrapperType( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * wrapper type.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a wrapper type below the path or not.
	 */
	default boolean isWrapperType( String aPath ) {
		if ( !isWrapperArrayType() ) {
			aPath = toNormalizedPath( aPath );
			final Class<?> theClass = get( aPath );
			if ( theClass != null ) {
				return SimpleType.isWrapperType( theClass );
			}
		}
		return false;
	}

	/**
	 * Retrieves the wrapper type represented by the root path.
	 * 
	 * @return The wrapper type in case we have a wrapper type types) structure
	 *         below the root path, else null
	 */
	default Class<?> getWrapperType() {
		return getWrapperArrayType( getRootPath() );
	}

	/**
	 * Retrieves the wrapper type represented by the given path.
	 *
	 * @param aPath the path representing an array of wrapper type.
	 * 
	 * @return The wrapper type in case we have a wrapper type types) below the
	 *         path, else null.
	 */
	default Class<?> getWrapperType( Object aPath ) {
		return getWrapperArrayType( toPath( aPath ) );
	}

	/**
	 * Retrieves the wrapper type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of wrapper type.
	 * 
	 * @return The wrapper type in case we have a wrapper type types) below the
	 *         path, else null.
	 */
	default Class<?> getWrapperType( Object... aPathElements ) {
		return getWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the wrapper type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of wrapper type.
	 * 
	 * @return The wrapper type in case we have a wrapper type types) below the
	 *         path, else null.
	 */
	default Class<?> getWrapperType( String... aPathElements ) {
		return getWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the wrapper type represented by the given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of wrapper type.
	 * 
	 * @return The wrapper type in case we have a wrapper type types) below the
	 *         path, else null.
	 */
	default Class<?> getWrapperType( Collection<?> aPathElements ) {
		return getWrapperArrayType( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the wrapper type represented by the given path.
	 *
	 * @param aPath the path representing an array of wrapper type.
	 * 
	 * @return The wrapper type in case we have a wrapper type types) below the
	 *         path, else null.
	 */
	default Class<?> getWrapperType( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final Class<?> theClass = get( aPath );
		return SimpleType.isWrapperType( theClass ) ? theClass : null;
	}

	// -------------------------------------------------------------------------

	//	/**
	//	 * Determines if the elements below the root path can be treated as a
	//	 * composite type.
	//	 *
	//	 * 
	//	 * @return True in case we have a composite type below the root path or not.
	//	 */
	//	default boolean isCompositeType() {
	//		return isCompositeType( getRootPath() );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path can be treated as a
	//	 * composite type.
	//	 *
	//	 * @param aPath the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type below the path or not.
	//	 */
	//	default boolean isCompositeType( Object aPath ) {
	//		return isCompositeType( toPath( aPath ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path elements can be treated
	//	 * as a composite type.
	//	 *
	//	 * @param aPathElements the path elements of the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type below the path or not.
	//	 */
	//	default boolean isCompositeType( Object... aPathElements ) {
	//		return isCompositeType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path elements can be treated
	//	 * as a composite type.
	//	 *
	//	 * @param aPathElements the path elements of the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type below the path or not.
	//	 */
	//	default boolean isCompositeType( String... aPathElements ) {
	//		return isCompositeType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path elements can be treated
	//	 * as a composite type.
	//	 *
	//	 * @param aPathElements the path elements of the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type below the path or not.
	//	 */
	//	default boolean isCompositeType( Collection<?> aPathElements ) {
	//		return isCompositeType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path can be treated as a
	//	 * composite type.
	//	 *
	//	 * @param aPath the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type below the path or not.
	//	 */
	//	default boolean isCompositeType( String aPath ) {
	//		Class<?> theClass = get( aPath );
	//		if ( theClass != null ) {
	//			return !SimpleType.isSimpleType( theClass );
	//		}
	//		return isDir( aPath );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the root path can be treated as a
	//	 * composite type array.
	//	 *
	//	 * 
	//	 * @return True in case we have a composite type array structure below the
	//	 *         root path or not.
	//	 */
	//	default boolean isCompositeArrayType() {
	//		return isCompositeArrayType( getRootPath() );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path can be treated as a
	//	 * composite type array.
	//	 *
	//	 * @param aPath the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type array structure below the
	//	 *         path or not.
	//	 */
	//	default boolean isCompositeArrayType( Object aPath ) {
	//		return isCompositeArrayType( toPath( aPath ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path elements can be treated
	//	 * as a composite type array.
	//	 *
	//	 * @param aPathElements the path elements of the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type array structure below the
	//	 *         path or not.
	//	 */
	//	default boolean isCompositeArrayType( Object... aPathElements ) {
	//		return isCompositeArrayType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path elements can be treated
	//	 * as a composite type array.
	//	 *
	//	 * @param aPathElements the path elements of the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type array structure below the
	//	 *         path or not.
	//	 */
	//	default boolean isCompositeArrayType( String... aPathElements ) {
	//		return isCompositeArrayType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path elements can be treated
	//	 * as a composite type array.
	//	 *
	//	 * @param aPathElements the path elements of the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type array structure below the
	//	 *         path or not.
	//	 */
	//	default boolean isCompositeArrayType( Collection<?> aPathElements ) {
	//		return isCompositeArrayType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines if the elements below the given path can be treated as a
	//	 * composite type array.
	//	 *
	//	 * @param aPath the path which to test.
	//	 * 
	//	 * @return True in case we have a composite type array structure below the
	//	 *         path or not.
	//	 */
	//	default boolean isCompositeArrayType( String aPath ) {
	//		// Array type? |-->
	//		Class<?> theClass = get( aPath );
	//		if ( theClass != null && theClass.isArray() ) {
	//			theClass = theClass.getComponentType();
	//			return !SimpleType.isSimpleArrayType( theClass );
	//		}
	//		// Array type? <--|
	//
	//		// Array notation? |-->
	//		theClass = get( aPath.endsWith( getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
	//		if ( theClass != null ) {
	//			return !SimpleType.isSimpleType( theClass );
	//		}
	//		// Array notation? <--|
	//		return false;
	//	}
	//
	//	/**
	//	 * Retrieves the composite type of the array represented by the root path.
	//	 *
	//	 * 
	//	 * @return The array's composite type in case we have a composite type array
	//	 *         structure below the root path, else null
	//	 */
	//	default Class<?> getCompositeArrayType() {
	//		return getCompositeArrayType( getRootPath() );
	//	}
	//
	//	/**
	//	 * Retrieves the composite type of the array represented by the given path.
	//	 *
	//	 * @param aPath the path representing an array of composite types.
	//	 * 
	//	 * @return The array's composite type in case we have a composite type array
	//	 *         structure below the path, else null.
	//	 */
	//	default Class<?> getCompositeArrayType( Object aPath ) {
	//		return getCompositeArrayType( toPath( aPath ) );
	//	}
	//
	//	/**
	//	 * Retrieves the composite type of the array represented by the given path.
	//	 *
	//	 * @param aPathElements the path elements of the path representing an array
	//	 *        of composite types.
	//	 * 
	//	 * @return The array's composite type in case we have a composite type array
	//	 *         structure below the path, else null.
	//	 */
	//	default Class<?> getCompositeArrayType( Object... aPathElements ) {
	//		return getCompositeArrayType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Retrieves the composite type of the array represented by the given path.
	//	 *
	//	 * @param aPathElements the path elements of the path representing an array
	//	 *        of composite types.
	//	 * 
	//	 * @return The array's composite type in case we have a composite type array
	//	 *         structure below the path, else null.
	//	 */
	//	default Class<?> getCompositeArrayType( String... aPathElements ) {
	//		return getCompositeArrayType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Retrieves the composite type of the array represented by the given path.
	//	 *
	//	 * @param aPathElements the path elements of the path representing an array
	//	 *        of composite types.
	//	 * 
	//	 * @return The array's composite type in case we have a composite type array
	//	 *         structure below the path, else null.
	//	 */
	//	default Class<?> getCompositeArrayType( Collection<?> aPathElements ) {
	//		return getCompositeArrayType( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Retrieves the composite type of the array represented by the given path.
	//	 *
	//	 * @param aPath the path representing an array of composite types.
	//	 * 
	//	 * @return The array's composite type in case we have a composite type array
	//	 *         structure below the path, else null.
	//	 */
	//	default Class<?> getCompositeArrayType( String aPath ) {
	//
	//		// Array type? |-->
	//		Class<?> theClass = get( aPath );
	//		if ( theClass != null && theClass.isArray() ) {
	//			theClass = theClass.getComponentType();
	//			if ( !SimpleType.isSimpleArrayType( theClass ) ) {
	//				return theClass;
	//			}
	//		}
	//		// Array type? <--|
	//
	//		// Array notation? |-->
	//		theClass = get( aPath.endsWith( getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
	//		if ( theClass != null ) {
	//			if ( !SimpleType.isSimpleType( theClass ) ) {
	//				return theClass;
	//			}
	//		}
	//		// Array notation? <--|
	//
	//		throw new IllegalArgumentException( "The provided path <" + aPath + "> does not direct to a composite array type!" );
	//	}

	/**
	 * Determines if the elements below the root path can be treated as a
	 * composite type directory.
	 *
	 * @return True in case we have a composite type directory structure below
	 *         the root path or not.
	 */
	default boolean isCompositeArrayDir() {
		return isCompositeArrayDir( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * composite type directory.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeArrayDir( Object aPath ) {
		return isCompositeArrayDir( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a composite type directory.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeArrayDir( Object... aPathElements ) {
		return isCompositeArrayDir( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a composite type directory.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeArrayDir( String... aPathElements ) {
		return isCompositeArrayDir( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a composite type directory.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeArrayDir( Collection<?> aPathElements ) {
		return isCompositeArrayDir( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * composite type directory.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeArrayDir( String aPath ) {
		aPath = toNormalizedPath( aPath );
		return isDir( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
	}

	/**
	 * Retrieves the directory of the composite type's array represented by the
	 * root path.
	 *
	 * @return The array's composite type in case we have a composite type array
	 *         structure below the root path, else null
	 */
	default ClassStructMap getCompositeArrayDir() {
		return getCompositeArrayDir( getRootPath() );
	}

	/**
	 * Retrieves the directory of the composite type's array represented by the
	 * given path.
	 *
	 * @param aPath the path representing an array of composite types.
	 * 
	 * @return The array's composite type in case we have a composite type array
	 *         structure below the path, else null.
	 */
	default ClassStructMap getCompositeArrayDir( Object aPath ) {
		return getCompositeArrayDir( toPath( aPath ) );
	}

	/**
	 * Retrieves the directory of the composite type's array represented by the
	 * given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of composite types.
	 * 
	 * @return The array's composite type in case we have a composite type array
	 *         structure below the path, else null.
	 */
	default ClassStructMap getCompositeArrayDir( Object... aPathElements ) {
		return getCompositeArrayDir( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory of the composite type's array represented by the
	 * given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of composite types.
	 * 
	 * @return The array's composite type in case we have a composite type array
	 *         structure below the path, else null.
	 */
	default ClassStructMap getCompositeArrayDir( String... aPathElements ) {
		return getCompositeArrayDir( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory of the composite type's array represented by the
	 * given path.
	 *
	 * @param aPathElements the path elements of the path representing an array
	 *        of composite types.
	 * 
	 * @return The array's composite type in case we have a composite type array
	 *         structure below the path, else null.
	 */
	default ClassStructMap getCompositeArrayDir( Collection<?> aPathElements ) {
		return getCompositeArrayDir( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory of the composite type's array represented by the
	 * given path.
	 *
	 * @param aPath the path representing an array of composite types.
	 * 
	 * @return The array's composite type in case we have a composite type array
	 *         structure below the path, else null.
	 */
	default ClassStructMap getCompositeArrayDir( String aPath ) {
		aPath = toNormalizedPath( aPath );
		if ( isCompositeArrayDir( aPath ) ) {
			return retrieveFrom( aPath.endsWith( getDelimiter() + getArraySelector() ) ? aPath : toPath( aPath, getArraySelector() ) );
		}
		return null;
	}

	// -------------------------------------------------------------------------

	/**
	 * Determines if the elements below the root path can be treated as a
	 * composite type directory.
	 *
	 * @return True in case we have a composite type directory structure below
	 *         the root path or not.
	 */
	default boolean isCompositeDir() {
		return isCompositeDir( getRootPath() );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * composite type directory.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeDir( Object aPath ) {
		return isCompositeDir( toPath( aPath ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a composite type directory.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeDir( Object... aPathElements ) {
		return isCompositeDir( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a composite type directory.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeDir( String... aPathElements ) {
		return isCompositeDir( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path elements can be treated
	 * as a composite type directory.
	 *
	 * @param aPathElements the path elements of the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeDir( Collection<?> aPathElements ) {
		return isCompositeDir( toPath( aPathElements ) );
	}

	/**
	 * Determines if the elements below the given path can be treated as a
	 * composite type directory.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have a composite type directory structure below
	 *         the path or not.
	 */
	default boolean isCompositeDir( String aPath ) {
		aPath = toNormalizedPath( aPath );
		// return isDir( aPath ) && !isCompositeArrayDir( aPath ) && !isSimpleType( aPath ) && !isSimpleArrayType( aPath );
		// return isDir( aPath ) && !isCompositeArrayDir();
		// Why is "!isCompositeArrayDir()" for root dir being tested? |--> 
		return isDir( aPath ) && !isCompositeArrayDir() && !isSimpleType( aPath ); // && !isSimpleArrayType( aPath );
		// Why is "!isCompositeArrayDir()" for root dir being tested? <--|
	}

	/**
	 * Retrieves the directory of the composite type represented by the root
	 * path.
	 *
	 * @return The 's composite type in case we have a composite type structure
	 *         below the root path, else null
	 */
	default ClassStructMap getCompositeDir() {
		return getCompositeDir( getRootPath() );
	}

	/**
	 * Retrieves the directory of the composite type represented by the given
	 * path.
	 *
	 * @param aPath the path representing an of composite types.
	 * 
	 * @return The 's composite type in case we have a composite type structure
	 *         below the path, else null.
	 */
	default ClassStructMap getCompositeDir( Object aPath ) {
		return getCompositeDir( toPath( aPath ) );
	}

	/**
	 * Retrieves the directory of the composite type represented by the given
	 * path.
	 *
	 * @param aPathElements the path elements of the path representing an of
	 *        composite types.
	 * 
	 * @return The 's composite type in case we have a composite type structure
	 *         below the path, else null.
	 */
	default ClassStructMap getCompositeDir( Object... aPathElements ) {
		return getCompositeDir( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory of the composite type represented by the given
	 * path.
	 *
	 * @param aPathElements the path elements of the path representing an of
	 *        composite types.
	 * 
	 * @return The 's composite type in case we have a composite type structure
	 *         below the path, else null.
	 */
	default ClassStructMap getCompositeDir( String... aPathElements ) {
		return getCompositeDir( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory of the composite type represented by the given
	 * path.
	 *
	 * @param aPathElements the path elements of the path representing an of
	 *        composite types.
	 * 
	 * @return The 's composite type in case we have a composite type structure
	 *         below the path, else null.
	 */
	default ClassStructMap getCompositeDir( Collection<?> aPathElements ) {
		return getCompositeDir( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory of the composite type represented by the given
	 * path.
	 *
	 * @param aPath the path representing an of composite types.
	 * 
	 * @return The 's composite type in case we have a composite type structure
	 *         below the path, else null.
	 */
	default ClassStructMap getCompositeDir( String aPath ) {
		aPath = toNormalizedPath( aPath );
		return isCompositeDir( aPath ) ? retrieveFrom( aPath ) : null;
	}
}
