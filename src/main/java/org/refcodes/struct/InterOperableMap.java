// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;

import org.refcodes.data.BooleanLiterals;
import org.refcodes.data.Literal;
import org.refcodes.mixin.AliasAccessor;

/**
 * The {@link InterOperableMap} provides convenience accessor default methods
 * for supporting primitive data types.
 */
public interface InterOperableMap<T> extends PathMap<T> {

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link InterOperableMapBuilder} provides convenience accessor default
	 * methods for supporting primitive data types.
	 * 
	 * @param <T> The type of the values being supported by this
	 *        {@link InterOperableMapBuilder}.
	 */
	public static interface InterOperableMapBuilder<T> extends MutableInterOperableMap<T>, PathMapBuilder<T> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsert( PathMap<T> aFrom ) {
			insert( (Object) aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( Collection<?> aToPathElements, PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( Object aToPath, PathMap<T> aFrom, Object aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( Object[] aToPathElements, PathMap<T> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( String aToPath, PathMap<T> aFrom, String aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertBetween( String[] aToPathElements, PathMap<T> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			return withInsertFrom( (Object) aFrom, aFromPathElements );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( PathMap<T> aFrom, Object aFromPath ) {
			return withInsertFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( PathMap<T> aFrom, Object... aFromPathElements ) {
			return withInsertFrom( (Object) aFrom, aFromPathElements );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( PathMap<T> aFrom, String aFromPath ) {
			insertFrom( (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertFrom( PathMap<T> aFrom, String... aFromPathElements ) {
			return withInsertFrom( (Object) aFrom, aFromPathElements );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( Collection<?> aToPathElements, PathMap<T> aFrom ) {
			return withInsertTo( aToPathElements, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( Object aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( Object aToPath, PathMap<T> aFrom ) {
			return withInsertTo( aToPath, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( Object[] aToPathElements, PathMap<T> aFrom ) {
			return withInsertTo( aToPathElements, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( String aToPath, PathMap<T> aFrom ) {
			insertTo( aToPath, (Object) aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withInsertTo( String[] aToPathElements, PathMap<T> aFrom ) {
			return withInsertTo( aToPathElements, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMerge( PathMap<T> aFrom ) {
			merge( (Object) aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( Collection<?> aToPathElements, PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( Object aToPath, PathMap<T> aFrom, Object aFromPath ) {
			mergeBetween( aToPath, (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( Object[] aToPathElements, PathMap<T> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( String aToPath, PathMap<T> aFrom, String aFromPath ) {
			mergeBetween( aToPath, (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeBetween( String[] aToPathElements, PathMap<T> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			return withMergeFrom( (Object) aFrom, aFromPathElements );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( PathMap<T> aFrom, Object aFromPath ) {
			return withMergeFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( PathMap<T> aFrom, Object... aFromPathElements ) {
			return withMergeFrom( (Object) aFrom, aFromPathElements );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( PathMap<T> aFrom, String aFromPath ) {
			mergeFrom( (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeFrom( PathMap<T> aFrom, String... aFromPathElements ) {
			return withMergeFrom( (Object) aFrom, aFromPathElements );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( Collection<?> aToPathElements, PathMap<T> aFrom ) {
			return withMergeTo( aToPathElements, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( Object aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( Object aToPath, PathMap<T> aFrom ) {
			return withMergeTo( aToPath, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( Object[] aToPathElements, PathMap<T> aFrom ) {
			return withMergeTo( aToPathElements, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( String aToPath, PathMap<T> aFrom ) {
			mergeTo( aToPath, (Object) aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withMergeTo( String[] aToPathElements, PathMap<T> aFrom ) {
			return withMergeTo( aToPathElements, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPut( Collection<?> aPathElements, T aValue ) {
			put( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPut( Object[] aPathElements, T aValue ) {
			put( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPut( Relation<String, T> aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPut( String aKey, T aValue ) {
			put( aKey, aValue );
			return this;
		}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default InterOperableMapBuilder<T> withPut( Object aPath, T aValue ) {
		//		put( toPath( aPath ), aValue );
		//		return this;
		//	}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPut( String[] aPathElements, T aValue ) {
			put( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Boolean} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Boolean} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
			putBoolean( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Boolean} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutBoolean( Object aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Boolean} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Boolean} to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutBoolean( Object[] aPathElements, Boolean aValue ) {
			putBoolean( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Boolean} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutBoolean( String aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Boolean} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Boolean} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutBoolean( String[] aPathElements, Boolean aValue ) {
			putBoolean( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Byte} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Byte} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutByte( Collection<?> aPathElements, Byte aValue ) {
			putByte( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Byte} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Byte} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutByte( Object aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Byte} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Byte} to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutByte( Object[] aPathElements, Byte aValue ) {
			putByte( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Byte} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Byte} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutByte( String aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Byte} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Byte} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutByte( String[] aPathElements, Byte aValue ) {
			putByte( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Character} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Character} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutChar( Collection<?> aPathElements, Character aValue ) {
			putChar( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Character} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Character} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutChar( Object aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Character} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Character} to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutChar( Object[] aPathElements, Character aValue ) {
			putChar( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Character} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Character} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutChar( String aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Character} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Character} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutChar( String[] aPathElements, Character aValue ) {
			putChar( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Class} value is converted
		 * accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Class} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <C> InterOperableMapBuilder<T> withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
			putClass( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Class} value is converted accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Class} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <C> InterOperableMapBuilder<T> withPutClass( Object aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Class} value is converted
		 * accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Class} to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <C> InterOperableMapBuilder<T> withPutClass( Object[] aPathElements, Class<C> aValue ) {
			putClass( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Class} value is converted accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Class} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <C> InterOperableMapBuilder<T> withPutClass( String aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Class} value is converted
		 * accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Class} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <C> InterOperableMapBuilder<T> withPutClass( String[] aPathElements, Class<C> aValue ) {
			putClass( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( int aIndex, PathMap<T> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( Object aPath, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( Object[] aPathElements, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( String aPath, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;

		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withPutDirAt( String[] aPathElements, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Double} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Double} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutDouble( Collection<?> aPathElements, Double aValue ) {
			putDouble( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Double} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Double} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutDouble( Object aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Double} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Double} to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutDouble( Object[] aPathElements, Double aValue ) {
			putDouble( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Double} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Double} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutDouble( String aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Double} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Double} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutDouble( String[] aPathElements, Double aValue ) {
			putDouble( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Enum} value is converted
		 * accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Enum} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <E extends Enum<E>> InterOperableMapBuilder<T> withPutEnum( Collection<?> aPathElements, E aValue ) {
			putEnum( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Enum} value is converted accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Enum} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <E extends Enum<E>> InterOperableMapBuilder<T> withPutEnum( Object aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Enum} value is converted
		 * accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Enum} to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <E extends Enum<E>> InterOperableMapBuilder<T> withPutEnum( Object[] aPathElements, E aValue ) {
			putEnum( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Enum} value is converted accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Enum} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <E extends Enum<E>> InterOperableMapBuilder<T> withPutEnum( String aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Enum} value is converted
		 * accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Enum} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default <E extends Enum<E>> InterOperableMapBuilder<T> withPutEnum( String[] aPathElements, E aValue ) {
			putEnum( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Float} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Float} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutFloat( Collection<?> aPathElements, Float aValue ) {
			putFloat( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Float} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Float} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutFloat( Object aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Float} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Float} to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutFloat( Object[] aPathElements, Float aValue ) {
			putFloat( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Float} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Float} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutFloat( String aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Float} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Float} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutFloat( String[] aPathElements, Float aValue ) {
			putFloat( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Integer} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Integer} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutInt( Collection<?> aPathElements, Integer aValue ) {
			putInt( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Integer} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Integer} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutInt( Object aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Integer} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Integer} to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutInt( Object[] aPathElements, Integer aValue ) {
			putInt( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Integer} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Integer} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutInt( String aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Integer} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Integer} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutInt( String[] aPathElements, Integer aValue ) {
			putInt( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Long} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Long} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutLong( Collection<?> aPathElements, Long aValue ) {
			putLong( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Long} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Long} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutLong( Object aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Long} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Long} to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutLong( Object[] aPathElements, Long aValue ) {
			putLong( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Long} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Long} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutLong( String aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Long} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Long} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutLong( String[] aPathElements, Long aValue ) {
			putLong( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Short} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Short} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutShort( Collection<?> aPathElements, Short aValue ) {
			putShort( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Short} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Short} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutShort( Object aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Short} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Short} to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutShort( Object[] aPathElements, Short aValue ) {
			putShort( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Short} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Short} representation for the {@link String}
		 *        value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutShort( String aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Short} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Short} value to be associated with the path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutShort( String[] aPathElements, Short aValue ) {
			putShort( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link String} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link String} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutString( Collection<?> aPathElements, String aValue ) {
			putString( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link String} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link String} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutString( Object aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link String} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link String} to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutString( Object[] aPathElements, String aValue ) {
			putString( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link String} value is converted accordingly.
		 * 
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link String} representation for the
		 *        {@link String} value.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutString( String aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link String} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link String} value to be associated with the
		 *        path.
		 * 
		 * @return This instance for further builder method calls.
		 */
		default InterOperableMapBuilder<T> withPutString( String[] aPathElements, String aValue ) {
			putString( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default InterOperableMapBuilder<T> withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// MUTATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link InterOperableMapBuilder} provides convenience accessor default
	 * methods for supporting primitive data types.
	 */
	public static interface MutableInterOperableMap<T> extends MutablePathMap<T> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default boolean containsValue( Object value ) {
			return values().contains( value );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insert(Object)}.
		 * 
		 * @param aFrom The {@link InterOperableMapBuilder} which is to be
		 *        inspected with the therein contained values being added with
		 *        their according determined paths.
		 */
		default void insert( InterOperableMapBuilder<T> aFrom ) {
			insert( (Object) aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link InterOperableMapBuilder} which is to be
		 *        inspected with the therein contained values being added with
		 *        their according determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertBetween( String aToPath, InterOperableMapBuilder<T> aFrom, String aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link InterOperableMapBuilder} which is to be
		 *        inspected with the therein contained values being added with
		 *        their according determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertFrom( InterOperableMapBuilder<T> aFrom, String aFromPath ) {
			insertFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link InterOperableMapBuilder} which is to be
		 *        inspected with the therein contained values being added with
		 *        their according determined paths.
		 */
		default void insertTo( String aToPath, InterOperableMapBuilder<T> aFrom ) {
			insertTo( aToPath, (Object) aFrom );
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Boolean} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putBoolean( Object aKey, Boolean aValue ) {
			return putBoolean( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Boolean} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Boolean} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putBoolean( Object[] aPathElements, Boolean aValue ) {
			return putBoolean( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Boolean} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Boolean} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putBoolean( Collection<?> aPathElements, Boolean aValue ) {
			return putBoolean( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Boolean} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putBoolean( String aKey, Boolean aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Boolean.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, Boolean.TRUE.equals( aValue ) ? (T) Literal.TRUE.getValue() : (T) Literal.FALSE.getValue() );
			}
			throw new NumberFormatException( "Unable to convert a boolean from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Boolean} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Boolean} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Boolean} value to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putBoolean( String[] aPathElements, Boolean aValue ) {
			return putBoolean( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Byte} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putByte( Object aKey, Byte aValue ) {
			return putByte( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Byte} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Byte} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putByte( Object[] aPathElements, Byte aValue ) {
			return putByte( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Byte} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Byte} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putByte( Collection<?> aPathElements, Byte aValue ) {
			return putByte( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Byte} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Byte} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putByte( String aKey, Byte aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Byte.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.toString() );
			}
			throw new NumberFormatException( "Unable to convert a byte from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Byte} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Byte} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Byte} value to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putByte( String[] aPathElements, Byte aValue ) {
			return putByte( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Character} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putChar( Object aKey, Character aValue ) {
			return putChar( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Character} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Character} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putChar( Object[] aPathElements, Character aValue ) {
			return putChar( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Character} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Character} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putChar( Collection<?> aPathElements, Character aValue ) {
			return putChar( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Character} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Character} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putChar( String aKey, Character aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Character.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.toString() );
			}
			throw new NumberFormatException( "Unable to convert a character from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Character} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Character} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Character} value to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putChar( String[] aPathElements, Character aValue ) {
			return putChar( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Class} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <C> T putClass( Object aKey, Class<C> aValue ) {
			return putClass( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Class} value is converted
		 * accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Class} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <C> T putClass( Object[] aPathElements, Class<C> aValue ) {
			return putClass( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Class} value is converted
		 * accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Class} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <C> T putClass( Collection<?> aPathElements, Class<C> aValue ) {
			return putClass( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Class} value is converted accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Class} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default <C> T putClass( String aKey, Class<C> aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Class.class ) ) {
				try {
					return put( aKey, (T) aValue );
				}
				catch ( Exception e ) {
					throw new NumberFormatException( "Unable to convert a class from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">: " + e.getMessage() );
				}
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.getName() );
			}
			throw new NumberFormatException( "Unable to convert a class from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Class} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Class} value is converted
		 * accordingly.
		 * 
		 * @param <C> The type of the class in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Class} value to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <C> T putClass( String[] aPathElements, Class<C> aValue ) {
			return putClass( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Double} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putDouble( Object aKey, Double aValue ) {
			return putDouble( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Double} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Double} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putDouble( Object[] aPathElements, Double aValue ) {
			return putDouble( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Double} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Double} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putDouble( Collection<?> aPathElements, Double aValue ) {
			return putDouble( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Double} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Double} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putDouble( String aKey, Double aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Double.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.toString() );
			}
			throw new NumberFormatException( "Unable to convert a double from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Double} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Double} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Double} value to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putDouble( String[] aPathElements, Double aValue ) {
			return putDouble( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Enum} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <E extends Enum<E>> T putEnum( Object aKey, E aValue ) {
			return putEnum( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Enum} value is converted
		 * accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Enum} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <E extends Enum<E>> T putEnum( Object[] aPathElements, E aValue ) {
			return putEnum( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Enum} value is converted
		 * accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Enum} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <E extends Enum<E>> T putEnum( Collection<?> aPathElements, E aValue ) {
			return putEnum( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Enum} value is converted accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Enum} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default <E extends Enum<E>> T putEnum( String aKey, E aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Enum.class ) ) {
				try {
					return put( aKey, (T) aValue );
				}
				catch ( Exception e ) {
					throw new NumberFormatException( "Unable to convert a enumeration from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">: " + e.getMessage() );
				}
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.name() );
			}
			throw new NumberFormatException( "Unable to convert a enumeration from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Enum} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Enum} value is converted
		 * accordingly.
		 * 
		 * @param <E> The type of the enumeration in question.
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Enum} value to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default <E extends Enum<E>> T putEnum( String[] aPathElements, E aValue ) {
			return putEnum( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Float} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putFloat( Object aKey, Float aValue ) {
			return putFloat( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Float} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Float} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putFloat( Object[] aPathElements, Float aValue ) {
			return putFloat( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Float} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Float} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putFloat( Collection<?> aPathElements, Float aValue ) {
			return putFloat( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Float} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Float} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putFloat( String aKey, Float aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Float.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.toString() );
			}
			throw new NumberFormatException( "Unable to convert a float from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Float} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Float} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Float} value to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putFloat( String[] aPathElements, Float aValue ) {
			return putFloat( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Integer} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putInt( Object aKey, Integer aValue ) {
			return putInt( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Integer} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Integer} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putInt( Object[] aPathElements, Integer aValue ) {
			return putInt( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Integer} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Integer} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putInt( Collection<?> aPathElements, Integer aValue ) {
			return putInt( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Integer} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Integer} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putInt( String aKey, Integer aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Integer.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.toString() );
			}
			throw new NumberFormatException( "Unable to convert a integer from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Integer} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link Integer} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Integer} value to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putInt( String[] aPathElements, Integer aValue ) {
			return putInt( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Long} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putLong( Object aKey, Long aValue ) {
			return putLong( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Long} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Long} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putLong( Object[] aPathElements, Long aValue ) {
			return putLong( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Long} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Long} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putLong( Collection<?> aPathElements, Long aValue ) {
			return putLong( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Long} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Long} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putLong( String aKey, Long aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Long.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.toString() );
			}
			throw new NumberFormatException( "Unable to convert a long from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );
		}

		/**
		 * Sets the {@link Long} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Long} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Long} value to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putLong( String[] aPathElements, Long aValue ) {
			return putLong( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Short} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putShort( Object aKey, Short aValue ) {
			return putShort( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Short} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Short} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putShort( Object[] aPathElements, Short aValue ) {
			return putShort( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Short} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link Short} to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putShort( Collection<?> aPathElements, Short aValue ) {
			return putShort( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link Short} value for the property referred to by the key.
		 * As property keys and values are of type {@link String}, the
		 * {@link Short} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link Short} representation for the {@link String}
		 *        value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putShort( String aKey, Short aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( Short.class ) ) {
				return put( aKey, (T) aValue );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue.toString() );
			}
			throw new NumberFormatException( "Unable to convert a short from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );

		}

		/**
		 * Sets the {@link Short} value for the property referred to by the path
		 * elements representing the addressed path. As property keys and values
		 * are of type {@link String}, the {@link Short} value is converted
		 * accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link Short} value to be associated with the path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putShort( String[] aPathElements, Short aValue ) {
			return putShort( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link Boolean} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link String} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putString( Object aKey, String aValue ) {
			return putString( InterOperableMap.asKey( aKey ), aValue );
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link String} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link String} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putString( Object[] aPathElements, String aValue ) {
			return putString( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link String} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The value {@link String} to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putString( Collection<?> aPathElements, String aValue ) {
			return putString( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * key. As property keys and values are of type {@link String}, the
		 * {@link String} value is converted accordingly.
		 *
		 * @param aKey The key for which to retrieve the value.
		 * @param aValue The {@link String} representation for the
		 *        {@link String} value.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		@SuppressWarnings("unchecked")
		default T putString( String aKey, String aValue ) {
			if ( aValue == null ) {
				return put( aKey, null );
			}
			if ( getType().isAssignableFrom( String.class ) ) {
				return put( aKey, (T) aValue );
			}
			throw new NumberFormatException( "Unable to convert a string from the value <" + aValue + "> of type <" + aValue.getClass().getName() + "> to type <" + getType().getName() + ">!" );

		}

		/**
		 * Sets the {@link String} value for the property referred to by the
		 * path elements representing the addressed path. As property keys and
		 * values are of type {@link String}, the {@link String} value is
		 * converted accordingly.
		 * 
		 * @param aPathElements The path elements of the path addressing the
		 *        value.
		 * @param aValue The {@link String} value to be associated with the
		 *        path.
		 * 
		 * @return The previous value associated with path, or null if there was
		 *         no mapping for the path. (A null return can also indicate
		 *         that the map previously associated null with the path, if the
		 *         implementation supports null values.)
		 */
		default T putString( String[] aPathElements, String aValue ) {
			return putString( toPath( aPathElements ), aValue );
		}
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBoolean( Object... aPathElements ) {
		return getBoolean( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBoolean( Collection<?> aPathElements ) {
		return getBoolean( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBoolean( Object aKey ) {
		return getBoolean( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBoolean( String... aPathElements ) {
		return getBoolean( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBoolean( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return false;
		}
		if ( theValue instanceof Boolean ) {
			return (Boolean) theValue;
		}
		if ( theValue instanceof String ) {
			for ( String eTrue : BooleanLiterals.TRUE.getNames() ) {
				if ( eTrue.equalsIgnoreCase( (String) theValue ) ) {
					return true;
				}
			}
			for ( String eFalse : BooleanLiterals.FALSE.getNames() ) {
				if ( eFalse.equalsIgnoreCase( (String) theValue ) ) {
					return false;
				}
			}
		}
		throw new NumberFormatException( "Unable to parse a boolean from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBooleanOr( Object aKey, Boolean aDefaultValue ) {
		final Boolean theValue = getBoolean( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBooleanOr( Object[] aPathElements, Boolean aDefaultValue ) {
		return getBooleanOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBooleanOr( Collection<?> aPathElements, Boolean aDefaultValue ) {
		return getBooleanOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBooleanOr( String aKey, Boolean aDefaultValue ) {
		final Boolean theValue = getBoolean( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Boolean} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Boolean} representation of the key's {@link String}
	 *         value of false if there is no such key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Boolean getBooleanOr( String[] aPathElements, Boolean aDefaultValue ) {
		return getBooleanOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByte( Object... aPathElements ) {
		return getByte( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByte( Collection<?> aPathElements ) {
		return getByte( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the key. As
	 * property keys and values are of type {@link String}, the {@link String}
	 * value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByte( Object aKey ) {
		return getByte( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByte( String... aPathElements ) {
		return getByte( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the key. As
	 * property keys and values are of type {@link String}, the {@link String}
	 * value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByte( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Byte ) {
			return (Byte) theValue;
		}
		if ( theValue instanceof String theString ) {
			if ( theString.endsWith( ".0" ) ) {
				theString = theString.substring( 0, theString.length() - 2 );
			}
			return Byte.valueOf( theString );
		}
		throw new NumberFormatException( "Unable to parse a byte from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByteOr( Object aKey, Byte aDefaultValue ) {
		final Byte theValue = getByte( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByteOr( Object[] aPathElements, Byte aDefaultValue ) {
		return getByteOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByteOr( Collection<?> aPathElements, Byte aDefaultValue ) {
		return getByteOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByteOr( String aKey, Byte aDefaultValue ) {
		final Byte theValue = getByte( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Byte} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Byte} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Byte getByteOr( String[] aPathElements, Byte aDefaultValue ) {
		return getByteOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getChar( Object aKey ) {
		return getChar( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getChar( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Character ) {
			return (Character) theValue;
		}
		if ( theValue instanceof String theString ) {
			if ( theString.length() == 1 ) {
				return theString.charAt( 0 );
			}
		}
		throw new NumberFormatException( "Unable to convert a character from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getChar( String... aPathElements ) {
		return getChar( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * path elements. As property keys and values are of type {@link String},
	 * the {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getChar( Object... aPathElements ) {
		return getChar( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * path elements. As property keys and values are of type {@link String},
	 * the {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getChar( Collection<?> aPathElements ) {
		return getChar( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * key or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getCharOr( Object aKey, Character aDefaultValue ) {
		final Character theValue = getChar( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * path represented by the given path elements or the provided default value
	 * if there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getCharOr( Object[] aPathElements, Character aDefaultValue ) {
		return getCharOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * path represented by the given path elements or the provided default value
	 * if there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getCharOr( Collection<?> aPathElements, Character aDefaultValue ) {
		return getCharOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * key or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getCharOr( String aKey, Character aDefaultValue ) {
		final Character theValue = getChar( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Character} value of the property referred to by the
	 * path represented by the given path elements or the provided default value
	 * if there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Character} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Character getCharOr( String[] aPathElements, Character aDefaultValue ) {
		return getCharOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClass( Object... aPathElements ) {
		return getClass( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClass( Collection<?> aPathElements ) {
		return getClass( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClass( Object aKey ) {
		return getClass( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClass( String... aPathElements ) {
		return getClass( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	@SuppressWarnings("unchecked")
	default <C> Class<C> getClass( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Class<?> ) {
			try {
				return (Class<C>) theValue;
			}
			catch ( ClassCastException e ) {
				throw new NumberFormatException( "Unable to parse a class from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">: " + e.getMessage() );
			}
		}
		if ( theValue instanceof String theString ) {
			try {
				return (Class<C>) Class.forName( theString );
			}
			catch ( Exception e ) {
				throw new NumberFormatException( "Unable to parse a class from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">: " + e.getMessage() );
			}
		}
		throw new NumberFormatException( "Unable to parse a class from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClassOr( Object aKey, Class<C> aDefaultValue ) {
		final Class<C> theValue = getClass( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClassOr( Object[] aPathElements, Class<C> aDefaultValue ) {
		return getClassOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClassOr( Collection<?> aPathElements, Class<C> aDefaultValue ) {
		return getClassOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClassOr( String aKey, Class<C> aDefaultValue ) {
		final Class<C> theValue = getClass( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Class} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param <C> The type of the class in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Class} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <C> Class<C> getClassOr( String[] aPathElements, Class<C> aDefaultValue ) {
		return getClassOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDouble( Object... aPathElements ) {
		return getDouble( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDouble( Collection<?> aPathElements ) {
		return getDouble( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDouble( Object aKey ) {
		return getDouble( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDouble( String... aPathElements ) {
		return getDouble( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDouble( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Double ) {
			return (Double) theValue;
		}
		if ( theValue instanceof String ) {
			return Double.valueOf( (String) theValue );
		}
		throw new NumberFormatException( "Unable to convert a double from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );

	}

	/**
	 * Returns the {@link Double} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDoubleOr( Object aKey, Double aDefaultValue ) {
		final Double theValue = getDouble( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDoubleOr( Object[] aPathElements, Double aDefaultValue ) {
		return getDoubleOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDoubleOr( Collection<?> aPathElements, Double aDefaultValue ) {
		return getDoubleOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDoubleOr( String aKey, Double aDefaultValue ) {
		final Double theValue = getDouble( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Double} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Double} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Double getDoubleOr( String[] aPathElements, Double aDefaultValue ) {
		return getDoubleOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the key. As
	 * property keys and values are of type {@link String}, the {@link String}
	 * value is converted accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aKey The key for which to retrieve the value.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnum( Class<E> aEnumType, Object aKey ) {
		return getEnum( aEnumType, asKey( aKey ) );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the key. As
	 * property keys and values are of type {@link String}, the {@link String}
	 * value is converted accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aKey The key for which to retrieve the value.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	@SuppressWarnings("unchecked")
	default <E extends Enum<E>> E getEnum( Class<E> aEnumType, String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof String ) {
			final E[] theValues = aEnumType.getEnumConstants();
			for ( E eValue : theValues ) {
				if ( eValue.name().equals( theValue ) ) {
					return eValue;
				}
			}
		}
		if ( theValue instanceof Enum<?> ) {
			try {
				return (E) theValue;
			}
			catch ( Exception e ) {
				throw new NumberFormatException( "Unable to convert a enumeration from the value <" + theValue + "> of type <" + theValue.getClass().getName() + "> to enum type <\" + aEnumType.getName() + \">:" + e.getMessage() );
			}
		}
		throw new NumberFormatException( "Unable to convert a enumeration from the value <" + theValue + "> of type <" + theValue.getClass().getName() + "> to enum type <" + aEnumType.getName() + ">!" );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the key. As
	 * property keys and values are of type {@link String}, the {@link String}
	 * value is converted accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnum( Class<E> aEnumType, String... aPathElements ) {
		return getEnum( aEnumType, toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnum( Class<E> aEnumType, Object... aPathElements ) {
		return getEnum( aEnumType, toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnum( Class<E> aEnumType, Collection<?> aPathElements ) {
		return getEnum( aEnumType, toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( Class<E> aEnumType, Object aKey, E aDefaultValue ) {
		final E theValue = getEnum( aEnumType, aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( Class<E> aEnumType, Object[] aPathElements, E aDefaultValue ) {
		return getEnumOr( aEnumType, toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( Class<E> aEnumType, Collection<?> aPathElements, E aDefaultValue ) {
		return getEnumOr( aEnumType, toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( Class<E> aEnumType, String aKey, E aDefaultValue ) {
		final E theValue = getEnum( aEnumType, aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aEnumType The type of the enumeration in question.
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * @param <E> The type of the enumeration in question.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( Class<E> aEnumType, String[] aPathElements, E aDefaultValue ) {
		return getEnumOr( aEnumType, toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	@SuppressWarnings("unchecked")
	default <E extends Enum<E>> E getEnumOr( Object aKey, E aDefaultValue ) {
		final E theValue = getEnum( (Class<E>) aDefaultValue.getClass(), aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( Object[] aPathElements, E aDefaultValue ) {
		return getEnumOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( Collection<?> aPathElements, E aDefaultValue ) {
		return getEnumOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	@SuppressWarnings("unchecked")
	default <E extends Enum<E>> E getEnumOr( String aKey, E aDefaultValue ) {
		final E theValue = getEnum( (Class<E>) aDefaultValue.getClass(), aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Enum} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Enum} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default <E extends Enum<E>> E getEnumOr( String[] aPathElements, E aDefaultValue ) {
		return getEnumOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloat( Object... aPathElements ) {
		return getFloat( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloat( Collection<?> aPathElements ) {
		return getFloat( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloat( Object aKey ) {
		return getFloat( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloat( String... aPathElements ) {
		return getFloat( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloat( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Float ) {
			return (Float) theValue;
		}
		if ( theValue instanceof String ) {
			return Float.valueOf( (String) theValue );
		}
		throw new NumberFormatException( "Unable to convert a float from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );

	}

	/**
	 * Returns the {@link Float} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloatOr( Object aKey, Float aDefaultValue ) {
		final Float theValue = getFloat( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloatOr( Object[] aPathElements, Float aDefaultValue ) {
		return getFloatOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloatOr( Collection<?> aPathElements, Float aDefaultValue ) {
		return getFloatOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloatOr( String aKey, Float aDefaultValue ) {
		final Float theValue = getFloat( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Float} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Float} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Float getFloatOr( String[] aPathElements, Float aDefaultValue ) {
		return getFloatOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getInt( Object... aPathElements ) {
		return getInt( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getInt( Collection<?> aPathElements ) {
		return getInt( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getInt( Object aKey ) {
		return getInt( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getInt( String... aPathElements ) {
		return getInt( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getInt( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Integer ) {
			return (Integer) theValue;
		}
		if ( theValue instanceof String theString ) {
			if ( theString.endsWith( ".0" ) ) {
				theString = theString.substring( 0, theString.length() - 2 );
			}
			return Integer.valueOf( theString );
		}
		throw new NumberFormatException( "Unable to convert an Integer from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getIntOr( Object aKey, Integer aDefaultValue ) {
		final Integer theValue = getInt( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getIntOr( Object[] aPathElements, Integer aDefaultValue ) {
		return getIntOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getIntOr( Collection<?> aPathElements, Integer aDefaultValue ) {
		return getIntOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getIntOr( String aKey, Integer aDefaultValue ) {
		final Integer theValue = getInt( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Integer} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Integer} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Integer getIntOr( String[] aPathElements, Integer aDefaultValue ) {
		return getIntOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLong( Object... aPathElements ) {
		return getLong( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLong( Collection<?> aPathElements ) {
		return getLong( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the key. As
	 * property keys and values are of type {@link String}, the {@link String}
	 * value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLong( Object aKey ) {
		return getLong( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLong( String... aPathElements ) {
		return getLong( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the key. As
	 * property keys and values are of type {@link String}, the {@link String}
	 * value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLong( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Long ) {
			return (Long) theValue;
		}
		if ( theValue instanceof String theString ) {
			if ( theString.endsWith( ".0" ) ) {
				theString = theString.substring( 0, theString.length() - 2 );
			}
			return Long.valueOf( theString );
		}
		throw new NumberFormatException( "Unable to convert an Long from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLongOr( Object aKey, Long aDefaultValue ) {
		final Long theValue = getLong( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLongOr( Object[] aPathElements, Long aDefaultValue ) {
		return getLongOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLongOr( Collection<?> aPathElements, Long aDefaultValue ) {
		return getLongOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLongOr( String aKey, Long aDefaultValue ) {
		final Long theValue = getLong( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Long} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Long} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Long getLongOr( String[] aPathElements, Long aDefaultValue ) {
		return getLongOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShort( Object... aPathElements ) {
		return getShort( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShort( Collection<?> aPathElements ) {
		return getShort( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShort( Object aKey ) {
		return getShort( asKey( aKey ) );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShort( String... aPathElements ) {
		return getShort( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShort( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof Short ) {
			return (Short) theValue;
		}
		if ( theValue instanceof String theString ) {
			if ( theString.endsWith( ".0" ) ) {
				theString = theString.substring( 0, theString.length() - 2 );
			}
			return Short.valueOf( theString );
		}
		throw new NumberFormatException( "Unable to convert an Short from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShortOr( Object aKey, Short aDefaultValue ) {
		final Short theValue = getShort( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShortOr( Object[] aPathElements, Short aDefaultValue ) {
		return getShortOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShortOr( Collection<?> aPathElements, Short aDefaultValue ) {
		return getShortOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the key or
	 * the provided default value if there is no such value for the given key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShortOr( String aKey, Short aDefaultValue ) {
		final Short theValue = getShort( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link Short} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link Short} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default Short getShortOr( String[] aPathElements, Short aDefaultValue ) {
		return getShortOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getString( Object... aPathElements ) {
		return getString( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getString( Collection<?> aPathElements ) {
		return getString( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getString( Object aKey ) {
		return getString( asKey( aKey ) );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the path
	 * elements. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * 
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getString( String... aPathElements ) {
		return getString( toPath( aPathElements ) );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the key.
	 * As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to retrieve the value.
	 * 
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getString( String aKey ) {
		final Object theValue = get( aKey );
		if ( theValue == null ) {
			return null;
		}
		if ( theValue instanceof String ) {
			return (String) theValue;
		}
		throw new NumberFormatException( "Unable to convert an String from the value <" + theValue + "> of type <" + theValue.getClass().getName() + ">!" );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getStringOr( Object aKey, String aDefaultValue ) {
		final String theValue = getString( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link String} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getStringOr( Object[] aPathElements, String aDefaultValue ) {
		return getStringOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getStringOr( Collection<?> aPathElements, String aDefaultValue ) {
		return getStringOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the {@link String} value of the property referred to by the key
	 * or the provided default value if there is no such value for the given
	 * key. As property keys and values are of type {@link String}, the
	 * {@link String} value is converted accordingly.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getStringOr( String aKey, String aDefaultValue ) {
		final String theValue = getString( aKey );
		return theValue == null ? aDefaultValue : theValue;
	}

	/**
	 * Returns the {@link String} value of the property referred to by the path
	 * represented by the given path elements or the provided default value if
	 * there is no such value for the given path. As property keys and values
	 * are of type {@link String}, the {@link String} value is converted
	 * accordingly.
	 * 
	 * @param aPathElements The path elements for which to retrieve the value.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 *
	 * @return The {@link String} representation of the key's {@link String}
	 *         value.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default String getStringOr( String[] aPathElements, String aDefaultValue ) {
		return getStringOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Converts the given {@link Object} to a {@link String} at best knowledge
	 * by additionally supporting {@link AliasAccessor} types.
	 * 
	 * @param aKey The {@link Object} to be converted.
	 * 
	 * @return The according {@link String}.
	 */
	static String asKey( Object aKey ) {
		if ( aKey instanceof String ) {
			return (String) aKey;
		}
		if ( aKey instanceof AliasAccessor && ( (AliasAccessor) aKey ).getAlias() != null ) {
			return ( (AliasAccessor) aKey ).getAlias();
		}
		return aKey != null ? aKey.toString() : null;
	}
}
