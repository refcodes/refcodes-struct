// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.data.Delimiter;

/**
 * Implementation of the {@link Property} interface.
 */
public class PropertyImpl extends RelationImpl<String, String> implements Property {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new property impl.
	 */
	protected PropertyImpl() {}

	/**
	 * Initializes the {@link PropertyImpl} with the given values.
	 * 
	 * @param aKey The according key to set.
	 * @param aValue The according value to set.
	 */
	public PropertyImpl( String aKey, String aValue ) {
		super( aKey, aValue );
	}

	/**
	 * Initializes the {@link PropertyImpl} with the given values.
	 * 
	 * @param aTupel A tupel is a key and a value with the assignment operator
	 *        "=" inbetween, e.g. "a=5" (the key would be a, the value would be
	 *        5).
	 */
	public PropertyImpl( String aTupel ) {
		this( aTupel, Delimiter.PROPERTY.getChar() );
	}

	/**
	 * Initializes the {@link PropertyImpl} with the given values.
	 * 
	 * @param aTupel A tupel is a key and a value with the assignment operator
	 *        "=" inbetween, e.g. "a=5" (the key would be a, the value would be
	 *        5).
	 * @param aTupelDelimeter The delimiter to be used for dividing the
	 *        key/value-pair.
	 */
	public PropertyImpl( String aTupel, char aTupelDelimeter ) {
		if ( aTupel == null ) {
			throw new IllegalArgumentException( "Unable to parse a <" + aTupel + "> into a key/value-pair." );
		}
		final int theIndex = aTupel.indexOf( aTupelDelimeter );
		if ( theIndex != -1 ) {
			_key = aTupel.substring( 0, theIndex );
			_value = aTupel.substring( theIndex + 1 );
		}
		else {
			_key = aTupel;
			_value = null;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Implementation of the {@link PropertyBuilder} interface.
	 */
	public static class PropertyBuilderImpl extends PropertyImpl implements PropertyBuilder {

		@Override
		public String toString() {
			return getClass().getSimpleName() + " [key=" + _key + ", value=" + _value + "]";
		}

		/**
		 * Use the builder methods or the bean attributes to set the key and the
		 * value.
		 */
		public PropertyBuilderImpl() {}

		/**
		 * Initializes the {@link PropertyBuilderImpl} with the given values.
		 * 
		 * @param aTupel A tupel is a key and a value with the assignment
		 *        operator "=" inbetween, e.g. "a=5" (the key would be a, the
		 *        value would be 5).
		 * @param aTupelDelimeter The delimiter to be used for dividing the
		 *        key/value-pair.
		 */
		public PropertyBuilderImpl( String aTupel, char aTupelDelimeter ) {
			super( aTupel, aTupelDelimeter );
		}

		/**
		 * Initializes the {@link PropertyBuilderImpl} with the given values.
		 * 
		 * @param aKey The according key to set.
		 * @param aValue The according value to set.
		 */
		public PropertyBuilderImpl( String aKey, String aValue ) {
			super( aKey, aValue );
		}

		/**
		 * Initializes the {@link PropertyBuilderImpl} with the given values.
		 * 
		 * @param aTupel A tupel is a key and a value with the assignment
		 *        operator "=" inbetween, e.g. "a=5" (the key would be a, the
		 *        value would be 5).
		 */
		public PropertyBuilderImpl( String aTupel ) {
			super( aTupel );
		}

		/**
		 * Sets the key.
		 *
		 * @param aKey the new key
		 */
		@Override
		public void setKey( String aKey ) {
			_key = aKey;
		}

		/**
		 * Sets the value.
		 *
		 * @param aValue the new value
		 */
		@Override
		public void setValue( String aValue ) {
			_value = aValue;
		}
	}
}
