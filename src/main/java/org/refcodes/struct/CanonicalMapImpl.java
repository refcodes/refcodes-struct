// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.Set;

import org.refcodes.data.Delimiter;

/**
 * The Class CanonicalMapImpl.
 */
public class CanonicalMapImpl implements CanonicalMap {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected CanonicalMapBuilder _canonicalMap;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link CanonicalMap} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public CanonicalMapImpl() {
		_canonicalMap = new CanonicalMapBuilderImpl();
	}

	/**
	 * Create a {@link CanonicalMap} instance using the provided path delimiter
	 * for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapImpl( char aDelimiter ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aDelimiter );
	}

	/**
	 * Create a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public CanonicalMapImpl( Object aObj ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aObj );
	}

	/**
	 * Create a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public CanonicalMapImpl( String aToPath, Object aObj ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aToPath, aObj );
	}

	/**
	 * Creates a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public CanonicalMapImpl( Object aObj, String aFromPath ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aObj, aFromPath );
	}

	/**
	 * Creates a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public CanonicalMapImpl( String aToPath, Object aObj, String aFromPath ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aToPath, aObj, aFromPath );
	}

	/**
	 * Creates a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapImpl( Object aObj, char aDelimiter ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aObj, aDelimiter );
	}

	/**
	 * Creates a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapImpl( String aToPath, Object aObj, char aDelimiter ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aToPath, aObj, aDelimiter );
	}

	/**
	 * Creates a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapImpl( Object aObj, String aFromPath, char aDelimiter ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aObj, aFromPath, aDelimiter );
	}

	/**
	 * Create a {@link CanonicalMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter ) {
		_canonicalMap = new CanonicalMapBuilderImpl( aToPath, aObj, aFromPath, aDelimiter );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return _canonicalMap.containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		return _canonicalMap.get( InterOperableMap.asKey( aKey ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _canonicalMap.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> values() {
		return _canonicalMap.values();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _canonicalMap.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _canonicalMap.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMap retrieveFrom( String aFromPath ) {
		return _canonicalMap.retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMap retrieveTo( String aToPath ) {
		return _canonicalMap.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getAnnotator() {
		return _canonicalMap.getAnnotator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _canonicalMap.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<String> getType() {
		return _canonicalMap.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return _canonicalMap.toDataStructure( aFromPath );
	}
}
