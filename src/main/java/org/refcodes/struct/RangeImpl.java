package org.refcodes.struct;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Implementation of the {@link Range} interface.
 *
 * @param <V> The type used for the values.
 */
public class RangeImpl<V extends Number & Comparable<V>> implements Range<V> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected V _minValue = null;
	protected V _maxValue = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new range impl.
	 */
	protected RangeImpl() {}

	/**
	 * Instantiates a new {@link Range} from the provided {@link String} using a
	 * <code>-</code> (minus) symbol to separate the minimum from the maximum,
	 * e.g. "1-10" for a range between 1 and 10.
	 * 
	 * @param aRange The {@link String} representation of the range.
	 * @param aType The targeted type for the {@link Range} instance.
	 * 
	 * @return The according {@link Range} instance.
	 */
	@SuppressWarnings("unchecked")
	RangeImpl( String aRange, Class<V> aType ) {
		final int index = aRange.indexOf( '-' );
		if ( index == -1 ) {
			throw new IllegalArgumentException( "The range <" + aRange + "> must be separated by a '-' (minus) symbol!" );
		}
		final String theMin = aRange.substring( 0, index );
		final String theMax = aRange.substring( index + 1 );

		final Method method;
		try {
			method = aType.getMethod( "valueOf", String.class );
			_minValue = (V) method.invoke( null, theMin );
			_maxValue = (V) method.invoke( null, theMax );
		}
		catch ( IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e ) {
			throw new IllegalArgumentException( "Creating range for type <" + aType.getName() + "> with range <" + aRange + "> failed when invoking the \"valueOf\" method with <" + theMin + "> and/or <" + theMax + ">!", e );
		}
	}

	/**
	 * Creates a new {@link Range} with the given values.
	 *
	 * @param aMinValue the min value
	 * @param aMaxValue the max value
	 */
	public RangeImpl( V aMinValue, V aMaxValue ) {
		_minValue = aMinValue;
		_maxValue = aMaxValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V getMinValue() {
		return _minValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V getMaxValue() {
		return _maxValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [minValue=" + _minValue + ", maxValue=" + _maxValue + "]";
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Implementation of the {@link RangeBuilder} interface.
	 *
	 * @param <V> The type used for the values.
	 */
	public static class RangeBuilderImpl<V extends Number & Comparable<V>> extends RangeImpl<V> implements RangeBuilder<V> {

		/**
		 * Instantiates a new range builder impl.
		 */
		public RangeBuilderImpl() {}

		/**
		 * Instantiates a new range builder impl.
		 *
		 * @param aMinValue the min value
		 * @param aMaxValue the max value
		 */
		public RangeBuilderImpl( V aMinValue, V aMaxValue ) {
			super( aMinValue, aMaxValue );
		}

		/**
		 * Sets the min value.
		 *
		 * @param aMinValue the new min value
		 */
		@Override
		public void setMinValue( V aMinValue ) {
			_minValue = aMinValue;
		}

		/**
		 * Sets the max value.
		 *
		 * @param aMaxValue the new max value
		 */
		@Override
		public void setMaxValue( V aMaxValue ) {
			_maxValue = aMaxValue;
		}
	}
}
