// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.refcodes.data.BooleanLiterals;
import org.refcodes.data.Delimiter;

/**
 * The {@link CanonicalMap} is an "intermediate" type adding type and
 * Data-Structure related functionality to any implementing class.
 */
public interface CanonicalMap extends InterOperableMap<String> {

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Interface CanonicalMapBuilder.
	 */
	public interface CanonicalMapBuilder extends MutableCanonicalMap, InterOperableMapBuilder<String> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPut( Collection<?> aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPut( Object[] aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPut( Relation<String, String> aProperty ) {
			put( aProperty );
			return this;
		}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default CanonicalMapBuilder withPut( String aKey, String aValue ) {
		//		put( aKey, aValue );
		//		return this;
		//	}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default CanonicalMapBuilder withPut( Object aPath, String aValue ) {
		//		put( toPath( aPath ), aValue );
		//		return this;
		//	}

		/**
		 * Builder method for {@link #put(Property)}.
		 *
		 * @param aProperty the property to be put.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		default CanonicalMapBuilder withPut( Property aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPut( String[] aKey, String aValue ) {
			put( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutBoolean( Object aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutBoolean( Object[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutBoolean( String aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutBoolean( String[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutByte( Collection<?> aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutByte( Object aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutByte( Object[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutByte( String aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutByte( String[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutChar( Collection<?> aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutChar( Object aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutChar( Object[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutChar( String aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutChar( String[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> CanonicalMapBuilder withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> CanonicalMapBuilder withPutClass( Object aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> CanonicalMapBuilder withPutClass( Object[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> CanonicalMapBuilder withPutClass( String aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> CanonicalMapBuilder withPutClass( String[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDouble( Collection<?> aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDouble( Object aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDouble( Object[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDouble( String aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDouble( String[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> CanonicalMapBuilder withPutEnum( Collection<?> aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> CanonicalMapBuilder withPutEnum( Object aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> CanonicalMapBuilder withPutEnum( Object[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> CanonicalMapBuilder withPutEnum( String aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> CanonicalMapBuilder withPutEnum( String[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutFloat( Collection<?> aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutFloat( Object aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutFloat( Object[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutFloat( String aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutFloat( String[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutInt( Collection<?> aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutInt( Object aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutInt( Object[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutInt( String aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutInt( String[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutLong( Collection<?> aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutLong( Object aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutLong( Object[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutLong( String aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutLong( String[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutShort( Collection<?> aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutShort( Object aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutShort( Object[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutShort( String aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutShort( String[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutString( Collection<?> aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutString( Object aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutString( Object[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutString( String aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutString( String[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsert( PathMap<String> aFrom ) {
			insert( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( Object aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( Object aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( String aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMerge( PathMap<String> aFrom ) {
			merge( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( Object aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( Object aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( String aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( int aIndex, PathMap<String> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMapBuilder withRemovePaths( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// MUTATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Interface MutableCanonicalMap.
	 */
	public interface MutableCanonicalMap extends CanonicalMap, MutableInterOperableMap<String>, Map<String, String> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default boolean containsValue( Object value ) {
			return values().contains( value );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insert(Object)}.
		 * 
		 * @param aFrom The {@link CanonicalMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 */
		default void insert( CanonicalMap aFrom ) {
			insert( (Object) aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link CanonicalMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertBetween( String aToPath, CanonicalMap aFrom, String aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link CanonicalMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertFrom( CanonicalMap aFrom, String aFromPath ) {
			insertFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link CanonicalMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( String aToPath, CanonicalMap aFrom ) {
			insertTo( aToPath, (Object) aFrom );
		}

		/**
		 * Adds the given element related to the given key. As the key passed is
		 * an enumeration, the {@link Enum#toString()} method is used on the
		 * enumeration to resolve the enumeration to a {link String} key.
		 * 
		 * @param aKey The key for which to add the element.
		 * @param aValue The value to be related with the given key.
		 * 
		 * @return The value being replaced by the provided value or null if
		 *         none value has been replaced.
		 */
		default String put( Object aKey, String aValue ) {
			return put( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ), aValue );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default String put( Object[] aPathElements, String aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * Puts the key/value-pair from the provided {@link Property}.
		 * 
		 * @param aProperty The property's key/value to be inserted.
		 * 
		 * @return The property value overwritten by the provided property.
		 */
		default String put( Property aProperty ) {
			return put( aProperty.getKey(), aProperty.getValue() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default String put( Relation<String, String> aRelation ) {
			return put( aRelation.getKey(), aRelation.getValue() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default String put( String[] aPathElements, String aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * This method inserts all elements (key/value-pairs) found in the
		 * provided {@link CanonicalMap} instances of interoperability reasons.
		 * 
		 * @param aProperties A {@link CanonicalMap} containing the key/value
		 *        pairs to be inserted.
		 */
		default void putAll( CanonicalMap aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( eKey ) );
			}
		}

		/**
		 * This method inserts all elements (key/value-pairs) found in the
		 * provided {@link java.util.Properties} instances of interoperability
		 * reasons.
		 * 
		 * @param aProperties A {@link java.util.Properties} containing the
		 *        key/value-pairs to be inserted.
		 */
		default void putAll( java.util.Properties aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( eKey ) );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default void putAll( Map<? extends String, ? extends String> aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( eKey ) );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( int aIndex, Object aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( int aIndex, PathMap<String> aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( Object aPath, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( String aPath, int aIndex, Object aDir ) {
			final CanonicalMap theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
			final CanonicalMap theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap putDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeAll( Collection<?> aPathQueryElements ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeAll( Object... aPathQueryElements ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeAll( Object aPathQuery ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeAll( aPathQuery ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeAll( Pattern aRegExp ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeAll( aRegExp ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeAll( String... aPathQueryElements ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeAll( String aPathQuery ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeAll( aPathQuery ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeDirAt( int aIndex ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeDirAt( aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeDirAt( Object aPath, int aIndex ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeDirAt( aPath, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeDirAt( Object[] aPathElements, int aIndex ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeDirAt( aPathElements, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeDirAt( String aPath, int aIndex ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeDirAt( aPath, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeDirAt( String[] aPathElements, int aIndex ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeDirAt( aPathElements, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeFrom( Collection<?> aPathElements ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeFrom( Object... aPathElements ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeFrom( Object aPath ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeFrom( aPath ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeFrom( String aPath ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeFrom( aPath ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removeFrom( String... aPathElements ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removePaths( Collection<?> aPaths ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removePaths( aPaths ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default CanonicalMap removePaths( String... aPaths ) {
			return new CanonicalMapImpl( MutableInterOperableMap.super.removePaths( aPaths ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Map<String, String> toDump( Map<String, String> aDump ) {
			aDump.putAll( this );
			return aDump;
		}
	}

	/**
	 * Same as {@link #asArray(String, char)} using a comma (',') as delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default String[] asArray( Object aKey ) {
		return asArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as an array with its
	 * elements being the elements of the value separated by the given delimiter
	 * char. Let the value associated to the given key be "1, 2, 3, 4, 5", then
	 * the corresponding array, when using the comma (',') char as delimiter,
	 * will be { "1", "2", "3", "4", "5" }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default String[] asArray( Object aKey, char aDelimiter ) {
		return asArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asArray(String, char)} using a comma (',') as delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default String[] asArray( String aKey ) {
		return asArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as an array with its
	 * elements being the elements of the value separated by the given delimiter
	 * char. Let the value associated to the given key be "1, 2, 3, 4, 5", then
	 * the corresponding array, when using the comma (',') char as delimiter,
	 * will be { "1", "2", "3", "4", "5" }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default String[] asArray( String aKey, char aDelimiter ) {
		String[] theArray = null;
		final String theValue = get( aKey );
		if ( theValue != null && theValue.length() != 0 ) {
			theArray = theValue.split( Pattern.quote( "" + aDelimiter ) );
		}
		return theArray;
	}

	/**
	 * Same as {@link #asBooleanArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default boolean[] asBooleanArray( Object aKey ) {
		return asBooleanArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a boolean array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default boolean[] asBooleanArray( Object aKey, char aDelimiter ) {
		return asBooleanArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asBooleanArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default boolean[] asBooleanArray( String aKey ) {
		return asBooleanArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a boolean array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default boolean[] asBooleanArray( String aKey, char aDelimiter ) {
		boolean[] theBooleanArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theBooleanArray = new boolean[theArray.length];
			next: for ( int i = 0; i < theArray.length; i++ ) {
				for ( String eTrue : BooleanLiterals.TRUE.getNames() ) {
					if ( eTrue.equalsIgnoreCase( theArray[i].trim() ) ) {
						theBooleanArray[i] = true;
						continue next;
					}
				}
				for ( String eFalse : BooleanLiterals.FALSE.getNames() ) {
					if ( eFalse.equalsIgnoreCase( theArray[i].trim() ) ) {
						theBooleanArray[i] = false;
						continue next;
					}
				}
				throw new NumberFormatException( "Unable to parse boolean from key <" + aKey + "> using delimiter '" + aDelimiter + "' at position <" + i + "> of the array <" + Arrays.toString( theArray ) + ">!" );
			}
		}
		return theBooleanArray;
	}

	/**
	 * Same as {@link #asByteArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default byte[] asByteArray( Object aKey ) {
		return asByteArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a byte array with its
	 * elements being the elements of the value separated by the given delimiter
	 * char. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * char as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default byte[] asByteArray( Object aKey, char aDelimiter ) {
		return asByteArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asByteArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default byte[] asByteArray( String aKey ) {
		return asByteArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a byte array with its
	 * elements being the elements of the value separated by the given delimiter
	 * char. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * char as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default byte[] asByteArray( String aKey, char aDelimiter ) {
		byte[] theToArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theToArray = new byte[theArray.length];
			String eValue;
			for ( int i = 0; i < theArray.length; i++ ) {
				eValue = theArray[i].trim();
				if ( eValue.endsWith( ".0" ) ) {
					eValue = eValue.substring( 0, eValue.length() - 2 );
				}
				theToArray[i] = Byte.valueOf( eValue );
			}
		}
		return theToArray;
	}

	/**
	 * Same as {@link #asCharArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default char[] asCharArray( Object aKey ) {
		return asCharArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a char array with its
	 * elements being the elements of the value separated by the given delimiter
	 * char. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * char as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default char[] asCharArray( Object aKey, char aDelimiter ) {
		return asCharArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asCharArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default char[] asCharArray( String aKey ) {
		return asCharArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a char array with its
	 * elements being the elements of the value separated by the given delimiter
	 * char. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * char as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default char[] asCharArray( String aKey, char aDelimiter ) {
		char[] theToArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theToArray = new char[theArray.length];
			String eValue;
			for ( int i = 0; i < theArray.length; i++ ) {
				eValue = theArray[i].trim();
				if ( eValue.isEmpty() ) {
					eValue = theArray[i]; // Trim or not to trim
				}
				if ( eValue.length() == 1 ) {
					theToArray[i] = eValue.charAt( 0 );
				}
				else {
					throw new NumberFormatException( "Unable to convert from key <" + aKey + "> using delimiter '" + aDelimiter + "' at position <" + i + "> of the array <" + Arrays.toString( theArray ) + ">!" );
				}
			}
		}
		return theToArray;
	}

	/**
	 * Same as {@link #asDoubleArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default double[] asDoubleArray( Object aKey ) {
		return asDoubleArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a double array with its
	 * elements being the elements of the value separated by the given delimiter
	 * double. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * double as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default double[] asDoubleArray( Object aKey, char aDelimiter ) {
		return asDoubleArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asDoubleArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default double[] asDoubleArray( String aKey ) {
		return asDoubleArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a double array with its
	 * elements being the elements of the value separated by the given delimiter
	 * double. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * double as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default double[] asDoubleArray( String aKey, char aDelimiter ) {
		double[] theToArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theToArray = new double[theArray.length];
			for ( int i = 0; i < theArray.length; i++ ) {
				theToArray[i] = Double.valueOf( theArray[i].trim() );
			}
		}
		return theToArray;
	}

	/**
	 * Same as {@link #asFloatArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default float[] asFloatArray( Object aKey ) {
		return asFloatArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a float array with its
	 * elements being the elements of the value separated by the given delimiter
	 * float. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * float as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default float[] asFloatArray( Object aKey, char aDelimiter ) {
		return asFloatArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asFloatArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default float[] asFloatArray( String aKey ) {
		return asFloatArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a float array with its
	 * elements being the elements of the value separated by the given delimiter
	 * float. Let the value associated to the given key be "true, false, true,
	 * false, true", then the corresponding array, when using the comma (',')
	 * float as delimiter, will be { true, false, true, false, true }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default float[] asFloatArray( String aKey, char aDelimiter ) {
		float[] theToArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theToArray = new float[theArray.length];
			for ( int i = 0; i < theArray.length; i++ ) {
				theToArray[i] = Float.valueOf( theArray[i].trim() );
			}
		}
		return theToArray;
	}

	/**
	 * Same as {@link #asIntArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default int[] asIntArray( Object aKey ) {
		return asIntArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a integer array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default int[] asIntArray( Object aKey, char aDelimiter ) {
		return asIntArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asIntArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default int[] asIntArray( String aKey ) {
		return asIntArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a integer array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default int[] asIntArray( String aKey, char aDelimiter ) {
		int[] theToArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theToArray = new int[theArray.length];
			String eValue;
			for ( int i = 0; i < theArray.length; i++ ) {
				eValue = theArray[i].trim();
				if ( eValue.endsWith( ".0" ) ) {
					eValue = eValue.substring( 0, eValue.length() - 2 );
				}
				theToArray[i] = Integer.valueOf( eValue );
			}
		}
		return theToArray;
	}

	/**
	 * Same as {@link #asLongArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default long[] asLongArray( Object aKey ) {
		return asLongArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a longeger array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default long[] asLongArray( Object aKey, char aDelimiter ) {
		return asLongArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asLongArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default long[] asLongArray( String aKey ) {
		return asLongArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a longeger array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default long[] asLongArray( String aKey, char aDelimiter ) {
		long[] theToArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theToArray = new long[theArray.length];
			String eValue;
			for ( int i = 0; i < theArray.length; i++ ) {
				eValue = theArray[i].trim();
				if ( eValue.endsWith( ".0" ) ) {
					eValue = eValue.substring( 0, eValue.length() - 2 );
				}
				theToArray[i] = Long.valueOf( eValue );
			}
		}
		return theToArray;
	}

	/**
	 * Same as {@link #asShortArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default short[] asShortArray( Object aKey ) {
		return asShortArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Treats the value associated with the given key as a shorteger array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default short[] asShortArray( Object aKey, char aDelimiter ) {
		return asShortArray( aKey instanceof String ? (String) aKey : ( aKey != null ? aKey.toString() : (String) null ) );
	}

	/**
	 * Same as {@link #asShortArray(String, char)} using a comma (',') as
	 * delimiter.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 */
	default short[] asShortArray( String aKey ) {
		return asShortArray( aKey, Delimiter.LIST.getChar() );
	}

	/**
	 * Treats the value associated with the given key as a shorteger array with
	 * its elements being the elements of the value separated by the given
	 * delimiter char. Let the value associated to the given key be "true,
	 * false, true, false, true", then the corresponding array, when using the
	 * comma (',') char as delimiter, will be { true, false, true, false, true
	 * }.
	 * 
	 * @param aKey The key, which's value is to be converted to an array.
	 * @param aDelimiter The delimiter to be used to identify the elements of
	 *        the future array.
	 * 
	 * @return The according array or null if no (or a null) value has been
	 *         associated to the key.
	 * 
	 * @throws NumberFormatException thrown in case conversion fails.
	 */
	default short[] asShortArray( String aKey, char aDelimiter ) {
		short[] theToArray = null;
		final String[] theArray = asArray( aKey, aDelimiter );
		if ( theArray != null ) {
			theToArray = new short[theArray.length];
			String eValue;
			for ( int i = 0; i < theArray.length; i++ ) {
				eValue = theArray[i].trim();
				if ( eValue.endsWith( ".0" ) ) {
					eValue = eValue.substring( 0, eValue.length() - 2 );
				}
				theToArray[i] = Short.valueOf( eValue );
			}
		}
		return theToArray;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default boolean containsValue( Object value ) {
		return values().contains( value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDirAt( String[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDirAt( Collection<?> aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDirAt( Object aPath, int aIndex ) {
		return getDirAt( toPath( aPath ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDirAt( Object[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap query( Pattern aRegExp ) {
		return new CanonicalMapImpl( InterOperableMap.super.query( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap query( String aPathQuery ) {
		return new CanonicalMapImpl( InterOperableMap.super.query( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		return new CanonicalMapImpl( InterOperableMap.super.queryBetween( aFromPath, aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		return new CanonicalMapImpl( InterOperableMap.super.queryBetween( aFromPath, aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap queryFrom( Pattern aRegExp, String aFromPath ) {
		return new CanonicalMapImpl( InterOperableMap.super.queryFrom( aRegExp, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap queryFrom( String aPathQuery, String aFromPath ) {
		return new CanonicalMapImpl( InterOperableMap.super.queryFrom( aPathQuery, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap queryTo( Pattern aRegExp, String aToPath ) {
		return new CanonicalMapImpl( InterOperableMap.super.queryTo( aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap queryTo( String aPathQuery, String aToPath ) {
		return new CanonicalMapImpl( InterOperableMap.super.queryTo( aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap retrieveBetween( Object aFromPath, Object aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap retrieveBetween( String aFromPath, String aToPath ) {
		final CanonicalMap thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap retrieveBetween( String[] aFromPath, String[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap retrieveFrom( Object aParentPath ) {
		return retrieveFrom( toPath( aParentPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	CanonicalMap retrieveFrom( String aFromPath );

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap retrieveTo( Object aToPath ) {
		return retrieveTo( toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	CanonicalMap retrieveTo( String aToPath );

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap getDir( Collection<?> aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDir( Object aPath ) {
		return getDir( toPath( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDir( Object... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap getDir( String aPath ) {
		return retrieveFrom( aPath );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default CanonicalMap getDir( String... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap[] getDirs( Collection<?> aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap[] getDirs( Object aPath ) {
		return getDirs( toPath( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap[] getDirs( Object... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap[] getDirs( String aPath ) {
		final CanonicalMap[] theDirs;
		if ( isIndexDir( aPath ) ) {
			final int[] theIndexes = getDirIndexes( aPath );
			theDirs = new CanonicalMap[theIndexes[theIndexes.length - 1] + 1];
			for ( int i : theIndexes ) {
				theDirs[theIndexes[i]] = getDirAt( aPath, theIndexes[i] );
			}
		}
		else {
			theDirs = new CanonicalMap[] { getDir( aPath ) };
		}
		return theDirs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CanonicalMap[] getDirs( String... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * This method creates a {@link Map} instance from this {@link CanonicalMap}
	 * instance's elements (key/value-pairs) as of interoperability reasons.
	 * 
	 * @return A {@link Map} object from the herein contained key/value-pairs .
	 */
	@Override
	default Map<String, String> toMap() {
		final Map<String, String> theProperties = new HashMap<>();
		for ( String eKey : keySet() ) {
			theProperties.put( eKey, get( eKey ) );
		}
		return theProperties;
	}

	/**
	 * This method creates a {@link java.util.Properties} instance from this
	 * {@link Properties} instance's elements (key/value-pairs) as of
	 * interoperability reasons.
	 * 
	 * @return A {@link java.util.Properties} object from the herein contained
	 *         key/value-pairs .
	 */
	default java.util.Properties toProperties() {
		final java.util.Properties theProperties = new java.util.Properties();
		for ( String eKey : keySet() ) {
			theProperties.put( toPropertyPath( eKey ), get( eKey ) );
		}
		return theProperties;
	}

	//	/**
	//	 * Determines whether there are indexed directories for the given
	//	 * (un-indexed) path starting from the right (end of the path). If this is
	//	 * the case, then the next available index is used for first matching
	//	 * indexed directory and the resulting path is returned.
	//	 * 
	//	 * @param aPathElements The path elements to convert.
	//	 * 
	//	 * @return The accordingly converted path
	//	 */
	//	default String toNextIndexPath( Collection<?> aPathElements ) {
	//		return toNextIndexPath( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines whether there are indexed directories for the given
	//	 * (un-indexed) path starting from the right (end of the path). If this is
	//	 * the case, then the next available index is used for first matching
	//	 * indexed directory and the resulting path is returned.
	//	 * 
	//	 * @param aPath The path to convert.
	//	 * 
	//	 * @return The accordingly converted path
	//	 */
	//	default String toNextIndexPath( Object aPath ) {
	//		return toNextIndexPath( toPath( aPath ) );
	//	}
	//
	//	/**
	//	 * Determines whether there are indexed directories for the given
	//	 * (un-indexed) path starting from the right (end of the path). If this is
	//	 * the case, then the next available index is used for first matching
	//	 * indexed directory and the resulting path is returned.
	//	 * 
	//	 * @param aPathElements The path elements to convert.
	//	 * 
	//	 * @return The accordingly converted path
	//	 */
	//	default String toNextIndexPath( Object... aPathElements ) {
	//		return toNextIndexPath( toPath( aPathElements ) );
	//	}
	//
	//	/**
	//	 * Determines whether there are indexed directories for the given
	//	 * (un-indexed) path starting from the right (end of the path). If this is
	//	 * the case, then the next available index is used for first matching
	//	 * indexed directory and the resulting path is returned.
	//	 * 
	//	 * @param aPath The path to convert.
	//	 * 
	//	 * @return The accordingly converted path
	//	 */
	//	default String toNextIndexPath( String aPath ) {
	//		Stack<String> thePathStack = toPathStack( aPath );
	//		Stack<String> theRemovedOnes = new Stack<>();
	//
	//		while ( !thePathStack.isEmpty() ) {
	//			if ( isIndexDir( thePathStack ) ) {
	//				thePathStack.push( "" + nextDirIndex( thePathStack ) );
	//				while ( !theRemovedOnes.isEmpty() ) {
	//					thePathStack.push( theRemovedOnes.pop() );
	//				}
	//				return toPath( thePathStack );
	//			}
	//			theRemovedOnes.push( thePathStack.pop() );
	//		}
	//		return aPath;
	//	}
}
