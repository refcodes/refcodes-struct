// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * The {@link BoundedIterator} decorates an {@link Iterator} to limit its
 * element to a maximum number.
 *
 */
public class BoundedIterator<T> implements Iterator<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Iterator<T> _iterator;
	private int _lengthBounds;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the given {@link Iterator} with the given length bounds.
	 * 
	 * @param aIterator The {@link Iterator} to which to apply the given length
	 *        bounds.
	 * @param aLengthBounds The length bounds to be applied to the given
	 *        iterator;
	 */
	public BoundedIterator( Iterator<T> aIterator, int aLengthBounds ) {
		_iterator = aIterator;
		_lengthBounds = aLengthBounds;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return _lengthBounds > 0 && _iterator.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {

		if ( hasNext() ) {
			_lengthBounds--;
			return _iterator.next();

		}
		throw new NoSuchElementException( "The bounds of the iterator have been reached or the underlying iterator has no more elements!" );
	}
}
