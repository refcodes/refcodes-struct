// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A {@link SimpleTypeMap} disects an Object into its primitive types such as
 * {@link Boolean}, {@link Byte}, {@link Short}, {@link Character},
 * {@link Integer}, {@link Long}, {@link Float} or {@link Double} instances as
 * well as its (not that primitive) types such as {@link String}, {@link Enum}
 * or {@link Class} instances. All of them mentioned types shall herewith be
 * called simple types.
 */
public interface SimpleTypeMap extends InterOperableMap<Object> {

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * A {@link SimpleTypeMapBuilder} sets builder functionality (as of the
	 * builder pattern) on top of the {@link MutableSimpleTypeMap}.
	 */
	public interface SimpleTypeMapBuilder extends MutableSimpleTypeMap, InterOperableMapBuilder<Object> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPut( Collection<?> aPathElements, Object aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPut( Object[] aPathElements, Object aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPut( Relation<String, Object> aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPut( String aKey, Object aValue ) {
			put( aKey, aValue );
			return this;
		}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default SimpleTypeMapBuilder withPut( Object aPath, Object aValue ) {
		//		put( toPath( aPath ), aValue );
		//		return this;
		//	}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPut( String[] aKey, Object aValue ) {
			put( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutBoolean( Object aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutBoolean( Object[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutBoolean( String aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutBoolean( String[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutByte( Collection<?> aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutByte( Object aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutByte( Object[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutByte( String aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutByte( String[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutChar( Collection<?> aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutChar( Object aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutChar( Object[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutChar( String aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutChar( String[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> SimpleTypeMapBuilder withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> SimpleTypeMapBuilder withPutClass( Object aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> SimpleTypeMapBuilder withPutClass( Object[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> SimpleTypeMapBuilder withPutClass( String aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> SimpleTypeMapBuilder withPutClass( String[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDouble( Collection<?> aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDouble( Object aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDouble( Object[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDouble( String aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDouble( String[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> SimpleTypeMapBuilder withPutEnum( Collection<?> aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> SimpleTypeMapBuilder withPutEnum( Object aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> SimpleTypeMapBuilder withPutEnum( Object[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> SimpleTypeMapBuilder withPutEnum( String aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> SimpleTypeMapBuilder withPutEnum( String[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutFloat( Collection<?> aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutFloat( Object aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutFloat( Object[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutFloat( String aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutFloat( String[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutInt( Collection<?> aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutInt( Object aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutInt( Object[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutInt( String aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutInt( String[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutLong( Collection<?> aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutLong( Object aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutLong( Object[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutLong( String aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutLong( String[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutShort( Collection<?> aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutShort( Object aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutShort( Object[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutShort( String aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutShort( String[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutString( Collection<?> aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutString( Object aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutString( Object[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutString( String aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutString( String[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsert( PathMap<Object> aFrom ) {
			insert( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<Object> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( Object aToPath, PathMap<Object> aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( Object[] aToPathElements, PathMap<Object> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( String aToPath, PathMap<Object> aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertBetween( String[] aToPathElements, PathMap<Object> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( PathMap<Object> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( PathMap<Object> aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( PathMap<Object> aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( PathMap<Object> aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertFrom( PathMap<Object> aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( Collection<?> aToPathElements, PathMap<Object> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( Object aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( Object aToPath, PathMap<Object> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( Object[] aToPathElements, PathMap<Object> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( String aToPath, PathMap<Object> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withInsertTo( String[] aToPathElements, PathMap<Object> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMerge( PathMap<Object> aFrom ) {
			merge( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<Object> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( Object aToPath, PathMap<Object> aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( Object[] aToPathElements, PathMap<Object> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( String aToPath, PathMap<Object> aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeBetween( String[] aToPathElements, PathMap<Object> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( PathMap<Object> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( PathMap<Object> aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( PathMap<Object> aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( PathMap<Object> aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeFrom( PathMap<Object> aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( Collection<?> aToPathElements, PathMap<Object> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( Object aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( Object aToPath, PathMap<Object> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( Object[] aToPathElements, PathMap<Object> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( String aToPath, PathMap<Object> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withMergeTo( String[] aToPathElements, PathMap<Object> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<Object> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( int aIndex, PathMap<Object> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( Object aPath, int aIndex, PathMap<Object> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<Object> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( String aPath, int aIndex, PathMap<Object> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<Object> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMapBuilder withRemovePaths( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// MUTATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link MutableSimpleTypeMap} adds functionality for manipulating the
	 * internal state to the {@link SimpleTypeMap}.
	 */
	public interface MutableSimpleTypeMap extends SimpleTypeMap, MutableInterOperableMap<Object>, Map<String, Object> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default boolean containsValue( Object value ) {
			return values().contains( value );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insert(Object)}.
		 * 
		 * @param aFrom The {@link SimpleTypeMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 */
		default void insert( SimpleTypeMap aFrom ) {
			insert( (Object) aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link SimpleTypeMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertBetween( String aToPath, SimpleTypeMap aFrom, String aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link SimpleTypeMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertFrom( SimpleTypeMap aFrom, String aFromPath ) {
			insertFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link SimpleTypeMap} which is to be inspected with
		 *        the therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( String aToPath, SimpleTypeMap aFrom ) {
			insertTo( aToPath, (Object) aFrom );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Object put( Object[] aPathElements, Object aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Object put( Relation<String, Object> aRelation ) {
			return put( aRelation.getKey(), aRelation.getValue() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Object put( String[] aPathElements, Object aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * This method inserts all elements (key/value-pairs) found in the
		 * provided {@link java.util.Properties} instances of interoperability
		 * reasons.
		 * 
		 * @param aProperties A {@link java.util.Properties} containing the
		 *        key/value-pairs to be inserted.
		 */
		default void putAll( java.util.Properties aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( (String) eKey ) );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default void putAll( Map<? extends String, ? extends Object> aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( (String) eKey ) );
			}
		}

		/**
		 * This method inserts all elements (key/value-pairs) found in the
		 * provided {@link SimpleTypeMap} instances of interoperability reasons.
		 * 
		 * @param aProperties A {@link SimpleTypeMap} containing the key/value
		 *        pairs to be inserted.
		 */
		default void putAll( SimpleTypeMap aProperties ) {
			for ( Object eKey : aProperties.keySet() ) {
				put( (String) eKey, get( (String) eKey ) );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( Collection<?> aPathElements, int aIndex, PathMap<Object> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( int aIndex, Object aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( int aIndex, PathMap<Object> aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( Object aPath, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( Object aPath, int aIndex, PathMap<Object> aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( Object[] aPathElements, int aIndex, PathMap<Object> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( String aPath, int aIndex, Object aDir ) {
			final SimpleTypeMap theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( String aPath, int aIndex, PathMap<Object> aDir ) {
			final SimpleTypeMap theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap putDirAt( String[] aPathElements, int aIndex, PathMap<Object> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeAll( Collection<?> aPathQueryElements ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeAll( Object... aPathQueryElements ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeAll( Object aPathQuery ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeAll( aPathQuery ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeAll( Pattern aRegExp ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeAll( aRegExp ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeAll( String... aPathQueryElements ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeAll( aPathQueryElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeAll( String aPathQuery ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeAll( aPathQuery ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeDirAt( int aIndex ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeDirAt( aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeDirAt( Object aPath, int aIndex ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeDirAt( aPath, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeDirAt( Object[] aPathElements, int aIndex ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeDirAt( aPathElements, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeDirAt( String aPath, int aIndex ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeDirAt( aPath, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeDirAt( String[] aPathElements, int aIndex ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeDirAt( aPathElements, aIndex ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeFrom( Collection<?> aPathElements ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeFrom( Object... aPathElements ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeFrom( Object aPath ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeFrom( aPath ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeFrom( String aPath ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeFrom( aPath ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removeFrom( String... aPathElements ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removeFrom( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removePaths( Collection<?> aPaths ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removePaths( aPaths ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default SimpleTypeMap removePaths( String... aPaths ) {
			return new SimpleTypeMapImpl( MutableInterOperableMap.super.removePaths( aPaths ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default Map<String, String> toDump( Map<String, String> aDump ) {
			Object eValue;
			for ( String eKey : keySet() ) {
				eValue = get( eKey );
				aDump.put( eKey, eValue != null ? eValue.toString() : null );
			}
			return aDump;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default boolean containsValue( Object value ) {
		return values().contains( value );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDirAt( Collection<?> aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDirAt( Object aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDirAt( Object[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDirAt( String[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap query( Pattern aRegExp ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.query( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap query( String aPathQuery ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.query( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.queryBetween( aFromPath, aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.queryBetween( aFromPath, aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap queryFrom( Pattern aRegExp, String aFromPath ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.queryFrom( aRegExp, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap queryFrom( String aPathQuery, String aFromPath ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.queryFrom( aPathQuery, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap queryTo( Pattern aRegExp, String aToPath ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.queryTo( aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap queryTo( String aPathQuery, String aToPath ) {
		return new SimpleTypeMapImpl( InterOperableMap.super.queryTo( aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap retrieveBetween( Object aFromPath, Object aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap retrieveBetween( String aFromPath, String aToPath ) {
		final SimpleTypeMap thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap retrieveBetween( String[] aFromPath, String[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap retrieveFrom( Object aParentPath ) {
		return retrieveFrom( toPath( aParentPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	SimpleTypeMap retrieveFrom( String aFromPath );

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap getDir( Collection<?> aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDir( Object aPath ) {
		return getDir( toPath( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDir( Object... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap getDir( String aPath ) {
		return retrieveFrom( aPath );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap getDir( String... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap[] getDirs( Collection<?> aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap[] getDirs( Object aPath ) {
		return getDirs( toPath( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap[] getDirs( Object... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap[] getDirs( String aPath ) {
		final SimpleTypeMap[] theDirs;
		if ( isIndexDir( aPath ) ) {
			final int[] theIndexes = getDirIndexes( aPath );
			theDirs = new SimpleTypeMap[theIndexes[theIndexes.length - 1] + 1];
			for ( int i : theIndexes ) {
				theDirs[theIndexes[i]] = getDirAt( aPath, theIndexes[i] );
			}
		}
		else {
			theDirs = new SimpleTypeMap[] { getDir( aPath ) };
		}
		return theDirs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap[] getDirs( String... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */

	@Override
	default SimpleTypeMap retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap retrieveTo( Object aToPath ) {
		return retrieveTo( toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	SimpleTypeMap retrieveTo( String aToPath );

	/**
	 * {@inheritDoc}
	 */
	@Override
	default SimpleTypeMap retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * This method creates a {@link Map} instance from this
	 * {@link SimpleTypeMap} instance's elements (key/value-pairs) as of
	 * interoperability reasons.
	 * 
	 * @return A {@link Map} object from the herein contained key/value-pairs .
	 */
	@Override
	default Map<String, Object> toMap() {
		final Map<String, Object> theProperties = new HashMap<>();
		for ( String eKey : keySet() ) {
			theProperties.put( eKey, get( eKey ) );
		}
		return theProperties;
	}
}
