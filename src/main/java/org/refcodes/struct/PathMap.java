// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

import org.refcodes.data.Annotator;
import org.refcodes.data.Delimiter;
import org.refcodes.data.Wildcard;
import org.refcodes.matcher.PathMatcher;
import org.refcodes.mixin.AnnotatorAccessor;
import org.refcodes.mixin.DelimiterAccessor;
import org.refcodes.mixin.Dumpable;
import org.refcodes.mixin.TypeAccessor;
import org.refcodes.struct.Table.TableDictionary.TableBuilder;

/**
 * A {@link PathMap} is a flat map by which each element is addressed by a path;
 * represented by the {@link PathMap}'s keys. Each path's elements is separated
 * from each other by the {@link #getDelimiter()} character, which, by default,
 * is set to '/'. For the sake of simplicity, we assume a path delimiter of '/'
 * being used when describing the functioning of the {@link PathMap}.
 * <p>
 * The {@link PathMap} distinguishes between leaves and directories. A leave is
 * reckoned to be the last path element in a path pointing to a value. In
 * contrast a directory is reckoned to be be a path element pointing to a
 * succeeding child path element (sub-directory).
 * <p>
 * Given the example below, "dilbert", "otto", "loki" and "gred" are reckoned to
 * be leaves whereas "animals", "dogs", "machines" and "robots" are recokned to
 * be directories: *
 * <ul>
 * <li>"/animals/dogs/dilbert"</li>
 * <li>"/animals/dogs/otto"</li>
 * <li>"/animals/loki"</li>
 * <li>"/machines/robots/greg"</li>
 * </ul>
 * <p>
 * To address an element in a {@link PathMap}, an absolute path is to be
 * provided, e.g. the provided path must start with the path delimiter. A valid
 * path would look something like "/animals/dogs/otto".
 * <p>
 * A path is also handled as an absolute path if it does not(!) begin with a
 * delimiter, e.g. both paths "/machines/robots/greg" and "machines/robots/greg"
 * point to the same "greg" leave.
 * <p>
 * Elements in a {@link PathMap} are reckoned to be an array when they belong to
 * the same hierarchy level and when their keys represent integer numbers. In
 * such a case additional array related functionality is at your hands. E.g.
 * {@link #isIndexDir(String)}, {@link #getDirAt(String, int)} or
 * {@link #getArray(String)}.
 * <p>
 * Given the below example, the elements below "/animals/dogs" can be
 * represented as an array with five elements (the path is denoted at the left
 * hand side of the assignment ":=" and the value at the right and side
 * accordingly):
 * <ul>
 * <li>"/animals/dogs/0" := "ace"</li>
 * <li>"/animals/dogs/1" := "bandit"</li>
 * <li>"/animals/dogs/2" := "radar"</li>
 * <li>"/animals/dogs/3" := "echo"</li>
 * <li>"/animals/dogs/4" := "snoopy"</li>
 * </ul>
 * The resulting array when calling {@link #getArray(String)} for the path
 * "/animals/dogs" would contain {"ace", "bandit", "radar", "echo", "snoopy"}.
 * Calling <code>isArray("/animals/dogs")</code> would return <code>true</code>
 * whereas calling <code>isArray("/animals")</code> would return false.
 * <p>
 * Before processing, an implementation of the {@link PathMap} should call
 * {@link #toNormalizedPath(String)} for each path provided, which will remove
 * any trailing path separators and add any missing prefixed delimiter.
 * <p>
 * As all the keys in the {@link PathMap} represent paths, we can apply some
 * path specific logic to a {@link PathMap}. This is reflected by methods such
 * as {@link MutablePathMap#insertFrom(Object, String)} and
 * {@link MutablePathMap#insertTo(String, Object)} or
 * {@link #retrieveFrom(String)} and {@link #retrieveTo(String)}.
 *
 * @param <T> The type of the value elements.
 */
public interface PathMap<T> extends Table<String, T>, TypeAccessor<T>, Dumpable, DelimiterAccessor, AnnotatorAccessor {

	/**
	 * Creates a printable representation of the content of this map.
	 * 
	 * @return A {@link String} with the content of this map which directly can
	 *         be printed.
	 */
	default String toPrintable() {
		final StringBuilder theBuilder = new StringBuilder();
		Object eValue;
		for ( String eKey : sortedKeys() ) {
			eValue = get( eKey );
			eValue = eValue != null ? "\"" + eValue + "\"" : "null";
			theBuilder.append( "\"" + eKey + "\" = " + eValue );
			theBuilder.append( System.lineSeparator() );
		}
		return theBuilder.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// MUTATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link MutablePathMap} adds mutable functionality to the
	 * {@link PathMap}.
	 *
	 * @param <T> The type of the value elements.
	 */
	public static interface MutablePathMap<T> extends PathMap<T>, TableDictionary<String, T>, Map<String, T> {

		/**
		 * Adds the given value to the path not overwriting any existing values:
		 * If the path points to a single value (and not to an array), then that
		 * value's path is converted to an array representation and the given
		 * value is appended to that array.
		 * 
		 * @param aPathElements The path elements to which to add the element.
		 * @param aValue The value to be added.
		 */
		default void add( Collection<?> aPathElements, T aValue ) {
			add( toPath( aPathElements ), aValue );
		}

		/**
		 * Adds the given value to the path not overwriting any existing values:
		 * If the path points to a single value (and not to an array), then that
		 * value's path is converted to an array representation and the given
		 * value is appended to that array.
		 * 
		 * @param aPath The path to which to add the element.
		 * @param aValue The value to be added.
		 */
		default void add( Object aPath, T aValue ) {
			add( toPath( aPath ), aValue );
		}

		/**
		 * Adds the given value to the path not overwriting any existing values:
		 * If the path points to a single value (and not to an array), then that
		 * value's path is converted to an array representation and the given
		 * value is appended to that array.
		 * 
		 * @param aPathElements The path elements to which to add the element.
		 * @param aValue The value to be added.
		 */
		default void add( Object[] aPathElements, T aValue ) {
			add( toPath( aPathElements ), aValue );
		}

		/**
		 * Adds the given value to the path not overwriting any existing values:
		 * If the path points to a single value (and not to an array), then that
		 * value's path is converted to an array representation and the given
		 * value is appended to that array.
		 * 
		 * @param aPath The path to which to add the element.
		 * @param aValue The value to be added.
		 */
		default void add( String aPath, T aValue ) {
			int index;
			if ( containsKey( aPath ) ) {
				final T theValue = remove( aPath );
				if ( isArray( aPath ) ) {
					index = lastArrayIndex( aPath );
					if ( index == -1 ) {
						index = 0;
					}
				}
				else {
					index = 0;
				}
				putValueAt( aPath, index, theValue );
				appendValueTo( aPath, aValue );
			}
			else if ( isArray( aPath ) ) {
				appendValueTo( aPath, aValue );
			}
			else {
				put( aPath, aValue );
			}
		}

		/**
		 * Adds the given value to the path not overwriting any existing values:
		 * If the path points to a single value (and not to an array), then that
		 * value's path is converted to an array representation and the given
		 * value is appended to that array.
		 * 
		 * @param aPathElements The path elements to which to add the element.
		 * @param aValue The value to be added.
		 */
		default void add( String[] aPathElements, T aValue ) {
			add( toPath( aPathElements ), aValue );
		}

		/**
		 * Adds an element to the next index as of
		 * {@link #nextArrayIndex(String[])}. If the path points to a single
		 * value (and not to an array), then that value's path is converted to
		 * an array representation and the given value is appended to that
		 * array.
		 * 
		 * @param aValue The value to be appended.
		 */
		default void add( T aValue ) {
			add( getRootPath(), aValue );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String[])}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( Collection<?> aPathElements, Object aDir ) {
			appendDirTo( toPath( aPathElements ), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String[])}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( Collection<?> aPathElements, PathMap<T> aDir ) {
			appendDirTo( toPath( aPathElements ), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex()}.
		 * 
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir()}.
		 */
		default void appendDirTo( Object aDir ) {
			appendDirTo( getRootPath(), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String[])}.
		 * 
		 * @param aPath The path to which to append the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( Object aPath, Object aDir ) {
			appendDirTo( toPath( aPath ), new PathMapImpl<>( aDir, getDelimiter(), getType() ) );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String[])}.
		 * 
		 * @param aPath The path to which to append the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( Object aPath, PathMap<T> aDir ) {
			appendDirTo( toPath( aPath ), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String[])}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( Object[] aPathElements, Object aDir ) {
			appendDirTo( toPath( aPathElements ), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String[])}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( Object[] aPathElements, PathMap<T> aDir ) {
			appendDirTo( toPath( aPathElements ), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex()}.
		 * 
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir()}.
		 */
		default void appendDirTo( PathMap<T> aDir ) {
			appendDirTo( getRootPath(), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String)}.
		 * 
		 * @param aPath The path to which to append the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( String aPath, Object aDir ) {
			appendDirTo( aPath, new PathMapImpl<>( aDir, getDelimiter(), getType() ) );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String)}.
		 * 
		 * @param aPath The path to which to append the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( String aPath, PathMap<T> aDir ) {
			final int nextIndex = nextDirIndex( aPath );
			for ( String eKey : aDir.keySet() ) {
				put( toPath( aPath, nextIndex, eKey ), aDir.get( eKey ) );
			}
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String)}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( String[] aPathElements, Object aDir ) {
			appendDirTo( toPath( aPathElements ), aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Adds all given properties to the next index as
		 * of {@link #nextDirIndex(String)}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the given elements.
		 * @param aDir The values to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default void appendDirTo( String[] aPathElements, PathMap<T> aDir ) {
			appendDirTo( toPath( aPathElements ), aDir );
		}

		/**
		 * Adds an element to the next index as of
		 * {@link #nextArrayIndex(String[])}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the next indexed element.
		 * @param aValue The value to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default void appendValueTo( Collection<?> aPathElements, T aValue ) {
			appendValueTo( toPath( aPathElements ), aValue );
		}

		/**
		 * Adds an element to the next index as of
		 * {@link #nextArrayIndex(String[])}.
		 * 
		 * @param aPath The path to which to append the next indexed element.
		 * @param aValue The value to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default void appendValueTo( Object aPath, T aValue ) {
			appendValueTo( toPath( aPath ), aValue );
		}

		/**
		 * Adds an element to the next index as of
		 * {@link #nextArrayIndex(String[])}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the next indexed element.
		 * @param aValue The value to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default void appendValueTo( Object[] aPathElements, T aValue ) {
			appendValueTo( toPath( aPathElements ), aValue );
		}

		/**
		 * Adds an element to the next index as of
		 * {@link #nextArrayIndex(String)}.
		 * 
		 * @param aPath The path to which to append the next indexed element.
		 * @param aValue The value to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default void appendValueTo( String aPath, T aValue ) {
			final int nextIndex = nextArrayIndex( aPath );
			put( toPath( aPath, nextIndex ), aValue );
		}

		/**
		 * Adds an element to the next index as of
		 * {@link #nextArrayIndex(String)}.
		 * 
		 * @param aPathElements The path elements of the path to which to append
		 *        the next indexed element.
		 * @param aValue The value to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default void appendValueTo( String[] aPathElements, T aValue ) {
			appendValueTo( toPath( aPathElements ), aValue );
		}

		/**
		 * Adds an element to the next index as of {@link #nextArrayIndex()}.
		 * 
		 * @param aValue The value to be appended.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray()}.
		 */
		default void appendValueTo( T aValue ) {
			appendValueTo( getRootPath(), aValue );
		}

		/**
		 * Removes an element identified by the given key.
		 * 
		 * @param aPathElements The path elements of the path which's element is
		 *        to be removed.
		 * 
		 * @return The value being removed.
		 */
		default T delete( Collection<?> aPathElements ) {
			return remove( toPath( aPathElements ) );
		}

		/**
		 * Removes an element identified by the given key.
		 * 
		 * @param aPath The path which's element is to be removed.
		 * 
		 * @return The value being removed.
		 */
		default T delete( Object aPath ) {
			return remove( toPath( aPath ) );
		}

		/**
		 * Removes an element identified by the given key.
		 * 
		 * @param aPathElements The path elements of the path which's element is
		 *        to be removed.
		 * 
		 * @return The value being removed.
		 */
		default T delete( Object[] aPathElements ) {
			return remove( toPath( aPathElements ) );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default T delete( String aPath ) {
			return remove( aPath );
		}

		/**
		 * Removes an element identified by the given key.
		 * 
		 * @param aPathElements The path elements of the path which's element is
		 *        to be removed.
		 * 
		 * @return The value being removed.
		 */
		default T delete( String[] aPathElements ) {
			return remove( toPath( aPathElements ) );
		}

		/**
		 * Inspects the given object and adds all elements found in the given
		 * object. Elements of type {@link Map}, {@link Collection} and arrays
		 * are identified and handled as of their type: The path for each value
		 * in a {@link Map} is appended with its according key. The path for
		 * each value in a {@link Collection} or array is appended with its
		 * according index of occurrence (in case of a {@link List} or an array,
		 * its actual index). In case of reflection, the path for each member is
		 * appended with its according mamber's name. All elements (e.g. the
		 * members and values) are inspected recursively which results in the
		 * according paths of the terminating values.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		void insert( Object aFrom );

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insert(Object)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void insert( PathMap<T> aFrom ) {
			insert( (Object) aFrom );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( toPath( aToPathElements ), aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertBetween( Collection<?> aToPathElements, PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( toPath( aToPathElements ), (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), aFrom, toNormalizedPath( InterOperableMap.asKey( aFromPath ) ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertBetween( Object aToPath, PathMap<T> aFromMap, Object aFromPath ) {
			insertBetween( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), (Object) aFromMap, toNormalizedPath( InterOperableMap.asKey( aFromPath ) ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( toPath( aToPathElements ), aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertBetween( Object[] aToPathElements, PathMap<T> aFrom, Object[] aFromPathElements ) {
			insertBetween( toPath( aToPathElements ), (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		void insertBetween( String aToPath, Object aFrom, String aFromPath );

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertBetween( String aToPath, PathMap<T> aFrom, String aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( toPath( aToPathElements ), aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertBetween( String[] aToPathElements, PathMap<T> aFrom, String[] aFromPathElements ) {
			insertBetween( toPath( aToPathElements ), (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, toPath( aFromPath ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertFrom( Object aFrom, Object... aFromPathElements ) {
			insertFrom( aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		void insertFrom( Object aFrom, String aFromPath );

		/**
		 * Same as {@link #insert(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertFrom( PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertFrom( PathMap<T> aFrom, Object aFromPath ) {
			insertFrom( (Object) aFrom, toPath( aFromPath ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertFrom( PathMap<T> aFrom, Object... aFromPathElements ) {
			insertFrom( (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void insertFrom( PathMap<T> aFrom, String aFromPath ) {
			insertFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void insertFrom( PathMap<T> aFrom, String... aFromPathElements ) {
			insertFrom( (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void insertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( toPath( aToPathElements ), aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( Collection<?> aToPathElements, PathMap<T> aFrom ) {
			insertTo( toPath( aToPathElements ), (Object) aFrom );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void insertTo( Object aToPath, Object aFrom ) {
			insertTo( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( Object aToPath, PathMap<T> aFromMap ) {
			insertTo( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), (Object) aFromMap );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void insertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( toPath( aToPathElements ), aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( Object[] aToPathElements, PathMap<T> aFrom ) {
			insertTo( toPath( aToPathElements ), (Object) aFrom );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		void insertTo( String aToPath, Object aFrom );

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( String aToPath, PathMap<T> aFrom ) {
			insertTo( aToPath, (Object) aFrom );
		}

		/**
		 * Same as {@link #insert(Object)} though starting insertion of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void insertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( toPath( aToPathElements ), aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void insertTo( String[] aToPathElements, PathMap<T> aFrom ) {
			insertTo( toPath( aToPathElements ), (Object) aFrom );
		}

		/**
		 * Inspects the given object and adds all elements found in the given
		 * object in case the targeted path does not exist or points to a null
		 * value. E.g. existing path/value pairs are not overwritten. Elements
		 * of type {@link Map}, {@link Collection} and arrays are identified and
		 * handled as of their type: The path for each value in a {@link Map} is
		 * appended with its according key. The path for each value in a
		 * {@link Collection} or array is appended with its according index of
		 * occurrence (in case of a {@link List} or an array, its actual index).
		 * In case of reflection, the path for each member is appended with its
		 * according mamber's name. All elements (e.g. the members and values)
		 * are inspected recursively which results in the according paths of the
		 * terminating values.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		void merge( Object aFrom );

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #merge(Object)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void merge( PathMap<T> aFrom ) {
			merge( (Object) aFrom );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( toPath( aToPathElements ), aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeBetween( Collection<?> aToPathElements, PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( toPath( aToPathElements ), (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFromObj The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void mergeBetween( Object aToPath, Object aFromObj, Object aFromPath ) {
			mergeBetween( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), aFromObj, toNormalizedPath( InterOperableMap.asKey( aFromPath ) ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void mergeBetween( Object aToPath, PathMap<T> aFromMap, Object aFromPath ) {
			mergeBetween( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), (Object) aFromMap, toNormalizedPath( InterOperableMap.asKey( aFromPath ) ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( toPath( aToPathElements ), aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeBetween( Object[] aToPathElements, PathMap<T> aFrom, Object[] aFromPathElements ) {
			mergeBetween( toPath( aToPathElements ), (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		void mergeBetween( String aToPath, Object aFrom, String aFromPath );

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void mergeBetween( String aToPath, PathMap<T> aFrom, String aFromPath ) {
			mergeBetween( aToPath, (Object) aFrom, aFromPath );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "to-path" and starting object
		 * introspection at the given "from-path", e.g. ignoring all paths not
		 * starting with the given path during the introspection process. The
		 * resulting paths do not start with the "from-path" path, them start
		 * with the given "to-path".
		 *
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( toPath( aToPathElements ), aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeBetween( String[] aToPathElements, PathMap<T> aFrom, String[] aFromPathElements ) {
			mergeBetween( toPath( aToPathElements ), (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void mergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, toPath( aFromPath ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		void mergeFrom( Object aFrom, String aFromPath );

		/**
		 * Same as {@link #merge(Object)} though starting object introspection
		 * at the given "path", e.g. ignoring all paths not starting with the
		 * given path during the introspection process. The resulting paths do
		 * not start with the provided path.
		 * 
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeFrom( PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void mergeFrom( PathMap<T> aFrom, Object aFromPath ) {
			mergeFrom( (Object) aFrom, toPath( aFromPath ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeFrom( PathMap<T> aFrom, Object... aFromPathElements ) {
			mergeFrom( (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 */
		default void mergeFrom( PathMap<T> aFrom, String aFromPath ) {
			mergeFrom( (Object) aFrom, aFromPath );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 */
		default void mergeFrom( PathMap<T> aFrom, String... aFromPathElements ) {
			mergeFrom( (Object) aFrom, toPath( aFromPathElements ) );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void mergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( toPath( aToPathElements ), aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void mergeTo( Collection<?> aToPathElements, PathMap<T> aFrom ) {
			mergeTo( toPath( aToPathElements ), (Object) aFrom );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFromObj The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void mergeTo( Object aToPath, Object aFromObj ) {
			mergeTo( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), aFromObj );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void mergeTo( Object aToPath, PathMap<T> aFromMap ) {
			mergeTo( toNormalizedPath( InterOperableMap.asKey( aToPath ) ), (Object) aFromMap );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void mergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( toPath( aToPathElements ), aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void mergeTo( Object[] aToPathElements, PathMap<T> aFrom ) {
			mergeTo( toPath( aToPathElements ), (Object) aFrom );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		void mergeTo( String aToPath, Object aFrom );

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void mergeTo( String aToPath, PathMap<T> aFrom ) {
			mergeTo( aToPath, (Object) aFrom );
		}

		/**
		 * Same as {@link #merge(Object)} though starting merging of object's
		 * introspected values at the given "path".
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 */
		default void mergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( toPath( aToPathElements ), aFrom );
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 */
		default void mergeTo( String[] aToPathElements, PathMap<T> aFrom ) {
			mergeTo( toPath( aToPathElements ), (Object) aFrom );
		}

		/**
		 * Puts the given value into the child's path, relative to the given
		 * parent's path.
		 * 
		 * @param aPathElements The path elements of the path relative to which
		 *        to put the value.
		 * @param aValue The value to be added.
		 * 
		 * @return The replaced value in case a value has already been set for
		 *         the resulting path, or null.
		 */
		default T put( Collection<?> aPathElements, T aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * Puts the given value into the child's path, relative to the given
		 * parent's path.
		 * 
		 * @param aPathElements The path elements of the path relative to which
		 *        to put the value.
		 * @param aValue The value to be added.
		 * 
		 * @return The replaced value in case a value has already been set for
		 *         the resulting path, or null.
		 */
		default T put( Object[] aPathElements, T aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default T put( Relation<String, T> aRelation ) {
			return put( aRelation.getKey(), aRelation.getValue() );
		}

		/**
		 * Puts the given value into the child's path, relative to the given
		 * parent's path.
		 * 
		 * @param aPathElements The path elements of the path relative to which
		 *        to put the value.
		 * @param aValue The value to be added.
		 * 
		 * @return The replaced value in case a value has already been set for
		 *         the resulting path, or null.
		 */
		default T put( String[] aPathElements, T aValue ) {
			return put( toPath( aPathElements ), aValue );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPathElements The elements of the path below where to put the
		 *        array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( Collection<?> aPathElements, Collection<T> aValues ) {
			return putArray( toPath( aPathElements ), aValues );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPathElements The elements of the path below where to put the
		 *        array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( Collection<?> aPathElements, T[] aValues ) {
			return putArray( toPath( aPathElements ), aValues );
		}

		/**
		 * Applies the {@link #putArray(Object[])} method for the root path "/".
		 * 
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( Collection<T> aValues ) {
			return putArray( "", aValues );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPath The path below where to put the array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( Object aPath, Collection<T> aValues ) {
			return putArray( toPath( aPath ), aValues );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPath The path below where to put the array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( Object aPath, T[] aValues ) {
			return putArray( toPath( aPath ), aValues );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPathElements The elements of the path below where to put the
		 *        array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( Object[] aPathElements, Collection<T> aValues ) {
			return putArray( toPath( aPathElements ), aValues );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPathElements The elements of the path below where to put the
		 *        array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( Object[] aPathElements, T[] aValues ) {
			return putArray( toPath( aPathElements ), aValues );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPath The path below where to put the array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( String aPath, Collection<T> aValues ) {
			aPath = toNormalizedPath( aPath );
			final T[] theResult = getArray( aPath );
			removeFrom( aPath );
			// Add new |-->
			if ( aValues != null && aValues.size() != 0 ) {
				int index = 0;
				final Iterator<T> e = aValues.iterator();
				while ( e.hasNext() ) {
					putValueAt( aPath, index++, e.next() );
				}
			}
			// Add new <--|
			return theResult;
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPath The path below where to put the array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( String aPath, T[] aValues ) {
			aPath = toNormalizedPath( aPath );
			final T[] theResult = getArray( aPath );
			removeFrom( aPath );
			// Add new |-->
			if ( aValues != null && aValues.length != 0 ) {
				for ( int i = 0; i < aValues.length; i++ ) {
					putValueAt( aPath, i, aValues[i] );
				}
			}
			// Add new <--|
			return theResult;
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPathElements The elements of the path below where to put the
		 *        array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( String[] aPathElements, Collection<T> aValues ) {
			return putArray( toPath( aPathElements ), aValues );
		}

		/**
		 * Sets the leaves below the given path in an array and replaces any
		 * existing array. Leaves in a {@link PathMap} are reckoned to be part
		 * of an array when they belong to the same hierarchy level and when
		 * their keys represent integer numbers. Given the below example, the
		 * elements below "/animals/dogs" can be represented as an array with
		 * five elements (the path is denoted at the left hand side of the
		 * assignment ":=" operator and the value at the right and side
		 * accordingly):
		 * <ul>
		 * <li>"/animals/dogs/0" := "ace"</li>
		 * <li>"/animals/dogs/1" := "bandit"</li>
		 * <li>"/animals/dogs/2" := "radar"</li>
		 * <li>"/animals/dogs/3" := "echo"</li>
		 * <li>"/animals/dogs/4" := "snoopy"</li>
		 * </ul>
		 * The resulting array when calling {@link #getArray(String)} for the
		 * path "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
		 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
		 * <code>true</code> whereas calling <code>isArray("/animals")</code>
		 * would return false.
		 *
		 * @param aPathElements The elements of the path below where to put the
		 *        array.
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( String[] aPathElements, T[] aValues ) {
			return putArray( toPath( aPathElements ), aValues );
		}

		/**
		 * Applies the {@link #putArray(Object[])} method for the root path "/".
		 * 
		 * @param aValues The values to be put.
		 * 
		 * @return An array of the leaves being replaced by the provided
		 *         elements.
		 */
		default T[] putArray( T[] aValues ) {
			return putArray( "", aValues );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( Collection<?> aPathElements, int aIndex, PathMap<T> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the root path at the
		 * given index.
		 * 
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( int aIndex, Object aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the root path at the
		 * given index.
		 * 
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( int aIndex, PathMap<T> aDir ) {
			return putDirAt( getRootPath(), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( Object aPath, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( Object aPath, int aIndex, PathMap<T> aDir ) {
			return putDirAt( toPath( aPath ), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( Object[] aPathElements, int aIndex, PathMap<T> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( String aPath, int aIndex, Object aDir ) {
			final PathMap<T> theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( String aPath, int aIndex, PathMap<T> aDir ) {
			final PathMap<T> theReplaced = removeDirAt( aPath, aIndex );
			insertTo( toPath( aPath, aIndex ), aDir );
			return theReplaced;

		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return The directory which has been replaced by the given directory.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> putDirAt( String[] aPathElements, int aIndex, PathMap<T> aDir ) {
			return putDirAt( toPath( aPathElements ), aIndex, aDir );
		}

		/**
		 * Puts a value below the path at the given index as of
		 * {@link #putArray(String, Object[])}.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aValue The value to be put at the index.
		 * 
		 * @return The value which has been replaced by the given value.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default T putValueAt( Collection<?> aPathElements, int aIndex, T aValue ) {
			final String thePath = toPath( aPathElements, aIndex );
			return put( thePath, aValue );
		}

		/**
		 * Puts a value below the root path at the given index as of
		 * {@link #putArray(String, Object[])}.
		 * 
		 * @param aIndex The index of the indexed element.
		 * @param aValue The value to be put at the index.
		 * 
		 * @return The value which has been replaced by the given value.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default T putValueAt( int aIndex, T aValue ) {
			final String thePath = toPath( aIndex );
			return put( thePath, aValue );
		}

		/**
		 * Puts a value below the path at the given index as of
		 * {@link #putArray(String, Object[])}.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aValue The value to be put at the index.
		 * 
		 * @return The value which has been replaced by the given value.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default T putValueAt( Object aPath, int aIndex, T aValue ) {
			final String thePath = toPath( aPath, aIndex );
			return put( thePath, aValue );
		}

		/**
		 * Puts a value below the path at the given index as of
		 * {@link #putArray(String, Object[])}.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aValue The value to be put at the index.
		 * 
		 * @return The value which has been replaced by the given value.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default T putValueAt( Object[] aPathElements, int aIndex, T aValue ) {
			final String thePath = toPath( aPathElements, aIndex );
			return put( thePath, aValue );
		}

		/**
		 * Puts a value below the path at the given index as of
		 * {@link #putArray(String, Object[])}.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aValue The value to be put at the index.
		 * 
		 * @return The value which has been replaced by the given value.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default T putValueAt( String aPath, int aIndex, T aValue ) {
			final String thePath = toPath( aPath, aIndex );
			return put( thePath, aValue );
		}

		/**
		 * Puts a value below the path at the given index as of
		 * {@link #putArray(String, Object[])}.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aValue The value to be put at the index.
		 * 
		 * @return The value which has been replaced by the given value.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isArray(String)}.
		 */
		default T putValueAt( String[] aPathElements, int aIndex, T aValue ) {
			final String thePath = toPath( aPathElements, aIndex );
			return put( thePath, aValue );
		}

		/**
		 * Removes the property as of {@link #remove(Object)} for the provided
		 * path elements (as of {@link #toPath(Object...)}). BEWARE: The method
		 * with two arguments as of {@link #remove(Object, Object)} behaves
		 * https://www.metacodes.pro as of its inheritance from
		 * {@link Map#remove(Object, Object)}.
		 * 
		 * @param aPathElements The elements of the path from where to remove
		 *        the property.
		 * 
		 * @return the previous value associated with the according path, or
		 *         null if there was no mapping for the according path.
		 */
		default T remove( Object... aPathElements ) {
			return remove( toPath( aPathElements ) );
		}

		/**
		 * Removes the property as of {@link #remove(Object)} for the provided
		 * path elements (as of {@link #toPath(String...)}).
		 * 
		 * @param aPathElements The elements of the path from where to remove
		 *        the property.
		 * 
		 * @return the previous value associated with the according path, or
		 *         null if there was no mapping for the according path.
		 */
		default T remove( String... aPathElements ) {
			return remove( toPath( aPathElements ) );
		}

		/**
		 * Removes the property as of {@link #remove(Object)} for the provided
		 * path elements (as of {@link #toPath(String...)}). This method
		 * overloads the method {@link Map#remove(Object, Object)} in order to
		 * prevent unexpected behavior of the {@link #remove(String...)} method!
		 * 
		 * @param aPathElement1 The first path element of the path from where to
		 *        remove the property.
		 * @param aPathElement2 The second path element of the path from where
		 *        to remove the property.
		 * 
		 * @return the previous value associated with the according path, or
		 *         null if there was no mapping for the according path.
		 */
		default T remove( String aPathElement1, String aPathElement2 ) {
			return remove( toPath( aPathElement1, aPathElement2 ) );
		}

		/**
		 * Removes all elements which's keys match the provided path query.
		 * Queries the keys of the instance using the {@link PathMatcher}'
		 * matching patterns, similar to the wildcards '*', '?' and '**' known
		 * when querying folders of a filesystem: The {@link PathMatcher}
		 * applies the following rules from the ANT path pattern to the query
		 * provided: A single asterisk ("*" as of {@link Wildcard#FILE}) matches
		 * zero or more characters within a path name. A double asterisk ("**"
		 * as of {@link Wildcard#PATH}) matches zero or more characters across
		 * directory levels. A question mark ("?" as of {@link Wildcard#CHAR})
		 * matches exactly one character within a path name. The single asterisk
		 * ("*" as of {@link Wildcard#FILE}), the double asterisk ("**" as of
		 * {@link Wildcard#PATH}) and the question mark ("?" as of
		 * {@link Wildcard#CHAR}) we refer to as wildcards.
		 * 
		 * @param aPathQueryElements The elements representing your path query
		 *        including your wildcards.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeAll( Collection<?> aPathQueryElements ) {
			return removeAll( toPath( aPathQueryElements ) );
		}

		/**
		 * Removes all elements which's keys match the provided path query.
		 * Queries the keys of the instance using the {@link PathMatcher}'
		 * matching patterns, similar to the wildcards '*', '?' and '**' known
		 * when querying folders of a filesystem: The {@link PathMatcher}
		 * applies the following rules from the ANT path pattern to the query
		 * provided: A single asterisk ("*" as of {@link Wildcard#FILE}) matches
		 * zero or more characters within a path name. A double asterisk ("**"
		 * as of {@link Wildcard#PATH}) matches zero or more characters across
		 * directory levels. A question mark ("?" as of {@link Wildcard#CHAR})
		 * matches exactly one character within a path name. The single asterisk
		 * ("*" as of {@link Wildcard#FILE}), the double asterisk ("**" as of
		 * {@link Wildcard#PATH}) and the question mark ("?" as of
		 * {@link Wildcard#CHAR}) we refer to as wildcards.
		 * 
		 * @param aPathQueryElements The elements representing your path query
		 *        including your wildcards.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeAll( Object... aPathQueryElements ) {
			return removeAll( toPath( aPathQueryElements ) );
		}

		/**
		 * Removes all elements which's keys match the provided path query.
		 * Queries the keys of the instance using the {@link PathMatcher}'
		 * matching patterns, similar to the wildcards '*', '?' and '**' known
		 * when querying folders of a filesystem: The {@link PathMatcher}
		 * applies the following rules from the ANT path pattern to the query
		 * provided: A single asterisk ("*" as of {@link Wildcard#FILE}) matches
		 * zero or more characters within a path name. A double asterisk ("**"
		 * as of {@link Wildcard#PATH}) matches zero or more characters across
		 * directory levels. A question mark ("?" as of {@link Wildcard#CHAR})
		 * matches exactly one character within a path name. The single asterisk
		 * ("*" as of {@link Wildcard#FILE}), the double asterisk ("**" as of
		 * {@link Wildcard#PATH}) and the question mark ("?" as of
		 * {@link Wildcard#CHAR}) we refer to as wildcards.
		 * 
		 * @param aPathQuery The path query including your wildcards.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeAll( Object aPathQuery ) {
			return removeAll( toPath( aPathQuery ) );
		}

		/**
		 * Removes all elements which's keys match the provided regular
		 * expression: Queries the keys of the instance using the provided
		 * {@link Pattern}.
		 *
		 * @param aRegExp The regular expression to be used for the query.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeAll( Pattern aRegExp ) {
			final Set<String> theSet = new HashSet<>();
			for ( String ePath : paths() ) {
				if ( aRegExp.matcher( ePath ).matches() ) {
					synchronized ( theSet ) {
						theSet.add( ePath );
					}
				}
			}
			return removePaths( theSet );
		}

		/**
		 * Removes all elements which's keys match the provided path query.
		 * Queries the keys of the instance using the {@link PathMatcher}'
		 * matching patterns, similar to the wildcards '*', '?' and '**' known
		 * when querying folders of a filesystem: The {@link PathMatcher}
		 * applies the following rules from the ANT path pattern to the query
		 * provided: A single asterisk ("*" as of {@link Wildcard#FILE}) matches
		 * zero or more characters within a path name. A double asterisk ("**"
		 * as of {@link Wildcard#PATH}) matches zero or more characters across
		 * directory levels. A question mark ("?" as of {@link Wildcard#CHAR})
		 * matches exactly one character within a path name. The single asterisk
		 * ("*" as of {@link Wildcard#FILE}), the double asterisk ("**" as of
		 * {@link Wildcard#PATH}) and the question mark ("?" as of
		 * {@link Wildcard#CHAR}) we refer to as wildcards.
		 * 
		 * @param aPathQueryElements The elements representing your path query
		 *        including your wildcards.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeAll( String... aPathQueryElements ) {
			return removeAll( toPath( aPathQueryElements ) );
		}

		/**
		 * Removes all elements which's keys match the provided path query.
		 * Queries the keys of the instance using the {@link PathMatcher}'
		 * matching patterns, similar to the wildcards '*', '?' and '**' known
		 * when querying folders of a filesystem: The {@link PathMatcher}
		 * applies the following rules from the ANT path pattern to the query
		 * provided: A single asterisk ("*" as of {@link Wildcard#FILE}) matches
		 * zero or more characters within a path name. A double asterisk ("**"
		 * as of {@link Wildcard#PATH}) matches zero or more characters across
		 * directory levels. A question mark ("?" as of {@link Wildcard#CHAR})
		 * matches exactly one character within a path name. The single asterisk
		 * ("*" as of {@link Wildcard#FILE}), the double asterisk ("**" as of
		 * {@link Wildcard#PATH}) and the question mark ("?" as of
		 * {@link Wildcard#CHAR}) we refer to as wildcards.
		 * 
		 * @param aPathQuery The path query including your wildcards.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeAll( String aPathQuery ) {
			final Set<String> theSet = new HashSet<>();
			final PathMatcher theMatcher = new PathMatcher( aPathQuery, getDelimiter() );
			for ( String ePath : paths() ) {
				if ( theMatcher.isMatching( ePath ) ) {
					synchronized ( theSet ) {
						theSet.add( ePath );
					}
				}
			}
			return removePaths( theSet );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Removes the elements of the given index below
		 * the root path. Given the following paths and index = 1: <code> 
		 * /0/0aaa
		 * /0/0bbb
		 * /0/0bbb
		 * /1/1aaa
		 * /1/1bbb
		 * /1/1bbb
		 * </code> You will get a result containing all the elements which's
		 * keys begin with "/1/". The keys of the result will exclude the path
		 * "prefix" "/1".
		 * 
		 * @param aIndex The index which to use.
		 * 
		 * @return The indexed elements without the indexed path "prefixes".
		 */
		default PathMap<T> removeDirAt( int aIndex ) {
			return removeDirAt( getRootPath(), aIndex );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Removes the elements of the given index below
		 * the given path. Given the following paths and index = 1 with a
		 * queried path "/root/child": <code> 
		 * /root/child/0/0aaa
		 * /root/child/0/0bbb
		 * /root/child/0/0bbb
		 * /root/child/1/1aaa
		 * /root/child/1/1bbb
		 * /root/child/1/1bbb
		 * </code> You will get a result containing all the elements which's
		 * keys begin with "/root/child/1/". The keys of the result will exclude
		 * the path "prefix" "/root/child/1".
		 *
		 * @param aPath The path from which to remove the indexed elements.
		 * @param aIndex The index which to use.
		 * 
		 * @return The indexed elements without the indexed path "prefixes".
		 */
		default PathMap<T> removeDirAt( Object aPath, int aIndex ) {
			return removeDirAt( toPath( aPath ), aIndex );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Removes the elements of the given index below
		 * the given path. Given the following paths and index = 1 with a
		 * queried path "/root/child": <code> 
		 * /root/child/0/0aaa
		 * /root/child/0/0bbb
		 * /root/child/0/0bbb
		 * /root/child/1/1aaa
		 * /root/child/1/1bbb
		 * /root/child/1/1bbb
		 * </code> You will get a result containing all the elements which's
		 * keys begin with "/root/child/1/".The keys of the result will exclude
		 * the path "prefix" "/root/child/1".
		 *
		 * @param aPathElements The elements of the path from which to remove
		 *        the indexed elements.
		 * @param aIndex The index which to use.
		 * 
		 * @return The indexed elements without the indexed path "prefixes".
		 */
		default PathMap<T> removeDirAt( Object[] aPathElements, int aIndex ) {
			return removeDirAt( toPath( aPathElements ), aIndex );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Removes the elements of the given index below
		 * the given path. Given the following paths and index = 1 with a
		 * queried path "/root/child": <code> 
		 * /root/child/0/0aaa
		 * /root/child/0/0bbb
		 * /root/child/0/0bbb
		 * /root/child/1/1aaa
		 * /root/child/1/1bbb
		 * /root/child/1/1bbb
		 * </code> You will get a result containing all the elements which's
		 * keys begin with "/root/child/1/".The keys of the result will exclude
		 * the path "prefix" "/root/child/1".
		 *
		 * @param aPath The path from which to remove the indexed elements.
		 * @param aIndex The index which to use.
		 * 
		 * @return The indexed elements without the indexed path "prefixes".
		 */
		default PathMap<T> removeDirAt( String aPath, int aIndex ) {
			return removeFrom( toPath( aPath, aIndex ) );
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Removes the elements of the given index below
		 * the given path. Given the following paths and index = 1 with a
		 * queried path "/root/child": <code> 
		 * /root/child/0/0aaa
		 * /root/child/0/0bbb
		 * /root/child/0/0bbb
		 * /root/child/1/1aaa
		 * /root/child/1/1bbb
		 * /root/child/1/1bbb
		 * </code> You will get a result containing all the elements which's
		 * keys begin with "/root/child/1/".The keys of the result will exclude
		 * the path "prefix" "/root/child/1".
		 *
		 * @param aPathElements The elements of the path from which to remove
		 *        the indexed elements.
		 * @param aIndex The index which to use.
		 * 
		 * @return The indexed elements without the indexed path "prefixes".
		 */
		default PathMap<T> removeDirAt( String[] aPathElements, int aIndex ) {
			return removeDirAt( toPath( aPathElements ), aIndex );
		}

		/**
		 * Removes all properties below the provided path and returns the
		 * {@link PathMap} with the removed elements, by which the paths exclude
		 * the parent's path.
		 * 
		 * @param aPathElements The path elements for the path from where to
		 *        remove all properties.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeFrom( Collection<?> aPathElements ) {
			return removeFrom( toPath( aPathElements ) );
		}

		/**
		 * Removes all properties below the provided path and returns the
		 * {@link PathMap} with the removed elements, by which the paths exclude
		 * the parent's path.
		 * 
		 * @param aPathElements The path elements for the path from where to
		 *        remove all properties.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeFrom( Object... aPathElements ) {
			return removeFrom( toPath( aPathElements ) );
		}

		/**
		 * Removes all properties below the provided path and returns the
		 * {@link PathMap} with the removed elements, by which the paths exclude
		 * the parent's path.
		 * 
		 * @param aPath The path from where to remove all properties.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeFrom( Object aPath ) {
			return removeFrom( toPath( aPath ) );
		}

		/**
		 * Removes all properties below the provided path and returns the
		 * {@link PathMap} with the removed elements, by which the paths exclude
		 * the parent's path.
		 * 
		 * @param aPath The path from where to remove all properties.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeFrom( String aPath ) {
			final PathMapBuilder<T> theRemoved = new PathMapBuilderImpl<>( getDelimiter(), getType() );
			aPath = toNormalizedPath( aPath ) + getDelimiter();
			final Iterator<String> e = paths().iterator();
			String eKey;
			while ( e.hasNext() ) {
				eKey = e.next();
				if ( eKey.startsWith( aPath ) ) {
					theRemoved.put( eKey.substring( aPath.length() ), get( eKey ) );
					e.remove();
				}
			}
			return theRemoved;
		}

		/**
		 * Removes all properties below the provided path and returns the
		 * {@link PathMap} with the removed elements, by which the paths exclude
		 * the parent's path.
		 * 
		 * @param aPathElements The path elements for the path from where to
		 *        remove all properties.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removeFrom( String... aPathElements ) {
			return removeFrom( toPath( aPathElements ) );
		}

		/**
		 * Removes the paths contained in the provided collection.
		 * 
		 * @param aPaths The paths to be removed.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removePaths( Collection<?> aPaths ) {
			final PathMapBuilder<T> theRemoved = new PathMapBuilderImpl<>( getDelimiter(), getType() );
			for ( Object ePath : aPaths ) {
				theRemoved.put( ePath instanceof String ? (String) ePath : toPath( ePath ), remove( ePath ) );
			}
			return theRemoved;
		}

		/**
		 * Removes the paths contained in the provided collection.
		 * 
		 * @param aPaths The paths to be removed.
		 * 
		 * @return The removed properties.
		 */
		default PathMap<T> removePaths( String... aPaths ) {
			final PathMapBuilder<T> theRemoved = new PathMapBuilderImpl<>( getDelimiter(), getType() );
			for ( String ePath : aPaths ) {
				theRemoved.put( ePath, remove( ePath ) );
			}
			return theRemoved;
		}

		/**
		 * Removes the value of an array at the root path with the given array
		 * index.
		 * 
		 * @param aIndex The index inside the array for which to remove the
		 *        value.
		 * 
		 * @return The removed value.
		 */
		default T removeValueAt( int aIndex ) {
			return removeValueAt( getRootPath(), aIndex );
		}

		/**
		 * Removes the value of an array at the given path path with the given
		 * array index.
		 * 
		 * @param aPath The path pointing to the array.
		 * @param aIndex The index inside the array for which to remove the
		 *        value.
		 * 
		 * @return The removed value.
		 */
		default T removeValueAt( Object aPath, int aIndex ) {
			return removeValueAt( toPath( aPath ), aIndex );
		}

		/**
		 * Removes the value of an array at the given path path with the given
		 * array index.
		 * 
		 * @param aPathElements The path elements for the path pointing to the
		 *        array.
		 * @param aIndex The index inside the array for which to remove the
		 *        value.
		 * 
		 * @return The removed value.
		 */
		default T removeValueAt( Object[] aPathElements, int aIndex ) {
			return removeValueAt( toPath( aPathElements ), aIndex );
		}

		/**
		 * Removes the value of an array at the given path path with the given
		 * array index.
		 * 
		 * @param aPath The path pointing to the array.
		 * @param aIndex The index inside the array for which to remove the
		 *        value.
		 * 
		 * @return The removed value.
		 */
		default T removeValueAt( String aPath, int aIndex ) {
			return remove( toPath( aPath, aIndex ) );
		}

		/**
		 * Removes the value of an array at the given path path with the given
		 * array index.
		 * 
		 * @param aPathElements The path elements for the path pointing to the
		 *        array.
		 * @param aIndex The index inside the array for which to remove the
		 *        value.
		 * 
		 * @return The removed value.
		 */
		default T removeValueAt( String[] aPathElements, int aIndex ) {
			return removeValueAt( toPath( aPathElements ), aIndex );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(Class)}, though in fact it is the same as
		 * {@link #insert(Object)} and provided for semantic emphasis and
		 * convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 */
		default <TYPE> void fromType( TYPE aType ) {
			fromType( getRootPath(), aType );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(Object, Class)}, though in fact it is the same as
		 * {@link #insertTo(Object, Object)} and provided for semantic emphasis
		 * and convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aFromPath The path representing the root from which to take
		 *        the data for the instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 */
		default <TYPE> void fromType( Object aFromPath, TYPE aType ) {
			fromType( toPath( aFromPath ), aType );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(Object[], Class)}, though in fact it is the same as
		 * {@link #insertTo(Object[], Object)} and provided for semantic
		 * emphasis and convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aFromPathElements The path elements representing the root from
		 *        which to take the data for the instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 */
		default <TYPE> void fromType( Object[] aFromPathElements, TYPE aType ) {
			fromType( toPath( aFromPathElements ), aType );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(Object[], Class)}, though in fact it is the same as
		 * {@link #insertTo(Object[], Object)} and provided for semantic
		 * emphasis and convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 * @param aFromPathElements The path elements representing the root from
		 *        which to take the data for the instance.
		 */
		default <TYPE> void fromType( TYPE aType, Object... aFromPathElements ) {
			fromType( toPath( aFromPathElements ), aType );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(String[], Class)}, though in fact it is the same as
		 * {@link #insertTo(String[], Object)} and provided for semantic
		 * emphasis and convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aFromPathElements The path elements representing the root from
		 *        which to take the data for the instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 */
		default <TYPE> void fromType( String[] aFromPathElements, TYPE aType ) {
			fromType( toPath( aFromPathElements ), aType );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(String[], Class)}, though in fact it is the same as
		 * {@link #insertTo(String[], Object)} and provided for semantic
		 * emphasis and convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 * @param aFromPathElements The path elements representing the root from
		 *        which to take the data for the instance.
		 */
		default <TYPE> void fromType( TYPE aType, String... aFromPathElements ) {
			fromType( toPath( aFromPathElements ), aType );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(Collection, Class)}, though in fact it is the same as
		 * {@link #insertTo(Collection, Object)} and provided for semantic
		 * emphasis and convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aFromPathElements The path elements representing the root from
		 *        which to take the data for the instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 */
		default <TYPE> void fromType( Collection<?> aFromPathElements, TYPE aType ) {
			fromType( toPath( aFromPathElements ), aType );
		}

		/**
		 * Fills this {@link PathMap} from the given type instance's data (e.g.
		 * bean properties). This is a counterpart method for
		 * {@link #toType(String, Class)}, though in fact it is the same as
		 * {@link #insertTo(String, Object)} and provided for semantic emphasis
		 * and convenience.
		 * 
		 * See also the methods {@link #insertFrom(Object, String)},
		 * {@link #insertTo(String, PathMap)} or
		 * {@link #insertBetween(String, Object, String)} as well as them
		 * variants.
		 *
		 * @param <TYPE> the generic type if which to create an instance.
		 * @param aFromPath The path representing the root from which to take
		 *        the data for the instance.
		 * @param aType the type if the instance to be filled with the herein
		 *        contained data.
		 */
		default <TYPE> void fromType( String aFromPath, TYPE aType ) {
			insertTo( aFromPath, aType );
		}

		/**
		 * Beta, don't know whether to keep this or not: Creates a class (a
		 * {@link String} text to be copy'n'pasted) with getters and setters for
		 * the herein contained paths and their values.
		 * 
		 * @return The text representing the source code to be copy'n'pasted.
		 */
		default String toSourceCode() {
			return toSourceCode( null, null );
		}

		/**
		 * Beta, don't know whether to keep this or not: Creates a class (a
		 * {@link String} text to be copy'n'pasted) with getters and setters for
		 * the herein contained paths and their values.
		 * 
		 * @param aClassName The class name for the class to be generated.
		 * @param aPackage The package to which the class is to belong.
		 * 
		 * @return The text representing the source code to be copy'n'pasted.
		 */
		default String toSourceCode( String aClassName, String aPackage ) {
			if ( aClassName == null || aClassName.isEmpty() ) {
				aClassName = "null";
			}
			final StringBuilder theBuffer = new StringBuilder();
			if ( aPackage != null && aPackage.length() != 0 ) {
				theBuffer.append( "package " + aPackage + ";" + System.lineSeparator() );
			}
			theBuffer.append( System.lineSeparator() );
			theBuffer.append( "import " + getClass().getName() + ";" + System.lineSeparator() );
			theBuffer.append( "import " + getType().getName() + ";" + System.lineSeparator() );
			theBuffer.append( System.lineSeparator() );
			theBuffer.append( "public class " + aClassName + " extends " + getClass().getSimpleName() + " {" + System.lineSeparator() );
			theBuffer.append( System.lineSeparator() );

			final List<String> theKeys = new ArrayList<>( keySet() );
			Collections.sort( theKeys );
			StringBuilder ePropertyBuffer;
			String ePropertyName;
			boolean hadDelimiter;
			char eChar;

			boolean isArray;
			final Set<String> theArrays = new HashSet<>();
			String eParent;
			for ( String eKey : theKeys ) {
				isArray = false;
				if ( hasParentPath( eKey ) ) {
					eParent = toParentPath( eKey );
					if ( theArrays.contains( eParent ) ) {
						continue;
					}
					if ( isArray( eParent ) ) {
						theArrays.add( eParent );
						eKey = eParent;
						isArray = true;
					}
				}

				// Property name |-->
				ePropertyBuffer = new StringBuilder();
				hadDelimiter = false;
				for ( int i = 0; i < eKey.length(); i++ ) {
					eChar = eKey.charAt( i );
					if ( eChar == getDelimiter() ) {
						hadDelimiter = true;
					}
					else {
						if ( Character.isAlphabetic( eChar ) || Character.isDigit( eChar ) ) {
							if ( hadDelimiter ) {
								ePropertyBuffer.append( Character.toUpperCase( eChar ) );
							}
							else {
								ePropertyBuffer.append( eChar );
							}
							hadDelimiter = false;
						}
					}
				}
				ePropertyName = ePropertyBuffer.toString();
				// Property name <--|

				if ( isArray ) {
					// Getter |-->
					theBuffer.append( "\tpublic " + getType().getSimpleName() + "[] get" + ePropertyName + "() {" + System.lineSeparator() );
					theBuffer.append( "\t\treturn getArray( \"" + eKey + "\" );" + System.lineSeparator() );
					theBuffer.append( "\t}" + System.lineSeparator() );
					theBuffer.append( System.lineSeparator() );
					// Getter <--|

					// Setter |-->
					theBuffer.append( "\tpublic void set" + ePropertyName + "(  " + getType().getSimpleName() + "[] aValues ) {" + System.lineSeparator() );
					theBuffer.append( "\t\tputArray( \"" + eKey + "\", aValues );" + System.lineSeparator() );
					theBuffer.append( "\t}" + System.lineSeparator() );
					theBuffer.append( System.lineSeparator() );
					// Setter <--|
				}
				else {
					// Getter |-->
					theBuffer.append( "\tpublic " + getType().getSimpleName() + " get" + ePropertyName + "() {" + System.lineSeparator() );
					theBuffer.append( "\t\treturn get( \"" + eKey + "\" );" + System.lineSeparator() );
					theBuffer.append( "\t}" + System.lineSeparator() );
					theBuffer.append( System.lineSeparator() );
					// Getter <--|

					// Setter |-->
					theBuffer.append( "\tpublic void set" + ePropertyName + "( " + getType().getSimpleName() + " aValue ) {" + System.lineSeparator() );
					theBuffer.append( "\t\tput( \"" + eKey + "\", aValue );" + System.lineSeparator() );
					theBuffer.append( "\t}" + System.lineSeparator() );
					theBuffer.append( System.lineSeparator() );
					// Setter <--|
				}
			}

			theBuffer.append( "}" );
			return theBuffer.toString();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link PathMapBuilder} adds builder functionality to the
	 * {@link MutablePathMap}.
	 * 
	 * @param <T> The type of the value elements.
	 */
	public static interface PathMapBuilder<T> extends MutablePathMap<T>, TableBuilder<String, T, PathMapBuilder<T>> {

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insert(Object)} method.
		 *
		 * @param aObj the obj
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insert(Object)
		 */
		default PathMapBuilder<T> withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insert(Object)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insert(Object)
		 */
		default PathMapBuilder<T> withInsert( PathMap<T> aFrom ) {
			insert( (Object) aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertBetween(String, Object, String)} method.
		 *
		 * @param aToPathElements The path elements of the to-path.
		 * @param aFrom the from
		 * @param aFromPathElements The path elements of the from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( Collection<?> aToPathElements, PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertBetween(String, Object, String)} method.
		 *
		 * @param aToPath The to-path.
		 * @param aFromObj the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( Object aToPath, Object aFromObj, Object aFromPath ) {
			insertBetween( aToPath, aFromObj, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( Object aToPath, PathMap<T> aFromMap, Object aFromPath ) {
			insertBetween( aToPath, (Object) aFromMap, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertBetween(String, Object, String)} method.
		 *
		 * @param aToPathElements The path elements of the to-path.
		 * @param aFrom the from
		 * @param aFromPathElements The path elements of the from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( Object[] aToPathElements, PathMap<T> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertBetween(String, Object, String)} method.
		 *
		 * @param aToPath The to-path.
		 * @param aFrom the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( String aToPath, PathMap<T> aFrom, String aFromPath ) {
			insertBetween( aToPath, (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertBetween(String, Object, String)} method.
		 *
		 * @param aToPathElements The path elements of the to-path.
		 * @param aFrom the from
		 * @param aFromPathElements The path elements of the from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withInsertBetween( String[] aToPathElements, PathMap<T> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPathElements The path elements representing the
		 *        from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPathElements The path elements representing the
		 *        from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPathElements The path elements representing the
		 *        from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements representing the path from
		 *        where to start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( PathMap<T> aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements representing the path from
		 *        where to start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( PathMap<T> aFrom, Object... aFromPathElements ) {
			insertFrom( (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( PathMap<T> aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements representing the path from
		 *        where to start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertFrom(Object, String)
		 */
		default PathMapBuilder<T> withInsertFrom( PathMap<T> aFrom, String... aFromPathElements ) {
			insertFrom( (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertTo(String, Object)} method.
		 *
		 * @param aToPathElements The path elements representing the to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements representing the sub-path
		 *        where to insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( Collection<?> aToPathElements, PathMap<T> aFrom ) {
			insertTo( aToPathElements, (Object) aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertTo(String, Object)} method.
		 *
		 * @param aToPath The to-path.
		 * @param aFromObj the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( Object aToPath, Object aFromObj ) {
			insertTo( aToPath, aFromObj );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( Object aToPath, PathMap<T> aFromMap ) {
			insertTo( aToPath, aFromMap );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertTo(String, Object)} method.
		 *
		 * @param aToPathElements The path elements representing the to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements representing the sub-path
		 *        where to insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( Object[] aToPathElements, PathMap<T> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertTo(String, Object)} method.
		 *
		 * @param aToPath The to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to insert the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( String aToPath, PathMap<T> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #insertTo(String, Object)} method.
		 *
		 * @param aToPathElements The path elements representing the to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #insertTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements representing the sub-path
		 *        where to insert the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #insertTo(String, Object)
		 */
		default PathMapBuilder<T> withInsertTo( String[] aToPathElements, PathMap<T> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #merge(Object)} method.
		 *
		 * @param aObj the obj
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #merge(Object)
		 */
		default PathMapBuilder<T> withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #merge(Object)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #merge(Object)
		 */
		default PathMapBuilder<T> withMerge( PathMap<T> aFrom ) {
			merge( (Object) aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeBetween(String, Object, String)} method.
		 *
		 * @param aToPathElements The path elements of the to-path.
		 * @param aFrom the from
		 * @param aFromPathElements The path elements of the from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( Collection<?> aToPathElements, PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeBetween(String, Object, String)} method.
		 *
		 * @param aToPath The to-path.
		 * @param aFromObj the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( Object aToPath, Object aFromObj, Object aFromPath ) {
			mergeBetween( aToPath, aFromObj, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( Object aToPath, PathMap<T> aFromMap, Object aFromPath ) {
			mergeBetween( aToPath, (Object) aFromMap, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeBetween(String, Object, String)} method.
		 *
		 * @param aToPathElements The path elements of the to-path.
		 * @param aFrom the from
		 * @param aFromPathElements The path elements of the from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( Object[] aToPathElements, PathMap<T> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeBetween(String, Object, String)} method.
		 *
		 * @param aToPath The to-path.
		 * @param aFrom the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( String aToPath, PathMap<T> aFrom, String aFromPath ) {
			mergeBetween( aToPath, (Object) aFrom, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeBetween(String, Object, String)} method.
		 *
		 * @param aToPathElements The path elements of the to-path.
		 * @param aFrom the from
		 * @param aFromPathElements The path elements of the from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeBetween(String, Object, String)}.
		 * 
		 * @param aToPathElements The path elements of the sub-path where to
		 *        merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements of the path from where to
		 *        start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeBetween(String, Object, String)
		 */
		default PathMapBuilder<T> withMergeBetween( String[] aToPathElements, PathMap<T> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPathElements The path elements representing the
		 *        from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPathElements The path elements representing the
		 *        from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPath The from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeFrom(Object, String)} method.
		 *
		 * @param aFrom the from
		 * @param aFromPathElements The path elements representing the
		 *        from-path.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements representing the path from
		 *        where to start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( PathMap<T> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( PathMap<T> aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements representing the path from
		 *        where to start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( PathMap<T> aFrom, Object... aFromPathElements ) {
			mergeFrom( (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPath The path from where to start adding elements of the
		 *        provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( PathMap<T> aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeFrom(Object, String)}.
		 * 
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * @param aFromPathElements The path elements representing the path from
		 *        where to start adding elements of the provided object.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeFrom(Object, String)
		 */
		default PathMapBuilder<T> withMergeFrom( PathMap<T> aFrom, String... aFromPathElements ) {
			mergeFrom( (Object) aFrom, aFromPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeTo(String, Object)} method.
		 *
		 * @param aToPathElements The path elements representing the to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements representing the sub-path
		 *        where to merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( Collection<?> aToPathElements, PathMap<T> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeTo(String, Object)} method.
		 *
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFromObj The object which is to be inspected with the therein
		 *        contained values being added with their according determined
		 *        paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( Object aToPath, Object aFromObj ) {
			mergeTo( aToPath, aFromObj );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFromMap The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( Object aToPath, PathMap<T> aFromMap ) {
			mergeTo( aToPath, aFromMap );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeTo(String, Object)} method.
		 *
		 * @param aToPathElements The path elements representing the to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements representing the sub-path
		 *        where to merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( Object[] aToPathElements, PathMap<T> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeTo(String, Object)} method.
		 *
		 * @param aToPath The to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPath The sub-path where to merge the object's introspected
		 *        values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( String aToPath, PathMap<T> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #mergeTo(String, Object)} method.
		 *
		 * @param aToPathElements The path elements representing the to-path.
		 * @param aFrom the from
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Method to semantically emphasize that we support our own types.
		 * Actually delegates to {@link #mergeTo(String, Object)}.
		 * 
		 * @param aToPathElements The path elements representing the sub-path
		 *        where to merge the object's introspected values to.
		 * @param aFrom The {@link PathMap} which is to be inspected with the
		 *        therein contained values being added with their according
		 *        determined paths.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 * 
		 * @see #mergeTo(String, Object)
		 */
		default PathMapBuilder<T> withMergeTo( String[] aToPathElements, PathMap<T> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * Builder method for {@link #put(Object, Object)}.
		 *
		 * @param aPathElements the path elements for the property to be put.
		 * @param aValue the value for the property to be put.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		default PathMapBuilder<T> withPut( Collection<?> aPathElements, T aValue ) {
			put( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * Builder method for {@link #put(Object, Object)}.
		 *
		 * @param aPathElements the path elements
		 * @param aValue the value for the property to be put.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		// default PathMapBuilder<T> withPut( Object aKey, T aValue ) {
		// put( toPath( aKey ), aValue );
		// return this;
		// }

		/**
		 * Builder method for {@link #put(Object, Object)}.
		 *
		 * @param aPathElements the path elements for the property to be put.
		 * @param aValue the value for the property to be put.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		default PathMapBuilder<T> withPut( Object[] aPathElements, T aValue ) {
			put( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default PathMapBuilder<T> withPut( Relation<String, T> aRelation ) {
			put( aRelation );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		//	@Override
		//	default PathMapBuilder<T> withPut( String aKey, T aValue ) {
		//		put( aKey, aValue );
		//		return this;
		//	}

		//	/**
		//	 * Builder method for {@link #put(Object, Object)}.
		//	 *
		//	 * @param aKey the path for the property to be put.
		//	 * @param aValue the value for the property to be put.
		//	 * @return The implementing instance as of the builder pattern.
		//	 */
		//	default PathMapBuilder<T> withPut( Object aKey, T aValue ) {
		//		put( toPath( aKey ), aValue );
		//		return this;
		//	}

		/**
		 * Builder method for {@link #put(Object, Object)}.
		 *
		 * @param aPathElements the path elements for the property to be put.
		 * @param aValue the value for the property to be put.
		 * 
		 * @return The implementing instance as of the builder pattern.
		 */
		default PathMapBuilder<T> withPut( String[] aPathElements, T aValue ) {
			put( toPath( aPathElements ), aValue );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the root path at the
		 * given index.
		 * 
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the root path at the
		 * given index.
		 * 
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( int aIndex, PathMap<T> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( Object aPath, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( Object[] aPathElements, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPath The path where to put the indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( String aPath, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;

		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * An indexed directory represents all elements which begin with a path
		 * which's last path element represents an index. There may by many
		 * sub-paths for the same indexed path which are all are included by the
		 * according directory. Puts the given values below the path at the
		 * given index.
		 * 
		 * @param aPathElements The path elements of the path where to put the
		 *        indexed element.
		 * @param aIndex The index of the indexed element.
		 * @param aDir The values to be put at the index.
		 * 
		 * @return This instance (on which the method call has been invoked) as
		 *         of the builder pattern.
		 * 
		 * @throws IllegalArgumentException in case the path does not represent
		 *         indexed elements as of {@link #isIndexDir(String)}.
		 */
		default PathMap<T> withPutDirAt( String[] aPathElements, int aIndex, PathMap<T> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #removeFrom( String)} method.
		 * 
		 * @param aPathElements The path elements of the path from where to
		 *        remove all properties.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 */
		default PathMapBuilder<T> withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #removeFrom( String)} method.
		 * 
		 * @param aPath The path from where to remove all properties.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 */
		default PathMapBuilder<T> withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #removeFrom( String)} method.
		 * 
		 * @param aPathElements The path elements of the path from where to
		 *        remove all properties.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 */
		default PathMapBuilder<T> withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #removeFrom( String)} method.
		 * 
		 * @param aPath The path from where to remove all properties.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 */
		default PathMapBuilder<T> withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #removeFrom( String)} method.
		 * 
		 * @param aPathElements The path elements of the path from where to
		 *        remove all properties.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 */
		default PathMapBuilder<T> withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * Convenience method for providing builder functionality to the
		 * {@link #removeFrom( String)} method.
		 * 
		 * @param aPathElements The path elements of the path from where to
		 *        remove all properties.
		 * 
		 * @return This instance as of the Builder-Pattern to apply succeeding
		 *         operations.
		 */
		default PathMapBuilder<T> withRemovePaths( String... aPathElements ) {
			removePaths( aPathElements );
			return this;
		}
	}

	char ANNOTATOR = Annotator.JAVA.getChar();
	char DELIMITER = Delimiter.PATH.getChar();

	/**
	 * Retrieves the entry names below the root including leaves as well as
	 * directories. A leave is reckoned to be the last path element in a path
	 * pointing to a value. In contrast a directory is reckoned to be be a path
	 * element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths "/dog/cat" and "/cat/mouse" in the
	 * {@link PathMap} and we call {@link #leaves()}, then the resulting
	 * {@link Set} will contain the values "dog" and "cat".
	 * 
	 * @return The relative directory names starting at the root path contained
	 *         in this {@link PathMap}.
	 */
	default Set<String> children() {
		return children( getRootPath() );
	}

	/**
	 * Retrieves the entry names below the given path including leaves as well
	 * as directories. A leave is reckoned to be the last path element in a path
	 * pointing to a value. In contrast a directory is reckoned to be be a path
	 * element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #children(String)} with "/animals", then the
	 * resulting {@link Set} will just contain "dogs" and "loki"; calling "/"
	 * will retrieve "animals" and "machines".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        children.
	 * 
	 * @return The child names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> children( Collection<?> aPathElements ) {
		return children( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the entry names below the given path including leaves as well
	 * as directories. A leave is reckoned to be the last path element in a path
	 * pointing to a value. In contrast a directory is reckoned to be be a path
	 * element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #dirs(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "dogs" and "loki"; calling "/" will
	 * retrieve "animals" and "machines".
	 * 
	 * @param aPath The path from which to start retrieving the children.
	 * 
	 * @return The child names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> children( Object aPath ) {
		return children( toPath( aPath ) );
	}

	/**
	 * Retrieves the entry names below the given path including leaves as well
	 * as directories. A leave is reckoned to be the last path element in a path
	 * pointing to a value. In contrast a directory is reckoned to be be a path
	 * element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #children(String)} with "/animals", then the
	 * resulting {@link Set} will just contain "dogs" and "loki"; calling "/"
	 * will retrieve "animals" and "machines".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        children.
	 * 
	 * @return The child names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> children( Object... aPathElements ) {
		return children( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the entry names below the given path including leaves as well
	 * as directories. A leave is reckoned to be the last path element in a path
	 * pointing to a value. In contrast a directory is reckoned to be be a path
	 * element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #dirs(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "dogs" and "loki"; calling "/" will
	 * retrieve "animals" and "machines".
	 * 
	 * @param aPath The path from which to start retrieving the children.
	 * 
	 * @return The child names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> children( String aPath ) {
		final String thePath = toNormalizedPath( aPath );
		final String theRootPath = getRootPath();
		final Set<String> theElements = new LinkedHashSet<>();
		keySet( thePath ).forEach( eKey -> {
			if ( eKey.isEmpty() && thePath.equals( theRootPath ) || ( eKey.length() > 0 ) ) {
				final int i = eKey.indexOf( getDelimiter(), 0 );
				if ( i == -1 ) {
					theElements.add( eKey );
				}
				else {
					eKey = eKey.substring( 0, i );
					if ( eKey.length() > 0 ) {
						theElements.add( eKey );
					}
				}
			}
		} );
		return theElements;
	}

	/**
	 * Retrieves the entry names below the given path including leaves as well
	 * as directories. A leave is reckoned to be the last path element in a path
	 * pointing to a value. In contrast a directory is reckoned to be be a path
	 * element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #children(String)} with "/animals", then the
	 * resulting {@link Set} will just contain "dogs" and "loki"; calling "/"
	 * will retrieve "animals" and "machines".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        children.
	 * 
	 * @return The child names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> children( String... aPathElements ) {
		return children( toPath( aPathElements ) );
	}

	/**
	 * Tests whether the path elements addressing the according path (key) exist
	 * as of {@link #containsKey(Object)} .
	 * 
	 * @param aPathElements The path elements of the path representing the key.
	 *
	 * @return True in case the element for the according path exists.
	 */
	default boolean containsKey( Collection<?> aPathElements ) {
		return containsKey( toPath( aPathElements ) );
	}

	/**
	 * Tests whether the path elements addressing the according path (key) exist
	 * as of {@link #containsKey(Object)} .
	 * 
	 * @param aPathElements The path elements of the path representing the key.
	 *
	 * @return True in case the element for the according path exists.
	 */
	default boolean containsKey( Object... aPathElements ) {
		return containsKey( toPath( aPathElements ) );
	}

	/**
	 * Tests whether the path elements addressing the according path (key) exist
	 * as of {@link #containsKey(Object)} .
	 * 
	 * @param aPathElements The path elements of the path representing the key.
	 *
	 * @return True in case the element for the according path exists.
	 */
	default boolean containsKey( String... aPathElements ) {
		return containsKey( toPath( aPathElements ) );
	}

	/**
	 * Applies the {@link #dirs(String)} method for the root path "/".
	 * 
	 * @return As of {@link #dirs(String)} applied to the root path.
	 * 
	 * @see #dirs(String)
	 */
	default Set<String> dirs() {
		return dirs( getRootPath() );
	}

	/**
	 * Retrieves the directory names below the given path elements excluding any
	 * entries representing leaves. A leave is reckoned to be the last path
	 * element in a path pointing to a value. In contrast a directory is
	 * reckoned to be be a path element pointing to a succeeding child path
	 * element (sub-directory). Given we have values for paths in our
	 * {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #dirs(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "dogs"; calling "/" will retrieve "animals"
	 * and "machines".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        directories.
	 * 
	 * @return The directory names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> dirs( Collection<?> aPathElements ) {
		return dirs( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory names below the given path excluding any entries
	 * representing leaves. A leave is reckoned to be the last path element in a
	 * path pointing to a value. In contrast a directory is reckoned to be be a
	 * path element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #dirs(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "dogs"; calling "/" will retrieve "animals"
	 * and "machines".
	 * 
	 * @param aPath The path from which to start retrieving the directories.
	 * 
	 * @return The directory names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> dirs( Object aPath ) {
		return dirs( toPath( aPath ) );
	}

	/**
	 * Retrieves the directory names below the given path elements excluding any
	 * entries representing leaves. A leave is reckoned to be the last path
	 * element in a path pointing to a value. In contrast a directory is
	 * reckoned to be be a path element pointing to a succeeding child path
	 * element (sub-directory). Given we have values for paths in our
	 * {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #dirs(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "dogs"; calling "/" will retrieve "animals"
	 * and "machines".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        directories.
	 * 
	 * @return The directory names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> dirs( Object... aPathElements ) {
		return dirs( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the directory names below the given path excluding any entries
	 * representing leaves. A leave is reckoned to be the last path element in a
	 * path pointing to a value. In contrast a directory is reckoned to be be a
	 * path element pointing to a succeeding child path element (sub-directory).
	 * Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #dirs(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "dogs"; calling "/" will retrieve "animals"
	 * and "machines".
	 * 
	 * @param aPath The path from which to start retrieving the directories.
	 * 
	 * @return The directory names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> dirs( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final Set<String> theDirs = new LinkedHashSet<>();
		keySet( aPath ).forEach( eKey -> {
			final int i = eKey.indexOf( getDelimiter(), 0 );
			if ( i != -1 ) {
				eKey = eKey.substring( 0, i );
				if ( eKey.length() > 0 ) {
					theDirs.add( eKey );
				}
			}
		} );
		return theDirs;
	}

	/**
	 * Retrieves the directory names below the given path elements excluding any
	 * entries representing leaves. A leave is reckoned to be the last path
	 * element in a path pointing to a value. In contrast a directory is
	 * reckoned to be be a path element pointing to a succeeding child path
	 * element (sub-directory). Given we have values for paths in our
	 * {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #dirs(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "dogs"; calling "/" will retrieve "animals"
	 * and "machines".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        directories.
	 * 
	 * @return The directory names (excluding the preceding given path) of the
	 *         directories below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> dirs( String... aPathElements ) {
		return dirs( toPath( aPathElements ) );
	}

	/**
	 * Finds all paths whose values equal that of the given value.
	 * 
	 * @param aValue The value for which to seek the paths.
	 * 
	 * @return The paths pointing to the according value.
	 */
	default Set<String> findPaths( T aValue ) {
		final Set<String> thePaths = new LinkedHashSet<>();
		if ( aValue != null ) {
			for ( String ePath : keySet() ) {
				if ( aValue.equals( get( ePath ) ) ) {
					thePaths.add( ePath );
				}
			}
		}
		return thePaths;
	}

	/**
	 * Converts an external key (path) which uses a different notation from
	 * {@link #getDelimiter()} for the path delimiter and converts those to the
	 * default delimiter.
	 * 
	 * @param aPath The key which's delimiters are to be converted to the
	 *        default {@link #getDelimiter()} delimiter.
	 * @param aDelimiters The "external" delimiters in question.
	 * 
	 * @return The converted key using the default delimiter
	 *         {@link #getDelimiter()} instead of the provided delimiters.
	 */
	default String fromExternalPath( String aPath, char... aDelimiters ) {
		if ( aDelimiters != null && aDelimiters.length > 0 ) {
			for ( char eDelimiter : aDelimiters ) {
				if ( eDelimiter != getDelimiter() ) {
					aPath = aPath.replace( eDelimiter, getDelimiter() );
				}
			}
		}
		return toNormalizedPath( aPath );
	}

	/**
	 * Experimental convenience method: Applies the {@link #get(Object)} method
	 * for the provided path elements by concatenating the elements with the
	 * path delimiter {@link #getDelimiter()} in between and taking care to
	 * avoid duplicate path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return As of {@link #get(Object)} applied path the path elements.
	 * 
	 * @see #get(Object)
	 */
	default T get( Collection<?> aPathElements ) {
		return get( toPath( aPathElements ) );
	}

	/**
	 * Experimental convenience method: Applies the {@link #get(Object)} method
	 * for the provided path elements by first converting them to a
	 * {@link String} (if not being a {@link String} already) and then
	 * concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return As of {@link #get(Object)} applied path the path elements.
	 * 
	 * @see #get(Object)
	 */
	default T get( Object... aPathElements ) {
		return get( toPath( aPathElements ) );
	}

	/**
	 * Applies the {@link #get(Object)} method for the provided path elements by
	 * concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return The value as of {@link #get(Object)} applied with the path from
	 *         the path elements.
	 * 
	 * @see #get(Object)
	 */
	default T get( String... aPathElements ) {
		return get( toPath( aPathElements ) );
	}

	/**
	 * Applies the {@link #get(Object)} method for the provided path
	 * {@link String}.
	 *
	 * @return As of {@link #get(Object)} applied path the path as key.
	 * 
	 * @see #get(Object)
	 */
	//	default T get( String aPath ) {
	//		return get( (Object) aPath );
	//	}

	/**
	 * Applies the {@link #getArray(String)} method for the root path "/".
	 * 
	 * @return As of {@link #getArray(String)} applied to the root path.
	 * 
	 * @see #getArray(String)
	 */
	default T[] getArray() {
		return getArray( getRootPath() );
	}

	/**
	 * Applies the {@link #getArray(String)} method for the provided path
	 * elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return As of {@link #getArray(String)} applied path the path elements.
	 * 
	 * @see #getArray(String)
	 */
	default T[] getArray( Collection<?> aPathElements ) {
		return getArray( toPath( aPathElements ) );
	}

	/**
	 * Applies the {@link #getArrayOr(String, Object[])} method for the provided
	 * path elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPath The path from where to retrieve the array the given key.
	 * 
	 * @return As of {@link #getArrayOr(String, Object[])} applied path the path
	 *         elements.
	 * 
	 * @see #getArrayOr(String, Object[])
	 */
	default T[] getArray( Object aPath ) {
		return getArray( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Applies the {@link #getArray(String)} method for the provided path
	 * elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return As of {@link #getArray(String)} applied path the path elements.
	 * 
	 * @see #getArray(String)
	 */
	default T[] getArray( Object... aPathElements ) {
		return getArray( toPath( aPathElements ) );
	}

	/**
	 * Applies the {@link #getArray(String)} method for the provided path
	 * elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return As of {@link #getArray(String)} applied path the path elements.
	 * 
	 * @see #getArray(String)
	 */
	default T[] getArray( String... aPathElements ) {
		return getArray( toPath( aPathElements ) );
	}

	/**
	 * Returns the leaves below the given path in an array. Leaves in a
	 * {@link PathMap} are reckoned to be part of an array when they belong to
	 * the same hierarchy level and when their keys represent integer numbers.
	 * Given the below example, the elements below "/animals/dogs" can be
	 * represented as an array with five elements (the path is denoted at the
	 * left hand side of the assignment ":=" operator and the value at the right
	 * and side accordingly):
	 * <ul>
	 * <li>"/animals/dogs/0" := "ace"</li>
	 * <li>"/animals/dogs/1" := "bandit"</li>
	 * <li>"/animals/dogs/2" := "radar"</li>
	 * <li>"/animals/dogs/3" := "echo"</li>
	 * <li>"/animals/dogs/4" := "snoopy"</li>
	 * </ul>
	 * The resulting array when calling {@link #getArray(String)} for the path
	 * "/animals/dogs" would contain {"ace", "bandit", "radar", "echo",
	 * "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would return
	 * <code>true</code> whereas calling <code>isArray("/animals")</code> would
	 * return false.
	 * 
	 * In case below the path instead of indexes there is a single value
	 * assigned to the path, then an array with a single value is returned!
	 *
	 * @param aPath The path from where to retrieve the array.
	 * 
	 * @return An array of the leaves found below the given path.
	 */
	@SuppressWarnings("unchecked")
	default T[] getArray( String aPath ) {
		aPath = toNormalizedPath( aPath );
		int index;
		boolean hasIllegalPathElements = false;

		// Determine the length of the array |-->
		int theMax = -1;
		final PathMap<T> theMap = retrieveFrom( aPath );
		for ( String eEntry : theMap.leaves() ) {
			try {
				if ( !theMap.hasValue( eEntry ) ) {
					throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
				}
				index = Integer.valueOf( eEntry );
				if ( index < 0 ) {
					throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
				}
				theMax = index > theMax ? index : theMax;
			}
			catch ( NumberFormatException ignore ) {
				// throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
				hasIllegalPathElements = true;
			}
		}
		//	if ( theMax == -1 ) {
		//		throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
		//	}
		// Determine the length of the array <--|

		// Fill the array |-->
		if ( theMax != -1 ) {
			final T[] theArray = (T[]) Array.newInstance( getType(), theMax + 1 );
			for ( String eEntry : theMap.children() ) {
				try {
					if ( !theMap.hasValue( eEntry ) ) {
						throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
					}
					index = Integer.valueOf( eEntry );
					theArray[index] = theMap.get( eEntry );
				}
				catch ( NumberFormatException ignore ) {
					// throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
				}
			}
			return theArray;
		}
		// Fill the array <--|

		// Single element pseudo array |-->
		else {
			final T theValue = get( aPath );
			if ( theValue != null ) {
				final T[] theArray = (T[]) Array.newInstance( getType(), 1 );
				theArray[0] = theValue;
				return theArray;
			}
		}
		// Single element pseudo array <--|

		if ( hasIllegalPathElements ) {
			throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
		}
		return (T[]) Array.newInstance( getType(), 0 );
	}

	/**
	 * Applies the {@link #getArrayOr(String, Object[])} method for the root
	 * path "/".
	 *
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return As of {@link #getArrayOr(String, Object[])} applied to the root
	 *         path.
	 * 
	 * @see #getArrayOr(String, Object[])
	 */
	default T[] getArrayOr( T[] aDefaultValue ) {
		return getArrayOr( getRootPath(), aDefaultValue );
	}

	/**
	 * Applies the {@link #getArrayOr(String, Object[])} method for the provided
	 * path elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return As of {@link #getArrayOr(String, Object[])} applied path the path
	 *         elements.
	 * 
	 * @see #getArrayOr(String, Object[])
	 */
	default T[] getArrayOr( Collection<?> aPathElements, T[] aDefaultValue ) {
		return getArrayOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Applies the {@link #getArrayOr(String, Object[])} method for the provided
	 * path elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return As of {@link #getArrayOr(String, Object[])} applied path the path
	 *         elements.
	 * 
	 * @see #getArrayOr(String, Object[])
	 */
	default T[] getArrayOr( Object[] aPathElements, T[] aDefaultValue ) {
		return getArrayOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Returns the leaves below the given path in an array. Leaves in a
	 * {@link PathMap} are reckoned to be part of an array when they belong to
	 * the same hierarchy level and when their keys represent integer numbers.
	 * Given the below example, the elements below "/animals/dogs" can be
	 * represented as an array with five elements (the path is denoted at the
	 * left hand side of the assignment ":=" operator and the value at the right
	 * and side accordingly):
	 * <ul>
	 * <li>"/animals/dogs/0" := "ace"</li>
	 * <li>"/animals/dogs/1" := "bandit"</li>
	 * <li>"/animals/dogs/2" := "radar"</li>
	 * <li>"/animals/dogs/3" := "echo"</li>
	 * <li>"/animals/dogs/4" := "snoopy"</li>
	 * </ul>
	 * The resulting array when calling {@link #getArrayOr(String, Object[])}
	 * for the path "/animals/dogs" would contain {"ace", "bandit", "radar",
	 * "echo", "snoopy"}. Calling <code>isArray("/animals/dogs")</code> would
	 * return <code>true</code> whereas calling <code>isArray("/animals")</code>
	 * would return false.
	 * 
	 * In case below the path instead of indexes there is a single value
	 * assigned to the path, then an array with a single value is returned!
	 *
	 * @param aPath The path from where to retrieve the array
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return An array of the leaves found below the given path.
	 */
	default T[] getArrayOr( String aPath, T[] aDefaultValue ) {
		try {
			return getArray( aPath );
		}
		catch ( IllegalArgumentException e ) {
			return aDefaultValue;
		}
	}

	/**
	 * Applies the {@link #getArrayOr(String, Object[])} method for the provided
	 * path elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return As of {@link #getArrayOr(String, Object[])} applied path the path
	 *         elements.
	 * 
	 * @see #getArrayOr(String, Object[])
	 */
	default T[] getArrayOr( String[] aPathElements, T[] aDefaultValue ) {
		return getArrayOr( toPath( aPathElements ), aDefaultValue );
	}

	/**
	 * Applies the {@link #getArrayOr(String, Object[])} method for the provided
	 * path elements by concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPath The path from where to retrieve the array
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return As of {@link #getArrayOr(String, Object[])} applied path the path
	 *         elements.
	 * 
	 * @see #getArrayOr(String, Object[])
	 */
	default T[] getArrayOr( Object aPath, T[] aDefaultValue ) {
		return getArrayOr( toNormalizedPath( InterOperableMap.asKey( aPath ) ), aDefaultValue );
	}

	/**
	 * Returns the indexes pointing to elements of the root "array" path. You
	 * may use the method {@link #getValueAt(int)} to address the value at the
	 * given index. Given the following paths: <code> 
	 * /0/0aaa
	 * /1/1bbb
	 * /2/2ccc
	 * </code> The root path "/" points to an array with the three indexes "0",
	 * "1" and "2".
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getArrayIndexes() {
		return getArrayIndexes( getRootPath() );
	}

	/**
	 * Returns the indexes pointing to elements of the given "array" path. You
	 * may use the method {@link #getValueAt(Collection, int)} to address the
	 * value at the given index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/1/1bbb
	 * /root/child/2/2ccc
	 * </code> The path "/root/child" points to an array with the three indexes
	 * "0", "1" and "2".
	 * 
	 * @param aPathElements The elements of the path for which to get the
	 *        indexes.
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getArrayIndexes( Collection<?> aPathElements ) {
		return getArrayIndexes( toPath( aPathElements ) );
	}

	/**
	 * Returns the indexes pointing to elements of the given "array" path. You
	 * may use the method {@link #getValueAt(Object[], int)} to address the
	 * value at the given index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/1/1bbb
	 * /root/child/2/2ccc
	 * </code> The path "/root/child" points to an array with the three indexes
	 * "0", "1" and "2".
	 * 
	 * @param aPathElements The elements of the path for which to get the
	 *        indexes.
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getArrayIndexes( Object... aPathElements ) {
		return getArrayIndexes( toPath( aPathElements ) );
	}

	/**
	 * Returns the indexes pointing to elements of the given "array" path. You
	 * may use the method {@link #getValueAt(String, int)} to address the value
	 * at the given index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/1/1bbb
	 * /root/child/2/2ccc
	 * </code> The path "/root/child" points to an array with the three indexes
	 * "0", "1" and "2".
	 * 
	 * @param aPath The path for which to get the indexes.
	 * 
	 * @return The indexes which can be used.
	 * 
	 * @throws IllegalArgumentException in case the path does not point to
	 *         indexed elements.
	 */
	default int[] getArrayIndexes( String aPath ) {
		final Set<Integer> theIndexes = new HashSet<>();

		aPath = toNormalizedPath( aPath );
		int index;

		// Determine the length of the array |-->
		final PathMap<T> theMap = retrieveFrom( aPath );
		for ( String eEntry : theMap.children() ) {
			try {
				if ( !theMap.hasValue( eEntry ) ) {
					throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
				}
				if ( !eEntry.startsWith( "" + ANNOTATOR ) ) {
					index = Integer.valueOf( eEntry );
					if ( index < 0 ) {
						throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
					}
					if ( !theIndexes.add( index ) ) {
						throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure as we have multiple entries for index <" + index + ">. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
					}
				}
			}
			catch ( NumberFormatException e ) {
				throw new IllegalArgumentException( "The provided path \"" + aPath + "\" does not point to an array structure. Use \"isArray( String )\" to test if the leaves below the given path can be retrieved as array." );
			}
		}
		// Determine the length of the array <--|

		final int[] theArray = new int[theIndexes.size()];
		int i = 0;
		for ( int eIndex : theIndexes ) {
			theArray[i] = eIndex;
			i++;
		}
		Arrays.sort( theArray );
		return theArray;
	}

	/**
	 * Returns the indexes pointing to elements of the given "array" path. You
	 * may use the method {@link #getValueAt(String[], int)} to address the
	 * value at the given index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/1/1bbb
	 * /root/child/2/2ccc
	 * </code> The path "/root/child" points to an array with the three indexes
	 * "0", "1" and "2".
	 * 
	 * @param aPathElements The elements of the path for which to get the
	 *        indexes.
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getArrayIndexes( String... aPathElements ) {
		return getArrayIndexes( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the elements of the given index below the
	 * path of the path "array" path. Given the following paths and index = 1
	 * with a queried path "/root/child": <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> You will get a result containing all the elements which's keys
	 * begin with "/root/child/1/".The keys of the result will exclude the path
	 * "prefix" "/root/child/1".
	 *
	 * @param aPathElements The elements of the path from which to get the
	 *        indexed elements.
	 * @param aIndex The index which to use.
	 * 
	 * @return The indexed elements without the indexed path "prefixes".
	 */
	default PathMap<T> getDirAt( Collection<?> aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the elements of the given index below the
	 * path of the root "array" path. Given the following paths and index = 1:
	 * <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> You will get a result containing all the elements which's keys
	 * begin with "/1/". The keys of the result will exclude the path "prefix"
	 * "/1".
	 * 
	 * @param aIndex The index which to use.
	 * 
	 * @return The indexed elements without the indexed path "prefixes".
	 */
	default PathMap<T> getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the elements of the given index below the
	 * path of the path "array" path. Given the following paths and index = 1
	 * with a queried path "/root/child": <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> You will get a result containing all the elements which's keys
	 * begin with "/root/child/1/". The keys of the result will exclude the path
	 * "prefix" "/root/child/1".
	 *
	 * @param aPath The path from which to get the indexed elements.
	 * @param aIndex The index which to use.
	 * 
	 * @return The indexed elements without the indexed path "prefixes".
	 */
	default PathMap<T> getDirAt( Object aPath, int aIndex ) {
		return getDirAt( toNormalizedPath( InterOperableMap.asKey( aPath ) ), aIndex );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the elements of the given index below the
	 * path of the path "array" path. Given the following paths and index = 1
	 * with a queried path "/root/child": <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> You will get a result containing all the elements which's keys
	 * begin with "/root/child/1/".The keys of the result will exclude the path
	 * "prefix" "/root/child/1".
	 *
	 * @param aPathElements The elements of the path from which to get the
	 *        indexed elements.
	 * @param aIndex The index which to use.
	 * 
	 * @return The indexed elements without the indexed path "prefixes".
	 */
	default PathMap<T> getDirAt( Object[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the elements of the given index below the
	 * path of the path "array" path. Given the following paths and index = 1
	 * with a queried path "/root/child": <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> You will get a result containing all the elements which's keys
	 * begin with "/root/child/1/".The keys of the result will exclude the path
	 * "prefix" "/root/child/1".
	 *
	 * @param aPath The path from which to get the indexed elements.
	 * @param aIndex The index which to use.
	 * 
	 * @return The indexed elements without the indexed path "prefixes".
	 */
	default PathMap<T> getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, aIndex ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the elements of the given index below the
	 * path of the path "array" path. Given the following paths and index = 1
	 * with a queried path "/root/child": <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> You will get a result containing all the elements which's keys
	 * begin with "/root/child/1/".The keys of the result will exclude the path
	 * "prefix" "/root/child/1".
	 *
	 * @param aPathElements The elements of the path from which to get the
	 *        indexed elements.
	 * @param aIndex The index which to use.
	 * 
	 * @return The indexed elements without the indexed path "prefixes".
	 */
	default PathMap<T> getDirAt( String[] aPathElements, int aIndex ) {
		return getDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the indexes pointing to elements of the root
	 * "array" path. You may use the method {@link #getDirAt(int)} to address
	 * the sub-map at the given index. Given the following paths: <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> The root path points to an index path with the two indexes "0"
	 * and "1".
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getDirIndexes() {
		return getDirIndexes( getRootPath() );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the indexes pointing to elements of the
	 * given "array" path. You may use the method
	 * {@link #getDirAt(Collection, int)} to address the sub-map at the given
	 * index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an index path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPathElements The elements of the path for which to get the
	 *        indexes.
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getDirIndexes( Collection<?> aPathElements ) {
		return getDirIndexes( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the indexes pointing to elements of the
	 * given "array" path. You may use the method
	 * {@link #getDirAt(Object[], int)} to address the sub-map at the given
	 * index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an index path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPath The path for which to get the indexes.
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getDirIndexes( Object aPath ) {
		return getDirIndexes( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the indexes pointing to elements of the
	 * given "array" path. You may use the method
	 * {@link #getDirAt(Object[], int)} to address the sub-map at the given
	 * index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an index path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPathElements The elements of the path for which to get the
	 *        indexes.
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getDirIndexes( Object... aPathElements ) {
		return getDirIndexes( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the indexes pointing to elements of the
	 * given "array" path. You may use the method {@link #getDirAt(String, int)}
	 * to address the sub-map at the given index. Given the following paths:
	 * <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an index path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPath The path for which to get the indexes.
	 * 
	 * @return The indexes which can be used.
	 * 
	 * @throws IllegalArgumentException in case the path does not point to
	 *         indexed elements.
	 */
	default int[] getDirIndexes( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final Set<Integer> theIndexes = new HashSet<>();
		int eIndex;
		int eNextDelimiterIndex;
		final Set<String> thePaths = paths( aPath );
		boolean isDirPath = false;
		final Iterator<String> e = thePaths.iterator();
		String ePath;
		while ( e.hasNext() ) {
			ePath = e.next();
			if ( ePath.startsWith( "" + getDelimiter() ) ) {
				ePath = ePath.substring( 1 );
			}
			eNextDelimiterIndex = ePath.indexOf( getDelimiter() );
			if ( eNextDelimiterIndex != -1 ) {
				isDirPath = true;
				ePath = ePath.substring( 0, eNextDelimiterIndex );
			}
			try {
				if ( ePath.startsWith( getAnnotator() + "" ) ) {
					e.remove();
				}
				else {
					eIndex = Integer.parseInt( ePath );
					if ( !theIndexes.contains( eIndex ) ) {
						theIndexes.add( eIndex );
					}
					e.remove();
				}
			}
			catch ( Exception exc ) {
				throw new IllegalArgumentException( "The path <" + ( ( aPath == null || aPath.isEmpty() ) ? getDelimiter() : aPath ) + "> does not point to an indexed path. Use \"isIndexDir( String aPath )\" to test beforehand." );
			}
		}

		if ( !isDirPath && !thePaths.isEmpty() ) {
			throw new IllegalArgumentException( "The path <" + ( ( aPath == null || aPath.isEmpty() ) ? getDelimiter() : aPath ) + "> does not point to an indexed path. Use \"isIndexDir( String aPath )\" to test beforehand." );
		}

		final int[] theArray = new int[theIndexes.size()];
		int i = 0;
		for ( int j : theIndexes ) {
			theArray[i] = j;
			i++;
		}
		Arrays.sort( theArray );
		return theArray;
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the indexes pointing to elements of the
	 * given "array" path. You may use the method
	 * {@link #getDirAt(String[], int)} to address the sub-map at the given
	 * index. Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an index path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPathElements The elements of the path for which to get the
	 *        indexes.
	 * 
	 * @return The indexes which can be used
	 */
	default int[] getDirIndexes( String... aPathElements ) {
		return getDirIndexes( toPath( aPathElements ) );
	}

	/**
	 * Returns the path pointing to the root of this {@link PathMap}. By default
	 * the Root-Path consists just of the one delimiter {@link #getDelimiter()}
	 * character.
	 * 
	 * @return The path pointing to the root of this {@link PathMap}.
	 */
	default String getRootPath() {
		return getDelimiter() + "";
	}

	/**
	 * Returns the value of an array at the given path path with the given array
	 * index or null if there is none such value.
	 * 
	 * @param aPathElements The path elements for the path pointing to the
	 *        array.
	 * @param aIndex The index inside the array for which to get a value.
	 * 
	 * @return The value or null if there is none such value.
	 */
	default T getValueAt( Collection<?> aPathElements, int aIndex ) {
		return get( toPath( aPathElements, aIndex ) );
	}

	/**
	 * Returns the value of an array at the root path with the given array index
	 * or null if there is none such value.
	 * 
	 * @param aIndex The index inside the array for which to get a value.
	 * 
	 * @return The value or null if there is none such value.
	 */
	default T getValueAt( int aIndex ) {
		return get( aIndex );
	}

	/**
	 * Returns the value of an array at the given path path with the given array
	 * index or null if there is none such value.
	 * 
	 * @param aPath The path pointing to the array.
	 * @param aIndex The index inside the array for which to get a value.
	 * 
	 * @return The value or null if there is none such value.
	 */
	default T getValueAt( Object aPath, int aIndex ) {
		return get( toPath( aPath, aIndex ) );
	}

	/**
	 * Returns the value of an array at the given path path with the given array
	 * index or null if there is none such value.
	 * 
	 * @param aPathElements The path elements for the path pointing to the
	 *        array.
	 * @param aIndex The index inside the array for which to get a value.
	 * 
	 * @return The value or null if there is none such value.
	 */
	default T getValueAt( Object[] aPathElements, int aIndex ) {
		return get( toPath( aPathElements ), aIndex );
	}

	/**
	 * Returns the value of an array at the given path path with the given array
	 * index or null if there is none such value.
	 * 
	 * @param aPath The path pointing to the array.
	 * @param aIndex The index inside the array for which to get a value.
	 * 
	 * @return The value or null if there is none such value.
	 */
	default T getValueAt( String aPath, int aIndex ) {
		return get( toPath( aPath, aIndex ) );
	}

	/**
	 * Returns the value of an array at the given path path with the given array
	 * index or null if there is none such value.
	 * 
	 * @param aPathElements The path elements for the path pointing to the
	 *        array.
	 * @param aIndex The index inside the array for which to get a value.
	 * 
	 * @return The value or null if there is none such value.
	 */
	default T getValueAt( String[] aPathElements, int aIndex ) {
		return get( toPath( aPathElements ), aIndex );
	}

	/**
	 * Determines whether the give path contains children as of
	 * {@link #children(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a entry.
	 * 
	 * @return True in case the given path points to a entry, else false.
	 */
	default boolean hasChildren( Collection<?> aPathElements ) {
		return hasChildren( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path contains children as of
	 * {@link #children(String)}.
	 * 
	 * @param aPath The path for which to determine whether it contains entries.
	 * 
	 * @return True in case the given path contains children.
	 */
	default boolean hasChildren( Object aPath ) {
		return hasChildren( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Determines whether the give path contains children as of
	 * {@link #children(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a entry.
	 * 
	 * @return True in case the given path points to a entry, else false.
	 */
	default boolean hasChildren( Object... aPathElements ) {
		return hasChildren( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path contains children as of
	 * {@link #children(String)}.
	 * 
	 * @param aPath The path for which to determine whether it contains entries.
	 * 
	 * @return True in case the given path contains children.
	 */
	default boolean hasChildren( String aPath ) {
		final Set<String> theChildren = children( aPath );
		return theChildren != null && theChildren.size() != 0;
	}

	/**
	 * Determines whether the give path contains children as of
	 * {@link #children(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a entry.
	 * 
	 * @return True in case the given path points to a entry, else false.
	 */
	default boolean hasChildren( String... aPathElements ) {
		return hasChildren( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the path points to an index
	 * inside an indexed "array" Given the following paths: <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> For paths "/0" and "/1" you will get true, for all others you
	 * will get false.
	 * 
	 * @param aPathElements The path elements including the index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( Collection<?> aPathElements ) {
		return hasDirAt( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the index below the path points
	 * to an element of an indexed "array". Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPathElements The elements of the path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( Collection<?> aPathElements, int aIndex ) {
		return hasDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the index below the root points
	 * to an element of an indexed "array" Given the following paths: <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> For indexes 0 and 1 you will get true, for all others you will
	 * get false.
	 * 
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( int aIndex ) {
		return hasDirAt( getRootPath(), aIndex );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the path points to an index
	 * inside an indexed "array" Given the following paths: <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> For paths "/0" and "/1" you will get true, for all others you
	 * will get false.
	 * 
	 * @param aPath The path including the index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( Object aPath ) {
		return hasDirAt( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the path points to an index
	 * inside an indexed "array" Given the following paths: <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> For paths "/0" and "/1" you will get true, for all others you
	 * will get false.
	 * 
	 * @param aPathElements The path elements including the index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( Object... aPathElements ) {
		return hasDirAt( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the index below the path points
	 * to an element of an indexed "array" Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPath The path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( Object aPath, int aIndex ) {
		return isDir( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the index below the path points
	 * to an element of an indexed "array". Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPathElements The elements of the path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( Object[] aPathElements, int aIndex ) {
		return isDir( toPath( aPathElements, aIndex ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the path points to an index
	 * inside an indexed "array" Given the following paths: <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> For paths "/0" and "/1" you will get true, for all others you
	 * will get false.
	 * 
	 * @param aPath The path including the index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( String aPath ) {
		final String[] thePathElements = toPathElements( aPath );
		if ( thePathElements != null && thePathElements.length > 0 ) {
			try {
				final int index = Integer.valueOf( thePathElements[thePathElements.length - 1] );
				aPath = toParentPath( aPath );
				return hasDirAt( aPath, index );
			}
			catch ( NumberFormatException ignore ) {}
		}
		return false;
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the path points to an index
	 * inside an indexed "array" Given the following paths: <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> For paths "/0" and "/1" you will get true, for all others you
	 * will get false.
	 * 
	 * @param aPathElements The path elements including the index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( String... aPathElements ) {
		return hasDirAt( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the index below the path points
	 * to an element of an indexed "array" Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPath The path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( String aPath, int aIndex ) {
		return isDir( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the index below the path points
	 * to an element of an indexed "array". Given the following paths: <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPathElements The elements of the path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasDirAt( String[] aPathElements, int aIndex ) {
		return hasDirAt( toPath( aPathElements ), aIndex );
	}

	/**
	 * Tests whether there exists a parent path for the given path. Given we
	 * have a path "/animals/dogs/otto", then its parent path is
	 * "/animals/dogs".
	 * 
	 * @param aPath The path for which to test if it has the parent path.
	 * 
	 * @return True in case the path has a parent, else false.
	 */
	default boolean hasParentPath( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final int index = aPath.lastIndexOf( getDelimiter() );
		return ( index > 0 );
	}

	/**
	 * Determines whether the provided path elements addressing the path
	 * represent a path within the given {@link PathMap}. The path not
	 * necessarily points to a leaf in terms of being a leave or a directory.
	 * Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf.
	 * 
	 * @param aPathElements The path elements addressing the path to be tested.
	 * 
	 * @return True in case we have a path being contained in the given
	 *         {@link PathMap}, else false.
	 */
	default boolean hasPath( Collection<?> aPathElements ) {
		return hasPath( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the provided path elements addressing the path
	 * represent a path within the given {@link PathMap}. The path not
	 * necessarily points to a leaf in terms of being a leave or a directory.
	 * Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf.
	 * 
	 * @param aPathElements The path elements addressing the path to be tested.
	 * 
	 * @return True in case we have a path being contained in the given
	 *         {@link PathMap}, else false.
	 */
	default boolean hasPath( Object... aPathElements ) {
		return hasPath( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the provided path represents a path within the given
	 * {@link PathMap}. The path not either points to a a leaf or a directory as
	 * of {@link #isChild(String)}. Use {@link #hasValue(String)} to explicitly
	 * test if we have a leaf or {@link #isDir(String)} to explicitly test if we
	 * have a directory.
	 * 
	 * @param aPath The path to be tested.
	 * 
	 * @return True in case we have a path being contained in the given
	 *         {@link PathMap}, else false.
	 */
	default boolean hasPath( Object aPath ) {
		return !paths( aPath ).isEmpty();
	}

	/**
	 * Determines whether the provided path represents a path within the given
	 * {@link PathMap}. The path not either points to a a leaf or a directory as
	 * of {@link #isChild(String)}. Use {@link #hasValue(String)} to explicitly
	 * test if we have a leaf or {@link #isDir(String)} to explicitly test if we
	 * have a directory.
	 * 
	 * @param aPath The path to be tested.
	 * 
	 * @return True in case we have a path being contained in the given
	 *         {@link PathMap}, else false.
	 */
	default boolean hasPath( String aPath ) {
		return !paths( aPath ).isEmpty();
	}

	/**
	 * Determines whether the provided path elements addressing the path
	 * represent a path within the given {@link PathMap}. The path not
	 * necessarily points to a leaf in terms of being a leave or a directory.
	 * Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf.
	 * 
	 * @param aPathElements The path elements addressing the path to be tested.
	 * 
	 * @return True in case we have a path being contained in the given
	 *         {@link PathMap}, else false.
	 */
	default boolean hasPath( String... aPathElements ) {
		return hasPath( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the index below the path points to an element of an
	 * "array". Given the following paths: <code> 
	 * /root/child/0/0bbb
	 * /root/child/1/1bbb
	 * /bla/abc
	 * /bla/xyz
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPathElements The elements of the path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( Collection<?> aPathElements, int aIndex ) {
		return hasValue( toPath( toPath( aPathElements ), Integer.toString( aIndex ) ) );
	}

	/**
	 * Determines whether the index below the root points to an element of an
	 * "array" Given the following paths: <code> 
	 * /0/0aaa
	 * /1/1aaa
	 * </code> For indexes 0 and 1 you will get true, for all others you will
	 * get false.
	 * 
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( int aIndex ) {
		return hasValueAt( getRootPath(), aIndex );
	}

	/**
	 * Determines whether the path points to an index inside an "array" Given
	 * the following paths: <code> 
	 * /root/child/0/0bbb
	 * /root/child/1/1bbb
	 * /bla/abc
	 * /bla/xyz
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 * 
	 * @param aPath The path including the index for which to test.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( Object aPath ) {
		return hasValueAt( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Determines whether the index below the path points to an element of an
	 * "array" Given the following paths: <code> 
	 * /root/child/0/0bbb
	 * /root/child/1/1bbb
	 * /bla/abc
	 * /bla/xyz
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPath The path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( Object aPath, int aIndex ) {
		return hasValue( toPath( aPath, aIndex ) );
	}

	/**
	 * Determines whether the index below the path points to an element of an
	 * "array". Given the following paths: <code> 
	 * /root/child/0/0bbb
	 * /root/child/1/1bbb
	 * /bla/abc
	 * /bla/xyz
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPathElements The elements of the path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( Object[] aPathElements, int aIndex ) {
		return hasValue( toPath( aPathElements, aIndex ) );
	}

	/**
	 * Determines whether the path points to an index inside an "array" Given
	 * the following paths: <code> 
	 * /root/child/0/0bbb
	 * /root/child/1/1bbb
	 * /bla/abc
	 * /bla/xyz
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 * 
	 * @param aPath The path including the index for which to test.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( String aPath ) {
		final String[] thePathElements = toPathElements( aPath );
		if ( thePathElements != null && thePathElements.length > 0 ) {
			try {
				final int index = Integer.valueOf( thePathElements[thePathElements.length - 1] );
				aPath = toParentPath( aPath );
				return hasValueAt( aPath, index );
			}
			catch ( NumberFormatException ignore ) {}
		}
		return false;
	}

	/**
	 * Determines whether the index below the path points to an element of an
	 * "array" Given the following paths: <code> 
	 * /root/child/0/0bbb
	 * /root/child/1/1bbb
	 * /bla/abc
	 * /bla/xyz
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPath The path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( String aPath, int aIndex ) {
		return hasValue( toPath( aPath, aIndex ) );
	}

	/**
	 * Determines whether the index below the path points to an element of an
	 * "array". Given the following paths: <code> 
	 * /root/child/0/0bbb
	 * /root/child/1/1bbb
	 * /bla/abc
	 * /bla/xyz
	 * </code> For path "/root/child" and indexes 0 and 1 you will get true, for
	 * all others you will get false.
	 *
	 * @param aPathElements The elements of the path for which to test.
	 * @param aIndex The index which to use.
	 * 
	 * @return True if the path at the index points to an array element.
	 */
	default boolean hasValueAt( String[] aPathElements, int aIndex ) {
		return hasValue( toPath( aPathElements, aIndex ) );
	}

	/**
	 * Applies the {@link #isArray(String)} method for the root path "/".
	 * 
	 * @return As of {@link #isArray(String)} applied to the root path.
	 * 
	 * @see #isArray(String)
	 */
	default boolean isArray() {
		return isArray( getRootPath() );
	}

	/**
	 * Determines as of {@link #getArray(String)} if the elements below the
	 * given path elements can be seen as an array.
	 *
	 * @param aPathElements the path elements
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArray( String... aPathElements ) {
		return isArray( toPath( aPathElements ) );
	}

	/**
	 * Determines as of {@link #getArray(String)} if the elements below the
	 * given path elements can be seen as an array.
	 *
	 * @param aPathElements the path elements
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArray( Collection<?> aPathElements ) {
		return isArray( toPath( aPathElements ) );
	}

	/**
	 * Determines as of {@link #getArray(String)} if the elements below the
	 * given path can be seen as an array.
	 *
	 * @param aPath the path
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArray( Object aPath ) {
		return isArray( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Determines as of {@link #getArray(String)} if the elements below the
	 * given path elements can be seen as an array.
	 *
	 * @param aPathElements the path elements
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArray( Object... aPathElements ) {
		return isArray( toPath( aPathElements ) );
	}

	/**
	 * Determines as of {@link #getArray(String)} if the elements below the
	 * given path can be seen as an array.
	 *
	 * @param aPath the path which to test.
	 * 
	 * @return True in case we have an array structure below the path or not.
	 */
	default boolean isArray( String aPath ) {
		final Set<Integer> theIndexes = new HashSet<>();
		aPath = toNormalizedPath( aPath );
		int index;
		int theAnnotated = 0;
		final PathMap<T> theMap = retrieveFrom( aPath );
		for ( String eEntry : theMap.children() ) {
			try {
				if ( !theMap.hasValue( eEntry ) ) {
					return false;
				}
				if ( eEntry.startsWith( getAnnotator() + "" ) ) {
					theAnnotated++;
				}
				else {
					index = Integer.valueOf( eEntry );
					if ( index < 0 ) {
						return false;
					}
					if ( !theIndexes.add( index ) ) {
						return false;
					}
				}
			}
			catch ( NumberFormatException e ) {
				return false;
			}
		}
		return theMap.size() - theAnnotated != 0;
	}

	/**
	 * Determines whether the give path points to an entry in terms of
	 * {@link #children(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a entry.
	 * 
	 * @return True in case the given path points to a entry, else false.
	 */
	default boolean isChild( Collection<?> aPathElements ) {
		return isChild( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to an entry in terms of
	 * {@link #children(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a entry.
	 * 
	 * @return True in case the given path points to a entry, else false.
	 */
	default boolean isChild( Object... aPathElements ) {
		return isChild( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to an entry in terms of
	 * {@link #children(String)}.
	 * 
	 * @param aPath The path for which to determine whether it points to a
	 *        entry.
	 * 
	 * @return True in case the given path points to a entry, else false.
	 */
	default boolean isChild( String aPath ) {
		return ( hasValue( aPath ) || isDir( aPath ) );
	}

	/**
	 * Determines whether the give path points to an entry in terms of
	 * {@link #children(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a entry.
	 * 
	 * @return True in case the given path points to a entry, else false.
	 */
	default boolean isChild( String... aPathElements ) {
		return ( hasValue( aPathElements ) || isDir( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a directory in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a directory.
	 * 
	 * @return True in case the given path points to a directory, else false.
	 */
	default boolean isDir( Collection<?> aPathElements ) {
		return isDir( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a directory in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPath The path for which to determine whether it points to a
	 *        directory.
	 * 
	 * @return True in case the given path points to a directory, else false.
	 */
	default boolean isDir( Object aPath ) {
		return isDir( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Determines whether the give path points to a directory in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a directory.
	 * 
	 * @return True in case the given path points to a directory, else false.
	 */
	default boolean isDir( Object... aPathElements ) {
		return isDir( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a directory in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPath The path for which to determine whether it points to a
	 *        directory.
	 * 
	 * @return True in case the given path points to a directory, else false.
	 */
	default boolean isDir( String aPath ) {
		// From #keySet( String ):
		aPath = toNormalizedPath( aPath );
		if ( !aPath.endsWith( getDelimiter() + "" ) ) {
			aPath = aPath + getDelimiter();
		}
		final String thePath = aPath;
		final String theRootPath = getRootPath();
		for ( String eKey : keySet() ) {
			if ( ( eKey.length() > thePath.length() && eKey.startsWith( thePath ) ) || ( eKey.equals( theRootPath ) && thePath.equals( theRootPath ) ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines whether the give path points to a directory in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a directory.
	 * 
	 * @return True in case the given path points to a directory, else false.
	 */
	default boolean isDir( String... aPathElements ) {
		return isDir( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a leaf in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a leaf.
	 * 
	 * @return True in case the given path points to a leaf, else false.
	 */
	default boolean isLeaf( Collection<?> aPathElements ) {
		return isLeaf( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a leaf in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPath The path for which to determine whether it points to a leaf.
	 * 
	 * @return True in case the given path points to a leaf, else false.
	 */
	default boolean isLeaf( Object aPath ) {
		return isLeaf( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Determines whether the give path points to a leaf in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a leaf.
	 * 
	 * @return True in case the given path points to a leaf, else false.
	 */
	default boolean isLeaf( Object... aPathElements ) {
		return isLeaf( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a leaf in terms of
	 * {@link #leaves(String)}.
	 * 
	 * @param aPath The path for which to determine whether it points to a leaf.
	 * 
	 * @return True in case the given path points to a leaf, else false.
	 */
	default boolean isLeaf( String aPath ) {
		return containsKey( aPath );
	}

	/**
	 * Determines whether the give path points to a leaf in terms of
	 * {@link #dirs(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a leaf.
	 * 
	 * @return True in case the given path points to a leaf, else false.
	 */
	default boolean isLeaf( String... aPathElements ) {
		return isLeaf( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the root path represents an
	 * "array" path with indexes within the given {@link PathMap}. The path not
	 * necessarily points to a leaf in terms of being a leave or a directory.
	 * Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf. Given the following paths:
	 * <code> 
	 * /0/0aaa
	 * /0/0bbb
	 * /0/0bbb
	 * /1/1aaa
	 * /1/1bbb
	 * /1/1bbb
	 * </code> The root path points to an indexed path with the two indexes "0"
	 * and "1".
	 * 
	 * @return True in case we have a path points to an array being contained in
	 *         the given {@link PathMap}, else false.
	 */
	default boolean isIndexDir() {
		return isIndexDir( getRootPath() );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the provided path elements
	 * represent an "array" path with indexes within the given {@link PathMap}.
	 * The path not necessarily points to a leaf in terms of being a leave or a
	 * directory. Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf. Given the following paths:
	 * <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an indexed path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPathElements The elements of the path to be tested.
	 * 
	 * @return True in case we have a path points to an array being contained in
	 *         the given {@link PathMap}, else false.
	 */
	default boolean isIndexDir( Collection<?> aPathElements ) {
		return isIndexDir( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the provided path elements
	 * represent an "array" path with indexes within the given {@link PathMap}.
	 * The path not necessarily points to a leaf in terms of being a leave or a
	 * directory. Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf. Given the following paths:
	 * <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an indexed path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPathElements The elements of the path to be tested.
	 * 
	 * @return True in case we have a path points to an array being contained in
	 *         the given {@link PathMap}, else false.
	 */
	default boolean isIndexDir( Object... aPathElements ) {
		return isIndexDir( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the provided path represents an
	 * "array" path with indexes within the given {@link PathMap}. The path not
	 * necessarily points to a leaf in terms of being a leave or a directory.
	 * Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf. Given the following paths:
	 * <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an indexed path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPath The path to be tested.
	 * 
	 * @return True in case we have a path points to an array being contained in
	 *         the given {@link PathMap}, else false.
	 */
	default boolean isIndexDir( String aPath ) {
		aPath = toNormalizedPath( aPath );
		try {
			int eNextDelimiterIndex;
			final Set<String> thePaths = paths( aPath );
			if ( thePaths.size() != 0 ) {
				String ePath;
				final Iterator<String> e = thePaths.iterator();
				while ( e.hasNext() ) {
					ePath = e.next();
					if ( ePath.isEmpty() ) {
						// throw new NumberFormatException( "Breaking out!" );
						return false;
					}
					if ( ePath.startsWith( "" + getDelimiter() ) ) {
						ePath = ePath.substring( 1 );
					}
					eNextDelimiterIndex = ePath.indexOf( getDelimiter() );
					if ( eNextDelimiterIndex != -1 ) {
						ePath = ePath.substring( 0, eNextDelimiterIndex );
					}
					if ( ePath.startsWith( getAnnotator() + "" ) ) {
						e.remove();
					}
					else {
						Integer.parseInt( ePath );
					}
				}
				return !thePaths.isEmpty();
			}
			else {
				return false;
			}
		}
		catch ( Exception e ) {
			return false;
		}
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Determines whether the provided path elements
	 * represent an "array" path with indexes within the given {@link PathMap}.
	 * The path not necessarily points to a leaf in terms of being a leave or a
	 * directory. Use {@link #isChild(String)} ({@link #hasValue(String)} or
	 * {@link #isDir(String)}) if we have a leaf. Given the following paths:
	 * <code> 
	 * /root/child/0/0aaa
	 * /root/child/0/0bbb
	 * /root/child/0/0bbb
	 * /root/child/1/1aaa
	 * /root/child/1/1bbb
	 * /root/child/1/1bbb
	 * </code> The path "/root/child" points to an indexed path with the two
	 * indexes "0" and "1".
	 * 
	 * @param aPathElements The elements of the path to be tested.
	 * 
	 * @return True in case we have a path points to an array being contained in
	 *         the given {@link PathMap}, else false.
	 */
	default boolean isIndexDir( String... aPathElements ) {
		return isIndexDir( toPath( aPathElements ) );
	}

	/**
	 * Determines whether this {@link PathMap} contains the same paths and equal
	 * assigned values as the provided other {@link PathMap}. May be used for
	 * implementing an {@link Object#equals(Object)} method.
	 * 
	 * @param aOther The other {@link PathMap} with which to compare this
	 *        {@link PathMap}.
	 * 
	 * @return True in case this {@link PathMap} contains the same paths and
	 *         equal assigned values as the provided other {@link PathMap}.
	 */
	default boolean isEqualTo( PathMap<?> aOther ) {
		if ( size() != aOther.size() ) {
			return false;
		}
		Object eValue;
		Object eOtherValue;
		for ( String eKey : keySet() ) {
			eValue = get( eKey );
			eOtherValue = aOther.get( eKey );
			if ( eValue == eOtherValue ) {
				continue;
			}
			if ( eValue != null ) {
				if ( !eValue.equals( eOtherValue ) ) {
					return false;
				}
			}
			else {
				return false;
			}
		}
		return true;
	}

	/**
	 * Determines whether the give path points to a directory which contains
	 * sub-directories.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a directory containing sub-directories.
	 * 
	 * @return True in case the given path points to a directory containing
	 *         sub-directories, else false.
	 */
	default boolean hasSubDirs( Collection<?> aPathElements ) {
		return hasSubDirs( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a directory which contains
	 * sub-directories.
	 * 
	 * @param aPath The path for which to determine whether it points to a
	 *        directory containing sub-directories.
	 * 
	 * @return True in case the given path points to a directory containing
	 *         sub-directories, else false.
	 */
	default boolean hasSubDirs( Object aPath ) {
		return hasSubDirs( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Determines whether the give path points to a directory which contains
	 * sub-directories.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them point to a directory containing sub-directories.
	 * 
	 * @return True in case the given path points to a directory containing
	 *         sub-directories, else false.
	 */
	default boolean hasSubDirs( Object... aPathElements ) {
		return hasSubDirs( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a directory which contains
	 * sub-directories.
	 * 
	 * @param aPath The path for which to determine whether it points to a
	 *        directory containing sub-directories.
	 * 
	 * @return True in case the given path points to a directory containing
	 *         sub-directories, else false.
	 */
	default boolean hasSubDirs( String aPath ) {
		return dirs( aPath ).size() != 0;
	}

	/**
	 * Determines whether the give path points to a leave in terms of
	 * {@link #leaves(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them points to a leave.
	 * 
	 * @return True in case the given path points to a leave, else false.
	 */
	default boolean hasValue( Collection<?> aPathElements ) {
		return hasValue( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a leave in terms of
	 * {@link #leaves(String)}.
	 * 
	 * @param aPath The path for which to determine whether it points to a
	 *        leave.
	 * 
	 * @return True in case the given path points to a leave, else false.
	 */
	default boolean hasValue( Object aPath ) {
		return hasValue( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Determines whether the give path points to a leave in terms of
	 * {@link #leaves(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them points to a leave.
	 * 
	 * @return True in case the given path points to a leave, else false.
	 */
	default boolean hasValue( Object... aPathElements ) {
		return hasValue( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the give path points to a leave in terms of
	 * {@link #leaves(String)}.
	 * 
	 * @param aPath The path for which to determine whether it points to a
	 *        leave.
	 * 
	 * @return True in case the given path points to a leave, else false.
	 */
	default boolean hasValue( String aPath ) {
		aPath = toNormalizedPath( aPath );
		return ( containsKey( aPath ) );
	}

	/**
	 * Determines whether the give path points to a leave in terms of
	 * {@link #leaves(String)}.
	 * 
	 * @param aPathElements The path elements for which to determine whether
	 *        them points to a leave.
	 * 
	 * @return True in case the given path points to a leave, else false.
	 */
	default boolean hasValue( String... aPathElements ) {
		return hasValue( toPath( aPathElements ) );
	}

	/**
	 * Determines whether the given path represents the toor path.
	 * 
	 * @param aPath The path to be tested.
	 * 
	 * @return True in case we have a root path, else false.
	 */
	default boolean isRootPath( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final String theRootPath = toNormalizedPath( getRootPath() );
		return theRootPath.equals( aPath );
	}

	/**
	 * Same as {@link #keySet()} with the difference, that only the paths are
	 * contained belonging to the given path, excluding the given path.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return The relative paths starting at the given path (excluding the
	 *         preceding given path) contained in this {@link PathMap}.
	 */
	default Set<String> keySet( String aPath ) {
		aPath = toNormalizedPath( aPath );
		if ( !aPath.endsWith( getDelimiter() + "" ) ) {
			aPath = aPath + getDelimiter();
		}
		final String thePath = aPath;
		final String theRootPath = getRootPath();
		final Set<String> theKeySet = new LinkedHashSet<>();
		keySet().forEach( eKey -> {
			if ( ( eKey.length() > thePath.length() && eKey.startsWith( thePath ) ) || ( eKey.equals( theRootPath ) && thePath.equals( theRootPath ) ) ) {
				final String ePath = eKey.substring( thePath.length() );
				theKeySet.add( ePath );
			}
		} );
		return theKeySet;
	}

	/**
	 * Same as {@link #keySet()} with the difference, that only the paths are
	 * contained belonging to the given path, excluding the given path.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return The relative paths starting at the given path (excluding the
	 *         preceding given path) contained in this {@link PathMap}.
	 */
	default Set<String> keySet( Collection<?> aPathElements ) {
		return keySet( toPath( aPathElements ) );
	}

	/**
	 * Same as {@link #keySet()} with the difference, that only the paths are
	 * contained belonging to the given path, excluding the given path.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return The relative paths starting at the given path (excluding the
	 *         preceding given path) contained in this {@link PathMap}.
	 */
	default Set<String> keySet( Object aPath ) {
		return keySet( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Same as {@link #keySet()} with the difference, that only the paths are
	 * contained belonging to the given path, excluding the given path.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return The relative paths starting at the given path (excluding the
	 *         preceding given path) contained in this {@link PathMap}.
	 */
	default Set<String> keySet( Object... aPathElements ) {
		return keySet( toPath( aPathElements ) );
	}

	/**
	 * Same as {@link #keySet()} with the difference, that only the paths are
	 * contained belonging to the given path, excluding the given path.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return The relative paths starting at the given path (excluding the
	 *         preceding given path) contained in this {@link PathMap}.
	 */
	default Set<String> keySet( String... aPathElements ) {
		return keySet( toPath( aPathElements ) );
	}

	/**
	 * Returns the last index as of {@link #getArrayIndexes()}.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastArrayIndex() {
		return lastArrayIndex( getRootPath() );
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(Collection)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastArrayIndex( Collection<?> aPathElements ) {
		return lastArrayIndex( toPath( aPathElements ) );
	}

	/**
	 * Returns the last index as of {@link #getArrayIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastArrayIndex( Object aPath ) {
		return lastArrayIndex( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(String[])}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastArrayIndex( Object... aPathElements ) {
		return lastArrayIndex( toPath( aPathElements ) );
	}

	/**
	 * Returns the last index as of {@link #getArrayIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastArrayIndex( String aPath ) {
		int theIndex = -1;
		final int[] theIndexes = getArrayIndexes( aPath );
		if ( theIndexes != null && theIndexes.length != 0 ) {
			theIndex = theIndexes[theIndexes.length - 1];
		}
		//	else {
		//		throw new IllegalArgumentException( "The path <" + aPath + "> does not point to an indexed array." );
		//	}
		return theIndex;
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(String[])}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastArrayIndex( String... aPathElements ) {
		return lastArrayIndex( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the last index as of
	 * {@link #getDirIndexes()}.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastDirIndex() {
		return lastDirIndex( getRootPath() );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(Collection)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastDirIndex( Collection<?> aPathElements ) {
		return lastDirIndex( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the last index as of
	 * {@link #getDirIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastDirIndex( Object aPath ) {
		return lastDirIndex( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(String[])}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastDirIndex( Object... aPathElements ) {
		return lastDirIndex( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the last index as of
	 * {@link #getDirIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastDirIndex( String aPath ) {
		int theIndex = -1;
		final int[] theIndexes = getDirIndexes( aPath );
		if ( theIndexes != null && theIndexes.length != 0 ) {
			theIndex = theIndexes[theIndexes.length - 1];
		}
		//	else {
		//		throw new IllegalArgumentException( "The path <" + aPath + "> does not point to an indexed directory." );
		//	}
		return theIndex;
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(String[])}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        last index.
	 * 
	 * @return The last index used by the "array" or -1 if there is none element
	 *         in the array (or no array at all).
	 */
	default int lastDirIndex( String... aPathElements ) {
		return lastDirIndex( toPath( aPathElements ) );
	}

	/**
	 * Applies the {@link #leaves(String)} method for the root path "/".
	 * 
	 * @return As of {@link #leaves(String)} applied to the root path.
	 * 
	 * @see #leaves(String)
	 */
	default Set<String> leaves() {
		return leaves( getRootPath() );
	}

	/**
	 * Retrieves the leaves names below the given path elements excluding any
	 * entries representing directories. A leave is reckoned to be the last path
	 * element in a path pointing to a value. In contrast a directory is
	 * reckoned to be be a path element pointing to a succeeding child path
	 * element (sub-directory). Given we have values for paths in our
	 * {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #leaves(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "loki"; calling "/animals/dogs" will
	 * retrieve "dilbert" and "otto".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        leaves.
	 * 
	 * @return The leave names (excluding the preceding given path) of the
	 *         leaves below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> leaves( Collection<?> aPathElements ) {
		return leaves( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the leaves names below the given path excluding any entries
	 * representing directories. A leave is reckoned to be the last path element
	 * in a path pointing to a value. In contrast a directory is reckoned to be
	 * be a path element pointing to a succeeding child path element
	 * (sub-directory). Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #leaves(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "loki"; calling "/animals/dogs" will
	 * retrieve "dilbert" and "otto".
	 * 
	 * @param aPath The path from which to start retrieving the leaves.
	 * 
	 * @return The leave names (excluding the preceding given path) of the
	 *         leaves below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> leaves( Object aPath ) {
		return leaves( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Retrieves the leaves names below the given path elements excluding any
	 * entries representing directories. A leave is reckoned to be the last path
	 * element in a path pointing to a value. In contrast a directory is
	 * reckoned to be be a path element pointing to a succeeding child path
	 * element (sub-directory). Given we have values for paths in our
	 * {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #leaves(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "loki"; calling "/animals/dogs" will
	 * retrieve "dilbert" and "otto".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        leaves.
	 * 
	 * @return The leave names (excluding the preceding given path) of the
	 *         leaves below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> leaves( Object... aPathElements ) {
		return leaves( toPath( aPathElements ) );
	}

	/**
	 * Retrieves the leaves names below the given path excluding any entries
	 * representing directories. A leave is reckoned to be the last path element
	 * in a path pointing to a value. In contrast a directory is reckoned to be
	 * be a path element pointing to a succeeding child path element
	 * (sub-directory). Given we have values for paths in our {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #leaves(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "loki"; calling "/animals/dogs" will
	 * retrieve "dilbert" and "otto".
	 * 
	 * @param aPath The path from which to start retrieving the leaves.
	 * 
	 * @return The leave names (excluding the preceding given path) of the
	 *         leaves below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> leaves( String aPath ) {
		aPath = toNormalizedPath( aPath );
		final Set<String> theLeaves = new LinkedHashSet<>();
		keySet( aPath ).forEach( eKey -> {
			if ( eKey.length() > 0 ) {
				if ( eKey.indexOf( getDelimiter(), 0 ) == -1 ) {
					theLeaves.add( eKey );
				}
			}
		} );
		return theLeaves;
	}

	/**
	 * Retrieves the leaves names below the given path elements excluding any
	 * entries representing directories. A leave is reckoned to be the last path
	 * element in a path pointing to a value. In contrast a directory is
	 * reckoned to be be a path element pointing to a succeeding child path
	 * element (sub-directory). Given we have values for paths in our
	 * {@link PathMap}:
	 * <ul>
	 * <li>"/animals/dogs/dilbert"</li>
	 * <li>"/animals/dogs/otto"</li>
	 * <li>"/animals/loki"</li>
	 * <li>"/machines/robots/greg"</li>
	 * </ul>
	 * When we call {@link #leaves(String)} with "/animals", then the resulting
	 * {@link Set} will just contain "loki"; calling "/animals/dogs" will
	 * retrieve "dilbert" and "otto".
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        leaves.
	 * 
	 * @return The leave names (excluding the preceding given path) of the
	 *         leaves below the given path contained within this
	 *         {@link PathMap}.
	 */
	default Set<String> leaves( String... aPathElements ) {
		return leaves( toPath( aPathElements ) );
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes()}.
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextArrayIndex() {
		return nextArrayIndex( getRootPath() );
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(Collection)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        next index to be used (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextArrayIndex( Collection<?> aPathElements ) {
		return nextArrayIndex( toPath( aPathElements ) );
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the next index to be used
	 *        (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextArrayIndex( Object aPath ) {
		return lastArrayIndex( toNormalizedPath( InterOperableMap.asKey( aPath ) ) ) + 1;
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(String...)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        next index to be used (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextArrayIndex( Object... aPathElements ) {
		return nextArrayIndex( toPath( aPathElements ) );
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the next index to be used
	 *        (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextArrayIndex( String aPath ) {
		try {
			return lastArrayIndex( aPath ) + 1;
		}
		catch ( IllegalArgumentException ignore ) {
			return 0;
		}
	}

	/**
	 * Returns the next index to be used (and not used yet) as of
	 * {@link #getArrayIndexes(String...)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        next index to be used (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextArrayIndex( String... aPathElements ) {
		return nextArrayIndex( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes()}.
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextDirIndex() {
		return nextDirIndex( getRootPath() );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(Collection)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        next index to be used (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextDirIndex( Collection<?> aPathElements ) {
		return nextDirIndex( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the next index to be used
	 *        (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextDirIndex( Object aPath ) {
		return lastDirIndex( toNormalizedPath( InterOperableMap.asKey( aPath ) ) ) + 1;
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(String...)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        next index to be used (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextDirIndex( Object... aPathElements ) {
		return nextDirIndex( toPath( aPathElements ) );
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(String)}.
	 * 
	 * @param aPath The path from which to determine the next index to be used
	 *        (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextDirIndex( String aPath ) {
		return lastDirIndex( aPath ) + 1;
	}

	/**
	 * An indexed directory represents all elements which begin with a path
	 * which's last path element represents an index. There may by many
	 * sub-paths for the same indexed path which are all are included by the
	 * according directory. Returns the next index to be used (and not used yet)
	 * as of {@link #getDirIndexes(String...)}.
	 * 
	 * @param aPathElements The elements of the path from which to determine the
	 *        next index to be used (and not used yet).
	 * 
	 * @return The next index to be used (and not used yet) as of the "array".
	 */
	default int nextDirIndex( String... aPathElements ) {
		return nextDirIndex( toPath( aPathElements ) );
	}

	/**
	 * Same as {@link #keySet()} with according semantics in the method name.
	 * 
	 * @return The paths which are assigned to values.
	 */
	default Set<String> paths() {
		return keySet();
	}

	/**
	 * Same as {@link #keySet(String)} with according semantics in the method
	 * name.
	 * 
	 * @param aPathElements The path elements from which to start seeking.
	 * 
	 * @return The paths (excluding the preceding given path) which are assigned
	 *         to values.
	 */
	default Set<String> paths( Collection<?> aPathElements ) {
		return paths( toPath( aPathElements ) );
	}

	/**
	 * Same as {@link #keySet(String)} with according semantics in the method
	 * name.
	 * 
	 * @param aPath The path from which to start seeking.
	 * 
	 * @return The paths (excluding the preceding given path) which are assigned
	 *         to values.
	 */
	default Set<String> paths( Object aPath ) {
		return paths( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Same as {@link #keySet(String)} with according semantics in the method
	 * name.
	 * 
	 * @param aPathElements The path elements from which to start seeking.
	 * 
	 * @return The paths (excluding the preceding given path) which are assigned
	 *         to values.
	 */
	default Set<String> paths( Object... aPathElements ) {
		return paths( toPath( aPathElements ) );
	}

	/**
	 * Same as {@link #keySet(String)} with according semantics in the method
	 * name.
	 * 
	 * @param aPath The path from which to start seeking.
	 * 
	 * @return The paths (excluding the preceding given path) which are assigned
	 *         to values.
	 */
	default Set<String> paths( String aPath ) {
		return keySet( aPath );
	}

	/**
	 * Same as {@link #keySet(String)} with according semantics in the method
	 * name.
	 * 
	 * @param aPathElements The path elements from which to start seeking.
	 * 
	 * @return The paths (excluding the preceding given path) which are assigned
	 *         to values.
	 */
	default Set<String> paths( String... aPathElements ) {
		return paths( toPath( aPathElements ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aQueryElements The elements representing the path query including
	 *        your wildcards.
	 * 
	 * @return The result contains the matching paths with the according values.
	 */
	default PathMap<T> query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aQueryElements The elements representing the path query including
	 *        your wildcards.
	 * 
	 * @return The result contains the matching paths with the according values.
	 */
	default PathMap<T> query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * Queries the keys of the instance using a regular expression as of the
	 * provided {@link Pattern} instance.
	 * 
	 * @param aRegExp The regular expression to be used for the query.
	 * 
	 * @return The matching properties.
	 */
	default PathMap<T> query( Pattern aRegExp ) {
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		for ( String ePath : paths() ) {
			if ( aRegExp.matcher( ePath ).matches() ) {
				theMap.put( ePath, get( ePath ) );
			}
		}
		return theMap;
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * 
	 * @return The result contains the matching paths with the according values.
	 */
	default PathMap<T> query( String aPathQuery ) {
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		final PathMatcher theMatcher = new PathMatcher( aPathQuery, getDelimiter() );
		for ( String ePath : paths() ) {
			if ( theMatcher.isMatching( ePath ) ) {
				theMap.put( ePath, get( ePath ) );
			}
		}
		return theMap;
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aQueryElements The elements representing the path query including
	 *        your wildcards.
	 * 
	 * @return The result contains the matching paths with the according values.
	 */
	default PathMap<T> query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path and the to-path) with the according values.
	 */
	default PathMap<T> queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path and the to-path) with the according values.
	 */
	default PathMap<T> queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toNormalizedPath( InterOperableMap.asKey( aFromPath ) ), toNormalizedPath( InterOperableMap.asKey( aPathQuery ) ), toNormalizedPath( InterOperableMap.asKey( aToPath ) ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path and the to-path) with the according values.
	 */
	default PathMap<T> queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * Queries the keys of the instance using a regular expression as of the
	 * provided {@link Pattern}.
	 * 
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * @param aRegExp The regular expression to be used for the query.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path and the to-path) with the according values.
	 */
	default PathMap<T> queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		final PathMap<T> theFrom = retrieveFrom( aFromPath );
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		for ( String ePath : theFrom.keySet() ) {
			if ( aRegExp.matcher( ePath ).matches() ) {
				theMap.put( ePath, theFrom.get( ePath ) );
			}
		}
		return theMap.retrieveTo( aToPath );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path and the to-path) with the according values.
	 */
	default PathMap<T> queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		final PathMap<T> theFrom = retrieveFrom( aFromPath );
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		final PathMatcher theMatcher = new PathMatcher( aPathQuery, getDelimiter() );
		for ( String ePath : theFrom.keySet() ) {
			if ( theMatcher.isMatching( ePath ) ) {
				theMap.put( ePath, theFrom.get( ePath ) );
			}
		}
		return theMap.retrieveTo( aToPath );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path and the to-path) with the according values.
	 */
	default PathMap<T> queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path) with the according values.
	 */
	default PathMap<T> queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path) with the according values.
	 */
	default PathMap<T> queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path) with the according values.
	 */
	default PathMap<T> queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * Queries the keys of the instance using a regular expression as of the
	 * provided {@link Pattern}.
	 * 
	 * @param aRegExp The regular expression to be used for the query.
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path) with the according values.
	 */
	default PathMap<T> queryFrom( Pattern aRegExp, String aFromPath ) {
		final PathMap<T> theFrom = retrieveFrom( aFromPath );
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		for ( String ePath : theFrom.keySet() ) {
			if ( aRegExp.matcher( ePath ).matches() ) {
				theMap.put( ePath, theFrom.get( ePath ) );
			}
		}
		return theMap;
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path) with the according values.
	 */
	default PathMap<T> queryFrom( String aPathQuery, String aFromPath ) {
		final PathMap<T> theFrom = retrieveFrom( aFromPath );
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		final PathMatcher theMatcher = new PathMatcher( aPathQuery, getDelimiter() );
		for ( String ePath : theFrom.keySet() ) {
			if ( theMatcher.isMatching( ePath ) ) {
				theMap.put( ePath, theFrom.get( ePath ) );
			}
		}
		return theMap;
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aFromPath The path from where to start querying and extracting the
	 *        paths.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         from-path) with the according values.
	 */
	default PathMap<T> queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * 
	 * @return The result contains the matching paths.
	 */
	default Set<String> queryPaths( Collection<?> aPathQuery ) {
		return queryPaths( toPath( aPathQuery ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * 
	 * @return The result contains the matching paths.
	 */
	default Set<String> queryPaths( Object aPathQuery ) {
		return queryPaths( toPath( aPathQuery ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * 
	 * @return The result contains the matching paths.
	 */
	default Set<String> queryPaths( Object... aPathQuery ) {
		return queryPaths( toPath( aPathQuery ) );
	}

	/**
	 * Queries the keys of the instance using a regular expression as of the
	 * provided {@link Pattern}.
	 * 
	 * @param aRegExp The regular expression to be used for the query.
	 * 
	 * @return The result contains the matching paths.
	 */
	default Set<String> queryPaths( Pattern aRegExp ) {
		final Set<String> theSet = new LinkedHashSet<>();
		for ( String ePath : paths() ) {
			if ( aRegExp.matcher( ePath ).matches() ) {
				synchronized ( theSet ) {
					theSet.add( ePath );
				}
			}
		}
		return theSet;
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * 
	 * @return The result contains the matching paths.
	 */
	default Set<String> queryPaths( String... aPathQuery ) {
		return queryPaths( toPath( aPathQuery ) );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * 
	 * @return The result contains the matching paths.
	 */
	default Set<String> queryPaths( String aPathQuery ) {
		final Set<String> theSet = new LinkedHashSet<>();
		final PathMatcher theMatcher = new PathMatcher( aPathQuery, getDelimiter() );
		for ( String ePath : paths() ) {
			if ( theMatcher.isMatching( ePath ) ) {
				synchronized ( theSet ) {
					theSet.add( ePath );
				}
			}
		}
		return theSet;
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         to-path) with the according values.
	 */
	default PathMap<T> queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), aToPath );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         to-path) with the according values.
	 */
	default PathMap<T> queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), aToPath );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         to-path) with the according values.
	 */
	default PathMap<T> queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), aToPath );
	}

	/**
	 * Queries the keys of the instance using a regular expression as of the
	 * provided {@link Pattern}.
	 * 
	 * @param aRegExp The regular expression to be used for the query.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         to-path) with the according values.
	 */
	default PathMap<T> queryTo( Pattern aRegExp, String aToPath ) {
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		for ( String ePath : paths() ) {
			if ( aRegExp.matcher( ePath ).matches() ) {
				theMap.put( ePath, get( ePath ) );
			}
		}
		return theMap.retrieveTo( aToPath );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         to-path) with the according values.
	 */
	default PathMap<T> queryTo( String aPathQuery, String aToPath ) {
		final PathMapBuilder<T> theMap = new PathMapBuilderImpl<>( getType() );
		final PathMatcher theMatcher = new PathMatcher( aPathQuery, getDelimiter() );
		final Set<String> thePaths = new LinkedHashSet<>( paths() );
		thePaths.forEach( ePath -> {
			if ( theMatcher.isMatching( ePath ) ) {
				theMap.put( ePath, get( ePath ) );
			}
		} );
		for ( String ePath : paths() ) {
			if ( theMatcher.isMatching( ePath ) ) {
				theMap.put( ePath, get( ePath ) );
			}
		}
		return theMap.retrieveTo( aToPath );
	}

	/**
	 * Queries the keys of the instance using the {@link PathMatcher}' matching
	 * patterns, similar to the wildcards '*', '?' and '**' known when querying
	 * folders of a filesystem: The {@link PathMatcher} applies the following
	 * rules from the ANT path pattern to the query provided: A single asterisk
	 * ("*" as of {@link Wildcard#FILE}) matches zero or more characters within
	 * a path name. A double asterisk ("**" as of {@link Wildcard#PATH}) matches
	 * zero or more characters across directory levels. A question mark ("?" as
	 * of {@link Wildcard#CHAR}) matches exactly one character within a path
	 * name. The single asterisk ("*" as of {@link Wildcard#FILE}), the double
	 * asterisk ("**" as of {@link Wildcard#PATH}) and the question mark ("?" as
	 * of {@link Wildcard#CHAR}) we refer to as wildcards.
	 * 
	 * @param aPathQuery The path query including your wildcards.
	 * @param aToPath The path where to relocate the result to.
	 * 
	 * @return The result contains the matching paths (with respect to the
	 *         to-path) with the according values.
	 */
	default PathMap<T> queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), aToPath );
	}

	/**
	 * Extracts a new {@link PathMap} from the elements of this {@link PathMap}
	 * found below the "from-path". The sub-paths will be relocated to the
	 * provided "to-path".
	 * 
	 * @param aFromPath The path from where to start extracting the paths.
	 * @param aToPath The path where to relocate the extracted paths to.
	 * 
	 * @return The {@link PathMap} with the elements below the provided path
	 *         "from-path" relocated to the given "to-path".
	 */
	default PathMap<T> retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		final PathMap<T> thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * Extracts a new {@link PathMap} from the elements of this {@link PathMap}
	 * found below the "from-path". The sub-paths will be relocated to the
	 * provided "to-path".
	 * 
	 * @param aFromPath The path from where to start extracting the paths.
	 * @param aToPath The path where to relocate the extracted paths to.
	 * 
	 * @return The {@link PathMap} with the elements below the provided path
	 *         "from-path" relocated to the given "to-path".
	 */
	default PathMap<T> retrieveBetween( Object aFromPath, Object aToPath ) {
		final PathMap<T> thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * Extracts a new {@link PathMap} from the elements of this {@link PathMap}
	 * found below the "from-path". The sub-paths will be relocated to the
	 * provided "to-path".
	 * 
	 * @param aFromPath The path from where to start extracting the paths.
	 * @param aToPath The path where to relocate the extracted paths to.
	 * 
	 * @return The {@link PathMap} with the elements below the provided path
	 *         "from-path" relocated to the given "to-path".
	 */
	default PathMap<T> retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		final PathMap<T> thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * Extracts a new {@link PathMap} from the elements of this {@link PathMap}
	 * found below the "from-path". The sub-paths will be relocated to the
	 * provided "to-path".
	 * 
	 * @param aFromPath The path from where to start extracting the paths.
	 * @param aToPath The path where to relocate the extracted paths to.
	 * 
	 * @return The {@link PathMap} with the elements below the provided path
	 *         "from-path" relocated to the given "to-path".
	 */
	default PathMap<T> retrieveBetween( String aFromPath, String aToPath ) {
		final PathMap<T> thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * Extracts a new {@link PathMap} from the elements of this {@link PathMap}
	 * found below the "from-path". The sub-paths will be relocated to the
	 * provided "to-path".
	 * 
	 * @param aFromPath The path from where to start extracting the paths.
	 * @param aToPath The path where to relocate the extracted paths to.
	 * 
	 * @return The {@link PathMap} with the elements below the provided path
	 *         "from-path" relocated to the given "to-path".
	 */
	default PathMap<T> retrieveBetween( String[] aFromPath, String[] aToPath ) {
		final PathMap<T> thePathMap = retrieveFrom( aFromPath );
		return thePathMap.retrieveTo( aToPath );
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * Gets the children elements of the provided parent's path, excluding the
	 * parent's path.
	 * 
	 * @param aFromPath The path from where to retrieve the children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> retrieveFrom( Object aFromPath ) {
		return retrieveFrom( toNormalizedPath( InterOperableMap.asKey( aFromPath ) ) );
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * Extracts a new {@link PathMap} from the elements of this {@link PathMap}
	 * found below the "from-path". The sub-paths will be the root paths for the
	 * new {@link PathMap}. Given we have a value for path "/dog/cat" in the
	 * {@link PathMap} and we call {@link #retrieveFrom(String)} with "/dog",
	 * then the resulting {@link PathMap} will contain the path "/cat" for that
	 * value.
	 * 
	 * @param aFromPath The path from where to start extracting the paths.
	 * 
	 * @return The {@link PathMap} with the elements below the provided path
	 *         which are root elements in the new {@link PathMap}.
	 */
	PathMap<T> retrieveFrom( String aFromPath );

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> getDir( Collection<?> aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * Gets the children elements of the provided parent's path, excluding the
	 * parent's path.
	 * 
	 * @param aPath The path from where to retrieve the children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> getDir( Object aPath ) {
		return getDir( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> getDir( Object... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path.
	 * 
	 * @param aPath The path from where to retrieve the children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> getDir( String aPath ) {
		return retrieveFrom( aPath );
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return The children of the given parent's path.
	 */
	default PathMap<T> getDir( String... aPathElements ) {
		return getDir( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * Gets the children elements of the provided parent's path, excluding the
	 * parent's path. If the parent points to a directory index (as of
	 * {@link #isIndexDir()} and the like methods), then the indexed directories
	 * are returned with their directory index corresponding to the returned
	 * array's index. parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return An array with the children directories of the given parent's
	 *         path: If the path points to an indexed directory (as of
	 *         {@link #isIndexDir()} or the like), then each indexed directory
	 *         is represented by an element of the array at the according index.
	 *         If the path does not point to an indexed directory, then the
	 *         directory itself is returned as the only element in the returned
	 *         array.
	 */
	default PathMap<T>[] getDirs( Collection<?> aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * Gets the children elements of the provided parent's path, excluding the
	 * parent's path. If the parent points to a directory index (as of
	 * {@link #isIndexDir()} and the like methods), then the indexed directories
	 * are returned with their directory index corresponding to the returned
	 * array's index. parent's path.
	 * 
	 * @param aPath The path from where to retrieve the children.
	 * 
	 * @return An array with the children directories of the given parent's
	 *         path: If the path points to an indexed directory (as of
	 *         {@link #isIndexDir()} or the like), then each indexed directory
	 *         is represented by an element of the array at the according index.
	 *         If the path does not point to an indexed directory, then the
	 *         directory itself is returned as the only element in the returned
	 *         array.
	 */
	default PathMap<T>[] getDirs( Object aPath ) {
		return getDirs( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path. If the parent points to a directory index
	 * (as of {@link #isIndexDir()} and the like methods), then the indexed
	 * directories are returned with their directory index corresponding to the
	 * returned array's index. parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return An array with the children directories of the given parent's
	 *         path: If the path points to an indexed directory (as of
	 *         {@link #isIndexDir()} or the like), then each indexed directory
	 *         is represented by an element of the array at the according index.
	 *         If the path does not point to an indexed directory, then the
	 *         directory itself is returned as the only element in the returned
	 *         array.
	 */
	default PathMap<T>[] getDirs( Object... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path. If the parent points to a directory index
	 * (as of {@link #isIndexDir()} and the like methods), then the indexed
	 * directories are returned with their directory index corresponding to the
	 * returned array's index. parent's path.
	 * 
	 * @param aPath The path from where to retrieve the children.
	 * 
	 * @return An array with the children directories of the given parent's
	 *         path: If the path points to an indexed directory (as of
	 *         {@link #isIndexDir()} or the like), then each indexed directory
	 *         is represented by an element of the array at the according index.
	 *         If the path does not point to an indexed directory, then the
	 *         directory itself is returned as the only element in the returned
	 *         array.
	 */
	@SuppressWarnings("unchecked")
	default PathMap<T>[] getDirs( String aPath ) {
		final PathMap<T>[] theDirs;
		if ( isIndexDir( aPath ) ) {
			final int[] theIndexes = getDirIndexes( aPath );
			theDirs = new PathMap[theIndexes[theIndexes.length - 1] + 1];
			for ( int i : theIndexes ) {
				theDirs[theIndexes[i]] = getDirAt( aPath, theIndexes[i] );
			}
		}
		else {
			theDirs = new PathMap[] { getDir( aPath ) };
		}
		return theDirs;
	}

	/**
	 * Gets the children elements of the provided parent's path elements,
	 * excluding the parent's path. If the parent points to a directory index
	 * (as of {@link #isIndexDir()} and the like methods), then the indexed
	 * directories are returned with their directory index corresponding to the
	 * returned array's index. parent's path.
	 * 
	 * @param aPathElements The path elements from where to retrieve the
	 *        children.
	 * 
	 * @return An array with the children directories of the given parent's
	 *         path: If the path points to an indexed directory (as of
	 *         {@link #isIndexDir()} or the like), then each indexed directory
	 *         is represented by an element of the array at the according index.
	 *         If the path does not point to an indexed directory, then the
	 *         array. directory itself is returned as the only element in the
	 *         returned
	 */
	default PathMap<T>[] getDirs( String... aPathElements ) {
		return getDirs( toPath( aPathElements ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * Extracts a new {@link PathMap} from this {@link PathMap}'s elements with
	 * the paths relocated to the provided "to-path". Given we have a value for
	 * path "/dog/cat" in the {@link PathMap} and we call
	 * {@link #retrieveTo(String)} with "/animals", then the resulting
	 * {@link PathMap} will contain the path "/animals/dog/cat" for that value.
	 * 
	 * @param aToPathElements The path elements representing the path where to
	 *        relocate the paths of this {@link PathMap} to.
	 * 
	 * @return The {@link PathMap} with the elements from the provided
	 *         {@link PathMap} with accordingly relocated paths.
	 */
	default PathMap<T> retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * Extracts a new {@link PathMap} from this {@link PathMap}'s elements with
	 * the paths relocated to the provided "to-path". Given we have a value for
	 * path "/dog/cat" in the {@link PathMap} and we call
	 * {@link #retrieveTo(String)} with "/animals", then the resulting
	 * {@link PathMap} will contain the path "/animals/dog/cat" for that value.
	 * 
	 * @param aToPath The path where to relocate the paths of this
	 *        {@link PathMap} to.
	 * 
	 * @return The {@link PathMap} with the elements from the provided
	 *         {@link PathMap} with accordingly relocated paths.
	 */
	default PathMap<T> retrieveTo( Object aToPath ) {
		return retrieveTo( toNormalizedPath( InterOperableMap.asKey( aToPath ) ) );
	}

	/**
	 * Extracts a new {@link PathMap} from this {@link PathMap}'s elements with
	 * the paths relocated to the provided "to-path". Given we have a value for
	 * path "/dog/cat" in the {@link PathMap} and we call
	 * {@link #retrieveTo(String)} with "/animals", then the resulting
	 * {@link PathMap} will contain the path "/animals/dog/cat" for that value.
	 * 
	 * @param aToPathElements The path elements representing the path where to
	 *        relocate the paths of this {@link PathMap} to.
	 * 
	 * @return The {@link PathMap} with the elements from the provided
	 *         {@link PathMap} with accordingly relocated paths.
	 */
	default PathMap<T> retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * Extracts a new {@link PathMap} from this {@link PathMap}'s elements with
	 * the paths relocated to the provided "to-path". Given we have a value for
	 * path "/dog/cat" in the {@link PathMap} and we call
	 * {@link #retrieveTo(String)} with "/animals", then the resulting
	 * {@link PathMap} will contain the path "/animals/dog/cat" for that value.
	 * 
	 * @param aToPath The path where to relocate the paths of this
	 *        {@link PathMap} to.
	 * 
	 * @return The {@link PathMap} with the elements from the provided
	 *         {@link PathMap} with accordingly relocated paths.
	 */
	PathMap<T> retrieveTo( String aToPath );

	/**
	 * Extracts a new {@link PathMap} from this {@link PathMap}'s elements with
	 * the paths relocated to the provided "to-path". Given we have a value for
	 * path "/dog/cat" in the {@link PathMap} and we call
	 * {@link #retrieveTo(String)} with "/animals", then the resulting
	 * {@link PathMap} will contain the path "/animals/dog/cat" for that value.
	 * 
	 * @param aToPathElements The path elements representing the path where to
	 *        relocate the paths of this {@link PathMap} to.
	 * 
	 * @return The {@link PathMap} with the elements from the provided
	 *         {@link PathMap} with accordingly relocated paths.
	 */
	default PathMap<T> retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * Retrieves an alphabetically sorted list containing all the keys found in
	 * the elements of this collection.
	 * 
	 * Sorts the {@link #keySet()} using the {@link PathSortStrategy#DEFAULT}
	 * sorting strategy.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys() {
		final List<String> theKeys = new ArrayList<>( keySet() );
		theKeys.sort( new PathComparator( getDelimiter(), PathSortStrategy.DEFAULT ) );
		return theKeys;
	}

	/**
	 * Retrieves an alphabetically sorted list containing the keys found below
	 * the given path of this collection.
	 * 
	 * Sorts the {@link #keySet(String)} using the
	 * {@link PathSortStrategy#DEFAULT} sorting strategy.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Collection<?> aPathElements ) {
		return sortedKeys( toPath( aPathElements ) );
	}

	/**
	 * Retrieves an alphabetically sorted list containing the keys found below
	 * the given path of this collection.
	 * 
	 * Sorts the {@link #keySet(String)} using the
	 * {@link PathSortStrategy#DEFAULT} sorting strategy.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Object aPath ) {
		return sortedKeys( toNormalizedPath( InterOperableMap.asKey( aPath ) ) );
	}

	/**
	 * Retrieves an alphabetically sorted list containing the keys found below
	 * the given path of this collection.
	 * 
	 * Sorts the {@link #keySet(String)} using the
	 * {@link PathSortStrategy#DEFAULT} sorting strategy.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Object... aPathElements ) {
		return sortedKeys( toPath( aPathElements ) );
	}

	/**
	 * Retrieves an alphabetically sorted list containing the keys found below
	 * the given path of this collection.
	 * 
	 * Sorts the {@link #keySet(String)} using the
	 * {@link PathSortStrategy#DEFAULT} sorting strategy.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( String... aPathElements ) {
		return sortedKeys( toPath( aPathElements ) );
	}

	/**
	 * Retrieves an alphabetically sorted list containing the keys found below
	 * the given path of this collection.
	 * 
	 * Sorts the {@link #keySet(String)} using the
	 * {@link PathSortStrategy#DEFAULT} sorting strategy.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( String aPath ) {
		final List<String> theKeys = new ArrayList<>( keySet( aPath ) );
		theKeys.sort( new PathComparator( getDelimiter(), PathSortStrategy.DEFAULT ) );
		return theKeys;
	}

	/**
	 * Retrieves a sorted list as of the provided {@link PathSortStrategy}
	 * containing all the keys found in the elements of this collection.
	 * 
	 * @param aPathSortStrategy The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( PathSortStrategy aPathSortStrategy ) {
		final List<String> theKeys = new ArrayList<>( keySet() );
		theKeys.sort( new PathComparator( getDelimiter(), aPathSortStrategy ) );
		return theKeys;
	}

	/**
	 * Retrieves a sorted list as of the provided {@link PathSortStrategy}
	 * containing all the keys found below the given path of this collection.
	 * 
	 * @param aPathSortStrategy The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( PathSortStrategy aPathSortStrategy, Collection<?> aPathElements ) {
		return sortedKeys( aPathSortStrategy, toPath( aPathElements ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link PathSortStrategy}
	 * containing all the keys found below the given path of this collection.
	 * 
	 * @param aPathSortStrategy The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( PathSortStrategy aPathSortStrategy, Object aPath ) {
		return sortedKeys( aPathSortStrategy, toPath( aPath ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link PathSortStrategy}
	 * containing all the keys found below the given path of this collection.
	 * 
	 * @param aPathSortStrategy The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( PathSortStrategy aPathSortStrategy, Object... aPathElements ) {
		return sortedKeys( aPathSortStrategy, toPath( aPathElements ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link PathSortStrategy}
	 * containing all the keys found below the given path of this collection.
	 * 
	 * @param aPathSortStrategy The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( PathSortStrategy aPathSortStrategy, String... aPathElements ) {
		return sortedKeys( aPathSortStrategy, toPath( aPathElements ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link PathSortStrategy}
	 * containing all the keys found below the given path of this collection.
	 * 
	 * @param aPathSortStrategy The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( PathSortStrategy aPathSortStrategy, String aPath ) {
		final List<String> theKeys = new ArrayList<>( keySet( aPath ) );
		theKeys.sort( new PathComparator( getDelimiter(), PathSortStrategy.DEFAULT ) );
		return theKeys;
	}

	/**
	 * Retrieves a sorted list as of the provided {@link Comparator} containing
	 * all the keys found in the elements of this collection.
	 * 
	 * @param aComparator The {@link Comparator} to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Comparator<String> aComparator ) {
		final List<String> theKeys = new ArrayList<>( keySet() );
		theKeys.sort( aComparator );
		return theKeys;
	}

	/**
	 * Retrieves a sorted list as of the provided {@link Comparator} containing
	 * all the keys found in the elements of this collection.
	 * 
	 * @param aComparator The {@link Comparator} to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Comparator<String> aComparator, Collection<?> aPathElements ) {
		return sortedKeys( aComparator, toPath( aPathElements ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link Comparator} containing
	 * all the keys found below the given path of this collection.
	 * 
	 * @param aComparator The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Comparator<String> aComparator, Object aPath ) {
		return sortedKeys( aComparator, toPath( aPath ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link Comparator} containing
	 * all the keys found below the given path of this collection.
	 * 
	 * @param aComparator The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Comparator<String> aComparator, Object... aPathElements ) {
		return sortedKeys( aComparator, toPath( aPathElements ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link Comparator} containing
	 * all the keys found below the given path of this collection.
	 * 
	 * @param aComparator The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPathElements The path elements from which to start retrieving the
	 *        paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Comparator<String> aComparator, String... aPathElements ) {
		return sortedKeys( aComparator, toPath( aPathElements ) );
	}

	/**
	 * Retrieves a sorted list as of the provided {@link Comparator} containing
	 * all the keys found below the given path of this collection.
	 * 
	 * @param aComparator The sorting strategy to be used when sorting the
	 *        {@link #keySet()}.
	 * 
	 * @param aPath The path from which to start retrieving the paths.
	 * 
	 * @return A sorted list with key objects being the keys of all elements in
	 *         this collection.
	 */
	default List<String> sortedKeys( Comparator<String> aComparator, String aPath ) {
		final List<String> theKeys = new ArrayList<>( keySet( aPath ) );
		theKeys.sort( aComparator );
		return theKeys;
	}

	/**
	 * Applies the {@link #toDataStructure(String)} method for the root path
	 * "/".
	 * 
	 * @return As of {@link #toDataStructure(String)} applied to the root path.
	 * 
	 * @see #toDataStructure(String)
	 */
	default Object toDataStructure() {
		return toDataStructure( getRootPath() );
	}

	/**
	 * Similar to the {@link #toMap(String)} method, though in case all the keys
	 * of a nested {@link Map} instance (applicable to the root {@link Map} as
	 * well) represent an array (as of {@link #getArray( String)}), then an
	 * array is generated where the index of each value in the {@link Map} is
	 * represented by the number representation of the {@link Map}'s key for
	 * that value. The result is a data structure consisting of {@link Map}
	 * instances and arrays.
	 * 
	 * @param aFromPath The path below which the {@link PathMap} to be converted
	 *        into a data structure.
	 * 
	 * @return A data structure being a mixture of {@link Map} instances and
	 *         arrays representing the data below the given path.
	 */
	Object toDataStructure( String aFromPath );

	/**
	 * Converts the given key (path) to an external key by converting the
	 * default delimiter {@link #getDelimiter()} to the provided delimiter. In
	 * case we have a provided delimiter different from the commonly used path
	 * delimiters "/" or "\", then a prefixed delimiter is removed.
	 * 
	 * @param aPath The key which is to be converted to its external
	 *        representation.
	 * @param aDelimiter The delimiter to be used for the external
	 *        representation instead if the default delimiter
	 *        {@link #getDelimiter()}.
	 * 
	 * @return The external representation of the key as of the provided
	 *         delimiter.
	 */
	default String toExternalPath( String aPath, char aDelimiter ) {
		if ( aPath != null ) {
			aPath = toNormalizedPath( aPath );
			if ( aDelimiter != Delimiter.PATH.getChar() && aDelimiter != Delimiter.DOS_PATH.getChar() ) {
				while ( aPath.startsWith( "" + getDelimiter() ) ) {
					aPath = aPath.substring( 1 );
				}
			}
			if ( aDelimiter != getDelimiter() ) {
				aPath = aPath.replace( getDelimiter(), aDelimiter );
			}
		}

		return aPath;
	}

	/**
	 * Updates the instance with this {@link PathMap}'s data. Instances inside
	 * the instance may not be updated!
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aFromPath The path representing the root from which to take the
	 *        data for the instance.
	 * @param aInstance The instance to be updated with the herein contained
	 *        data.
	 */
	default <TYPE> void toInstance( String aFromPath, TYPE aInstance ) {
		try {
			TypeUtility.toInstance( toDataStructure( aFromPath ), aInstance );
		}
		catch ( InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | InvocationTargetException | ClassNotFoundException e ) {
			throw new IllegalArgumentException( "Cannot update the instance of type <" + aInstance.getClass().getName() + "> from path <" + aFromPath + ">!", e );
		}
	}

	/**
	 * Updates the instance with this {@link PathMap}'s data. Instances inside
	 * the instance may not be updated!
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aInstance The instance to be updated with the herein contained
	 *        data.
	 */
	default <TYPE> void toInstance( TYPE aInstance ) {
		toInstance( getRootPath(), aInstance );
	}

	/**
	 * Returns the last path element from the given path.
	 * 
	 * @param aPath The path for which to get the last path element.
	 * 
	 * @return The last path element or null if there is none path elements in
	 *         the path.
	 */
	default String toLeaf( String aPath ) {
		String theLeaf = null;
		final String[] theElements = toPathElements( aPath );
		if ( theElements != null && theElements.length != 0 ) {
			theLeaf = theElements[theElements.length - 1];
		}
		return theLeaf;
	}

	/**
	 * Applies the {@link #toMap(String)} method for the root path "/".
	 * 
	 * @return As of {@link #toMap(String)} applied to the root path.
	 * 
	 * @throws IllegalStateException the illegal state exception
	 */
	default Map<String, ?> toMap() {
		return toMap( getRootPath() );
	}

	/**
	 * Creates a nested {@link Map} structure from the {@link PathMap}'s entries
	 * below the provided path. Each element of the paths (below the provided
	 * path), except of the last (pointing to the path's value) is represented
	 * by a {@link Map} contained in the preceeding's path element.
	 * 
	 * @param aFromPath The path from which to start retrieving the sub-paths
	 *        which will be the root directories in the returned {@link Map}
	 * 
	 * @return The nested Map representing this {@link PathMap} starting from
	 *         the given path.
	 * 
	 * @throws IllegalStateException in case the {@link PathMap} contains a path
	 *         which points to a value and which is also part of other paths.
	 *         Such a state cannot be represented by a nested {@link Map}
	 *         structure as the element would represent a leaf and a node at the
	 *         same time.
	 */
	@SuppressWarnings("unchecked")
	default Map<String, ?> toMap( String aFromPath ) {
		aFromPath = StructureUtility.toGenericPath( this, aFromPath );
		final Map<String, Object> theMap = new HashMap<>();
		final Set<String> theKeySet = keySet();
		final List<String> theKeys = new ArrayList<>( theKeySet );
		Collections.sort( theKeys );
		String ePathElement;
		int ePathDepth;
		String eTail;

		for ( String ePath : theKeys ) {
			if ( ePath.startsWith( aFromPath ) && ePath.length() > aFromPath.length() ) {
				eTail = ePath.substring( aFromPath.length() + 1 );
				final String[] ePathArray = eTail.split( getRootPath() );
				if ( ePathArray.length > 0 ) {
					Map<String, Object> eMap = theMap;
					ePathDepth = 0;
					final Iterator<String> e = Arrays.asList( ePathArray ).iterator();
					while ( e.hasNext() ) {
						ePathElement = e.next();
						ePathDepth++;
						if ( e.hasNext() ) {
							try {
								final Object toObj = eMap.get( ePathElement );
								if ( toObj != null ) {
									Map<String, Object> toMap = null;
									// We have a map |-->
									if ( toObj instanceof Map ) {
										eMap = (Map<String, Object>) toObj;
									}
									// We have a map <--|
									// We have an array |-->
									else if ( toObj.getClass().isArray() ) {
										final Object[] toArray = (Object[]) toObj;
										for ( Object eElement : toArray ) {
											if ( eElement instanceof Map ) {
												toMap = (Map<String, Object>) eElement;
												break;
											}
										}
										if ( toMap == null ) {
											final List<Object> toList = Arrays.asList( toArray );
											toMap = new HashMap<>();
											toList.add( toMap );
											eMap.put( ePathElement, toList.toArray() );
											eMap = toMap;
										}
										else {
											eMap = toMap;
										}
									}
									// We have an array <--|
									// We have a value |-->
									else {
										final List<Object> toList = new ArrayList<>();
										toList.add( toObj );
										toMap = new HashMap<>();
										toList.add( toMap );
										eMap.put( ePathElement, toList.toArray() );
										eMap = toMap;
									}
									// We have a value <--|
								}
								else {
									final Map<String, Object> toMap = new HashMap<>();
									eMap.put( ePathElement, toMap );
									eMap = toMap;
								}

							}
							catch ( ClassCastException ex ) {
								throw new IllegalStateException( "There is already a value for path element <" + ePathElement + "> (at a path depth of <" + ePathDepth + ">) in the path <" + ePath + ">, unable to create an according nested Map in that position for succeeding path elements!" );
							}

						}
						// Terminating value |-->
						else {
							final Object toObj = eMap.get( ePathElement );
							final T fromValue = get( ePath );
							if ( toObj != null ) {
								final Map<String, Object> toMap = null;
								// We have an array |-->
								if ( toObj.getClass().isArray() ) {
									final Object[] toArray = (Object[]) toObj;
									final List<Object> toList = Arrays.asList( toArray );
									toList.add( fromValue );
									eMap.put( ePathElement, toList.toArray() );
									eMap = toMap;
								}
								// We have an array <--|
								// We have a value or a map |-->
								else {
									final List<Object> toList = new ArrayList<>();
									toList.add( toObj );
									toList.add( fromValue );
									eMap.put( ePathElement, toList.toArray() );
								}
								// We have a value or a map <--|
							}
							else {
								eMap.put( ePathElement, fromValue );
							}
						}
						// Terminating value <--|
					}
				}
			}
		}
		return theMap;
	}

	/**
	 * Normalizes a path for it to start with the delimiter (as of
	 * {@link #getDelimiter()}) and for it not to end with a delimiter. This
	 * implementation will return in empty {@link String} in case the path is an
	 * empty {@link String} or the path just consists of a delimiter: Any
	 * missing prefixed (starting) delimiter is added and any suffixed
	 * delimiters are removed, ending up with an empty {@link String}
	 * 
	 * @param aPath The path to be normalized.
	 * 
	 * @return The normalized path; afterwards (in case the path was not an
	 *         empty {@link String} or just the delimiter itself) it starts with
	 *         a delimiter and does not end with a delimiter.
	 */
	default String toNormalizedPath( String aPath ) {
		if ( aPath != null ) {
			final String theDouble = "" + getDelimiter() + getDelimiter();
			while ( aPath.contains( theDouble ) ) {
				aPath = aPath.replaceAll( Pattern.quote( theDouble ), "" + getDelimiter() );
			}

			if ( !aPath.startsWith( getDelimiter() + "" ) ) {
				aPath = getDelimiter() + aPath;
			}
			while ( aPath.endsWith( getDelimiter() + "" ) ) {
				aPath = aPath.substring( 0, aPath.length() - 1 );
			}
		}
		if ( aPath == null || aPath.isEmpty() ) {
			aPath = getRootPath();
		}
		return aPath;
	}

	/**
	 * Returns the path representing the parent path for the given path. Given
	 * we have a path "/animals/dogs/otto", then its parent path is
	 * "/animals/dogs".
	 * 
	 * @param aPath The path for which to get the parent's path.
	 * 
	 * @return The parent path.
	 * 
	 * @throws IllegalArgumentException thrown in case there is no such parent
	 *         path for the given path; test beforehand using
	 *         {@link #hasParentPath(String)}.
	 */
	default String toParentPath( String aPath ) {
		aPath = toNormalizedPath( aPath );
		if ( aPath != null && aPath.equals( getRootPath() ) ) {
			throw new IllegalArgumentException( "The path \"" + aPath + "\" does not have a parent path!" );
		}
		final int index = aPath.lastIndexOf( getDelimiter() );
		aPath = aPath.substring( 0, index );
		if ( aPath.isEmpty() ) {
			aPath = getRootPath();
		}
		return aPath;
	}

	/**
	 * Creates a normalized path from the provided path elements by first
	 * converting them to a {@link String} (if not being a {@link String}
	 * already) and then concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return The concatenated and normalized path.
	 */
	default String toPath( Collection<?> aPathElements ) {
		if ( aPathElements != null ) {
			return toPath( aPathElements.toArray( new Object[aPathElements.size()] ) );
		}
		return null;
	}

	/**
	 * Creates a normalized path from the provided object.
	 * 
	 * @param aPath The path object to be converted to an according path.
	 * 
	 * @return The normalized path.
	 */
	default String toPath( Object aPath ) {
		return toNormalizedPath( ( aPath instanceof String ? (String) aPath : ( aPath != null ? aPath.toString() : (String) null ) ) );
	}

	/**
	 * Creates a normalized path from the provided path elements by first
	 * converting them to a {@link String} (if not being a {@link String}
	 * already) and then concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return The concatenated and normalized path.
	 */
	default String toPath( Object... aPathElements ) {
		final StringBuilder theBuffer = new StringBuilder();
		String eStrElement;
		for ( Object eElement : aPathElements ) {
			if ( eElement != null ) {
				if ( eElement instanceof Collection ) {
					eStrElement = toPath( (Collection<?>) eElement );
				}
				else if ( eElement instanceof Object[] ) {
					eStrElement = toPath( (Object[]) eElement );
				}
				else {
					eStrElement = ( eElement instanceof String ? (String) eElement : ( eElement != null ? eElement.toString() : null ) );
				}
				while ( eStrElement != null && eStrElement.length() > 0 && eStrElement.charAt( 0 ) == getDelimiter() ) {
					eStrElement = eStrElement.substring( 1 );
				}
				while ( eStrElement != null && eStrElement.length() > 0 && eStrElement.charAt( eStrElement.length() - 1 ) == getDelimiter() ) {
					eStrElement = eStrElement.substring( 0, eStrElement.length() - 1 );
				}
				theBuffer.append( eStrElement );
				theBuffer.append( getDelimiter() );
			}
		}
		return toNormalizedPath( theBuffer.toString() );
	}

	/**
	 * Creates a normalized path from the provided path elements by
	 * concatenating the elements with the path delimiter
	 * {@link #getDelimiter()} in between and taking care to avoid duplicate
	 * path delimiters.
	 * 
	 * @param aPathElements The elements of the path to be concatenated with the
	 *        path delimiter.
	 * 
	 * @return The concatenated and normalized path.
	 */
	default String toPath( String... aPathElements ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( String eElement : aPathElements ) {
			while ( eElement != null && eElement.length() > 0 && eElement.charAt( 0 ) == getDelimiter() ) {
				eElement = eElement.substring( 1 );
			}
			while ( eElement != null && eElement.length() > 0 && eElement.charAt( eElement.length() - 1 ) == getDelimiter() ) {
				eElement = eElement.substring( 0, eElement.length() - 1 );
			}
			theBuffer.append( eElement );
			theBuffer.append( getDelimiter() );
		}
		return toNormalizedPath( theBuffer.toString() );
	}

	/**
	 * Retrieves the path's elements separated from each other by the path
	 * delimiter {@link #getDelimiter()}.
	 * 
	 * @param aPath The path from which to retrieve the elements.
	 * 
	 * @return The elements excluding the delimiter.
	 */
	default String[] toPathElements( String aPath ) {
		String[] theElements = null;
		if ( aPath != null && aPath.length() != 0 ) {
			while ( aPath.startsWith( getRootPath() ) ) {
				aPath = aPath.substring( 1 );
			}
			while ( aPath.endsWith( getRootPath() ) ) {
				aPath = aPath.substring( 0, aPath.length() - 1 );
			}
			if ( aPath.length() != 0 ) {
				theElements = aPath.split( Pattern.quote( getDelimiter() + "" ) );

			}
		}
		return theElements;
	}

	/**
	 * Retrieves a mutable {@link List} with the path's elements separated from
	 * each other by the path delimiter {@link #getDelimiter()}.
	 * 
	 * @param aPath The path from which to retrieve the mutable {@link List}
	 *        containing the according path elements.
	 * 
	 * @return The mutable {@link List} containing the path elements excluding
	 *         the delimiter.
	 */
	default List<String> toPathList( String aPath ) {
		return new ArrayList<>( Arrays.asList( toPathElements( aPath ) ) );
	}

	/**
	 * Retrieves a {@link Stack} with the path's elements separated from each
	 * other by the path delimiter {@link #getDelimiter()} put on the stack: The
	 * first path element is put first, the last path element is put last, so
	 * the last path element ends up being on top of the {@link Stack}.
	 * 
	 * @param aPath The path from which to retrieve the {@link Stack} containing
	 *        the according path elements.
	 * 
	 * @return The {@link Stack} containing the path elements excluding the
	 *         delimiter, with the last path element being on top of the
	 *         {@link Stack}.
	 */
	default Stack<String> toPathStack( String aPath ) {
		final Stack<String> theStack = new Stack<>();
		final String[] thePathElements = toPathElements( aPath );
		if ( thePathElements != null ) {
			theStack.addAll( Arrays.asList( thePathElements ) );
		}
		return theStack;
	}

	/**
	 * Converts the properties for saving; that when saved, them properties will
	 * not start with a delimiter, making them (when there are no sub-paths for
	 * the properties) look just like normal well known properties, enabling
	 * interchangeability with other systems reading the properties.
	 * 
	 * @param aPath The path to be normalized.
	 * 
	 * @return The normalized path; afterwards it does not start with a
	 *         delimiter any more.
	 */
	default String toPropertyPath( String aPath ) {
		if ( aPath != null ) {
			while ( aPath.startsWith( getRootPath() ) ) {
				aPath = aPath.substring( 1 );
			}
			while ( aPath.endsWith( getRootPath() ) ) {
				aPath = aPath.substring( 0, aPath.length() - 1 );
			}
			return aPath;
		}
		return null;
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	default <TYPE> TYPE toType( Class<TYPE> aType ) {
		return toType( getRootPath(), aType );
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aFromPath The path representing the root from which to take the
	 *        data for the instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	default <TYPE> TYPE toType( Object aFromPath, Class<TYPE> aType ) {
		return toType( toNormalizedPath( InterOperableMap.asKey( aFromPath ) ), aType );
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aFromPathElements The path elements representing the root from
	 *        which to take the data for the instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	default <TYPE> TYPE toType( Object[] aFromPathElements, Class<TYPE> aType ) {
		return toType( toPath( aFromPathElements ), aType );
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * @param aFromPathElements The path elements representing the root from
	 *        which to take the data for the instance.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	default <TYPE> TYPE toType( Class<TYPE> aType, Object... aFromPathElements ) {
		return toType( toPath( aFromPathElements ), aType );
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aFromPathElements The path elements representing the root from
	 *        which to take the data for the instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	default <TYPE> TYPE toType( String[] aFromPathElements, Class<TYPE> aType ) {
		return toType( toPath( aFromPathElements ), aType );
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * @param aFromPathElements The path elements representing the root from
	 *        which to take the data for the instance.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	default <TYPE> TYPE toType( Class<TYPE> aType, String... aFromPathElements ) {
		return toType( toPath( aFromPathElements ), aType );
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aFromPathElements The path elements representing the root from
	 *        which to take the data for the instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	default <TYPE> TYPE toType( Collection<?> aFromPathElements, Class<TYPE> aType ) {
		return toType( toPath( aFromPathElements ), aType );
	}

	/**
	 * Creates an instance of the given type filled with this {@link PathMap}'s
	 * data.
	 *
	 * @param <TYPE> the generic type if which to create an instance.
	 * @param aFromPath The path representing the root from which to take the
	 *        data for the instance.
	 * @param aType the type if the instance to be filled with the herein
	 *        contained data.
	 * 
	 * @return the instance initialized with the herein contained data.
	 */
	@SuppressWarnings("unchecked")
	default <TYPE> TYPE toType( String aFromPath, Class<TYPE> aType ) {
		try {
			Object theDataStructure = get( aFromPath );
			if ( theDataStructure != null ) {
				try {
					if ( theDataStructure instanceof String ) {
						final Class<?> theType = Class.forName( (String) theDataStructure );
						if ( theType != null && aType.isAssignableFrom( theType ) ) {
							aType = (Class<TYPE>) theType;
						}
					}
					else if ( theDataStructure instanceof Class ) {
						final Class<?> theType = (Class<TYPE>) theDataStructure;
						if ( aType.isAssignableFrom( theType ) ) {
							aType = (Class<TYPE>) theType;
						}
					}
				}
				catch ( Exception ignore ) {}
			}
			// theDataStructure = toDataStructure( aFromPath ); // TODO 2024-05-29 - Verify not unmarshaling Map to Array types is the way to go here.
			theDataStructure = toMap( aFromPath );
			if ( SimpleType.isSimpleType( aType ) && getRootPath().equals( aFromPath ) && theDataStructure instanceof Map ) {
				final Map<?, ?> theMap = (Map<?, ?>) theDataStructure;
				if ( theMap.size() == 1 && theMap.containsKey( "" ) ) {
					return TypeUtility.toType( theMap.get( "" ), aType );
				}
			}
			return TypeUtility.toType( theDataStructure, aType );

		}
		catch ( InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException | InvocationTargetException | ClassNotFoundException e ) {
			throw new IllegalArgumentException( "Cannot create an instance of type <" + aType.getName() + "> from path <" + aFromPath + ">!", e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Provides the default backing map for {@link PathMap} implementations
	 * (defaults to {@link LinkedHashMap}).
	 * 
	 * @param <T> The type of the value elements.
	 * 
	 * @return The according backing {@link Map} instance.
	 */
	static <T> Map<String, T> toDefaultBackingMap() {
		return new LinkedHashMap<>();
	}
}
