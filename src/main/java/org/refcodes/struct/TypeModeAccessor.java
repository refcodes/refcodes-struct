// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Provides an accessor for a type mode property.
 */
public interface TypeModeAccessor {

	/**
	 * Retrieves the type mode from the type mode property.
	 * 
	 * @return The type mode stored by the type mode property.
	 */
	SimpleType getTypeMode();

	/**
	 * Provides a mutator for a type mode property.
	 */
	public interface TypeModeMutator {

		/**
		 * Sets the type mode for the type mode property.
		 * 
		 * @param aTypeMode The type mode to be stored by the type mode
		 *        property.
		 */
		void setTypeMode( SimpleType aTypeMode );
	}

	/**
	 * Provides a builder method for a type mode property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface TypeModeBuilder<B extends TypeModeBuilder<B>> {

		/**
		 * Sets the type mode for the type mode property.
		 * 
		 * @param aTypeMode The type mode to be stored by the type mode
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withTypeMode( SimpleType aTypeMode );
	}

	/**
	 * Provides a type mode property.
	 */
	public interface TypeModeProperty extends TypeModeAccessor, TypeModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SimpleType}
		 * (setter) as of {@link #setTypeMode(SimpleType)} and returns the very
		 * same value (getter).
		 * 
		 * @param aTypeMode The {@link SimpleType} to set (via
		 *        {@link #setTypeMode(SimpleType)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SimpleType letTypeMode( SimpleType aTypeMode ) {
			setTypeMode( aTypeMode );
			return aTypeMode;
		}
	}
}
