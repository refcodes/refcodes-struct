// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.refcodes.data.Delimiter;

/**
 * The {@link PathMapImpl} class implements the {@link PathMap} interface and
 * provides a hook {@link #createBackingMap()} method which may be overridden
 * for providing another {@link Map} type as backing map than the default one.
 *
 * @param <T> The type of the value elements.
 */
public class PathMapImpl<T> implements PathMap<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final PathMapBuilder<T> _pathMap;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create a {@link PathMap} instance from the provided {@link PathMap}
	 * instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the {@link PathMap}'s path
	 * delimiter for the path declarations the the type of the {@link PathMap}.
	 *
	 * @param aPathMap The {@link PathMap} from which the elements are to be
	 *        added.
	 */
	protected PathMapImpl( PathMap<T> aPathMap ) {
		_pathMap = new PathMapBuilderImpl<>( aPathMap, aPathMap.getDelimiter(), aPathMap.getType() ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Create a {@link PathMap} instance using the provided path delimiter for
	 * the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( char aDelimiter, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aDelimiter, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Create a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 * 
	 * @param aObj The object from which the elements are to be added.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( Object aObj, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aObj, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Create a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 * 
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( String aToPath, Object aObj, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aToPath, aObj, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Creates a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 * 
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( Object aObj, String aFromPath, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aObj, aFromPath, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Creates a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH} for the path declarations.
	 * 
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( String aToPath, Object aObj, String aFromPath, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aToPath, aObj, aFromPath, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Creates a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 * 
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( Object aObj, char aDelimiter, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aObj, aDelimiter, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Creates a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( String aToPath, Object aObj, char aDelimiter, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aToPath, aObj, aDelimiter, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Creates a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 * 
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( Object aObj, String aFromPath, char aDelimiter, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aObj, aFromPath, aDelimiter, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	/**
	 * Create a {@link PathMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aType The type of the values being referenced by the paths.
	 */
	public PathMapImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter, Class<T> aType ) {
		_pathMap = new PathMapBuilderImpl<>( aToPath, aObj, aFromPath, aDelimiter, aType ) {
			private static final long serialVersionUID = 1L;

			protected Map<String, T> createBackingMap() {
				Map<String, T> theMap = PathMapImpl.this.createBackingMap();
				if ( theMap == null ) {
					theMap = super.createBackingMap();
				}
				return theMap;
			}
		};
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return _pathMap.containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsValue( Object aValue ) {
		return _pathMap.containsValue( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T get( Object aKey ) {
		return _pathMap.get( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _pathMap.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<T> values() {
		return _pathMap.values();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _pathMap.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _pathMap.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _pathMap.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getAnnotator() {
		return _pathMap.getAnnotator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PathMap<T> retrieveFrom( String aFromPath ) {
		return _pathMap.retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PathMap<T> retrieveTo( String aToPath ) {
		return _pathMap.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T> getType() {
		return _pathMap.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return _pathMap.toDataStructure();
	}

	// /////////////////////////////////////////////////////////////////////////
	// UTILITY:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook for creating an individual backing {@link Map} to be used by the
	 * {@link PathMapImpl} type (defaults to {@link LinkedHashMap}).
	 * 
	 * @return The backing {@link Map} to be used.
	 */
	protected Map<String, T> createBackingMap() {
		return PathMap.toDefaultBackingMap();
	}
}
