// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Iterator;

import org.refcodes.mixin.Clearable;

/**
 * Basic functionality being provided by any {@link Container} (collection)
 * style class.
 *
 * @param <E> The type of the elements being stored in the
 *        {@link MutableContainer}.
 */
public interface Container<E> extends Elements<E>, Containable {

	/**
	 * Extends the {@link Container} interface with {@link Clearable}
	 * functionality (as of {@link #clear()}). The case of having a plain
	 * {@link Container} without dedicated {@link MutableElements#add(Object)}
	 * or {@link MutableElements#remove(Object)} methods but with a dedicated
	 * {@link #clear()} method seems to be quite common, therefore this
	 * interface has been provided.
	 * 
	 * @param <E> The type of the elements being stored in the
	 *        {@link ClearableContainer}.
	 */
	public interface ClearableContainer<E> extends Container<E>, ClearableElements<E> {}

	/**
	 * Extends the {@link Container} with mutable (writable) functionality,
	 * ehttps://www.metacodes.proly by enabling the {@link Iterator#remove()}
	 * method in the {@link Iterator} provided via {@link #iterator()}.
	 * 
	 * @param <E> The type of the elements being stored in the
	 *        {@link MutableContainer}.
	 */
	public interface MutableContainer<E> extends ClearableContainer<E>, MutableElements<E> {}
}
