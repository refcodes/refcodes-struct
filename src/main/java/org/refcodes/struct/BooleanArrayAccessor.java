// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Provides an accessor for a boolean array property.
 */
public interface BooleanArrayAccessor {

	/**
	 * Retrieves the boolean array from the boolean array property.
	 * 
	 * @return The boolean array stored by the boolean array property.
	 */
	boolean[] getBooleans();

	/**
	 * Provides a mutator for a boolean array property.
	 */
	public interface BooleanArrayMutator {

		/**
		 * Sets the boolean array for the boolean array property.
		 * 
		 * @param aBooleans The boolean array to be stored by the boolean array
		 *        property.
		 */
		void setBooleans( boolean[] aBooleans );
	}

	/**
	 * Provides a builder method for a boolean array property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BooleanArrayBuilder<B extends BooleanArrayBuilder<B>> {

		/**
		 * Sets the boolean array for the boolean array property.
		 * 
		 * @param aBooleans The boolean array to be stored by the boolean array
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBooleans( boolean[] aBooleans );
	}

	/**
	 * Provides a boolean array property.
	 */
	public interface BooleanArrayProperty extends BooleanArrayAccessor, BooleanArrayMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given boolean array (setter)
		 * as of {@link #setBooleans(boolean[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aBooleans The boolean array to set (via
		 *        {@link #setBooleans(boolean[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default boolean[] letBooleans( boolean[] aBooleans ) {
			setBooleans( aBooleans );
			return aBooleans;
		}
	}
}
