// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * A {@link Referencee} encapsulates a reference to an instance which might be
 * null at the time of the referencee's creation. This way a reference to a non
 * yet existing object can be provided.
 * 
 * @param <T> The type of the referencee to be encapsulated by this
 *        {@link Referencee}.
 */
public class Referencee<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private T _reference;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the according reference.
	 * 
	 * @return The according reference.
	 */
	public T getReference() {
		return _reference;
	}

	/**
	 * Sets the according reference.
	 * 
	 * @param aReference The according reference.
	 */
	public void setReference( T aReference ) {
		_reference = aReference;
	}

	/**
	 * Sets the according reference and returns the reference being set. With
	 * this method, a {@link Referencee} can be introduced in places where the
	 * actual reference is being created and expected.
	 * 
	 * @param aReference The according reference to be set.
	 * 
	 * @return The reference being set.
	 */
	public T referTo( T aReference ) {
		_reference = aReference;
		return _reference;
	}
}
