// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Comparator;
import java.util.regex.Pattern;

/**
 * Comparator for path elements, it takes care to sort index values correctly,
 * e.g. "1000" must come after "9": The paths "/foo/bar/1000/wow" and
 * "/foo/bar/9/wow" should be sorted as follows:
 * 
 * <code>
 * "/foo/bar/9/wow"
 * "/foo/bar/1000/wow"
 * </code>
 */
public class PathComparator implements Comparator<String> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private char _delimiter;
	private PathSortStrategy _pathSortStrategy;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link PathComparator} with the given path delimiter.
	 * 
	 * @param aDelimiter The path delimiter to use.
	 */
	public PathComparator( char aDelimiter ) {
		this( aDelimiter, PathSortStrategy.DEFAULT );
	}

	/**
	 * Constructs the {@link PathComparator} with the given path delimiter.
	 * 
	 * @param aDelimiter The path delimiter to use.
	 * @param aPathSortStrategy The {@link PathSortStrategy} to use for
	 *        comparisons.
	 */
	public PathComparator( char aDelimiter, PathSortStrategy aPathSortStrategy ) {
		_delimiter = aDelimiter;
		_pathSortStrategy = aPathSortStrategy != null ? aPathSortStrategy : PathSortStrategy.DEFAULT;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public int compare( String o1, String o2 ) {
		if ( _pathSortStrategy.isIgnoreCase() ) {
			o1 = o1.toLowerCase();
			o2 = o2.toLowerCase();
		}
		if ( _pathSortStrategy.isAlphanumeric() ) {
			final String theDelimiter = Pattern.quote( "" + _delimiter );
			final String[] thePathA = o1.split( theDelimiter );
			final String[] thePathB = o2.split( theDelimiter );
			final int theMin = thePathA.length < thePathB.length ? thePathA.length : thePathB.length;
			final int theMax = thePathA.length > thePathB.length ? thePathA.length : thePathB.length;
			int eLength;
			final StringBuilder theBufA = new StringBuilder();
			final StringBuilder theBufB = new StringBuilder();
			for ( int i = 0; i < theMax; i++ ) {
				if ( i < thePathA.length ) {
					theBufA.append( _delimiter );
				}
				if ( i < thePathB.length ) {
					theBufB.append( _delimiter );
				}
				if ( i < theMin ) {
					try {
						Integer.valueOf( thePathA[i] );
						Integer.valueOf( thePathB[i] );
						eLength = thePathA[i].length() > thePathB[i].length() ? thePathA[i].length() : thePathB[i].length();
						while ( thePathA[i].length() < eLength ) {
							thePathA[i] = "0" + thePathA[i];
						}
						while ( thePathB[i].length() < eLength ) {
							thePathB[i] = "0" + thePathB[i];
						}

					}
					catch ( Exception ignore ) {}
				}
				if ( i < thePathA.length ) {
					theBufA.append( thePathA[i] );
				}
				if ( i < thePathB.length ) {
					theBufB.append( thePathB[i] );
				}
			}
			return theBufA.toString().compareTo( theBufB.toString() );
		}
		return o1.compareTo( o2 );
	}
}
