// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.refcodes.mixin.Clearable;

/**
 * This interface provides functionality for working with keys.
 *
 * @param <K> The type of the key.
 * @param <V> The type of the value which relates to a key.
 */
public interface Keys<K, V> {

	/**
	 * Tests whether there is an element with the given key.
	 * 
	 * @param aKey The key for which to test whether there is an element.
	 * 
	 * @return True in case there is an element for the given key.
	 */
	boolean containsKey( Object aKey );

	/**
	 * This method is defined for the sake of {@link Map} conformity. Tests
	 * whether the provided value has a key assigned.
	 * 
	 * @param aValue The value to test if a key references this value.
	 * 
	 * @return True in case the given value is referenced by a key.
	 */
	boolean containsValue( Object aValue );

	/**
	 * Retrieves the element assigned to the given key.
	 * 
	 * @param aKey The key for which to get the element.
	 * 
	 * @return The value for the key in question or null if there is none such
	 *         value.
	 */
	V get( Object aKey );

	/**
	 * Retrieves the element assigned to the given key or the provided default
	 * value if there is no such value for the given key.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case there is no such value for
	 *        the given key.
	 * 
	 * @return The value for the key in question or the default value if there
	 *         is none such value.
	 */
	default V getOr( Object aKey, V aDefaultValue ) {
		final V theValue = get( aKey );
		if ( theValue == null || ( theValue instanceof String && ( (String) theValue ).isEmpty() ) ) {
			return aDefaultValue;
		}
		return theValue;
	}

	/**
	 * Retrieves the element assigned to the given key. If the key or the krey's
	 * value does not exist, then the default value is returned.
	 * 
	 * @param aKey The key for which to get the element.
	 * @param aDefaultValue The default value in case the key's value is null.
	 * 
	 * @return The column in question or null if there is none such column.
	 */
	//	default V get( Object aKey, V aDefaultValue ) {
	//		V theValue = get( aKey );
	//		return theValue != null ? theValue : aDefaultValue;
	//	}

	/**
	 * Retrieves the element assigned to the given key. In case the given key
	 * does not exist, then an {@link KeyNotFoundException} is thrown.
	 * 
	 * @param aKey The key for which to get the element.
	 * 
	 * @return The value assigned to the given key.
	 *
	 * @throws KeyNotFoundException thrown in case this instance does not
	 *         contain the requested element (no such key found in the
	 *         collection).
	 */
	default V use( K aKey ) throws KeyNotFoundException {
		if ( !containsKey( aKey ) ) {
			throw new KeyNotFoundException( "There is no such element with key <" + aKey + "> found in this instance!", ( aKey != null ) ? aKey.toString() : null );
		}
		return get( aKey );
	}

	/**
	 * Retrieves a collection containing all the keys found in the elements of
	 * this collection.
	 * 
	 * @return A collection with key objects being the keys of all elements in
	 *         this collection.
	 */
	Set<K> keySet();

	/**
	 * Returns a {@link Collection} view of the values related to the contained
	 * keys.
	 * 
	 * @return A {@link Collection} view of the values related to the contained
	 *         keys.
	 */
	Collection<V> values();

	/**
	 * Adds mutable functionality to the {@link Keys} interface.
	 * 
	 * @param <K> The type of the key.
	 * @param <V> The type of the value which relates to a key.
	 */
	public interface MutableKeys<K, V> extends Keys<K, V>, Clearable {

		/**
		 * Removes an element identified by the given key.
		 * 
		 * @param aKey The key which's element is to be removed.
		 * 
		 * @return The value being removed.
		 */
		V delete( String aKey );
	}

	/**
	 * Mixin for mutable functionality accompanying the {@link Keys} interface.
	 * 
	 * @param <K> The type of the key.
	 * @param <V> The type of the value which relates to a key.
	 */
	public interface MutableValues<K, V> extends MutableKeys<K, V> {

		/**
		 * Puts an element identified by the given key.
		 *
		 * @param aKey The key which's element is to be put.
		 * @param aValue the value
		 * 
		 * @return The value being overwritten.
		 */
		V put( K aKey, V aValue );
	}
}
