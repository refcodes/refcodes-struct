// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Provides an accessor for a float array property.
 */
public interface FloatArrayAccessor {

	/**
	 * Retrieves the float array from the float array property.
	 * 
	 * @return The float array stored by the float array property.
	 */
	float[] getFloats();

	/**
	 * Provides a mutator for a float array property.
	 */
	public interface FloatArrayMutator {

		/**
		 * Sets the float array for the float array property.
		 * 
		 * @param aFloats The float array to be stored by the float array
		 *        property.
		 */
		void setFloats( float[] aFloats );
	}

	/**
	 * Provides a builder method for a float array property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FloatArrayBuilder<B extends FloatArrayBuilder<B>> {

		/**
		 * Sets the float array for the float array property.
		 * 
		 * @param aFloats The float array to be stored by the float array
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFloats( float[] aFloats );
	}

	/**
	 * Provides a float array property.
	 */
	public interface FloatArrayProperty extends FloatArrayAccessor, FloatArrayMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given float array (setter) as
		 * of {@link #setFloats(float[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aFloats The float array to set (via
		 *        {@link #setFloats(float[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default float[] letFloats( float[] aFloats ) {
			setFloats( aFloats );
			return aFloats;
		}
	}
}
