// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.Set;

import org.refcodes.data.Delimiter;

/**
 * The Class ClassStructMapImpl.
 */
public class ClassStructMapImpl implements ClassStructMap {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected OverridableClassStructMapBuilderImpl _classStructMap;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link ClassStructMap} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public ClassStructMapImpl() {
		_classStructMap = new OverridableClassStructMapBuilderImpl();
	}

	/**
	 * Create a {@link ClassStructMap} instance using the provided path
	 * delimiter for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapImpl( char aDelimiter ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aDelimiter );
	}

	/**
	 * Create a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public ClassStructMapImpl( Object aObj ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj );
	}

	/**
	 * Create a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public ClassStructMapImpl( Object aObj, String aFromPath ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj, aFromPath );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj, String aFromPath ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj, aFromPath );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapImpl( Object aObj, char aDelimiter ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj, aDelimiter );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj, char aDelimiter ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj, aDelimiter );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapImpl( Object aObj, String aFromPath, char aDelimiter ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj, aFromPath, aDelimiter );
	}

	/**
	 * Create a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj, aFromPath, aDelimiter );
	}

	// -------------------------------------------------------------------------

	/**
	 * Create an empty {@link ClassStructMap} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aTypeMode );
	}

	/**
	 * Create a {@link ClassStructMap} instance using the provided path
	 * delimiter for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( char aDelimiter, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aDelimiter, aTypeMode );
	}

	/**
	 * Create a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( Object aObj, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj, aTypeMode );
	}

	/**
	 * Create a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj, aTypeMode );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( Object aObj, String aFromPath, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj, aFromPath, aTypeMode );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj, String aFromPath, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj, aFromPath, aTypeMode );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( Object aObj, char aDelimiter, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj, aDelimiter, aTypeMode );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj, char aDelimiter, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj, aDelimiter, aTypeMode );
	}

	/**
	 * Creates a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( Object aObj, String aFromPath, char aDelimiter, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aObj, aFromPath, aDelimiter, aTypeMode );
	}

	/**
	 * Create a {@link ClassStructMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	public ClassStructMapImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter, SimpleType aTypeMode ) {
		_classStructMap = new OverridableClassStructMapBuilderImpl( aToPath, aObj, aFromPath, aDelimiter, aTypeMode );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleType getTypeMode() {
		return _classStructMap.getTypeMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArraySelector() {
		return ARRAY_SELECTOR;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return _classStructMap.containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> get( Object aKey ) {
		return _classStructMap.get( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _classStructMap.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Class<?>> values() {
		return _classStructMap.values();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _classStructMap.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _classStructMap.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClassStructMap retrieveFrom( String aFromPath ) {
		return _classStructMap.retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClassStructMap retrieveTo( String aToPath ) {
		return _classStructMap.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getAnnotator() {
		return _classStructMap.getAnnotator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _classStructMap.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<Class<?>> getType() {
		return _classStructMap.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return _classStructMap.toDataStructure( aFromPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class OverridableClassStructMapBuilderImpl extends ClassStructMapBuilderImpl {

		private static final long serialVersionUID = 1L;

		public OverridableClassStructMapBuilderImpl( char aDelimiter, SimpleType aTypeMode ) {
			super( aDelimiter, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj, char aDelimiter, SimpleType aTypeMode ) {
			super( aObj, aDelimiter, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj, String aFromPath, char aDelimiter, SimpleType aTypeMode ) {
			super( aObj, aFromPath, aDelimiter, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj, String aFromPath, SimpleType aTypeMode ) {
			super( aObj, aFromPath, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj, SimpleType aTypeMode ) {
			super( aObj, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj, char aDelimiter, SimpleType aTypeMode ) {
			super( aToPath, aObj, aDelimiter, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter, SimpleType aTypeMode ) {
			super( aToPath, aObj, aFromPath, aDelimiter, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath, SimpleType aTypeMode ) {
			super( aToPath, aObj, aFromPath, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj, SimpleType aTypeMode ) {
			super( aToPath, aObj, aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl( SimpleType aTypeMode ) {
			super( aTypeMode );
		}

		public OverridableClassStructMapBuilderImpl() {}

		public OverridableClassStructMapBuilderImpl( char aDelimiter ) {
			super( aDelimiter );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj, char aDelimiter ) {
			super( aObj, aDelimiter );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj, String aFromPath, char aDelimiter ) {
			super( aObj, aFromPath, aDelimiter );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj, String aFromPath ) {
			super( aObj, aFromPath );
		}

		public OverridableClassStructMapBuilderImpl( Object aObj ) {
			super( aObj );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj, char aDelimiter ) {
			super( aToPath, aObj, aDelimiter );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter ) {
			super( aToPath, aObj, aFromPath, aDelimiter );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath ) {
			super( aToPath, aObj, aFromPath );
		}

		public OverridableClassStructMapBuilderImpl( String aToPath, Object aObj ) {
			super( aToPath, aObj );
		}

		@Override
		public String getArraySelector() {
			return ClassStructMapImpl.this.getArraySelector();
		}
	}
}
