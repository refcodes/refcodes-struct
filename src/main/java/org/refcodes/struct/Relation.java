// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.mixin.KeyAccessor;
import org.refcodes.mixin.ValueAccessor;

/**
 * Provides an accessor for a key-to-value property useful in some occasions.
 *
 * @param <K> The key's type
 * @param <V> The value's type
 */
public interface Relation<K, V> extends KeyAccessor<K>, ValueAccessor<V> {

	/**
	 * Adds builder functionality to a {@link Relation}.
	 *
	 * @param <K> The key's type
	 * @param <V> The value's type
	 */
	public interface RelationBuilder<K, V> extends KeyProperty<K>, ValueProperty<V>, Relation<K, V>, KeyBuilder<K, RelationBuilder<K, V>>, ValueBuilder<V, RelationBuilder<K, V>> {

		/**
		 * With key.
		 *
		 * @param aKey the key
		 * 
		 * @return the relation builder
		 */
		@Override
		default RelationBuilder<K, V> withKey( K aKey ) {
			setKey( aKey );
			return this;
		}

		/**
		 * With value.
		 *
		 * @param aValue the value
		 * 
		 * @return the relation builder
		 */
		@Override
		default RelationBuilder<K, V> withValue( V aValue ) {
			setValue( aValue );
			return this;
		}
	}
}
