// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.Set;

import org.refcodes.data.Delimiter;

/**
 * An implementation of the {@link SimpleTypeMap}.
 */
public class SimpleTypeMapImpl implements SimpleTypeMap {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected SimpleTypeMapBuilderImpl _primitiveTypeMap;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link SimpleTypeMap} instance using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public SimpleTypeMapImpl() {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl();
	}

	/**
	 * Create a {@link SimpleTypeMap} instance using the provided path delimiter
	 * for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapImpl( char aDelimiter ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aDelimiter );
	}

	/**
	 * Create a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public SimpleTypeMapImpl( Object aObj ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aObj );
	}

	/**
	 * Create a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public SimpleTypeMapImpl( String aToPath, Object aObj ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aToPath, aObj );
	}

	/**
	 * Creates a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public SimpleTypeMapImpl( Object aObj, String aFromPath ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aObj, aFromPath );
	}

	/**
	 * Creates a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public SimpleTypeMapImpl( String aToPath, Object aObj, String aFromPath ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aToPath, aObj, aFromPath );
	}

	/**
	 * Creates a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapImpl( Object aObj, char aDelimiter ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aObj, aDelimiter );
	}

	/**
	 * Creates a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapImpl( String aToPath, Object aObj, char aDelimiter ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aToPath, aObj, aDelimiter );
	}

	/**
	 * Creates a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapImpl( Object aObj, String aFromPath, char aDelimiter ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aObj, aFromPath, aDelimiter );
	}

	/**
	 * Create a {@link SimpleTypeMap} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public SimpleTypeMapImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter ) {
		_primitiveTypeMap = new SimpleTypeMapBuilderImpl( aToPath, aObj, aFromPath, aDelimiter );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey( Object aKey ) {
		return _primitiveTypeMap.containsKey( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object get( Object aKey ) {
		return _primitiveTypeMap.get( InterOperableMap.asKey( aKey ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> keySet() {
		return _primitiveTypeMap.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Object> values() {
		return _primitiveTypeMap.values();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return _primitiveTypeMap.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return _primitiveTypeMap.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap retrieveFrom( String aFromPath ) {
		return _primitiveTypeMap.retrieveFrom( aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleTypeMap retrieveTo( String aToPath ) {
		return _primitiveTypeMap.retrieveTo( aToPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getAnnotator() {
		return _primitiveTypeMap.getAnnotator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _primitiveTypeMap.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<Object> getType() {
		return _primitiveTypeMap.getType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object toDataStructure( String aFromPath ) {
		return _primitiveTypeMap.toDataStructure( aFromPath );
	}
}
