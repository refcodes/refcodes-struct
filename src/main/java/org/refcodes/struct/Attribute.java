// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Interface describing a bean's attribute with a key represented by a
 * {@link String} and the value being of any type.
 */
public interface Attribute extends Relation<String, Object> {

	/**
	 * Adds builder functionality to an {@link Attribute}.
	 */
	public interface AttributeBuilder extends Attribute, RelationBuilder<String, Object> {

		/**
		 * With key.
		 *
		 * @param aKey the key
		 * 
		 * @return the attribute builder
		 */
		@Override
		default AttributeBuilder withKey( String aKey ) {
			setKey( aKey );
			return this;
		}

		/**
		 * With value.
		 *
		 * @param aValue the value
		 * 
		 * @return the attribute builder
		 */
		@Override
		default AttributeBuilder withValue( Object aValue ) {
			setValue( aValue );
			return this;
		}
	}
}
