// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.ClassStructMap.ClassStructMapBuilder;

/**
 * The Class ClassStructBuilderImpl.
 */
public class ClassStructMapBuilderImpl extends PathMapBuilderImpl<Class<?>> implements ClassStructMapBuilder {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_DEBUG_OUTPUT = false;

	private static final String[] BLACKLISTED_PROPERTIES = { "class" };

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private SimpleType _typeMode = SimpleType.KEEP_TYPES;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link ClassStructMapBuilder} instance using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public ClassStructMapBuilderImpl() {
		this( SimpleType.DEFAULT );
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance using the provided path
	 * delimiter for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapBuilderImpl( char aDelimiter ) {
		this( aDelimiter, SimpleType.DEFAULT );
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public ClassStructMapBuilderImpl( Object aObj ) {
		this( aObj, SimpleType.DEFAULT );
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public ClassStructMapBuilderImpl( String aToPath, Object aObj ) {
		this( aToPath, aObj, SimpleType.DEFAULT );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public ClassStructMapBuilderImpl( Object aObj, String aFromPath ) {
		this( aObj, aFromPath, SimpleType.DEFAULT );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public ClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath ) {
		this( aToPath, aObj, aFromPath, SimpleType.DEFAULT );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapBuilderImpl( Object aObj, char aDelimiter ) {
		this( aObj, aDelimiter, SimpleType.DEFAULT );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapBuilderImpl( String aToPath, Object aObj, char aDelimiter ) {
		this( aToPath, aObj, aDelimiter, SimpleType.DEFAULT );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapBuilderImpl( Object aObj, String aFromPath, char aDelimiter ) {
		this( aObj, aFromPath, aDelimiter, SimpleType.DEFAULT );
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public ClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter ) {
		this( aToPath, aObj, aFromPath, aDelimiter, SimpleType.DEFAULT );
	}

	// -------------------------------------------------------------------------

	/**
	 * Create an empty {@link ClassStructMapBuilder} instance using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ClassStructMapBuilderImpl( SimpleType aTypeMode ) {
		super( (Class) Class.class );
		_typeMode = aTypeMode;
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance using the provided path
	 * delimiter for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( char aDelimiter, SimpleType aTypeMode ) {
		super( aDelimiter, (Class) Class.class );
		_typeMode = aTypeMode;
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( Object aObj, SimpleType aTypeMode ) {
		super( (Class) Class.class );
		_typeMode = aTypeMode;
		fromValue( getRootPath(), aObj );
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( String aToPath, Object aObj, SimpleType aTypeMode ) {
		super( aToPath, (Class) Class.class );
		_typeMode = aTypeMode;
		fromValue( getRootPath(), aObj );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( Object aObj, String aFromPath, SimpleType aTypeMode ) {
		super( aFromPath, (Class) Class.class );
		_typeMode = aTypeMode;
		fromValue( getRootPath(), aObj );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)} using the default path
	 * delimiter "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath, SimpleType aTypeMode ) {
		super( (Class) Class.class );
		_typeMode = aTypeMode;
		insertBetween( aToPath, aObj, aFromPath );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( Object aObj, char aDelimiter, SimpleType aTypeMode ) {
		super( aDelimiter, (Class) Class.class );
		_typeMode = aTypeMode;
		fromValue( getRootPath(), aObj );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( String aToPath, Object aObj, char aDelimiter, SimpleType aTypeMode ) {
		super( aToPath, aDelimiter, (Class) Class.class );
		_typeMode = aTypeMode;
		fromValue( getRootPath(), aObj );
	}

	/**
	 * Creates a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( Object aObj, String aFromPath, char aDelimiter, SimpleType aTypeMode ) {
		super( aFromPath, aDelimiter, (Class) Class.class );
		_typeMode = aTypeMode;
		fromValue( getRootPath(), aObj );
	}

	/**
	 * Create a {@link ClassStructMapBuilder} instance containing the elements
	 * as of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 * @param aTypeMode The {@link SimpleType} to use when processing primitive
	 *        types, wrapper types or complex types.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ClassStructMapBuilderImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter, SimpleType aTypeMode ) {
		super( aDelimiter, (Class) Class.class );
		_typeMode = aTypeMode;
		insertBetween( aToPath, aObj, aFromPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SimpleType getTypeMode() {
		return _typeMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClassStructMapBuilder retrieveFrom( String aFromPath ) {
		return new ClassStructMapBuilderImpl( super.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClassStructMapBuilder retrieveTo( String aToPath ) {
		return new ClassStructMapBuilderImpl( super.retrieveTo( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClassStructMapBuilder withPut( String aKey, Class<?> aValue ) {
		put( aKey, aValue );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void fromValue( String aToPath, Object aObj ) {
		if ( aObj instanceof Class ) {
			fromClass( aToPath, (Class<?>) aObj, new HashSet<>() );
		}
		else {
			fromValue( aToPath, aObj, new HashSet<>() );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PathMap<Class<?>> fromObject( Object aFrom ) {
		return new ClassStructMapImpl( aFrom, getDelimiter() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Class<?> fromInstance( Object aValue ) {
		if ( aValue instanceof Class ) {
			final Class<?> theClass = (Class<?>) aValue;
			if ( String.class.isAssignableFrom( theClass ) ) {
				return (Class<?>) theClass;
			}
			else if ( theClass.isEnum() ) {
				return (Class<?>) theClass;
			}
			return _typeMode.toType( theClass );
		}
		return null;
	}

	private void fromClass( String aToPath, Class<?> aClass, Set<Object> aVisited ) {
		if ( !aVisited.contains( aClass ) ) {
			aVisited.add( aClass );
			// Do we have a primitive / wrapper type? |-->
			Class<?> theClass = fromInstance( aClass );
			if ( theClass != null ) {
				put( aToPath, theClass );
			}
			else if ( aClass.isEnum() ) {
				put( aToPath, aClass );
			}
			// Do we have a primitive / wrapper type? <--|
			else if ( aClass.isArray() ) {
				// String theToPath = _typeMode.hasComplexTypeSupport() && _typeMode.hasArraySupport() ? aToPath : toPath( aToPath, getArraySelector() ); // [1]
				// Do we have a primitive / wrapper array type? |-->
				theClass = fromInstance( aClass.getComponentType() );
				if ( theClass != null ) {
					put( toPath( aToPath, getArraySelector() ), theClass );
				}
				// Do we have a primitive / wrapper array type? <--|
				else {
					// [1] Should we use the array type ?  |-->
					// if ( _typeMode.hasComplexTypeSupport() && _typeMode.hasArraySupport() ) {
					// 	put( theToPath, aClass );
					// }
					// [1] Should we use the array type ?  <--|
					fromClass( toPath( aToPath, getArraySelector() ), aClass.getComponentType(), aVisited );
				}
			}
			else {
				// if ( _typeMode.hasComplexTypeSupport() ) {
				//	// Do we already have an array type from previous call [1]? |--> 
				//	if ( !containsKey( aToPath ) ) {
				//		put( aToPath, aClass );
				//	}
				//	// Do we already have an array type from previous call [1]? <--|
				// }
				final Map<String, Class<?>> theMap = new HashMap<>();
				String ePropertyName;
				Class<?> eType;

				// Methods |-->
				final Method[] theMethods = aClass.getMethods();
				if ( theMethods != null && theMethods.length > 0 ) {
					for ( Method eMethod : theMethods ) {
						if ( TypeUtility.isGetter( eMethod ) ) {
							try {
								ePropertyName = TypeUtility.toGetterPropertyName( eMethod );
								eType = eMethod.getReturnType();
								if ( !isBlackListed( ePropertyName ) && !theMap.containsKey( ePropertyName ) ) {
									// @formatter:off
									if ( !aVisited.contains( eType ) /* && !isBlacklisted( eInvoked.getClass() )*/ ) {
										theMap.put( ePropertyName, eType );
									}
									// @formatter:on
								}
							}
							catch ( IllegalArgumentException ignore ) {
								//	System.out.println( ignore.getMessage() );
								//	ignore.printStackTrace();
							}
						}
					}
				}
				// Methods <--|

				// Fields |-->
				final Field[] theFields = aClass.getDeclaredFields();
				if ( theFields != null && theFields.length > 0 ) {
					for ( Field eField : theFields ) {
						try {
							ePropertyName = TypeUtility.toPropertyName( eField );
							if ( !isBlackListed( ePropertyName ) && !theMap.containsKey( ePropertyName ) ) {
								// Ignore "transient" fields |-->
								if ( !Modifier.isTransient( eField.getModifiers() ) ) {
									eType = eField.getType();
									if ( eType != null && !aVisited.contains( eType ) ) { // && !isBlacklisted( eInvoked.getClass() )
										theMap.put( ePropertyName, eType );
									}
								}
								// Ignore "transient" fields <--|
							}
						}
						catch ( IllegalArgumentException ignore ) {
							//	System.out.println( ignore.getMessage() );
							//	ignore.printStackTrace();
						}
					}
				}
				// Fields <--|

				if ( !theMap.isEmpty() ) {
					if ( IS_DEBUG_OUTPUT ) {
						System.out.println( aToPath + ": fromObject(): " + aClass.getName() + " := " + aClass.toString() );
					}
					fromMap( aToPath, theMap, aVisited );
				}
			}
		}
		aVisited.remove( aClass );
	}

	private void fromMap( String aToPath, Map<String, Class<?>> aMap, Set<Object> aVisited ) {
		if ( !aVisited.contains( aMap ) ) {
			aVisited.add( aMap );
			Class<?> eValue;
			String eBasePath;
			for ( String eKey : aMap.keySet() ) {
				eBasePath = toPath( aToPath, eKey );
				eValue = aMap.get( eKey );
				if ( IS_DEBUG_OUTPUT ) {
					System.out.println( aToPath + ": fromMap(): " + aMap.getClass().getName() + " := " + aMap.toString() );
				}
				fromClass( eBasePath, eValue, aVisited );
			}
			aVisited.remove( aMap );
		}
	}

	private boolean isBlackListed( String aPropertyName ) {
		for ( String eBlackListed : BLACKLISTED_PROPERTIES ) {
			if ( eBlackListed.equals( aPropertyName ) ) {
				return true;
			}
		}
		return false;
	}
}
