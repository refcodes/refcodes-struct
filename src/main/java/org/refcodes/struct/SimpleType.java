// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import org.refcodes.data.BooleanLiterals;
import org.refcodes.data.Literal;
import org.refcodes.time.DateFormat;
import org.refcodes.time.DateFormats;

/**
 * The {@link SimpleType} enumeration provides functionality useful when working
 * wit Java's primitive types, their wrapper counterparts as well as with some
 * other crucial types such as {@link String}, {@link Date}, {@link Enum},
 * {@link Class} or {@link Locale} types. This enumeration is the single point
 * for "simple type" conversion for many other functionality so code is not
 * scattered around the various libraries disjunct from each other.
 */
public enum SimpleType {

	/**
	 * Convert primitive types (e.g. {@link Byte#TYPE}) to their wrapper types
	 * (e.g. {@link Byte}). Array types are converted to non array types with
	 * the according array notation (as of {@link #WRAPPER_TYPES}).
	 */
	DEFAULT(false, true, false),

	// -------------------------------------------------------------------------

	/**
	 * Keep primitive types (e.g. {@link Byte#TYPE}) as well as their wrapper
	 * types (e.g. {@link Byte}). Array types are converted to non array types
	 * with the according array notation.
	 */
	KEEP_TYPES(true, true, false),

	/**
	 * Convert primitive types (e.g. {@link Byte#TYPE}) to their wrapper types
	 * (e.g. {@link Byte}). Array types are converted to non array types with
	 * the according array notation.
	 */
	WRAPPER_TYPES(false, true, false),

	/**
	 * Convert wrapper types (e.g. {@link Byte}) to their primitive types (e.g.
	 * {@link Byte#TYPE}). Array types are converted to non array types with the
	 * according array notation.
	 */
	PRIMITIVE_TYPES(true, false, false),

	// -------------------------------------------------------------------------

	/**
	 * Keep primitive types (e.g. {@link Byte#TYPE}) as well as their wrapper
	 * types (e.g. {@link Byte}). According array types are preserved!
	 */
	KEEP_TYPES_WITH_ARRAY_TYPES(true, true, true),

	/**
	 * Convert primitive types (e.g. {@link Byte#TYPE}) to their wrapper types
	 * (e.g. {@link Byte}). According array types are preserved!
	 */
	WRAPPER_TYPES_WITH_ARRAY_TYPES(false, true, true),

	/**
	 * Convert wrapper types (e.g. {@link Byte}) to their primitive types (e.g.
	 * {@link Byte#TYPE}). According array types are preserved!
	 */
	PRIMITIVE_TYPES_WITH_ARRAY_TYPES(true, false, true);

	// -------------------------------------------------------------------------

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _hasPrimitiveTypeSupport;
	private boolean _hasWrapperTypeSupport;
	private boolean _hasArrayTypeSupport;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private SimpleType( boolean hasPrimitiveTypeSupport, boolean hasWrapperTypeSupport, boolean hasArrayTypeSupport ) {
		_hasPrimitiveTypeSupport = hasPrimitiveTypeSupport;
		_hasWrapperTypeSupport = hasWrapperTypeSupport;
		_hasArrayTypeSupport = hasArrayTypeSupport;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns true in case primitive (array) types are supported types.
	 * 
	 * @return True in case primitive (array) types are supported types.
	 */
	public boolean hasPrimitiveTypeSupport() {
		return _hasPrimitiveTypeSupport;
	}

	/**
	 * Returns true in case wrapper (array) types are supported types.
	 * 
	 * @return True in case wrapper (array) types are supported types.
	 */
	public boolean hasWrapperTypeSupport() {
		return _hasWrapperTypeSupport;
	}

	/**
	 * Returns true in case primitive type arrays or arrays of their wrappers
	 * are supported types.
	 * 
	 * @return True in case primitive type arrays or arrays of their wrappers
	 *         are supported types.
	 */
	public boolean hasArrayTypeSupport() {
		return _hasArrayTypeSupport;
	}

	/**
	 * Converts the passed type as of the {@link SimpleType}.
	 * 
	 * @param aClass The type to be converted.
	 * 
	 * @return The converted type.
	 */
	public Class<?> toType( Class<?> aClass ) {
		final boolean isPrimitiveType = isPrimitiveType( aClass, _hasArrayTypeSupport );
		final boolean isWrapperType = isWrapperType( aClass, _hasArrayTypeSupport );
		if ( ( _hasPrimitiveTypeSupport && _hasWrapperTypeSupport ) && ( isPrimitiveType || isWrapperType ) ) {
			return isPrimitiveType ? toPrimitiveType( aClass, _hasArrayTypeSupport ) : toWrapperType( aClass, _hasArrayTypeSupport );
		}
		else if ( isWrapperType && _hasPrimitiveTypeSupport && !_hasWrapperTypeSupport ) {
			return toPrimitiveType( aClass, _hasArrayTypeSupport );
		}
		else if ( isPrimitiveType && !_hasPrimitiveTypeSupport && _hasWrapperTypeSupport ) {
			return toWrapperType( aClass, _hasArrayTypeSupport );
		}

		else if ( isPrimitiveType && _hasPrimitiveTypeSupport ) {
			return toPrimitiveType( aClass, _hasArrayTypeSupport );
		}
		else if ( isWrapperType && _hasWrapperTypeSupport ) {
			return toWrapperType( aClass, _hasArrayTypeSupport );
		}
		return null;
	}

	/**
	 * Converts the given type to a primitive type if possible and returns the
	 * result or null if the given type is not a wrapper type. Convenience
	 * method for {@link #toPrimitiveType(Class, boolean)} without array
	 * support.
	 * 
	 * @param aClass The type to be converted.
	 * 
	 * @return The converted type.
	 */
	public static Class<?> toPrimitiveType( Class<?> aClass ) {
		return toPrimitiveType( aClass, false );
	}

	/**
	 * Converts the given type to a primitive type if possible and returns the
	 * result or null if the given type is not a wrapper type.
	 * 
	 * @param aClass The type to be converted.
	 * @param hasArraySupport True in case primitive type arrays or arrays of
	 *        their wrappers are supported types.
	 * 
	 * @return The converted type.
	 */
	public static Class<?> toPrimitiveType( Class<?> aClass, boolean hasArraySupport ) {
		if ( Boolean.class.isAssignableFrom( aClass ) || Boolean.TYPE.isAssignableFrom( aClass ) ) {
			return Boolean.TYPE;
		}
		if ( Byte.class.isAssignableFrom( aClass ) || Byte.TYPE.isAssignableFrom( aClass ) ) {
			return Byte.TYPE;
		}
		if ( Character.class.isAssignableFrom( aClass ) || Character.TYPE.isAssignableFrom( aClass ) ) {
			return Character.TYPE;
		}
		if ( Double.class.isAssignableFrom( aClass ) || Double.TYPE.isAssignableFrom( aClass ) ) {
			return Double.TYPE;
		}
		if ( Float.class.isAssignableFrom( aClass ) || Float.TYPE.isAssignableFrom( aClass ) ) {
			return Float.TYPE;
		}
		if ( Integer.class.isAssignableFrom( aClass ) || Integer.TYPE.isAssignableFrom( aClass ) ) {
			return Integer.TYPE;
		}
		if ( Long.class.isAssignableFrom( aClass ) || Long.TYPE.isAssignableFrom( aClass ) ) {
			return Long.TYPE;
		}
		if ( Short.class.isAssignableFrom( aClass ) || Short.TYPE.isAssignableFrom( aClass ) ) {
			return Short.TYPE;
		}
		if ( hasArraySupport && aClass.isArray() ) {
			final Class<?> theArrayClass = aClass.getComponentType();

			if ( Boolean.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Boolean.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Boolean.TYPE, 0 ).getClass();
			}

			if ( Byte.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Byte.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Byte.TYPE, 0 ).getClass();
			}

			if ( Character.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Character.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Character.TYPE, 0 ).getClass();
			}

			if ( Double.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Double.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Double.TYPE, 0 ).getClass();
			}

			if ( Float.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Float.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Float.TYPE, 0 ).getClass();
			}

			if ( Integer.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Integer.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Integer.TYPE, 0 ).getClass();
			}

			if ( Long.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Long.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Long.TYPE, 0 ).getClass();
			}

			if ( Short.TYPE.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Short.class.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Short.TYPE, 0 ).getClass();
			}
		}
		return null;
	}

	/**
	 * Converts the given type to a wrapper type if possible and returns the
	 * result or null if the given type is not a primitive type. Convenience
	 * method for {@link #toWrapperType(Class, boolean)} without array support.
	 * 
	 * @param aClass The type to be converted.
	 * 
	 * @return The converted type.
	 */
	public static Class<?> toWrapperType( Class<?> aClass ) {
		return toWrapperType( aClass, false );
	}

	/**
	 * Converts the given type to a wrapper type if possible and returns the
	 * result or null if the given type is not a primitive type.
	 * 
	 * @param aClass The type to be converted.
	 * @param hasArraySupport True in case primitive type arrays or arrays of
	 *        their wrappers are supported types.
	 * 
	 * @return The converted type.
	 */
	public static Class<?> toWrapperType( Class<?> aClass, boolean hasArraySupport ) {
		if ( Boolean.class.isAssignableFrom( aClass ) || Boolean.TYPE.isAssignableFrom( aClass ) ) {
			return Boolean.class;
		}
		if ( Byte.class.isAssignableFrom( aClass ) || Byte.TYPE.isAssignableFrom( aClass ) ) {
			return Byte.class;
		}
		if ( Character.class.isAssignableFrom( aClass ) || Character.TYPE.isAssignableFrom( aClass ) ) {
			return Character.class;
		}
		if ( Double.class.isAssignableFrom( aClass ) || Double.TYPE.isAssignableFrom( aClass ) ) {
			return Double.class;
		}
		if ( Float.class.isAssignableFrom( aClass ) || Float.TYPE.isAssignableFrom( aClass ) ) {
			return Float.class;
		}
		if ( Integer.class.isAssignableFrom( aClass ) || Integer.TYPE.isAssignableFrom( aClass ) ) {
			return Integer.class;
		}
		if ( Long.class.isAssignableFrom( aClass ) || Long.TYPE.isAssignableFrom( aClass ) ) {
			return Long.class;
		}
		if ( Short.class.isAssignableFrom( aClass ) || Short.TYPE.isAssignableFrom( aClass ) ) {
			return Short.class;
		}

		if ( hasArraySupport && aClass.isArray() ) {
			final Class<?> theArrayClass = aClass.getComponentType();

			if ( Boolean.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Boolean.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Boolean.class, 0 ).getClass();
			}

			if ( Byte.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Byte.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Byte.class, 0 ).getClass();
			}

			if ( Character.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Character.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Character.class, 0 ).getClass();
			}

			if ( Double.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Double.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Double.class, 0 ).getClass();
			}

			if ( Float.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Float.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Float.class, 0 ).getClass();
			}

			if ( Integer.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Integer.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Integer.class, 0 ).getClass();
			}

			if ( Long.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Long.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Long.class, 0 ).getClass();
			}

			if ( Short.class.isAssignableFrom( theArrayClass ) ) {
				return aClass;
			}
			if ( Short.TYPE.isAssignableFrom( theArrayClass ) ) {
				return Array.newInstance( Short.class, 0 ).getClass();
			}
		}

		return null;
	}

	/**
	 * Tests whether the given class represents a primitive (array) type.
	 * 
	 * @param aClass The type to be tested.
	 * @param hasArraySupport True in case primitive type arrays or arrays of
	 *        their wrappers are supported types.
	 * 
	 * @return True in case of being a primitive type, else false. A return
	 *         value of false does not mean that we have a wrapper type (use
	 *         {@link #isWrapperType(Class)}).
	 */
	public static boolean isPrimitiveType( Class<?> aClass, boolean hasArraySupport ) {
		return isPrimitiveType( aClass ) || ( hasArraySupport && isPrimitiveArrayType( aClass ) );
	}

	/**
	 * Tests whether the given class represents a primitive type.
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a primitive type, else false. A return
	 *         value of false does not mean that we have a wrapper type (use
	 *         {@link #isWrapperType(Class)}).
	 */
	public static boolean isPrimitiveType( Class<?> aClass ) {
		if ( Boolean.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Byte.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Character.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Double.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Float.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Integer.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Long.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Short.TYPE.isAssignableFrom( aClass ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Tests whether the given class represents a primitive array type.
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a primitive array type, else false. A
	 *         return value of false does not mean that we have a wrapper array
	 *         type (use {@link #isWrapperArrayType(Class)}).
	 */
	public static boolean isPrimitiveArrayType( Class<?> aClass ) {
		return PrimitiveArrayType.isPrimitiveArrayType( aClass );
	}

	/**
	 * Tests whether the given class represents a wrapper (array) type.
	 * 
	 * @param aClass The type to be tested.
	 * @param hasArraySupport True in case wrapper type arrays or arrays of
	 *        their wrappers are supported types.
	 * 
	 * @return True in case of being a wrapper type, else false. A return value
	 *         of false does not mean that we have a wrapper type (use
	 *         {@link #isWrapperType(Class)}).
	 */
	public static boolean isWrapperType( Class<?> aClass, boolean hasArraySupport ) {
		return isWrapperType( aClass ) || ( hasArraySupport && isWrapperArrayType( aClass ) );
	}

	/**
	 * Tests whether the given class represents a wrapper type of a primitive
	 * type.
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a wrapper type of a primitive type, else
	 *         false. A return value of false does not mean that we have a
	 *         primitive type (use {@link #isPrimitiveType(Class)}).
	 */
	public static boolean isWrapperType( Class<?> aClass ) {
		if ( Boolean.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Byte.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Character.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Double.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Float.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Integer.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Long.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Short.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Tests whether the given class represents a wrapper array type of a
	 * primitive array type.
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a wrapper array type of a primitive array
	 *         type, else false. A return value of false does not mean that we
	 *         have a primitive array type (use
	 *         {@link #isPrimitiveArrayType(Class)}).
	 */
	public static boolean isWrapperArrayType( Class<?> aClass ) {
		if ( Boolean[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Byte[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Character[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Double[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Float[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Integer[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Long[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Short[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Tests whether the given class represents a simple type. A simple type is
	 * reckoned to be one of the wrapper types {@link Boolean}, {@link Byte},
	 * {@link Short}, {@link Character}, {@link Integer}, {@link Long},
	 * {@link Float} or {@link Double} as well as their wrapper types as well as
	 * the (not that primitive) types {@link String}, {@link Enum} and
	 * {@link Class}
	 * 
	 * @param aClass The type to be tested.
	 * @param hasArraySupport True in case simple type arrays are supported
	 *        types.
	 * 
	 * @return True in case of being a simple type.
	 */
	public static boolean isSimpleType( Class<?> aClass, boolean hasArraySupport ) {
		return isSimpleType( aClass ) || ( hasArraySupport && isSimpleArrayType( aClass ) );
	}

	/**
	 * Tests whether the given class represents a simple type. A simple type is
	 * reckoned to be one of the wrapper types {@link Boolean}, {@link Byte},
	 * {@link Short}, {@link Character}, {@link Integer}, {@link Long},
	 * {@link Float} or {@link Double} as well as their wrapper types as well as
	 * the (not that primitive) types {@link String}, {@link Enum} and
	 * {@link Class}
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a simple type.
	 */
	public static boolean isSimpleType( Class<?> aClass ) {
		if ( String.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Number.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Boolean.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Character.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Enum.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Class.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Date.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( LocalDate.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( LocalDateTime.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Locale.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( UUID.class.isAssignableFrom( aClass ) ) {
			return true;
		}
		return isPrimitiveType( aClass ) || isWrapperType( aClass );
	}

	/**
	 * Tests whether the given class represents a simple type array. A simple
	 * type is reckoned to be one of the wrapper types {@link Boolean},
	 * {@link Byte}, {@link Short}, {@link Character}, {@link Integer},
	 * {@link Long}, {@link Float} or {@link Double} as well as their wrapper
	 * types as well as the (not that primitive) types {@link String},
	 * {@link Enum} and {@link Class}
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a simple type array.
	 */
	public static boolean isSimpleArrayType( Class<?> aClass ) {
		if ( String[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Number[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Boolean[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Character[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Enum[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Class[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Date[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( LocalDate[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( LocalDateTime[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( Locale[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		if ( UUID[].class.isAssignableFrom( aClass ) ) {
			return true;
		}
		return isPrimitiveArrayType( aClass ) || isWrapperArrayType( aClass );
	}

	/**
	 * Tests whether the given class represents a composite type. A composite
	 * type is not a simple type (nor a primitive type and neither a wrapper
	 * type).
	 * 
	 * @param aClass The type to be tested.
	 * @param hasArraySupport True in case composite type arrays are supported
	 *        types.
	 * 
	 * @return True in case of being a composite type.
	 */
	public static boolean isCompositeType( Class<?> aClass, boolean hasArraySupport ) {
		return isCompositeType( aClass ) || ( hasArraySupport && isCompositeArrayType( aClass ) );
	}

	/**
	 * Tests whether the given class represents a composite type. A composite
	 * type is not a simple type (nor a primitive type and neither a wrapper
	 * type).
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a composite type.
	 */
	public static boolean isCompositeType( Class<?> aClass ) {
		return !isSimpleArrayType( aClass );
	}

	/**
	 * Tests whether the given class represents a composite type array. A simple
	 * type is reckoned to be one of the wrapper types {@link Boolean},
	 * {@link Byte}, {@link Short}, {@link Character}, {@link Integer},
	 * {@link Long}, {@link Float} or {@link Double} as well as their wrapper
	 * types as well as the (not that primitive) types {@link String},
	 * {@link Enum} and {@link Class}
	 * 
	 * @param aClass The type to be tested.
	 * 
	 * @return True in case of being a composite type array.
	 */
	public static boolean isCompositeArrayType( Class<?> aClass ) {
		return !isSimpleArrayType( aClass );
	}

	/**
	 * Converts a simple type to a {@link String} which can be converted back
	 * with the companion method {@link #toSimpleType(String, Class)}.
	 * 
	 * @param <T> The simple type's type.
	 * 
	 * @param aValue The value of the simple type to be converted.
	 * 
	 * @return The according {@link String} or if the value was not a simple
	 *         type.
	 */
	public static <T> String fromSimpleType( T aValue ) {
		// String:
		if ( String.class.isAssignableFrom( aValue.getClass() ) ) {
			return (String) aValue;
		}
		// Number:
		else if ( Number.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromNumber( aValue );
		}
		// Boolean:
		else if ( Boolean.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromNumber( aValue );
		}
		// Character:
		else if ( Character.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromCharacter( (Character) aValue );
		}
		// Enumeration:
		else if ( Enum.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromEnum( (Enum<?>) aValue );
		}
		// Class:
		else if ( Class.class.isAssignableFrom( aValue.getClass() ) ) {
			return ( (Class<?>) aValue ).getName();
		}
		// Date:
		else if ( Date.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromDate( (Date) aValue );
		}
		// LocalDate:
		else if ( LocalDate.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromLocalDate( (LocalDate) aValue );
		}
		// LocalDateTime:
		else if ( LocalDateTime.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromLocalDateTime( (LocalDateTime) aValue );
		}
		// Locale:
		else if ( Locale.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromLocale( (Locale) aValue );
		}
		// UUID:
		else if ( UUID.class.isAssignableFrom( aValue.getClass() ) ) {
			return fromUUID( (UUID) aValue );
		}
		return null;
	}

	/**
	 * Converts a {@link String} to a simple type which can be converted back
	 * with the companion method {@link #fromSimpleType(Object)}.
	 *
	 * @param <TYPE> the generic type
	 * @param aValue The value to be converted to the simple type.
	 * @param aType the a type
	 * 
	 * @return The according simple type or null if the type was not a simple
	 *         type.
	 */
	@SuppressWarnings("unchecked")
	public static <TYPE> TYPE toSimpleType( String aValue, Class<TYPE> aType ) {
		// String:
		if ( aType.isAssignableFrom( String.class ) ) {
			return (TYPE) aValue;
		}
		// Integer + int:
		else if ( aType.isAssignableFrom( Integer.class ) || aType.equals( int.class ) ) {
			return (TYPE) Integer.valueOf( aValue );
		}
		// Short + short:
		else if ( aType.isAssignableFrom( Short.class ) || aType.equals( short.class ) ) {
			return (TYPE) Short.valueOf( aValue );
		}
		// Byte + byte:
		else if ( aType.isAssignableFrom( Byte.class ) || aType.equals( byte.class ) ) {
			return (TYPE) Byte.valueOf( aValue );
		}
		// Float + float:
		else if ( aType.isAssignableFrom( Float.class ) || aType.equals( float.class ) ) {
			return (TYPE) Float.valueOf( aValue );
		}
		// Double + double:
		else if ( aType.isAssignableFrom( Double.class ) || aType.equals( double.class ) ) {
			return (TYPE) Double.valueOf( aValue );
		}
		// Long + long:
		else if ( aType.isAssignableFrom( Long.class ) || aType.equals( long.class ) ) {
			return (TYPE) Long.valueOf( aValue );
		}
		// Boolean + boolean:
		else if ( aType.isAssignableFrom( Boolean.class ) || aType.equals( boolean.class ) ) {
			// return (TYPE) Boolean.valueOf( aValue ); // Makes anything not "true" become <false>!
			if ( aValue != null ) {
				if ( aValue.equalsIgnoreCase( Literal.TRUE.getValue() ) ) {
					return (TYPE) Boolean.TRUE;
				}
				if ( aValue.equalsIgnoreCase( Literal.FALSE.getValue() ) ) {
					return (TYPE) Boolean.FALSE;
				}
			}
			// throw new IllegalArgumentException( "Cannot convert value <" + aValue + "> to type <" + aType.getName() + ">!" );
		}
		// Character + char
		else if ( aType.isAssignableFrom( Character.class ) || aType.equals( char.class ) ) {
			return (TYPE) toCharacter( aValue );
		}
		// Enum:
		else if ( Enum.class.isAssignableFrom( aType ) ) {
			return (TYPE) toEnum( aValue, aType );
		}
		// Class:
		else if ( aType.isAssignableFrom( Class.class ) ) {
			try {
				return (TYPE) Class.forName( aValue );
			}
			catch ( ClassNotFoundException ignore ) {}
		}
		// Date:
		else if ( aType.isAssignableFrom( Date.class ) ) {
			return (TYPE) toDate( aValue );
		}
		// LocalDate:
		else if ( aType.isAssignableFrom( LocalDate.class ) ) {
			return (TYPE) toLocalDate( aValue );
		}
		// LocalDateTime:
		else if ( aType.isAssignableFrom( LocalDateTime.class ) ) {
			return (TYPE) toLocalDateTime( aValue );
		}
		// Locale:
		else if ( Locale.class.isAssignableFrom( aType ) ) {
			return (TYPE) toLocale( aValue );
		}
		// UUID:
		else if ( UUID.class.isAssignableFrom( aType ) ) {
			return (TYPE) toUUID( aValue );
		}
		return null;
	}

	/**
	 * Invokes a method with a simple type value's {@link String}
	 * representation, converting the {@link String} instance into the according
	 * simple type.
	 * 
	 * @param <T> The type of the instance on which to do the method invocation.
	 * 
	 * @param aToInstance The instance on which to invoke the method.
	 * @param aToMethod The method to be invoked.
	 * @param aToValue The {@link String} representation of the value to be
	 *        passed to the method.
	 * @param aToType The (simple type) to which the value is to be converted.
	 * 
	 * @return True in case a simple type was detected and invocation was done.
	 *         False in case the type of the value to be invoked was not a
	 *         simple type. See also {@link #isSimpleType(Class)}.
	 * 
	 * @throws NoSuchMethodException in case there is none such method for the
	 *         given instance.
	 * @throws IllegalAccessException in case method invocation has access
	 *         restrictions.
	 * @throws InvocationTargetException in case upon invocation an exceptional
	 *         situation occurred.
	 * @throws InstantiationException thrown in case instantiation of required
	 *         types failed.
	 * @throws ClassNotFoundException thrown in case a required type's class was
	 *         not found on the class path.
	 */
	public static <T> boolean invokeSimpleType( T aToInstance, Method aToMethod, String aToValue, Class<?> aToType ) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
		// String:
		if ( aToType.equals( String.class ) ) {
			aToMethod.invoke( aToInstance, aToValue );
		}
		// Number:
		else if ( Number.class.isAssignableFrom( aToType ) ) {
			final Constructor<String> theCtor = toNumber( aToType );
			aToMethod.invoke( aToInstance, theCtor.newInstance( aToValue ) );
		}
		// Boolean:
		else if ( Boolean.class.isAssignableFrom( aToType ) ) {
			final Constructor<String> theCtor = toNumber( aToType );
			aToMethod.invoke( aToInstance, theCtor.newInstance( aToValue ) );
		}
		// Character:
		else if ( aToType.equals( Character.class ) ) {
			aToMethod.invoke( aToInstance, toCharacter( aToValue ) );
		}
		// Enum:
		else if ( Enum.class.isAssignableFrom( aToType ) ) {
			aToMethod.invoke( aToInstance, toEnum( aToValue, aToType ) );
		}
		// Class:
		else if ( aToType.equals( Class.class ) ) {
			aToMethod.invoke( aToInstance, Class.forName( aToValue ) );
		}
		// Date:
		else if ( aToType.equals( Date.class ) ) {
			aToMethod.invoke( aToInstance, toDate( aToValue ) );
		}
		// LocalDate:
		else if ( aToType.equals( LocalDate.class ) ) {
			aToMethod.invoke( aToInstance, toLocalDate( aToValue ) );
		}
		// LocalDateTime:
		else if ( aToType.equals( LocalDateTime.class ) ) {
			aToMethod.invoke( aToInstance, toLocalDateTime( aToValue ) );
		}
		// Locale:
		else if ( Locale.class.isAssignableFrom( aToType ) ) {
			aToMethod.invoke( aToInstance, toLocale( aToValue ) );
		}
		// UUID:
		else if ( UUID.class.isAssignableFrom( aToType ) ) {
			aToMethod.invoke( aToInstance, toUUID( aToValue ) );
		}
		// boolean:
		else if ( aToType.equals( boolean.class ) ) {
			if ( !BooleanLiterals.isTrue( aToValue ) && !BooleanLiterals.isFalse( aToValue ) ) {
				return false;
			}
			// aToMethod.invoke( aToInstance, Boolean.parseBoolean( aToValue ) );
			aToMethod.invoke( aToInstance, BooleanLiterals.isTrue( aToValue ) );
		}
		// byte:
		else if ( aToType.equals( byte.class ) ) {
			aToMethod.invoke( aToInstance, Byte.parseByte( aToValue ) );
		}
		// char:
		else if ( aToType.equals( char.class ) ) {
			aToMethod.invoke( aToInstance, toCharacter( aToValue ) );
		}
		// short:
		else if ( aToType.equals( short.class ) ) {
			aToMethod.invoke( aToInstance, Short.parseShort( aToValue ) );
		}
		// int:
		else if ( aToType.equals( int.class ) ) {
			aToMethod.invoke( aToInstance, Integer.parseInt( aToValue ) );
		}
		// long:
		else if ( aToType.equals( long.class ) ) {
			aToMethod.invoke( aToInstance, Long.parseLong( aToValue ) );
		}
		// float:
		else if ( aToType.equals( float.class ) ) {
			aToMethod.invoke( aToInstance, Float.parseFloat( aToValue ) );
		}
		// double:
		else if ( aToType.equals( double.class ) ) {
			aToMethod.invoke( aToInstance, Double.parseDouble( aToValue ) );
		}
		else {
			return false;
		}
		return true;
	}

	/**
	 * Invokes a field with a simple type value's {@link String} representation,
	 * converting the {@link String} instance into the according simple type.
	 *
	 * @param <T> The type of the instance on which to do the field invocation.
	 * @param aToInstance The instance on which to invoke the field.
	 * @param aToField the field to be invoked.
	 * @param aToValue The {@link String} representation of the value to be
	 *        passed to the field.
	 * @param aToType The (simple type) to which the value is to be converted.
	 * 
	 * @return True in case a simple type was detected and invocation was done.
	 *         False in case the type of the value to be invoked was not a
	 *         simple type. See also {@link #isSimpleType(Class)}.
	 * 
	 * @throws NoSuchMethodException in case there is none such method for the
	 *         given instance.
	 * @throws IllegalAccessException in case method invocation has access
	 *         restrictions.
	 * @throws InstantiationException thrown in case instantiation of required
	 *         types failed.
	 * @throws InvocationTargetException in case upon invocation an exceptional
	 *         situation occurred.
	 * @throws ClassNotFoundException thrown in case a required type's class was
	 *         not found on the class path.
	 */
	public static <T> boolean invokeSimpleType( T aToInstance, Field aToField, String aToValue, Class<?> aToType ) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException {
		// String:
		if ( aToType.equals( String.class ) ) {
			aToField.set( aToInstance, aToValue );
		}
		// Number:
		else if ( Number.class.isAssignableFrom( aToType ) ) {
			final Constructor<String> theCtor = toNumber( aToType );
			aToField.set( aToInstance, theCtor.newInstance( aToValue ) );
		}
		// Boolean:
		else if ( Boolean.class.isAssignableFrom( aToType ) ) {
			final Constructor<String> theCtor = toNumber( aToType );
			aToField.set( aToInstance, theCtor.newInstance( aToValue ) );
		}
		// Character:
		else if ( aToType.equals( Character.class ) ) {
			aToField.set( aToInstance, toCharacter( aToValue ) );
		}
		// Enum:
		else if ( Enum.class.isAssignableFrom( aToType ) ) {
			aToField.set( aToInstance, toEnum( aToValue, aToType ) );
		}
		// Class:
		else if ( aToType.equals( Class.class ) ) {
			aToField.set( aToInstance, Class.forName( aToValue ) );
		}
		// Date:
		else if ( aToType.equals( Date.class ) ) {
			aToField.set( aToInstance, toDate( aToValue ) );
		}
		// LocalDate:
		else if ( aToType.equals( LocalDate.class ) ) {
			aToField.set( aToInstance, toLocalDate( aToValue ) );
		}
		// LocalDateTime:
		else if ( aToType.equals( LocalDateTime.class ) ) {
			aToField.set( aToInstance, toLocalDateTime( aToValue ) );
		}
		// Locale:
		else if ( Locale.class.isAssignableFrom( aToType ) ) {
			aToField.set( aToInstance, toLocale( aToValue ) );
		}
		// UUID:
		else if ( UUID.class.isAssignableFrom( aToType ) ) {
			aToField.set( aToInstance, toUUID( aToValue ) );
		}
		// boolean:
		else if ( aToType.equals( boolean.class ) ) {
			if ( !BooleanLiterals.isTrue( aToValue ) && !BooleanLiterals.isFalse( aToValue ) ) {
				return false;
			}
			// aToField.setBoolean( aToInstance, Boolean.parseBoolean( aToValue ) );
			aToField.setBoolean( aToInstance, BooleanLiterals.isTrue( aToValue ) );
		}
		// byte:
		else if ( aToType.equals( byte.class ) ) {
			aToField.setByte( aToInstance, Byte.parseByte( aToValue ) );
		}
		// char:
		else if ( aToType.equals( char.class ) ) {
			aToField.setChar( aToInstance, toCharacter( aToValue ) );
		}
		// short:
		else if ( aToType.equals( short.class ) ) {
			aToField.setShort( aToInstance, Short.parseShort( aToValue ) );
		}
		// int:
		else if ( aToType.equals( int.class ) ) {
			aToField.setInt( aToInstance, Integer.parseInt( aToValue ) );
		}
		// long:
		else if ( aToType.equals( long.class ) ) {
			aToField.setLong( aToInstance, Long.parseLong( aToValue ) );
		}
		// float:
		else if ( aToType.equals( float.class ) ) {
			aToField.setFloat( aToInstance, Float.parseFloat( aToValue ) );
		}
		// double:
		else if ( aToType.equals( double.class ) ) {
			aToField.setDouble( aToInstance, Double.parseDouble( aToValue ) );
		}
		else {
			return false;
		}
		return true;
	}

	/**
	 * Invokes an array at the given index with a simple type value's
	 * {@link String} representation, converting the {@link String} instance
	 * into the according simple type.
	 *
	 * @param <T> The type of the instance on which to do the array invocation.
	 * @param aToArray the array where to invoke the simple type's value.
	 * @param aToIndex the index into the array where to invoke the value to.
	 * @param aToValue The {@link String} representation of the value to be
	 *        passed to the array.
	 * @param aToType The (simple type) to which the value is to be converted.
	 * 
	 * @return True in case a simple type was detected and invocation was done.
	 *         False in case the type of the value to be invoked was not a
	 *         simple type. See also {@link #isSimpleType(Class)}.
	 * 
	 * @throws NoSuchMethodException in case there is none such method for the
	 *         given instance.
	 * @throws InstantiationException thrown in case instantiation of required
	 *         types failed.
	 * @throws IllegalAccessException in case method invocation has access
	 *         restrictions.
	 * @throws InvocationTargetException in case upon invocation an exceptional
	 *         situation occurred.
	 * @throws ClassNotFoundException thrown in case a required type's class was
	 *         not found on the class path.
	 */
	public static <T> boolean invokeSimpleType( T aToArray, int aToIndex, String aToValue, Class<?> aToType ) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		// String:
		if ( aToType.equals( String.class ) ) {
			Array.set( aToArray, aToIndex, aToValue );
		}
		// Number:
		else if ( Number.class.isAssignableFrom( aToType ) ) {
			final Constructor<String> theCtor = toNumber( aToType );
			Array.set( aToArray, aToIndex, theCtor.newInstance( aToValue ) );
		}
		// Boolean:
		else if ( Boolean.class.isAssignableFrom( aToType ) ) {
			final Constructor<String> theCtor = toNumber( aToType );
			Array.set( aToArray, aToIndex, theCtor.newInstance( aToValue ) );
		}
		// Character:
		else if ( aToType.equals( Character.class ) ) {
			Array.set( aToArray, aToIndex, toCharacter( aToValue ) );
		}
		// Enum:
		else if ( Enum.class.isAssignableFrom( aToType ) ) {
			Array.set( aToArray, aToIndex, toEnum( aToValue, aToType ) );
		}
		// Class:
		else if ( aToType.equals( Class.class ) ) {
			Array.set( aToArray, aToIndex, Class.forName( aToValue ) );
		}
		// Date:
		else if ( aToType.equals( Date.class ) ) {
			Array.set( aToArray, aToIndex, toDate( aToValue ) );
		}
		// LocalDate:
		else if ( aToType.equals( LocalDate.class ) ) {
			Array.set( aToArray, aToIndex, toLocalDate( aToValue ) );
		}
		// LocalDateTime:
		else if ( aToType.equals( LocalDateTime.class ) ) {
			Array.set( aToArray, aToIndex, toLocalDateTime( aToValue ) );
		}
		// Locale:
		else if ( Locale.class.isAssignableFrom( aToType ) ) {
			Array.set( aToArray, aToIndex, toLocale( aToValue ) );
		}
		// UUID:
		else if ( UUID.class.isAssignableFrom( aToType ) ) {
			Array.set( aToArray, aToIndex, toUUID( aToValue ) );
		}
		// boolean:
		else if ( aToType.equals( boolean.class ) ) {
			if ( !BooleanLiterals.isTrue( aToValue ) && !BooleanLiterals.isFalse( aToValue ) ) {
				return false;
			}
			// Array.set( aToArray, aToIndex, Boolean.parseBoolean( aToValue ) );
			Array.set( aToArray, aToIndex, BooleanLiterals.isTrue( aToValue ) );
		}
		// byte:
		else if ( aToType.equals( byte.class ) ) {
			Array.set( aToArray, aToIndex, Byte.parseByte( aToValue ) );
		}
		// char:
		else if ( aToType.equals( char.class ) ) {
			Array.set( aToArray, aToIndex, toCharacter( aToValue ) );
		}
		// short:
		else if ( aToType.equals( short.class ) ) {
			Array.set( aToArray, aToIndex, Short.parseShort( aToValue ) );
		}
		// int:
		else if ( aToType.equals( int.class ) ) {
			Array.set( aToArray, aToIndex, Integer.parseInt( aToValue ) );
		}
		// long:
		else if ( aToType.equals( long.class ) ) {
			Array.set( aToArray, aToIndex, Long.parseLong( aToValue ) );
		}
		// float:
		else if ( aToType.equals( float.class ) ) {
			Array.set( aToArray, aToIndex, Float.parseFloat( aToValue ) );
		}
		// double:
		else if ( aToType.equals( double.class ) ) {
			Array.set( aToArray, aToIndex, Double.parseDouble( aToValue ) );
		}
		else {
			return false;
		}
		return true;
	}

	private static <T> String fromDate( Date aValue ) {
		return DateFormat.ISO_OFFSET_DATE_TIME.getFormatter().format( Instant.ofEpochMilli( ( aValue ).getTime() ) );
	}

	private static Date toDate( String aValue ) {
		return DateFormats.asDate( aValue );
	}

	private static <T> String fromLocalDate( LocalDate aValue ) {
		return DateFormat.ISO_LOCAL_DATE.getFormatter().format( aValue );
	}

	private static LocalDate toLocalDate( String aValue ) {
		return DateFormats.asLocalDate( aValue );
	}

	private static <T> String fromLocalDateTime( LocalDateTime aValue ) {
		return DateFormat.ISO_LOCAL_DATE_TIME.getFormatter().format( aValue );
	}

	private static LocalDateTime toLocalDateTime( String aValue ) {
		return DateFormats.asLocalDateTime( aValue );
	}

	private static <T> String fromEnum( Enum<?> aValue ) {
		return aValue.name();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Enum<?> toEnum( String aValue, Class<?> aType ) {
		return Enum.valueOf( (Class<Enum>) aType, aValue );
	}

	private static <T> String fromLocale( Locale aValue ) {
		return aValue.getLanguage() + "-" + aValue.getCountry();
	}

	private static Locale toLocale( String aValue ) {
		return Locale.forLanguageTag( aValue );
	}

	private static <T> String fromUUID( UUID aValue ) {
		return aValue.toString();
	}

	private static UUID toUUID( String aValue ) {
		return UUID.fromString( aValue );
	}

	private static <T> String fromCharacter( Character aValue ) {
		return aValue.toString();
	}

	private static Character toCharacter( String aValue ) {
		if ( aValue.length() != 0 ) {
			return Character.valueOf( aValue.charAt( 0 ) );
		}
		return null;
	}

	private static <T> String fromNumber( T aValue ) {
		String theValue = aValue.toString();
		if ( theValue.endsWith( ".0" ) ) {
			theValue = theValue.substring( 0, theValue.length() - 2 );
		}
		return theValue;
	}

	@SuppressWarnings("unchecked")
	private static Constructor<String> toNumber( Class<?> aType ) throws NoSuchMethodException {
		final Constructor<String> theCtor = (Constructor<String>) aType.getConstructor( String.class );
		// Don't in Java 9 |-->
		try {
			theCtor.setAccessible( true );
		}
		catch ( Exception ignore ) {}
		// Don't in Java 9 <--|
		return theCtor;
	}
}