// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Provides an accessor for a byte array property.
 */
public interface ByteArrayAccessor {

	/**
	 * Retrieves the byte array from the byte array property.
	 * 
	 * @return The byte array stored by the byte array property.
	 */
	byte[] getBytes();

	/**
	 * Provides a mutator for a byte array property.
	 */
	public interface ByteArrayMutator {

		/**
		 * Sets the byte array for the byte array property.
		 * 
		 * @param aBytes The byte array to be stored by the byte array property.
		 */
		void setBytes( byte[] aBytes );
	}

	/**
	 * Provides a builder method for a byte array property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ByteArrayBuilder<B extends ByteArrayBuilder<B>> {

		/**
		 * Sets the byte array for the byte array property.
		 * 
		 * @param aBytes The byte array to be stored by the byte array property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBytes( byte[] aBytes );
	}

	/**
	 * Provides a byte array property.
	 */
	public interface ByteArrayProperty extends ByteArrayAccessor, ByteArrayMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given byte array (setter) as
		 * of {@link #setBytes(byte[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aBytes The byte array to set (via {@link #setBytes(byte[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default byte[] letBytes( byte[] aBytes ) {
			setBytes( aBytes );
			return aBytes;
		}
	}
}
