// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.struct.Property.PropertyBuilder;

/**
 * Class describing a beans's attribute.
 */
public class AttributeImpl extends RelationImpl<String, Object> implements Attribute {

	/**
	 * Instantiates a new attribute impl.
	 */
	protected AttributeImpl() {}

	/**
	 * Constructs a key-to-value property.
	 * 
	 * @param aKey The key for the key-value property.
	 * @param aValue The value for the key-value property.
	 */
	public AttributeImpl( String aKey, Object aValue ) {
		super( aKey, aValue );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Implementation of the {@link PropertyBuilder} interface.
	 */
	public static class AttributeBuilderImpl extends AttributeImpl implements AttributeBuilder {

		/**
		 * Instantiates a new attribute builder impl.
		 */
		public AttributeBuilderImpl() {}

		/**
		 * Instantiates a new attribute builder impl.
		 *
		 * @param aKey the key
		 * @param aValue the value
		 */
		public AttributeBuilderImpl( String aKey, Object aValue ) {
			super( aKey, aValue );
		}

		/**
		 * Sets the key.
		 *
		 * @param aKey the new key
		 */
		@Override
		public void setKey( String aKey ) {
			_key = aKey;
		}

		/**
		 * Sets the value.
		 *
		 * @param aValue the new value
		 */
		@Override
		public void setValue( Object aValue ) {
			_value = aValue;
		}
	}
}