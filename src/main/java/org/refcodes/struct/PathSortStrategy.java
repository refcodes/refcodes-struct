// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * The {@link PathSortStrategy} defines strategies to use when sorting e.g. when
 * using a {@link PathComparator} or the method
 * {@link CanonicalMap#sortedKeys(PathSortStrategy)}
 *
 */
public enum PathSortStrategy {

	/**
	 * Default sort strategy being
	 * {@link PathSortStrategy#ALPHANUMERIC_IGNORE_CASE}
	 */
	DEFAULT(true, true),

	/**
	 * Sort alphabetically, differentiating between upper case and lower case,
	 * e.g. "10" will be sorted before "2".
	 */
	ALPHABETIC(false, false),

	/**
	 * Sort alphabetically, not differentiating between upper case and lower
	 * case, e.g. "10" will be sorted before "2".
	 */
	ALPHABETIC_IGNORE_CASE(false, true),

	/**
	 * Sort alphabetically, differentiating between upper case and lower case
	 * and considering any path elements representing an integral number, e.g.
	 * "10" will be sorted below "2".
	 */
	ALPHANUMERIC(true, false),

	/**
	 * Sort alphabetically, not differentiating between upper case and lower
	 * case and considering any path elements representing an integral number,
	 * e.g. "10" will be sorted below "2".
	 */
	ALPHANUMERIC_IGNORE_CASE(true, true);

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _isAlphanumeric;
	private boolean _isIgnoreCase;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private PathSortStrategy( boolean isAlphanumeric, boolean isIgnoreCase ) {
		_isAlphanumeric = isAlphanumeric;
		_isIgnoreCase = isIgnoreCase;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether to sort alphabetically and considering any path
	 * elements representing an integral number, e.g. "10" will be sorted below
	 * "2".
	 * 
	 * @return True in case to sort alphanumerically.
	 */
	public boolean isAlphanumeric() {
		return _isAlphanumeric;
	}

	/**
	 * Determines whether to sort by not differentiating between upper case and
	 * lower case.
	 * 
	 * @return True when not differentiating between upper case and lower case.
	 */
	public boolean isIgnoreCase() {
		return _isIgnoreCase;
	}
}
