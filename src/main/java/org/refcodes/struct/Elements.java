// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Collection;
import java.util.Iterator;

import org.refcodes.mixin.Clearable;

/**
 * Basic functionality being provided by any {@link Elements} (collection) style
 * class.
 *
 * @param <E> The type of the elements being stored in the
 *        {@link MutableElements}.
 */
public interface Elements<E> {

	/**
	 * Returns an {@link Iterator} instance of all the data elements contained
	 * in the {@link Elements} (collection).
	 * -------------------------------------------------------------------------
	 * ATTENTION: The {@link Iterator#remove()} method may be disabled in the
	 * returned {@link Iterator} instance as the {@link Elements} is supposed to
	 * be read-only.
	 * 
	 * @return A (read-only) {@link Iterator} providing the elements of the
	 *         {@link Elements}.
	 */
	Iterator<E> iterator();

	/**
	 * Tests whether the specified object is an element of the {@link Elements}
	 * (collection). Returns true if this {@link Elements} contains the
	 * specified element. More formally, returns true if and only if this
	 * {@link Elements} contains an element e such that (obj==null ? e==null :
	 * obj.equals(e))
	 * 
	 * @param aElement The object to be tested whether it is contained in this
	 *        {@link Elements} or not.
	 * 
	 * @return Description True in case the given object is contained in this
	 *         {@link Elements}.
	 */
	boolean contains( Object aElement );

	/**
	 * Extends the {@link Elements} interface with {@link Clearable}
	 * functionality (as of {@link #clear()}). The case of having a plain
	 * {@link Elements} without dedicated {@link MutableElements#add(Object)} or
	 * {@link MutableElements#remove(Object)} methods but with a dedicated
	 * {@link #clear()} method seems to be quite common, therefore this
	 * interface has been provided.
	 * 
	 * @param <E> The type of the elements being stored in the
	 *        {@link ClearableElements}.
	 */
	public interface ClearableElements<E> extends Elements<E>, Clearable {}

	/**
	 * Extends the {@link Elements} with mutable (writable) functionality,
	 * ehttps://www.metacodes.proly by enabling the {@link Iterator#remove()}
	 * method in the {@link Iterator} provided via {@link #iterator()} and by
	 * providing the {@link #add(Object)} and the {@link #remove(Object)}
	 * methods.
	 * 
	 * @param <E> The type of the elements being stored in the
	 *        {@link MutableElements}.
	 */
	public interface MutableElements<E> extends ClearableElements<E> {

		/**
		 * Same as {@link Collection#add(Object)}.
		 * 
		 * @param aElement the element whose presence in this collection is to
		 *        be ensured
		 * 
		 * @return True if this collection changed as a result of the call
		 * 
		 * @see Collection#add(Object)
		 */
		boolean add( E aElement );

		/**
		 * Same as {@link Collection#remove(Object)}.
		 * 
		 * @param aElement The element to be removed from this collection, if
		 *        present
		 * 
		 * @return True if an element was removed as a result of this call.
		 * 
		 * @see Collection#remove(Object)
		 */
		boolean remove( Object aElement );

		/**
		 * Returns an {@link Iterator} instance of all the data elements
		 * contained in the container (collection).
		 * ---------------------------------------------------------------------
		 * ATTENTION: The {@link Iterator#remove()} method is enabled in the
		 * returned {@link Iterator} instance as the {@link MutableElements} is
		 * possibly readable and writable.
		 * 
		 * @return A (writable) {@link Iterator} providing the elements of the
		 *         {@link Elements} (possibly with the {@link Iterator#remove()}
		 *         method being enabled).
		 */
		@Override
		Iterator<E> iterator();
	}
}
