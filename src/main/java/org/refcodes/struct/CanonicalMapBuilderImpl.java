// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;

/**
 * The Class CanonicalMapBuilderImpl.
 */
public class CanonicalMapBuilderImpl extends PathMapBuilderImpl<String> implements CanonicalMapBuilder {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link CanonicalMapBuilder} instance using the default
	 * path delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public CanonicalMapBuilderImpl() {
		super( String.class );
	}

	/**
	 * Create a {@link CanonicalMapBuilder} instance using the provided path
	 * delimiter for the path declarations.
	 *
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapBuilderImpl( char aDelimiter ) {
		super( aDelimiter, String.class );
	}

	/**
	 * Create a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public CanonicalMapBuilderImpl( Object aObj ) {
		super( aObj, String.class );
	}

	/**
	 * Create a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public CanonicalMapBuilderImpl( String aToPath, Object aObj ) {
		super( aToPath, aObj, String.class );
	}

	/**
	 * Creates a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public CanonicalMapBuilderImpl( Object aObj, String aFromPath ) {
		super( aObj, aFromPath, String.class );
	}

	/**
	 * Creates a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)} using the default path delimiter
	 * "/" ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public CanonicalMapBuilderImpl( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath, String.class );
	}

	/**
	 * Creates a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapBuilderImpl( Object aObj, char aDelimiter ) {
		super( aObj, aDelimiter, String.class );
	}

	/**
	 * Creates a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapBuilderImpl( String aToPath, Object aObj, char aDelimiter ) {
		super( aToPath, aObj, aDelimiter, String.class );
	}

	/**
	 * Creates a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapBuilderImpl( Object aObj, String aFromPath, char aDelimiter ) {
		super( aObj, aFromPath, aDelimiter, String.class );
	}

	/**
	 * Create a {@link CanonicalMapBuilder} instance containing the elements as
	 * of {@link MutablePathMap#insert(Object)}.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 * @param aDelimiter The path delimiter to be used for the path
	 *        declarations.
	 */
	public CanonicalMapBuilderImpl( String aToPath, Object aObj, String aFromPath, char aDelimiter ) {
		super( aToPath, aObj, aFromPath, aDelimiter, String.class );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder retrieveFrom( String aFromPath ) {
		return new CanonicalMapBuilderImpl( super.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder retrieveTo( String aToPath ) {
		return new CanonicalMapBuilderImpl( super.retrieveTo( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CanonicalMapBuilder withPut( String aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PathMap<String> fromObject( Object aFrom ) {
		return new CanonicalMapImpl( aFrom, getDelimiter() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String fromInstance( Object aValue ) {
		return SimpleType.fromSimpleType( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected <TYPE> TYPE toInstance( String aValue, Class<TYPE> aType ) {
		return SimpleType.toSimpleType( aValue, aType );
	}
}
