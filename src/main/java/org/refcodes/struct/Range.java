// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.mixin.MaxValueAccessor;
import org.refcodes.mixin.MinValueAccessor;

/**
 * Interface describing a range with a minimum value and a maximum value.
 * 
 * @param <V> The type used for the values.
 */
public interface Range<V extends Number & Comparable<V>> extends MinValueAccessor<V>, MaxValueAccessor<V> {

	/**
	 * Creates a {@link Range} from the provided {@link String} using a
	 * <code>-</code> (minus) symbol to separate the minimum from the maximum,
	 * e.g. "1-10" for a range between 1 and 10.
	 * 
	 * @param aRange The {@link String} representation of the range.
	 * @param aType The targeted type for the {@link Range} instance.
	 * 
	 * @return The according {@link Range} instance.
	 */
	static <V extends Number & Comparable<V>> Range<V> toRange( String aRange, Class<V> aType ) {
		return new RangeImpl<>( aRange, aType );
	}

	/**
	 * Returns true if the given value is within the range of
	 * {@link #getMinValue()} and {@link #getMaxValue()} (including the minimum
	 * and maximum values).
	 * 
	 * @param aValue The value to test whether it is in range.
	 * 
	 * @return True in case of being in range, else false.
	 */
	default boolean isMember( V aValue ) {
		return aValue.compareTo( getMinValue() ) >= 0 && aValue.compareTo( getMaxValue() ) <= 0;
	}

	/**
	 * Adds builder functionality to a {@link Range}.
	 * 
	 * @param <V> The type used for the values.
	 */
	public interface RangeBuilder<V extends Number & Comparable<V>> extends MinValueProperty<V>, MinValueBuilder<V, RangeBuilder<V>>, MaxValueProperty<V>, MaxValueBuilder<V, RangeBuilder<V>>, Range<V> {

		/**
		 * With min value.
		 *
		 * @param aMinValue the min value
		 * 
		 * @return the range builder
		 */
		@Override
		default RangeBuilder<V> withMinValue( V aMinValue ) {
			setMinValue( aMinValue );
			return this;
		}

		/**
		 * With max value.
		 *
		 * @param aMaxValue the max value
		 * 
		 * @return the range builder
		 */
		@Override
		default RangeBuilder<V> withMaxValue( V aMaxValue ) {
			setMaxValue( aMaxValue );
			return this;
		}
	}
}
