// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import org.refcodes.mixin.KeyAccessor;

/**
 * Thrown in case this instance does not contain the requested element (no such
 * key found in the collection).
 */
public class AmbiguousKeyRuntimeException extends StructureRuntimeException implements KeyAccessor<String> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _key;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aKey The ambiguous key in question.
	 */
	public AmbiguousKeyRuntimeException( String aMessage, String aKey, String aErrorCode ) {
		super( aMessage, aErrorCode );
		_key = aKey;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aKey The ambiguous key in question.
	 */
	public AmbiguousKeyRuntimeException( String aMessage, String aKey, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
		_key = aKey;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aKey The ambiguous key in question.
	 */
	public AmbiguousKeyRuntimeException( String aMessage, String aKey, Throwable aCause ) {
		super( aMessage, aCause );
		_key = aKey;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aKey The ambiguous key in question.
	 */
	public AmbiguousKeyRuntimeException( String aMessage, String aKey ) {
		super( aMessage );
		_key = aKey;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aKey The ambiguous key in question.
	 */
	public AmbiguousKeyRuntimeException( String aKey, Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
		_key = aKey;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param aKey The ambiguous key in question.
	 */
	public AmbiguousKeyRuntimeException( String aKey, Throwable aCause ) {
		super( aCause );
		_key = aKey;
	}

	// /////////////////////////////////////////////////////////////////////////^
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _key };
	}
}
