// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Provides an accessor for a String array property.
 */
public interface StringArrayAccessor {

	/**
	 * Retrieves the String array from the String array property.
	 * 
	 * @return The String array stored by the String array property.
	 */
	String[] getStrings();

	/**
	 * Provides a mutator for a String array property.
	 */
	public interface StringArrayMutator {

		/**
		 * Sets the String array for the String array property.
		 * 
		 * @param aStrings The String array to be stored by the String array
		 *        property.
		 */
		void setStrings( String[] aStrings );
	}

	/**
	 * Provides a builder method for a String array property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface StringArrayBuilder<B extends StringArrayBuilder<B>> {

		/**
		 * Sets the String array for the String array property.
		 * 
		 * @param aStrings The String array to be stored by the String array
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withStrings( String[] aStrings );
	}

	/**
	 * Provides a String array property.
	 */
	public interface StringArrayProperty extends StringArrayAccessor, StringArrayMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given String array (setter) as
		 * of {@link #setStrings(String[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aStrings The String array to set (via
		 *        {@link #setStrings(String[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String[] letStrings( String[] aStrings ) {
			setStrings( aStrings );
			return aStrings;
		}
	}
}
