// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

import java.util.Map;

import org.refcodes.struct.PathMap.PathMapBuilder;

/**
 * Utility class to avoid redundant code regarding various {@link PathMap}
 * implementations.
 */
public class StructureUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private StructureUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String SYSTEM_PROPERTY_STRUCT_KEEP_MAPS = "struct.keepMaps";
	public static final String ENVIRONMENT_VARIABLE_STRUCT_KEEP_MAPS = "STRUCT_KEEPMAPS";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static Boolean _isKeepMaps = null;

	// /////////////////////////////////////////////////////////////////////////
	// UTILITY:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook method for implementing the {@link PathMap#retrieveFrom(String)}
	 * method.
	 * 
	 * @param <T> The type of the {@link PathMap} the functionality is to be
	 *        applied to.
	 * @param aFromPathMap The {@link PathMap} from which to retrieve.
	 * @param aFromPath The path from where to start extracting the paths.
	 * @param aToPathMap The {@link PathMapBuilder} which to put the result in.
	 */
	public static <T> void retrieveFrom( PathMap<T> aFromPathMap, String aFromPath, PathMapBuilder<T> aToPathMap ) {
		final String theFromPath = toGenericPath( aFromPathMap, aFromPath );
		for ( String ePath : aFromPathMap.paths() ) {
			if ( ePath.startsWith( theFromPath ) ) {
				final String eToPath = ePath.substring( theFromPath.length() );
				if ( eToPath.startsWith( aFromPathMap.getRootPath() ) || eToPath.isEmpty() ) {
					aToPathMap.put( eToPath, aFromPathMap.get( ePath ) );
				}
			}
		}
	}

	/**
	 * Hook method for implementing the {@link PathMap#retrieveTo(String)}
	 * method.
	 * 
	 * @param <T> The type of the {@link PathMap} the functionality is to be
	 *        applied to.
	 * @param aFromPathMap The {@link PathMap} from which to retrieve.
	 * @param aToPath The path where to relocate the paths of this
	 *        {@link PathMap} to.
	 * @param aToPathMap The {@link PathMapBuilder} which to put the result in.
	 */
	public static <T> void retrieveTo( PathMap<T> aFromPathMap, String aToPath, PathMapBuilder<T> aToPathMap ) {
		final String thetoPath = toGenericPath( aFromPathMap, aToPath );
		for ( String ePath : aFromPathMap.paths() ) {
			aToPathMap.put( thetoPath + ePath, aFromPathMap.get( ePath ) );
		}
	}

	/**
	 * Hook method for implementing the {@link PathMap#toDataStructure()}
	 * method.
	 * 
	 * The <code>org.refcodes.runtime.SystemProperty#STRUCT_KEEP_MAPS</code>
	 * {@value #SYSTEM_PROPERTY_STRUCT_KEEP_MAPS} as well as the
	 * <code>org.refcodes.runtime.EnvironmentVariable#STRUCT_KEEP_MAPS</code>
	 * {@value #ENVIRONMENT_VARIABLE_STRUCT_KEEP_MAPS} can be used to disable
	 * the conversion of {@link Map} instances into arrays by setting it to
	 * <code>true</code> (system property beats environment variable).
	 * 
	 * @param aPathMap The {@link PathMap} from which to get the data structure.
	 * @param aFromPath The path below which the {@link PathMap} to be converted
	 *        into a data structure.
	 * 
	 * @return A data structure being a mixture of {@link Map} instances and
	 *         arrays representing the data below the given path.
	 */
	public static Object toDataStructure( PathMap<?> aPathMap, String aFromPath ) {
		if ( _isKeepMaps == null ) {
			synchronized ( StructureUtility.class ) {
				if ( _isKeepMaps == null ) {
					if ( System.getProperty( SYSTEM_PROPERTY_STRUCT_KEEP_MAPS ) != null ) {
						_isKeepMaps = Boolean.parseBoolean( System.getProperty( SYSTEM_PROPERTY_STRUCT_KEEP_MAPS ) );
					}
					else if ( System.getenv( ENVIRONMENT_VARIABLE_STRUCT_KEEP_MAPS ) != null ) {
						_isKeepMaps = Boolean.parseBoolean( System.getenv( ENVIRONMENT_VARIABLE_STRUCT_KEEP_MAPS ) );
					}
					else {
						_isKeepMaps = false;
					}
				}
			}
		}
		if ( _isKeepMaps ) {
			return aPathMap.toMap( aFromPath ); // TODO 2024-05-27 - Verify if returning Map types instead of Arrays is OK.

		}
		return TypeUtility.toUnwrappedMap( aPathMap.toMap( aFromPath ) );
	}

	/**
	 * Normalizes the path as of {@link PathMap#toNormalizedPath(String)} and
	 * unifies it so that a path never ends with a delimiter
	 * ({@link PathMap#getDelimiter()}). This is necessary as the root path is
	 * represented just by the delimiter, meaning that it also ends with the
	 * delimiter, though generic operations may require that a path must not end
	 * with the delimiter. This means that the root path is an empty
	 * {@link String}.
	 * 
	 * @param aPathMap The {@link PathMap} for which to unify the path.
	 * @param aPath The path to be unified.
	 * 
	 * @return The resulting generic path never ending with a delimiter.
	 */
	public static String toGenericPath( PathMap<?> aPathMap, String aPath ) {
		aPath = aPathMap.toNormalizedPath( aPath );
		while ( aPath.endsWith( aPathMap.getDelimiter() + "" ) ) {
			aPath = aPath.substring( 0, aPath.length() - 1 );
		}
		return aPath;
	}
}
