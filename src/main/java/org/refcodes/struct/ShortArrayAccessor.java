// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Provides an accessor for a short array property.
 */
public interface ShortArrayAccessor {

	/**
	 * Retrieves the short array from the short array property.
	 * 
	 * @return The short array stored by the short array property.
	 */
	short[] getShorts();

	/**
	 * Provides a mutator for a short array property.
	 */
	public interface ShortArrayMutator {

		/**
		 * Sets the short array for the short array property.
		 * 
		 * @param aShorts The short array to be stored by the short array
		 *        property.
		 */
		void setShorts( short[] aShorts );
	}

	/**
	 * Provides a builder method for a short array property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ShortArrayBuilder<B extends ShortArrayBuilder<B>> {

		/**
		 * Sets the short array for the short array property.
		 * 
		 * @param aShorts The short array to be stored by the short array
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withShorts( short[] aShorts );
	}

	/**
	 * Provides a short array property.
	 */
	public interface ShortArrayProperty extends ShortArrayAccessor, ShortArrayMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given short array (setter) as
		 * of {@link #setShorts(short[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aShorts The short array to set (via
		 *        {@link #setShorts(short[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default short[] letShorts( short[] aShorts ) {
			setShorts( aShorts );
			return aShorts;
		}
	}
}
