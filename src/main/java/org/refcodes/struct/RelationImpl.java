// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Implementation of the {@link Relation} interface.
 *
 * @param <K> The key's type
 * @param <V> The value's type
 */
public class RelationImpl<K, V> implements Relation<K, V> {

	protected K _key;

	protected V _value;

	/**
	 * Instantiates a new relation impl.
	 */
	protected RelationImpl() {};

	/**
	 * Constructs a {@link Relation}.
	 * 
	 * @param aKey The key for the key-value property.
	 * @param aValue The value for the key-value property.
	 */
	public RelationImpl( K aKey, V aValue ) {
		_key = aKey;
		_value = aValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public K getKey() {
		return _key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V getValue() {
		return _value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[key=" + _key + ", value=" + _value + "]";
	}

	/**
	 * Implementation of the {@link RelationBuilder} interface.
	 *
	 * @param <K> the key type
	 * @param <V> the value type
	 */
	public static class RelationBuilderImpl<K, V> extends RelationImpl<K, V> implements RelationBuilder<K, V> {

		/**
		 * Instantiates a new relation builder impl.
		 */
		public RelationBuilderImpl() {}

		/**
		 * Instantiates a new relation builder impl.
		 *
		 * @param aKey the key
		 * @param aValue the value
		 */
		public RelationBuilderImpl( K aKey, V aValue ) {
			super( aKey, aValue );
		}

		/**
		 * Sets the key.
		 *
		 * @param aKey the new key
		 */
		@Override
		public void setKey( K aKey ) {
			_key = aKey;
		}

		/**
		 * Sets the value.
		 *
		 * @param aValue the new value
		 */
		@Override
		public void setValue( V aValue ) {
			_value = aValue;
		}
	}
}
