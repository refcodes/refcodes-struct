// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.struct;

/**
 * Basic functionality being provided by any {@link Table} (map) style class.
 *
 * @param <K> The type of the key.
 * @param <V> The type of the value which relates to a key.
 */
public interface Table<K, V> extends Keys<K, V>, Containable {

	/**
	 * Extends the {@link Table} with mutable (writable) functionality,
	 * ehttps://www.metacodes.proly by providing {@link #put(Object, Object)}
	 * and {@link #delete(String)} methods.
	 * 
	 * @param <K> The type of the key.
	 * @param <V> The type of the value which relates to a key.
	 */
	public interface TableDictionary<K, V> extends Table<K, V>, MutableValues<K, V> {

		/**
		 * Adds the given element related to the given key.
		 *
		 * @param aRelation the relation
		 * 
		 * @return The value being replaced by the provided value or null if
		 *         none value has been replaced.
		 */
		V put( Relation<K, V> aRelation );

		/**
		 * Provides a builder method for a {@link Relation} property returning
		 * the builder for applying multiple build operations.
		 *
		 * @param <K> the key type
		 * @param <V> The type of the value which relates to a key.
		 * @param <B> The builder to return in order to be able to apply
		 *        multiple build operations.
		 */
		public interface TableBuilder<K, V, B extends TableBuilder<K, V, B>> extends TableDictionary<K, V> {

			/**
			 * Builder method for {@link #put(Object, Object)}.
			 *
			 * @param aKey the key for the property to be put.
			 * @param aValue the value for the property to be put.
			 * 
			 * @return The implementing instance as of the builder pattern.
			 */
			B withPut( K aKey, V aValue );

			/**
			 * Builder method for {@link #put(Relation)}.
			 *
			 * @param aRelation the property to be put.
			 * 
			 * @return The implementing instance as of the builder pattern.
			 */
			B withPut( Relation<K, V> aRelation );

		}
	}
}
