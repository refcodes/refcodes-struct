# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides structure definitions and collections for unified access to structure and collection alike types.***

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-structure</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

## How do I get started? ##

This artifact contains base definitions regarding structures being used by the  `REFCODES.ORG` artifacts, to unify management of various kinds of structures.

Please note the `PathMap`, which's extension, the `HttpBodyMap` (found in the `refcodes-web` artifact) is used to represent dynamic data types produced e.g. by `JavaScript` clients.   

See the [Javadoc](https://www.javadoc.io/doc/org.refcodes/refcodes-structure) for a 
full overview on this artifact.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-struct/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.